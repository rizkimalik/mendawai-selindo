﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="calltype_tre.aspx.vb" Inherits="ICC.calltype_tre" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="text/javascript">
        function CobaCoba() {
            //ASPxGridLookup1.GetInputElement().SetValue = "8,9,10";

        }

        function OnGridFocusedRowChanged() {

            var grid = ASPxGridLookup1.GetGridView();
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'ORGANIZATION_ID', OnGetRowValues);
        }
        function OnJenisTransaksiChange(cmbParent) {

            var comboValue = cmbParent.GetSelectedItem().value;
            if (comboValue)
                ASPxGridView1.GetEditor("UnitKerja").PerformCallback(comboValue.toString());
            //alert(comboValue.toString());
        }
        //function OnJenisTransaksiChange(cmbParent) {
        //    var comboValue = cmbParent.GetSelectedItem().value;
        //    if (isUpdating)
        //        //return;
        //        ASPxGridView1.GetEditor("UnitKerja").PerformCallback(comboValue.toString());
        //    if (comboValue)
        //        ASPxGridView1.GetEditor("UnitKerja").PerformCallback(comboValue.toString());
        //    //alert(comboValue.toString());
        //}

        function OnUnitKerjaChange(cmbParent) {
            var comboValue = cmbParent.GetSelectedItem().value;
            //alert(comboValue);
            if (isUpdating)
                //return;
                ASPxGridView1.GetEditor("NameSubject").PerformCallback(comboValue.toString());
            if (comboValue)
                ASPxGridView1.GetEditor("NameSubject").PerformCallback(comboValue.toString());
            //alert(comboValue.toString());
        }

        function OnUnitKategory(cmbParent) {
            var comboValue = cmbParent.GetSelectedItem().value;
            if (isUpdating)
                //return;
                ASPxGridView1.GetEditor("SubjectTable").PerformCallback(comboValue.toString());
            if (comboValue)
                ASPxGridView1.GetEditor("SubjectTable").PerformCallback(comboValue.toString());
            //alert(comboValue.toString());
        }

        var combo = null;
        var isUpdating = true;
        // ]]>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h4 class="headline">Data Reason
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Width="100%" runat="server" DataSourceID="DsCategory"
        KeyFieldName="ID" SettingsPager-PageSize="15" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowHorizontalScrollBar="true"  />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="130px">
                <EditButton Visible="True">
                    <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="True">
                    <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="false">
                    <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton>
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton>
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <%--<dx:GridViewDataTextColumn FieldName="JenisTransaksi"></dx:GridViewDataTextColumn>--%>
            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" VisibleIndex="0"
                Width="30px" HeaderStyle-HorizontalAlign="Center" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="JenisTransaksi" Caption="Category" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText" Width="150px">
                <PropertiesComboBox TextFormatString="{1}" TextField="Name" ValueField="CategoryID" DataSourceID="dsmCategory">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnJenisTransaksiChange(s); }"></ClientSideEvents>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="UnitKerja" Caption="Enquiry Type" Settings-FilterMode="DisplayText" Settings-AutoFilterCondition="Contains" Width="200px">
                <PropertiesComboBox IncrementalFilteringMode="Contains" TextFormatString="{1}" TextField="SubName" ValueField="SubCategory1ID" DataSourceID="dsmSubCategoryLv1">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnUnitKerjaChange(s); }"></ClientSideEvents>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Enquiry Detail" FieldName="NameSubject" Settings-FilterMode="DisplayText" Width="200px" Settings-AutoFilterCondition="Contains">
                <PropertiesComboBox TextFormatString="{1}" TextField="SubName" ValueField="SubCategory2ID" DataSourceID="dsmSubCategoryLv2">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataTextColumn Caption="Reason" FieldName="SubjectTable" Width="300px" Settings-AutoFilterCondition="Contains">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="TujuanEskalasi" Caption="Escalation Department" Settings-FilterMode="DisplayText" Settings-AutoFilterCondition="Contains" Width="150px">
                <PropertiesComboBox TextFormatString="{1}" TextField="ORGANIZATION_NAME" ValueField="ORGANIZATION_ID" DataSourceID="sqlDataSource3">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>         
            <dx:GridViewDataTextColumn Caption="SLA (Days)" FieldName="SLA" Width="100px" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ReasonCode" Caption="Reason Code"  Settings-AutoFilterCondition="Contains" Width="200px">
            </dx:GridViewDataTextColumn>
			<dx:GridViewDataTextColumn Caption="User Update" FieldName="UserUpdate" Width="150px" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Date Update" FieldName="DateUpdate" Width="150px" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" HeaderStyle-HorizontalAlign="left" 
                Width="100px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Active" Value="Y" />
                        <dx:ListEditItem Text="In Active" Value="N" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select ORGANIZATION_ID, ORGANIZATION_NAME from mOrganization where flag='0'"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mSubCategoryLv2"></asp:SqlDataSource>
    <asp:SqlDataSource ID="DsCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsmCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="select top 10 * from mCategory Where NA='Y'"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsmSubCategoryLv1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="select * from mSubCategoryLv1 Where NA='Y' and CategoryID=@CategoryID">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="CategoryID" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="Ds_EmailReasonCode" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM Temp_EmailReasonCode"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsmSubCategoryLv2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="select ID,CategoryID,SubCategory2ID,SubCategory1ID,SubName,Description,NA  from mSubCategoryLv2 Where NA='Y' and SubCategory1ID=@SubCategory1ID">
        <%--SelectCommand="select a.SubCategory2ID,a.SubName  from mSubCategoryLv2 a left outer join mSubCategoryLv1 b on a.SubCategory1ID=b.SubCategory1ID Where a.NA='Y' and b.ID=@SubCategory1ID">--%>
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="SubCategory1ID" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
