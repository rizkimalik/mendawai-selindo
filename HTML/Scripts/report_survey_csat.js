import { url } from "./config.js";

document.addEventListener('DOMContentLoaded', (event) => {
    $.ajaxSetup({ cache: false });

    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== '';
    }

    function LoadReportCSAT() {
        const start_date = $('#MainContent_dt_strdate_I').val();
        const end_date = $('#MainContent_dt_endate_I').val();
        const type_value = $('#cb_type').val();

        const store_report_csat = new DevExpress.data.CustomStore({
            key: 'TicketNumber',
            load(options) {
                const deferred = $.Deferred();
                const args = {};
                [
                    'skip',
                    'take',
                    'requireTotalCount',
                    'requireGroupCount',
                    'sort',
                    'filter',
                    'totalSummary',
                    'group',
                    'groupSummary',
                ].forEach((i) => {
                    if (i in options && isNotEmpty(options[i])) {
                        args[i] = JSON.stringify(options[i]);
                    }
                });

                $.ajax({
                    url: `${url}/report/csat?start_date=${start_date}&end_date=${end_date}&type_value=${type_value}`,
                    dataType: 'json',
                    method: 'GET',
                    cache: false,
                    async: true,
                    data: args,
                    success(result) {
                        if (result.status === 200) {
                            deferred.resolve(result.data, {
                                totalCount: result.totalCount,
                                summary: result.summary,
                                groupCount: result.groupCount,
                            });
                        } else {
                            alert(result.data)
                            deferred.resolve([], {
                                totalCount: 0,
                                summary: result.summary,
                                groupCount: result.groupCount,
                            });
                        }
                    },
                    error() {
                        deferred.reject('Data Loading');
                    },
                });
                return deferred.promise();
            },
        });

        $('#dxReportSurveyCSAT').dxDataGrid({
            dataSource: store_report_csat,
            remoteOperations: true,
            paging: {
                pageSize: 10,
            },
            pager: {
                visible: true,
                allowedPageSizes: [10, 20, 50],
                showPageSizeSelector: true,
                showInfo: true,
                showNavigationButtons: true,
            },
            allowColumnResizing: true,
            columnWidth: 120,
            showBorders: true,
            showRowLines: true,
            hoverStateEnabled: true,
            // filterRow: {
            //     visible: true,
            // },
            columns: [{
                caption: 'Interaction ID',
                dataField: 'UniqueID',
                width: 250
            }, {
                dataField: 'Channel',
            }, {
                caption: 'Ticket Number',
                dataField: 'TicketNumber',
                width: 200

            }, {
                caption: 'Result Survey CSAT',
                dataField: 'ResultCSAT',
                width: 250
            }, {
                caption: 'Value Detail',
                dataField: 'ValueDetail',
                width: 300
            }, {
                caption: 'CSAT Date',
                dataField: 'DateCSAT',
                width: 150
            }, {
                caption: 'Reason Code',
                dataField: 'ReasonCode',
                width: 300
            }, {
                caption: 'Created Date Ticket',
                dataField: 'DateCreate',
                width: 150
            }],
        }).dxDataGrid('instance');
    }
    LoadReportCSAT();

    $('#btn-submit').click(function () {
        LoadReportCSAT();
    });

    $('#btn-export').click(function (e) {
        $('#btn-export').prop('disabled', true);
        const start_date = $('#MainContent_dt_strdate_I').val();
        const end_date = $('#MainContent_dt_endate_I').val();
        const format = $('#MainContent_ddList').val();
        const type_value = $('#cb_type').val();

        $.ajax({
            url: `${url}/report/csat_export?start_date=${start_date}&end_date=${end_date}&type_value=${type_value}`,
            dataType: 'json',
            method: 'GET',
            cache: false,
            async: true,
            success({ status, data }) {
                if (status === 200) {
                    const workbook = new ExcelJS.Workbook();
                    const worksheet = workbook.addWorksheet('Report Survey CSAT');

                    worksheet.columns = [{
                        header: 'Interaction ID',
                        key: 'UniqueID',
                        width: 50
                    }, {
                        header: 'Channel',
                        key: 'Channel',
                        width: 15,
                    }, {
                        header: 'Ticket Number',
                        key: 'TicketNumber',
                        width: 30
                    
                    }, {
                        header: 'Result Survey CSAT',
                        key: 'ResultCSAT',
                        width: 30
                    }, {
                        header: 'Value Detail',
                        key: 'ValueDetail',
                        width: 50
                    }, {
                        header: 'CSAT Date',
                        key: 'DateCSAT',
                        width: 20
                    }, {
                        header: 'Reason Code',
                        key: 'ReasonCode',
                        width: 50
                    }, {
                        header: 'Created Date Ticket',
                        key: 'DateCreate',
                        width: 20
                    }];
                    worksheet.addRows(data);
                    worksheet.autoFilter = 'A1:AI1';
                    worksheet.eachRow(function (row, rowNumber) {
                        row.eachCell((cell, colNumber) => {
                            if (rowNumber === 1) {
                                cell.alignment = { vertical: 'middle', horizontal: 'center' },
                                    cell.fill = {
                                        type: 'pattern',
                                        pattern: 'solid',
                                        fgColor: { argb: '666666' }
                                    },
                                    cell.font = {
                                        color: { argb: 'FFFFFF' },
                                        name: 'Times New Roman',
                                        size: 10,
                                    }
                            }
                            else {
                                cell.alignment = { vertical: 'middle', horizontal: 'left', wrapText: true },
                                    cell.font = {
                                        name: 'Times New Roman',
                                        size: 10,
                                    }
                            }
                        })
                        row.commit();
                    });
                    worksheet.columns.forEach(column => {
                        // column.width = column.header.length < 10 ? 10 : column.header.length;
                        column.border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }
                    });

                    if (format === 'xlsx') {
                        workbook.xlsx.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingSurveyCSAT_${start_date}_to_${end_date}.xlsx`);
                        });
                    }
                    else if (format === 'xls') {
                        workbook.xlsx.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingSurveyCSAT_${start_date}_to_${end_date}.xls`);
                        });
                    }
                    else if (format === 'csv') {
                        workbook.csv.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingSurveyCSAT_${start_date}_to_${end_date}.csv`);
                        });
                    }
                    else if (format === 'pdf') {
                        const doc = new jsPDF({
                            orientation: "landscape",
                            unit: "px",
                            format: "a0",
                            putOnlyUsedFonts: true,
                        });

                        doc.autoTable({
                            theme: 'grid',
                            headStyles: {
                                fillColor: '#666666',
                                textColor: '#FFFFFF',
                            },
                            body: data,
                            margin: 5,
                            columns: [{
                                header: 'Interaction ID',
                                dataKey: 'UniqueID',
                            }, {
                                header: 'Channel',
                                dataKey: 'Channel',
                            }, {
                                header: 'Ticket Number',
                                dataKey: 'TicketNumber',
                            
                            }, {
                                header: 'Result Survey CSAT',
                                dataKey: 'ResultCSAT',
                            }, {
                                header: 'Value Detail',
                                dataKey: 'ValueDetail',
                            }, {
                                header: 'CSAT Date',
                                dataKey: 'DateCSAT',
                            }, {
                                header: 'Reason Code',
                                dataKey: 'ReasonCode',
                            }, {
                                header: 'Created Date Ticket',
                                dataKey: 'DateCreate',
                            }],
                        });
                        doc.save(`ReportingSurveyCSAT_${start_date}_to_${end_date}.pdf`);
                    }
                    else if (format === 'rtf') {
                        alert('cannot export')
                    }
                }
                else {
                    alert(data); //alert max range.
                }

                $('#btn-export').prop('disabled', false);
            },
            error() {
                alert('Data Loading');
                $('#btn-export').prop('disabled', false);
            },
            // timeout: 60 * 1000,
        });

        e.cancel = true;
    });

});
