import { url } from "./config.js";

document.addEventListener('DOMContentLoaded', (event) => {
    $.ajaxSetup({ cache: false });
    const user_create = $('#MainContent_TrxUserName').val();

    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== '';
    }

    $('#dxDataCustomer').show();

    const store_customer = new DevExpress.data.CustomStore({
        key: 'CustomerID',
        load(options) {
            const deferred = $.Deferred();
            const args = {};
            [
                'skip',
                'take',
                'requireTotalCount',
                'requireGroupCount',
                'sort',
                'filter',
                'totalSummary',
                'group',
                'groupSummary',
            ].forEach((i) => {
                if (i in options && isNotEmpty(options[i])) {
                    args[i] = JSON.stringify(options[i]);
                }
            });

            $.ajax({
                url: `${url}/customer.php?action=data`,
                dataType: 'json',
                method: 'GET',
                cache: false,
                async: true,
                data: args,
                success(result) {
                    deferred.resolve(result.data, {
                        totalCount: result.totalCount,
                        summary: result.summary,
                        groupCount: result.groupCount,
                    });
                },
                error() {
                    deferred.reject('Data Loading Error');
                },
                timeout: 5000,
            });

            return deferred.promise();
        },
    });

    $('#dxDataCustomer').dxDataGrid({
        dataSource: store_customer,
        remoteOperations: true,
        paging: {
            pageSize: 15,
        },
        pager: {
            visible: true,
            allowedPageSizes: [15, 30, 50],
            showPageSizeSelector: true,
            showInfo: true,
            showNavigationButtons: true,
        },
        allowColumnResizing: true,
        columnWidth: 120,
        showBorders: true,
        showRowLines: true,
        hoverStateEnabled: true,
        filterRow: {
            visible: true,
        },
        masterDetail: {
            enabled: true,
            template(container, options) {
                MasterDetail(container, options)
            },
        },
        columns: [{
            type: 'buttons',
            caption: 'Actions',
            buttons: [{
                hint: 'Select Customer',
                //icon: 'fieldchooser',
                icon: 'img/icon/Text-Edit-icon2.png',
                onClick(e) {
                    const data = e.row.data;
                    RedirectThreadToMainframe(data.CustomerID)
                    e.event.preventDefault();
                },
            }]
        }, {
            dataField: 'CustomerID',
            dataType: 'string',
            allowEditing: false
        }, {
            dataField: 'Name',
            dataType: 'string',
            with: 200
        }, {
            dataField: 'Email',
            dataType: 'string',
            with: 200
        }, {
            caption: 'Phone Number',
            dataField: 'PhoneNumber',
            dataType: 'string',
        }, {
            caption: 'Gender',
            dataField: 'JenisKelamin',
            dataType: 'string',
        }, {
            dataField: 'NIK',
            dataType: 'string',
            with: 200
        }, {
            caption: 'Address',
            dataField: 'Alamat',
            dataType: 'string',
            with: 250
        }],
    }).dxDataGrid('instance');
    // $("#dxGridCustomer").dxDataGrid("instance").refresh();

    const MasterDetail = (container, options) => {
        const CustomerID = options.key;
        container.append($(`
            <div class="panel-tab clearfix">
                <ul class="tab-bar">
                    <li class="active"><a href="#account_number_${CustomerID}" data-toggle="tab"><i class="fa fa-credit-card"></i> Data Account Number</a></li>
                    <li><a href="#customer_channel_${CustomerID}" data-toggle="tab"><i class="fa fa-group"></i> Data Channel Customer</a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="account_number_${CustomerID}">
                        <div id="dxAccountCustomer_${CustomerID}"></div>
                    </div>
                    <div class="tab-pane fade" id="customer_channel_${CustomerID}">
                        <div id="dxCustomerChannel_${CustomerID}"></div>
                    </div>
                </div>
            </div>
        `));

        const store_accountnumber = new DevExpress.data.CustomStore({
            key: 'ID',
            load() {
                const deferred = $.Deferred();
                $.ajax({
                    url: `${url}/account_number/${CustomerID}`,
                    dataType: 'json',
                    method: 'GET',
                    cache: false,
                    async: true,
                    success(result) {
                        deferred.resolve(result.data, {
                            totalCount: result.data.length
                        });
                    },
                    error() {
                        deferred.reject('Data Loading Error');
                    },
                    timeout: 5000,
                });

                return deferred.promise();
            },
            insert(values) {
                values.CustomerID = CustomerID;
                values.Usercreate = user_create;
                $.ajax({
                    url: `${url}/account_number/insert`,
                    method: 'POST',
                    data: values,
                    success(result) {
                        if (result.status === 200) {
                            alert('Insert Data Success.')
                        } else {
                            alert(result.data)
                        }
                    },
                    error(error) {
                        console.error(error);
                        alert('Data error.')
                    },
                    timeout: 5000,
                });
            },
            update(ID, values) {
                $.ajax({
                    url: `${url}/account_number/update`,
                    method: 'PUT',
                    data: { ID, values },
                    success(result) {
                        if (result.status === 200) {
                            alert('Update Data Success.')
                        } else {
                            alert(result.data)
                        }
                    },
                    error(error) {
                        console.error(error);
                        alert('Data error.')
                    },
                    timeout: 5000,
                });
            },
            remove(ID) {
                $.ajax({
                    url: `${url}/account_number/delete`,
                    method: 'DELETE',
                    data: { ID },
                    success(result) {
                        console.log(result)
                    },
                    error() {
                        console.error('Data Loading Error');
                        alert('Data error.')
                    },
                    timeout: 5000,
                });
            }
        });

        const store_customerchannel = new DevExpress.data.CustomStore({
            key: 'ID',
            load() {
                const deferred = $.Deferred();
                $.ajax({
                    url: `${url}/customer_channel/${CustomerID}`,
                    dataType: 'json',
                    method: 'GET',
                    cache: false,
                    async: true,
                    success(result) {
                        deferred.resolve(result.data, {
                            totalCount: result.data.length
                        });
                    },
                    error() {
                        deferred.reject('Data Loading Error');
                    },
                    timeout: 5000,
                });

                return deferred.promise();
            },
            insert(values) {
                values.CustomerID = CustomerID;
                values.UserCreate = user_create;
                $.ajax({
                    url: `${url}/customer_channel/insert`,
                    method: 'POST',
                    data: values,
                    success(result) {
                        if (result.status === 200) {
                            alert('Insert Data Success.')
                        } else {
                            alert(result.data)
                        }
                    },
                    error(error) {
                        console.error(error);
                        alert('Data error.')
                    },
                    timeout: 5000,
                });
            },
            update(ID, values) {
                $.ajax({
                    url: `${url}/customer_channel/update`,
                    method: 'PUT',
                    data: { ID, values },
                    success(result) {
                        if (result.status === 200) {
                            alert('Update Data Success.')
                        } else {
                            alert(result.data)
                        }
                    },
                    error(error) {
                        console.error(error);
                        alert('Data error.')
                    },
                    timeout: 5000,
                });
            },
            remove(ID) {
                $.ajax({
                    url: `${url}/customer_channel/delete`,
                    method: 'DELETE',
                    data: { ID },
                    success(result) {
                        console.log(result)
                    },
                    error() {
                        console.error('Data Loading Error');
                        alert('Data error.')
                    },
                    timeout: 5000,
                });
            }
        });

        $('#dxAccountCustomer_' + CustomerID).dxDataGrid({
            dataSource: store_accountnumber,
            showBorders: true,
            showRowLines: true,
            allowColumnResizing: true,
            remoteOperations: true,
            filterRow: {
                visible: true,
            },
            paging: {
                pageSize: 5,
            },
            pager: {
                visible: true,
                allowedPageSizes: [5, 10, 20],
                showPageSizeSelector: true,
                showInfo: true,
            },
            editing: {
                mode: 'row',
                allowUpdating: true,
                allowAdding: true,
                allowDeleting: true,
                useIcons: true,
            },
            columns: [{
                type: 'buttons',
                caption: 'Actions',
                buttons: ['edit', 'delete'],
            }, {
                caption: 'Account Number',
                dataField: 'NomorRekening',
                dataType: 'string',
            }]
        });

        $('#dxCustomerChannel_' + CustomerID).dxDataGrid({
            dataSource: store_customerchannel,
            showBorders: true,
            showRowLines: true,
            allowColumnResizing: true,
            filterRow: {
                visible: true,
            },
            paging: {
                pageSize: 5,
            },
            pager: {
                visible: true,
                allowedPageSizes: [5, 10, 20],
                showPageSizeSelector: true,
                showInfo: true,
            },
            editing: {
                mode: 'row',
                allowUpdating: true,
                allowAdding: true,
                allowDeleting: true,
                useIcons: true,
            },
            columns: [{
                type: 'buttons',
                buttons: ['edit', 'delete'],
            }, {
                caption: 'Channel',
                dataField: 'ValueChannel',
                dataType: 'string',
            }, {
                caption: 'Type',
                dataField: 'FlagChannel',
                dataType: 'string',
                // allowEditing: false
            }]
        });
    }

});
