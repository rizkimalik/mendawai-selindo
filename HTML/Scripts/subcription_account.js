import { url } from "./config.js";

document.addEventListener('DOMContentLoaded', (event) => {
    $.ajaxSetup({ cache: false });
    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== '';
    }

    function UnsubcribeChannel(id) {
        if (confirm("Unsubcribe Channel?") == true) {
            $.ajax({
                url: `${url}/subcription_account.php?action=unsubcribe`,
                dataType: 'json',
                method: 'POST',
                cache: false,
                async: true,
                data: JSON.stringify({ id: id }),
                success(result) {
                    alert('Success Unsubcribe.')
                    $("#dxGridDataSubcription").dxDataGrid("instance").refresh();
                },
                error() {
                    console.log('error');
                },
                timeout: 5000,
            });
        }
    }

    const store_subcription_account = new DevExpress.data.CustomStore({
        key: 'id',
        load(options) {
            const deferred = $.Deferred();
            const args = {};
            [
                'skip',
                'take',
                'requireTotalCount',
                'requireGroupCount',
                'sort',
                'filter',
                'totalSummary',
                'group',
                'groupSummary',
            ].forEach((i) => {
                if (i in options && isNotEmpty(options[i])) {
                    args[i] = JSON.stringify(options[i]);
                }
            });

            $.ajax({
                url: `${url}/subcription_account.php?action=data`,
                dataType: 'json',
                method: 'GET',
                cache: false,
                async: true,
                data: args,
                success(result) {
                    deferred.resolve(result.data, {
                        totalCount: result.totalCount,
                        summary: result.summary,
                        groupCount: result.groupCount,
                    });
                },
                error() {
                    deferred.reject('Data Loading Error');
                },
                timeout: 5000,
            });

            return deferred.promise();
        },
    });

    $('#dxGridDataSubcription').dxDataGrid({
        dataSource: store_subcription_account,
        remoteOperations: true,
        paging: {
            pageSize: 15,
        },
        pager: {
            visible: true,
            allowedPageSizes: [15, 30, 50],
            showPageSizeSelector: true,
            showInfo: true,
            showNavigationButtons: true,
        },
        allowColumnResizing: true,
        columnMinWidth: 120,
        showBorders: true,
        showRowLines: true,
        hoverStateEnabled: true,
        filterRow: {
            visible: true,
        },
        columns: [{
            type: 'buttons',
            caption: 'Actions',
            buttons: [{
                hint: 'Unsubcribe',
                icon: 'img/icon/Text-Edit-icon2.png',
                onClick(e) {
                    const data = e.row.data;
                    UnsubcribeChannel(data.id);
                    e.event.preventDefault();
                },
            }],
        }, {
            caption: 'Channel',
            dataField: 'channel',
            dataType: 'string',
        }, {
            caption: 'Page ID',
            dataField: 'page_id',
            dataType: 'string',
        }, {
            caption: 'Page Name',
            dataField: 'page_name',
            dataType: 'string',
        }, {
            caption: 'Page Category',
            dataField: 'page_category',
            dataType: 'string',
        }, {
            caption: 'Account ID',
            dataField: 'account_id',
            dataType: 'string',
        }, {
            caption: 'URL API',
            dataField: 'url_api',
            dataType: 'string',
        }, {
            caption: 'Token',
            dataField: 'token',
            dataType: 'string',
        }, {
            caption: 'Token Secret',
            dataField: 'token_secret',
            dataType: 'string',
        }, {
            caption: 'Create At',
            dataField: 'created_at',
            dataType: 'string',
        }, {
            caption: 'Update At',
            dataField: 'updated_at',
            dataType: 'string',
        }],
    }).dxDataGrid('instance');

});
