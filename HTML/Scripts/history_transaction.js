import { url } from "./config.js";

document.addEventListener('DOMContentLoaded', (event) => {
    $.ajaxSetup({ cache: false });
    // const user_create = $('#MainContent_TrxUserName').val();

    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== '';
    }

    const store_history = new DevExpress.data.CustomStore({
        key: 'CustomerID',
        load(options) {
            const deferred = $.Deferred();
            const args = {};
            [
                'skip',
                'take',
                'requireTotalCount',
                'requireGroupCount',
                'sort',
                'filter',
                'totalSummary',
                'group',
                'groupSummary',
            ].forEach((i) => {
                if (i in options && isNotEmpty(options[i])) {
                    args[i] = JSON.stringify(options[i]);
                }
            });

            $.ajax({
                url: `${url}/history_transaction.php?action=data`,
                dataType: 'json',
                method: 'GET',
                cache: false,
                async: true,
                data: args,
                success(result) {
                    deferred.resolve(result.data, {
                        totalCount: result.totalCount,
                        summary: result.summary,
                        groupCount: result.groupCount,
                    });
                },
                error() {
                    deferred.reject('Data Loading Error');
                },
                timeout: 10000,
            });

            return deferred.promise();
        },
    });

    $('#dxDataHistoryTransaction').dxDataGrid({
        dataSource: store_history,
        remoteOperations: true,
        paging: {
            pageSize: 15,
        },
        pager: {
            visible: true,
            allowedPageSizes: [15, 30, 50],
            showPageSizeSelector: true,
            showInfo: true,
            showNavigationButtons: true,
        },
        allowColumnResizing: true,
        columnWidth: 120,
        showBorders: true,
        showRowLines: true,
        hoverStateEnabled: true,
        filterRow: {
            visible: true,
        },
        columns: [{
            type: 'buttons',
            caption: 'Actions',
            buttons: [{
                hint: 'View Details',
                // icon: 'find',
                icon: 'img/icon/Text-Edit-icon2.png',
                onClick(e) {
                    const data = e.row.data;
                    ShowUpdateTransaction(data.TicketNumber);
                    e.event.preventDefault();
                }
            }],
        }, {
            caption: 'Channel',
            dataField: 'TicketSourceName',
        }, {
            dataField: 'TicketNumber',
            width: 200
        }, {
            dataField: 'CustomerID',
        }, {
            dataField: 'Name',
            width: 200
        }, {
            dataField: 'EMAIL',
            width: 200
        }, {
            caption: 'Interaction ID',
            dataField: 'GenesysID',
            width: 200
        }, {
            dataField: 'ThreadTicket',
        }, {
            caption: 'Phone Number',
            dataField: 'PhoneNumber',
        }, {
            caption: 'Address',
            dataField: 'alamat',
        }, {
            caption: 'Account',
            dataField: 'AccountInbound',
        }, {
            caption: 'Reason',
            dataField: 'SubCategory3Name',
            width: 300
        }, {
            caption: 'User Issue Remark',
            dataField: 'DetailComplaint',
            width: 300
        }, {
            caption: 'Agent Response',
            dataField: 'ResponComplaint',
            width: 300
        }, {
            dataField: 'Status',
        },{
            dataField: 'UserCreate',
        }, {
            dataField: 'DateCreate',
        }, {
            dataField: 'LastResponseBy',
        }, {
            dataField: 'LastResponseDate',
            // dataType: 'datetime',
        }],

       /*  {
            type: 'buttons',
            caption: 'Parent Ticket Action',
            buttons: [{
                hint: 'Parent Child Ticket',
                // icon: 'group',
                icon: 'img/icon/Text-Edit-icon2.png',
                onClick(e) {
                    const data = e.row.data;
                    ShowParentChild(data.TicketNumber);
                    e.event.preventDefault();
                },
            },],
        }, */
    }).dxDataGrid('instance');
});
