import { url } from "./config.js";

document.addEventListener('DOMContentLoaded', (event) => {
    $.ajaxSetup({ cache: false });

    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== '';
    }

    function LoadReportTransaction() {
        const start_date = $('#MainContent_dt_strdate_I').val();
        const end_date = $('#MainContent_dt_endate_I').val();
        const cmb_customer = $('#MainContent_cm_customer_id_I').val();
        const customer_id = cmb_customer.split(';')[0];

        const store_report_transaction = new DevExpress.data.CustomStore({
            key: 'TicketNumber',
            load(options) {
                const deferred = $.Deferred();
                const args = {};
                [
                    'skip',
                    'take',
                    'requireTotalCount',
                    'requireGroupCount',
                    'sort',
                    'filter',
                    'totalSummary',
                    'group',
                    'groupSummary',
                ].forEach((i) => {
                    if (i in options && isNotEmpty(options[i])) {
                        args[i] = JSON.stringify(options[i]);
                    }
                });

                $.ajax({
                    url: `${url}/report_transaction.php?action=data&start_date=${start_date}&end_date=${end_date}&customer_id=${customer_id}`,
                    dataType: 'json',
                    method: 'GET',
                    cache: false,
                    async: true,
                    data: args,
                    success(result) {
                        if (result.status === 200) {
                            deferred.resolve(result.data, {
                                totalCount: result.totalCount,
                                summary: result.summary,
                                groupCount: result.groupCount,
                            });
                        } else {
                            alert(result.data)
                            deferred.resolve([], {
                                totalCount: 0,
                                summary: result.summary,
                                groupCount: result.groupCount,
                            });
                        }
                    },
                    error() {
                        deferred.reject('Data Loading');
                    },
                    // timeout: 10000,
                });

                return deferred.promise();
            },
        });

        $('#dxReport_BaseOnTransaction').dxDataGrid({
            dataSource: store_report_transaction,
            remoteOperations: true,
            paging: {
                pageSize: 10,
            },
            pager: {
                visible: true,
                allowedPageSizes: [10, 20, 50],
                showPageSizeSelector: true,
                showInfo: true,
                showNavigationButtons: true,
            },
            allowColumnResizing: true,
            columnWidth: 120,
            showBorders: true,
            showRowLines: true,
            hoverStateEnabled: true,
            filterRow: {
                visible: true,
            },
            columns: [{
                dataField: 'TicketNumber',
                width: 200
            }, {
                caption: 'Channel',
                dataField: 'TicketSourceName',
            }, {
                caption: 'Account',
                dataField: 'AccountInbound',
            }, {
                caption: 'Interaction ID',
                dataField: 'ThreadID',
            }, {
                caption: 'Customer ID',
                dataField: 'NIK',
            }, {
                caption: 'Customer Name',
                dataField: 'CustomerName',
            }, {
                caption: 'Department',
                dataField: 'Department',
            }, {
                caption: 'Category',
                dataField: 'CategoryName',
            }, {
                caption: 'Enquiry Type',
                dataField: 'Level1',
            }, {
                caption: 'Enquiry Detail',
                dataField: 'Level2',
            }, {
                caption: 'Enquiry Reason',
                dataField: 'Level3',
                width: 300
            }, {
                caption: 'Detail Complaint',
                dataField: 'DetailComplaint',
                width: 300
            }, {
                caption: 'Detail Response',
                dataField: 'ResponComplaint',
                width: 300
            }, {
                caption: 'Ticket Status',
                dataField: 'TicketStatus',
            }, {
                dataField: 'SLA',
            }, {
                dataField: 'TicketPosition',
            }, {
                caption: 'Created By',
                dataField: 'CreatedBy',
            }, {
                caption: 'Created Date',
                dataField: 'CreatedDate',
            }, {
                dataField: 'LastResponseBy',
            }, {
                dataField: 'LastResponseDate',
            }],
        }).dxDataGrid('instance');
    }
    LoadReportTransaction();

    $('#btn-submit').click(function () {
        LoadReportTransaction();
    });

    $('#btn-export').click(function (e) {
        $('#btn-export').prop('disabled', true);
        const start_date = $('#MainContent_dt_strdate_I').val();
        const end_date = $('#MainContent_dt_endate_I').val();
        const format = $('#MainContent_ddList').val();
        const cmb_customer = $('#MainContent_cm_customer_id_I').val();
        const customer_id = cmb_customer.split(';')[0];

        $.ajax({
            url: `${url}/report_transaction.php?action=export&start_date=${start_date}&end_date=${end_date}&customer_id=${customer_id}`,
            dataType: 'json',
            method: 'GET',
            cache: false,
            async: true,
            success({ status, data }) {
                if (status === 200) {
                    const workbook = new ExcelJS.Workbook();
                    const worksheet = workbook.addWorksheet('Report base on SLA');

                    worksheet.columns = [{
                        header: 'TicketNumber',
                        key: 'TicketNumber'
                    }, {
                        header: 'Channel',
                        key: 'TicketSourceName',
                    }, {
                        header: 'Account',
                        key: 'AccountInbound',
                    }, {
                        header: 'Interaction ID',
                        key: 'ThreadID',
                    }, {
                        header: 'Customer ID',
                        key: 'NIK',
                    }, {
                        header: 'Customer Name',
                        key: 'CustomerName',
                    }, {
                        header: 'Department',
                        key: 'Department',
                    }, {
                        header: 'Category',
                        key: 'CategoryName',
                    }, {
                        header: 'Enquiry Type',
                        key: 'Level1',
                    }, {
                        header: 'Enquiry Detail',
                        key: 'Level2',
                    }, {
                        header: 'Enquiry Reason',
                        key: 'Level3',
                    }, {
                        header: 'Detail Complaint',
                        key: 'DetailComplaint',
                    }, {
                        header: 'Detail Response',
                        key: 'ResponComplaint',
                    }, {
                        header: 'Ticket Status',
                        key: 'TicketStatus',
                    }, {
                        header: 'SLA',
                        key: 'SLA',
                    }, {
                        header: 'Ticket Position',
                        key: 'TicketPosition',
                    }, {
                        header: 'Created By',
                        key: 'CreatedBy',
                    }, {
                        header: 'Created Date',
                        key: 'CreatedDate',
                    }, {
                        header: 'Last Response By',
                        key: 'LastResponseBy',
                    }, {
                        header: 'Last Response Date',
                        key: 'LastResponseDate'
                    }];
                    worksheet.addRows(data);
                    worksheet.autoFilter = 'A1:AI1';
                    worksheet.eachRow(function (row, rowNumber) {
                        row.eachCell((cell, colNumber) => {
                            if (rowNumber === 1) {
                                cell.alignment = { vertical: 'middle', horizontal: 'center' },
                                    cell.fill = {
                                        type: 'pattern',
                                        pattern: 'solid',
                                        fgColor: { argb: '666666' }
                                    },
                                    cell.font = {
                                        color: { argb: 'FFFFFF' },
                                        name: 'Times New Roman',
                                        size: 10,
                                    }
                            }
                            else {
                                cell.alignment = { vertical: 'middle', horizontal: 'left', wrapText: true },
                                    cell.font = {
                                        name: 'Times New Roman',
                                        size: 10,
                                    }
                            }
                        })
                        row.commit();
                    });
                    worksheet.columns.forEach(column => {
                        // column.width = column.header.length < 10 ? 10 : column.header.length;
                        column.border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }
                    });

                    if (format === 'xlsx') {
                        workbook.xlsx.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingBaseonTransaction_${start_date}_to_${end_date}.xlsx`);
                        });
                    }
                    else if (format === 'xls') {
                        workbook.xlsx.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingBaseonTransaction_${start_date}_to_${end_date}.xls`);
                        });
                    }
                    else if (format === 'csv') {
                        workbook.csv.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingBaseonTransaction_${start_date}_to_${end_date}.csv`);
                        });
                    }
                    else if (format === 'pdf') {
                        const doc = new jsPDF({
                            orientation: "landscape",
                            unit: "px",
                            format: "a0",
                            putOnlyUsedFonts: true,
                        });

                        doc.autoTable({
                            theme: 'grid',
                            headStyles: {
                                fillColor: '#666666',
                                textColor: '#FFFFFF',
                            },
                            body: data,
                            margin: 5,
                            columns: [{
                                header: 'Ticket Number',
                                dataKey: 'TicketNumber',
                            }, {
                                header: 'Channel',
                                dataKey: 'TicketSourceName',
                            }, {
                                header: 'Account',
                                dataKey: 'AccountInbound',
                            }, {
                                header: 'Interaction ID',
                                dataKey: 'ThreadID',
                            }, {
                                header: 'Customer ID',
                                dataKey: 'NIK',
                            }, {
                                header: 'Customer Name',
                                dataKey: 'CustomerName',
                            }, {
                                header: 'Department',
                                dataKey: 'Department',
                            }, {
                                header: 'Category',
                                dataKey: 'CategoryName',
                            }, {
                                header: 'Enquiry Type',
                                dataKey: 'Level1',
                            }, {
                                header: 'Enquiry Detail',
                                dataKey: 'Level2',
                            }, {
                                header: 'Enquiry Reason',
                                dataKey: 'Level3',
                            }, {
                                header: 'Detail Complaint',
                                dataKey: 'DetailComplaint',
                            }, {
                                header: 'Detail Response',
                                dataKey: 'ResponComplaint',
                            }, {
                                header: 'Ticket Status',
                                dataKey: 'TicketStatus',
                            }, {
                                header: 'SLA',
                                dataKey: 'SLA',
                            }, {
                                header: 'Ticket Position',
                                dataKey: 'TicketPosition',
                            }, {
                                header: 'Created By',
                                dataKey: 'CreatedBy',
                            }, {
                                header: 'Created Date',
                                dataKey: 'CreatedDate',
                            }, {
                                header: 'Last Response By',
                                dataKey: 'LastResponseBy',
                            }, {
                                header: 'Last Response Date',
                                dataKey: 'LastResponseDate',
                            }],
                        });
                        doc.save(`ReportingBaseonTransaction_${start_date}_to_${end_date}.pdf`);
                    }
                    else if (format === 'rtf') {
                        alert('cannot export')
                    }
                }
                else {
                    alert(data); //alert max range.
                }

                $('#btn-export').prop('disabled', false);
            },
            error() {
                alert('Data Loading');
                $('#btn-export').prop('disabled', false);
            },
            // timeout: 60 * 1000,
        });

        e.cancel = true;
    });

});
