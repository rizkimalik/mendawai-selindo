﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="ticket.aspx.vb" Inherits="ICC.ticket1" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">

    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-user"></i> Data Customer
                        <div class="btn-group pull-right">
                            <a href="#panelDataCustomer" data-toggle="collapse"><i class="fa fa-arrows-v"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" id="panelDataCustomer">
                        <div class="row">
                            <div class="col-lg-12">
                                <label>CustomerID *</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="321212310 - nexx" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Name *</label>
                                <input type="text" class="form-control" value="321212310 - nexx" readonly>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Phone Number *</label>
                                <input type="text" class="form-control" value="321212310 - nexx" readonly>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Email *</label>
                                <input type="text" class="form-control" value="321212310 - nexx" readonly>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Gender</label>
                                <input type="text" class="form-control" value="321212310 - nexx" readonly>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Address</label>
                                <textarea class="form-control" rows="3" readonly>jl rambutan</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-warning btn-sm">Edit</button>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-user"></i> Information From
                        <div class="btn-group pull-right">
                            <a href="#panelDataCustomer" data-toggle="collapse"><i class="fa fa-arrows-v"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" id="panelDataCustomer">
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Name *</label>
                                <input type="text" class="form-control" value="321212310 - nexx" readonly>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Phone Number *</label>
                                <input type="text" class="form-control" value="321212310 - nexx" readonly>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Email *</label>
                                <input type="text" class="form-control" value="321212310 - nexx" readonly>
                            </div>
                        </div>
                        <div class="seperator"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Address</label>
                                <textarea class="form-control" rows="3" readonly>jl rambutan</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-9 col-sm-9">
                <!-- Data Ticket -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-file"></i> Ticket Properties
                    </div>
                    <div class="panel-tab clearfix">
                        <ul class="tab-bar">
                            <li class="active">
                                <a href="#TicketTitle" data-toggle="tab"><i class="fa fa-pencil"></i> Ticket Title</a>
                            </li>
                            <li>
                                <a href="#TicketJourney" data-toggle="tab"><i class="fa fa-home"></i> Ticket Journey</a>
                            </li>
                            <li>
                                <a href="#TicketHistory" data-toggle="tab"><i class="fa fa-envelope"></i> Ticket History</a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="TicketTitle">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Company</label>
                                            <input type="text" placeholder="Your Company" class="form-control input-sm parsley-validated" data-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Website</label>
                                            <input type="text" placeholder="Your Website" class="form-control input-sm parsley-validated" data-required="true" data-type="url">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Message</label>
                                    <textarea class="form-control parsley-validated" placeholder="Your message here..." rows="3" data-required="true"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-success btn-sm">Create Ticket</button>
                                    </div>  
                                </div>
                            </div>
                            <div class="tab-pane fade" id="TicketJourney">
                                <p>from</p>
                            </div>
                            <div class="tab-pane fade" id="TicketHistory">
                                <p>grid</p>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Data Journey -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-file"></i> Ticket Journey
                        <ul class="tool-bar">
                            <li><a href="#" class="refresh-widget" data-toggle="tooltip" data-placement="bottom"
                                    data-original-title="Refresh"><i class="fa fa-refresh"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="timeline-wrapper" style="height: 600px; overflow:auto;">
                            <div class="timeline-item timeline-start" style="width:100%">
                                <div class="panel">
                                    <div class="panel-body">
                                        <table class="table table-bordered table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td rowspan="2" class="text-nowrap"><strong
                                                            class="font-14 text-capitalize">Asuransi</strong><br><strong
                                                            class="font-14 text-capitalize">ASKES
                                                            BPJS</strong><br><strong
                                                            class="font-14 text-capitalize">ASKES</strong></td>
                                                    <td><strong class="font-14">Problem, Intermitten, </strong></td>
                                                    <td class="text-center"><strong class="font-14">Open</strong></td>
                                                    <td class="text-center"><strong class="font-14">Achmad Robi</strong>
                                                    </td>
                                                    <td class="text-right text-nowrap"><strong
                                                            class="font-14">07/05/2021 10:17:24</strong></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div
                                                            style="overflow:auto;overflow-x:auto;overflow-y:hidden;max-width:200">
                                                            <p>tolong di cek ya<br></p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-item">
                                <div class="timeline-info">
                                    <div class="timeline-icon bg-grey"><i class="fa fa-file-text-o"></i></div>
                                    <div class="time">07/05/2021 10:28:46</div>
                                </div>
                                <div class="panel panel-default timeline-panel">
                                    <div class="panel-heading">
                                        <div class="row" style="height:18px">
                                            <div class="col-lg-4"><label>Ahmad Firdaus</label></div>
                                            <div class="col-lg-5 text-center"></div>
                                            <div class="col-lg-3 text-right"><label>Pending</label></div>
                                        </div>
                                    </div>
                                    <div class="panel-body" style="color:black">
                                        <table class="table table-bordered table-condensed table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <td colspan="2"><label>Update By</label></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Achmad Robi</td>
                                                    <td>25/05/2021 08:52:58</td>
                                                </tr>
                                                <tr>
                                                    <td>Achmad Robi</td>
                                                    <td>21/05/2021 14:59:33</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p>Escalated to CAC</p>
                                        <table class="table table-bordered table-condensed">
                                            <thead>
                                                <tr>
                                                    <td><label>No Registrasi</label></td>
                                                    <td><label>Date Created</label></td>
                                                    <td><label>Status</label></td>
                                                    <td><label>Response Date</label></td>
                                                    <td><label>Technision</label></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><label>SCM07052021-00003</label></td>
                                                    <td><label>07/05/2021 10:28:46</label></td>
                                                    <td><label>Pending</label></td>
                                                    <td><label>09/07/2021 13:44:40</label></td>
                                                    <td><label>Salim</label></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p>test keterangan
                                            <br><br><strong>update </strong><em>keterangan </em><span
                                                style="text-decoration: underline;">eskalasi</span>
                                        </p><a class="btn btn-xs btn-default" onclick="popupEditJourney(3)">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </asp:Content>