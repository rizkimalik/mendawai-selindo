﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="email_inbox.aspx.vb" Inherits="ICC.email_inbox" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function updateSpam(value) {
            if (confirm("Do you want to process?")) {
                var test = new FormData();
                $.ajax({
                    type: "POST",
                    url: "AjaxPages/updateSpam.aspx?id=" + value,
                    contentType: false,
                    processData: false,
                    data: test,
                    success: function (response) {
                        //alert("Ticket has been update successfully.");
                        window.location.reload();
                    }
                });
                return true;
            }
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">
        Data Email <asp:Label ID="Status" runat="server"></asp:Label>
        <div class="btn-group pull-right">
             <div class="btn-group">
                 <button class="btn btn-default" id="btn_revert" runat="server" type="submit">Revert All Email</button>
                 <button class="btn btn-default">Action</button>
                 <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                 <ul class="dropdown-menu slidedown">
                     <li><a href="?status=inbox">Inbox</a></li>
                     <li><a href="?status=queing&idpage=2027">Queing</a></li>
                     <li><a href="?status=revert&idpage=2027">Revert</a></li>
                     <li><a href="email_send.aspx?status=send">Sent Mail</a></li>
                     <li><a href="?status=compose">Compose</a></li>
                 </ul>
             </div>
         </div>
        <span class="line bg-danger" style="margin-top: 15px;"></span>
    </h4>


    <dx:ASPxGridView ID="ASPxGridView_Inbox" ClientInstanceName="ASPxGridView_Inbox" Width="100%" runat="server" DataSourceID="sql_inbox" KeyFieldName="ID"
        SettingsPager-PageSize="10" Theme="MetropolisBlue">
        <SettingsPager>
            <AllButton Text="All" />
            <NextPageButton Text="Next &gt;" />
            <PrevPageButton Text="&lt; Prev" />
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="10%">
                <DataItemTemplate>
                    <!-- <a href="?status=compose&idpage=<%= Request.QueryString("idpage") %>">
                        <asp:Image ImageUrl="img/icon/Apps-text-editor-icon2.png" ID="img_insert" runat="server" ToolTip="New Email" />
                    </a> -->
                    <a href="mainframe.aspx?agentid=<%# Session("username")%>&channel=email&genesysid=<%# Eval("EMAIL_ID")%>&customerid=&account=<%# Eval("EFROM")%>&accountid=<%# Eval("EFROM")%>&subject=&phoneChat=&emailaddress=<%# Eval("EFROM")%>&source=inbound&threadid=-">
                        <asp:Image ImageUrl="img/icon/Apps-text-editor-icon2.png" ID="Ticket" runat="server" ToolTip="Create Ticket" />
                    </a>
                    <a href="mailto:<%# Eval("EFROM")%>?subject=RE:<%# Eval("ESUBJECT")%>">
                        <asp:Image ImageUrl="img/icon/reply.png" ID="Image1" runat="server" ToolTip="Reply Email" />
                    </a>
                    <a href="?ivcid=<%# Eval("IVC_ID")%>&emailid=<%# Eval("EMAIL_ID")%>&from=<%# Eval("EFROM")%>&to=<%# Eval("ETO")%>&status=detail&idpage=<%= Request.QueryString("idpage") %>&detail=inbox">
                        <asp:Image ImageUrl="img/icon/clone.png" ID="Image2" runat="server" ToolTip="Detail Email" />
                    </a>
                    <a href="#" onclick="if (!updateSpam('<%# Eval("IVC_ID")%>')) return false;">
                        <asp:Image ImageUrl="img/icon/spam.png" Height="15px" ID="ImageSpam" runat="server" ToolTip="Spam Email" />
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Subject" FieldName="ESUBJECT" HeaderStyle-HorizontalAlign="left" Width="40%" />
            <dx:GridViewDataTextColumn Caption="Email" FieldName="EFROM" HeaderStyle-HorizontalAlign="left" Width="35%" />
            <dx:GridViewDataTextColumn Caption="Email Date" FieldName="Email_Date" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Agent" FieldName="agent" HeaderStyle-HorizontalAlign="left" Width="15%" />
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sql_inbox" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>


    <dx:ASPxGridView ID="ASPxGridView_Queing" ClientInstanceName="ASPxGridView_Queing" Width="100%" runat="server" DataSourceID="SqlData_Queing" KeyFieldName="ID"
        SettingsPager-PageSize="10" Theme="MetropolisBlue">
        <SettingsPager>
            <AllButton Text="All" />
            <NextPageButton Text="Next &gt;" />
            <PrevPageButton Text="&lt; Prev" />
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="Subject" FieldName="ESUBJECT" HeaderStyle-HorizontalAlign="left" Width="40%" />
            <dx:GridViewDataTextColumn Caption="Email" FieldName="EFROM" HeaderStyle-HorizontalAlign="left" Width="35%" />
            <dx:GridViewDataTextColumn Caption="Email Date" FieldName="Email_Date" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Agent" FieldName="agent" HeaderStyle-HorizontalAlign="left" Width="15%" />
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlData_Queing" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    
    
    <dx:ASPxGridView ID="ASPxGridView_Revert" ClientInstanceName="ASPxGridView_Revert" Width="100%" runat="server" DataSourceID="SqlData_Revert" KeyFieldName="EMAIL_ID"
        SettingsPager-PageSize="10" Theme="MetropolisBlue">
        <SettingsPager>
            <AllButton Text="All" />
            <NextPageButton Text="Next &gt;" />
            <PrevPageButton Text="&lt; Prev" />
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="10%">
                <DataItemTemplate>
                    <a href="?status=revert&idpage=<%= Request.QueryString("idpage") %>&email_id=<%# Eval("EMAIL_ID")%>">
                        <asp:Image ImageUrl="img/icon/reply.png" ID="img_revert" runat="server" ToolTip="Revert Email" />
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Idle (Day)" FieldName="IDLE_DAY" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Agent" FieldName="agent" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Date Receive" FieldName="datereceive" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Email" FieldName="EFROM" HeaderStyle-HorizontalAlign="left" Width="35%" />
            <dx:GridViewDataTextColumn Caption="Subject" FieldName="ESUBJECT" HeaderStyle-HorizontalAlign="left" Width="40%" />
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlData_Revert" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>

    <div id="div_send" runat="server">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <asp:TextBox ID="txt_to" runat="server" CssClass="form-control" placeholder="To" ValidationGroup="btnsend"></asp:TextBox>
        </div>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="btnsend"
            ErrorMessage="* Invalid Email Format" ControlToValidate="txt_to" ForeColor="red"
            SetFocusOnError="True"
            ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*">
        </asp:RegularExpressionValidator>
        <br />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <asp:TextBox ID="txt_cc" runat="server" CssClass="form-control" placeholder="Cc" ValidationGroup="btnsend"></asp:TextBox>
        </div>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="btnsend"
            ErrorMessage="* Invalid Email Format" ControlToValidate="txt_cc" ForeColor="red"
            SetFocusOnError="True"
            ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*">
        </asp:RegularExpressionValidator>
        <br />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-edit"></i></span>
            <asp:TextBox ID="txt_subject" runat="server" CssClass="form-control" placeholder="Subject"></asp:TextBox>
        </div>
        <br />
        <dx:ASPxHtmlEditor ID="ASPxHtmlEditor1" runat="server" Width="100%">
            <Toolbars>
                <dx:HtmlEditorToolbar Name="StandardToolbar2">
                    <Items>
                        <dx:ToolbarParagraphFormattingEdit Width="120px">
                            <Items>
                                <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                <dx:ToolbarListEditItem Text="Address" Value="address" />
                                <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                            </Items>
                        </dx:ToolbarParagraphFormattingEdit>
                        <dx:ToolbarFontNameEdit>
                            <Items>
                                <dx:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman" />
                                <dx:ToolbarListEditItem Text="Tahoma" Value="Tahoma" />
                                <dx:ToolbarListEditItem Text="Verdana" Value="Verdana" />
                                <dx:ToolbarListEditItem Text="Arial" Value="Arial" />
                                <dx:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif" />
                                <dx:ToolbarListEditItem Text="Courier" Value="Courier" />
                            </Items>
                        </dx:ToolbarFontNameEdit>
                        <dx:ToolbarFontSizeEdit>
                            <Items>
                                <dx:ToolbarListEditItem Text="1 (8pt)" Value="1" />
                                <dx:ToolbarListEditItem Text="2 (10pt)" Value="2" />
                                <dx:ToolbarListEditItem Text="3 (12pt)" Value="3" />
                                <dx:ToolbarListEditItem Text="4 (14pt)" Value="4" />
                                <dx:ToolbarListEditItem Text="5 (18pt)" Value="5" />
                                <dx:ToolbarListEditItem Text="6 (24pt)" Value="6" />
                                <dx:ToolbarListEditItem Text="7 (36pt)" Value="7" />
                            </Items>
                        </dx:ToolbarFontSizeEdit>
                        <dx:ToolbarBoldButton BeginGroup="True"></dx:ToolbarBoldButton>
                        <dx:ToolbarItalicButton></dx:ToolbarItalicButton>
                        <dx:ToolbarUnderlineButton></dx:ToolbarUnderlineButton>
                        <dx:ToolbarStrikethroughButton></dx:ToolbarStrikethroughButton>
                        <dx:ToolbarJustifyLeftButton BeginGroup="True"></dx:ToolbarJustifyLeftButton>
                        <dx:ToolbarJustifyCenterButton></dx:ToolbarJustifyCenterButton>
                        <dx:ToolbarJustifyRightButton></dx:ToolbarJustifyRightButton>
                        <dx:ToolbarJustifyFullButton></dx:ToolbarJustifyFullButton>
                        <dx:ToolbarBackColorButton BeginGroup="True"></dx:ToolbarBackColorButton>
                        <dx:ToolbarFontColorButton></dx:ToolbarFontColorButton>
                    </Items>
                </dx:HtmlEditorToolbar>
            </Toolbars>
        </dx:ASPxHtmlEditor>
        <br />
        <dx:ASPxMemo ID="ASPxMemo1" runat="server" Visible="false"></dx:ASPxMemo>
        <asp:TextBox ID="txt_body" runat="server" TextMode="MultiLine" Height="200px" Width="100%" Visible="false"></asp:TextBox>
        <iframe id="iframe_body" runat="server" width="100%" height="400px" frameborder="1px" style="border-style: solid"></iframe>
        <%-- <dx:ASPxHtmlEditor ID="htmlreply" Width="100%" Height="300px" runat="server">
            <Settings AllowHtmlView="false" AllowPreview="false" />
        </dx:ASPxHtmlEditor>--%>
        <asp:RequiredFieldValidator ID="rqr_body_reply" ControlToValidate="txt_body" ForeColor="Red"
            runat="server" Display="Dynamic" Text="* Body Not Empty" ValidationGroup="btnsend"
            ErrorMessage="Please enter a value.">
        </asp:RequiredFieldValidator>
        <br />
        <div class="upload-file">
            <label data-title="Select file" for="upload-demo">
                <asp:FileUpload ID="uploadreply" runat="server" Visible="true" CssClass="upload-demo" onchange="get_filename(this);" />
                <span data-title="No file selected..."></span>
            </label>
        </div>
        <br />
        <div id="div_attachment" runat="server" visible="true">
            <table class="table-bordered table-condensed table-hover table-striped" id="Table1" style="width: 100%; border: 0px;">
                <thead>
                    <tr>
                        <th style="font-size: x-small; text-align: left;">Url attchment</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Literal ID="ltr_attchment" runat="server"></asp:Literal>
                </tbody>
            </table>
        </div>
        <br />
        <div>
            <div class="text-right">
                <button id="btn_send" runat="server" type="submit" class="btn btn-danger" validationgroup="btnsend"><i class="fa fa-send"></i>&nbsp;Send</button>
                <button id="btn_cancel" runat="server" class="btn btn-danger" type="submit"><i class="fa fa-arrow-circle-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
    
</asp:Content>

