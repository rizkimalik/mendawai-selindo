﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="status.aspx.vb" Inherits="ICC.status" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="div_status" runat="server">
        <h4 class="headline">Master Data Status
			<span class="line bg-info"></span>
        </h4>
        <div class="padding-md" style="margin-top: -20px;">
            <div class="row">
                <dx:ASPxGridView ID="gv_status" runat="server" KeyFieldName="id" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                    DataSourceID="sql_status" Width="100%" Theme="Metropolis">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="10, 15" ShowAllItem="false" />
                    </SettingsPager>
                    <SettingsPager PageSize="15" />
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="false" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <Columns>
                        <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                            ButtonType="Image" FixedStyle="Left" Width="100px">
                            <EditButton Visible="true">
                                <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                            </EditButton>
                            <NewButton Visible="true">
                                <Image ToolTip="New" Url="img/Icon/Apps-text-editor-icon2.png" />
                            </NewButton>
                            <DeleteButton Visible="true">
                                <Image ToolTip="Delete" Url="img/Icon/Actions-edit-clear-icon2.png" />
                            </DeleteButton>
                            <CancelButton Visible="true">
                                <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                </Image>
                            </CancelButton>
                            <UpdateButton Visible="true">
                                <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                            </UpdateButton>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Caption="ID" FieldName="id" Width="50px" CellStyle-HorizontalAlign="left" Visible="false">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="No" FieldName="Urutan" Width="50px" CellStyle-HorizontalAlign="left">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Status" FieldName="status"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Active/InActive" FieldName="NA" Width="150px">
                            <PropertiesComboBox>
                                <Items>
                                    <dx:ListEditItem Text="Active" Value="Y" />
                                    <dx:ListEditItem Text="InActive" Value="N" />
                                </Items>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                    </Columns>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="sql_status" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mStatus order by Urutan asc" DeleteCommand="delete from mStatus where ID=@ID"></asp:SqlDataSource>
            </div>
            <!-- /.row -->
        </div>
    </div>
</asp:Content>
