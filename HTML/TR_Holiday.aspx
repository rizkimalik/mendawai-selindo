﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="TR_Holiday.aspx.vb" Inherits="ICC.TR_Holiday" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Master Data Hari Libur
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="ds_select" KeyFieldName="ID"
        Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false"
            ShowGroupPanel="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="130px">
                <EditButton Visible="true">
                    <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="True">
                    <Image ToolTip="New" Url="img/Icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="true">
                    <Image ToolTip="Delete" Url="img/Icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton Visible="true">
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton Visible="true">
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Name" FieldName="NAME" VisibleIndex="2"></dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn Caption="Start Date" FieldName="START_DATE" VisibleIndex="3" PropertiesDateEdit-DisplayFormatString="dd-MM-yyyy" Width="200px"></dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn Caption="End Date" FieldName="END_DATE" VisibleIndex="4" PropertiesDateEdit-DisplayFormatString="dd-MM-yyyy" Width="200px"></dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn Caption="User Create" FieldName="USER_CREATE" VisibleIndex="5" ReadOnly="true" Visible="false"></dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn Caption="Date Create" FieldName="DATECREATE" VisibleIndex="6" PropertiesDateEdit-DisplayFormatString="dd-MM-yyyy hh:mm:ss" ReadOnly="true" Visible="false"></dx:GridViewDataDateColumn>
        </Columns>
        <Settings ShowGroupPanel="True" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="ds_select" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"      
        SelectCommand="SELECT * FROM LIBUR">
    </asp:SqlDataSource>
</asp:Content>

