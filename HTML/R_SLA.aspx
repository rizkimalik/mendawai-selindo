﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="R_SLA.aspx.vb" Inherits="ICC.R_SLA" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="module" src="scripts/report_sla.js"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h4 class="headline">Report base on SLA
			<span class="line bg-warning"></span>
        </h4>
        <br />
        <dx:ASPxLabel Visible="false" runat="server" ID="sqlOutput"></dx:ASPxLabel>
    </div>
    <div class="row" style="margin-bottom: -15px;">
        <div class="col-sm-2">
            <label>Start Date</label>
            <dx:ASPxDateEdit ID="dt_strdate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2">
            <label>End Date</label>
            <dx:ASPxDateEdit ID="dt_endate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2" style="margin-top: 5px;">
            <br />
            <button type="button" id="btn-submit" class="btn btn-sm btn-default">Submit</button>

            <%-- <dx:ASPxButton ID="btn_Submit" runat="server" Theme="Metropolis" AutoPostBack="False" Text="Submit" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton> --%>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <div style="overflow: hidden;">
                <div id="dxReportBaseOnSLA"></div>

                <%--<asp:GridView ID="GridView1" runat="server" Width="100%" Theme="Metropolis"
                    Styles-Header-Font-Bold="true" Font-Size="X-Small" SettingsPager-PageSize="10" PagerSettings-Mode="Numeric" DataSourceID="LinqDataSource1">
                </asp:GridView> --%>
                <%--<dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView" runat="server" Width="100%" Theme="Metropolis"
                    Styles-Header-Font-Bold="true" Font-Size="X-Small" SettingsPager-PageSize="10" DataSourceID="LinqDataSource1">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="15, 20, 25" ShowAllItem="true" />
                    </SettingsPager>
                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="User Status" FieldName="StrStatusPelapor" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Thread Ticket" FieldName="ThreadTicket" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Rekening Number" FieldName="NomorRekening" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Account" FieldName="AccountInbound" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Channel" FieldName="TicketSourceName" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="SourceInformation" FieldName="SumberInformasi" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Account ID" FieldName="AccountID" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="NIK" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Customer Name" FieldName="CustomerName" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="CIF Number" FieldName="CIF" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Phone Number" FieldName="PhoneNumber" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Department" FieldName="Channel_Code" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Category" FieldName="CategoryName" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Priority Scale" FieldName="SkalaPrioritas" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Enquiry Type" FieldName="Level1" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Enquiry Detail" FieldName="Level2" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Reason" FieldName="Level3" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataMemoColumn Caption="User Issue Remark" FieldName="Description" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataMemoColumn>
                        <dx:GridViewDataMemoColumn Caption="Agent Response" FieldName="ResponComplaint" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataMemoColumn>
                        <dx:GridViewDataTextColumn Caption="Amount" FieldName="Amount" Width="150px" Settings-AutoFilterCondition="Contains"  PropertiesTextEdit-DisplayFormatString="{0:n2}"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ticket Status" FieldName="TicketStatus" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Created By" FieldName="CreatedBy" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Created Date" FieldName="DateCreate" Width="150px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd hh:mm:ss"></dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn Caption="Solved By" FieldName="UserSolved" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Solved Date" FieldName="DateSolved" Width="150px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd hh:mm:ss"></dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn Caption="Closed By" FieldName="ClosedBy" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Closed Date" FieldName="DateClose" Width="150px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd hh:mm:ss"></dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn Caption="Last Response By" FieldName="LastResponseBy" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Last Response Date" FieldName="LastResponseDate" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="SLA" FieldName="SLA" Width="150px" CellStyle-HorizontalAlign="Left" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Description SLA" FieldName="UsedDaySLAOK" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView> --%>
            </div>

        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="csv" Text="CSV" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <%--<asp:ListItem Value="rtf" Text="RTF" />--%>
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <button type="button" id="btn-export" class="btn btn-sm btn-default">Export</button>

            <%--<dx:ASPxButton ID="btn_Export" runat="server" Text="Export" Theme="Metropolis" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton>--%>
        </div>
    </div>
    <hr />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1"></dx:ASPxGridViewExporter>

    <%--Metode Dengan View Tabel--%>
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ICC.R_SLA_View_DBMLDataContext"
        EntityTypeName="" TableName="V_SLA_ReportingS" Where="DateCreate >= @StartTanggalFilter And DateCreate <= @EndTanggalFilter" OrderBy="DateCreate ASC">
        <WhereParameters>
            <asp:SessionParameter Name="StartTanggalFilter" SessionField="StartTanggalFilter" Type="DateTime"/>
            <asp:SessionParameter Name="EndTanggalFilter" SessionField="EndTanggalFilter" Type="DateTime"/>
        </WhereParameters>
    </asp:LinqDataSource>
</asp:Content>
