﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxGridView.ASPxGridView
Imports DevExpress.Web.ASPxGridView

Public Class mt_adduser
    Inherits System.Web.UI.Page

    Dim Proses As New ClsConn
    Dim upPage As String = String.Empty
    Dim _ClasLoq As New WebServiceTransaction
    Dim _strTime As String = DateTime.Now.ToString("yyyy")
    Dim readUser, readerUser As SqlDataReader
    Dim _InsertActivity As New WebServiceTransaction
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        TrxLoginType.Value = Session("LoginTypeAngka")
        TrxUserCreated.Value = Session("UserName")
        dsUser.SelectCommand = "select * from msUser"
        If Session("LoginTypeAngka") = "10" Then
            dsLevelUser.SelectCommand = "SELECT * FROM MTROLEUser WHERE NumberNya <> '10' And NumberNya <> '5' order by NumberNya Asc"
        Else
            dsLevelUser.SelectCommand = "SELECT * FROM MTROLEUser order by NumberNya Asc"
        End If
        updateAlert()
    End Sub
    Private Sub ASPxGridView1_InitNewRow(sender As Object, e As DevExpress.Web.Data.ASPxDataInitNewRowEventArgs) Handles ASPxGridView1.InitNewRow
        If ValidasilicenseUser() = 1 Then
            DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("RedirectAccess.aspx?idx=2")
        Else

        End If
    End Sub
    Protected Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        dsUser.SelectCommand = "select * from msUser"
        If Session("LoginTypeAngka") = "10" Then

        Else

        End If
    End Sub
    Protected Sub ASPxGridView1_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles ASPxGridView1.RowDeleting
        Dim _strloq As String = String.Empty
        Dim _UserID As String = e.Keys("USERID")
        Dim _LevelUserSelect As String = e.Values("LEVELUSER")
        Dim _Action As String = "Delete"
        Dim _UserCreate As String = Session("UserName")
        If Session("LoginTypeAngka") = "10" Then
            If _LevelUserSelect = "Administrator" Or _LevelUserSelect = "Account User Admin" Then
                Throw New Exception("Delete Level User " & _LevelUserSelect & " Access Denied")
            Else
                _InsertActivity.InsertActivityManagementUserDelete(_Action, _UserID, _UserCreate)
                Dim str As String = "delete from msUser where USERID=@USERID"
                Try
                    dsUser.DeleteCommand = str
                    _strloq = "delete from msUser where USERID='" & _UserID & "'"
                    _ClasLoq.LogSuccess(_strTime, _strloq)
                Catch ex As Exception
                    _ClasLoq.LogError(_strTime, ex, _strloq)
                    Response.Write(ex.Message)
                End Try
            End If
        Else
            _InsertActivity.InsertActivityManagementUserDelete(_Action, _UserID, _UserCreate)
            Dim str As String = "delete from msUser where USERID=@USERID"
            Try
                dsUser.DeleteCommand = str
                _strloq = "delete from msUser where USERID='" & _UserID & "'"
                _ClasLoq.LogSuccess(_strTime, _strloq)
            Catch ex As Exception
                _ClasLoq.LogError(_strTime, ex, _strloq)
                Response.Write(ex.Message)
            End Try
        End If
    End Sub
    Protected Sub ASPxGridView1_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        Dim _UserName As String = e.NewValues("USERNAME")
        Dim _Password As String = e.NewValues("PASSWORD")
        Dim _Name As String = e.NewValues("NAME")
        Dim _LevelUser As String = e.NewValues("LEVELUSER")
        Dim _Organization As String = e.NewValues("ORGANIZATION")
        Dim _EmailAddress As String = e.NewValues("EMAIL_ADDRESS")
        Dim _KirimEmailNotif As String = e.NewValues("KIRIMEMAIL")
        Dim _StatuNotif As String = e.NewValues("NA")
        Dim _StatusUser As String = e.NewValues("STATUS_USER")
        Dim _strloq As String = String.Empty
        Dim _Action As String = "Insert"
        Dim _UserID As String = "-"
        Dim _UserCreate As String = Session("UserName")
        Dim _INBOUND As String = e.NewValues("INBOUND")
        Dim _EMAIL As String = e.NewValues("EMAIL")
        Dim _CHAT As String = e.NewValues("CHAT")
        Dim _WHATSAPP As String = e.NewValues("WHATSAPP")
        Dim _FACEBOOK As String = e.NewValues("FACEBOOK")
        Dim _TWITTER As String = e.NewValues("TWITTER")
        Dim _INSTAGRAM As String = e.NewValues("INSTAGRAM")
        Dim _MAX_INBOUND As String = e.NewValues("MAX_INBOUND")
        Dim _MAX_EMAIL As String = e.NewValues("MAX_EMAIL")
        Dim _MAX_CHAT As String = e.NewValues("MAX_CHAT")
        Dim _MAX_WHATSAPP As String = e.NewValues("MAX_WHATSAPP")
        Dim _MAX_FACEBOOK As String = e.NewValues("MAX_FACEBOOK")
        Dim _MAX_TWITTER As String = e.NewValues("MAX_TWITTER")
        Dim _MAX_INSTAGRAM As String = e.NewValues("MAX_INSTAGRAM")

        If ValidasilicenseUser() = 1 Then
            Response.Redirect("RedirectAccess.aspx")
        Else
            If (ValidasiInputObject(_UserName) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("UserName is empty")
            End If
            If (ValidasiInputObject(_Password) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("Password is empty")
            End If
            If (ValidasiInputObject(_Name) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("Name is empty")
            End If
            If (ValidasiInputObject(_EmailAddress) = True) Then
                If IsValidEmailFormat(_EmailAddress) = True Then
                Else
                    e.Cancel = True
                    Throw New Exception("Format email address is not valid")
                End If
            Else
                e.Cancel = True
                Throw New Exception("Email Address is empty")
            End If
            If (ValidasiInputObject(_LevelUser) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("Level User is empty")
            End If

            If (ValidasiInputObject(_Organization) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("Organization is empty")
            End If


            If (ValidasiInputObject(_StatuNotif) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("Status Notif is empty")
            End If
            If (ValidasiInputObject(_KirimEmailNotif) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("Kirim Email is empty")
            End If
            If (ValidasiInputObject(_StatusUser) = True) Then
            Else
                e.Cancel = True
                Throw New Exception("Status User is empty")
            End If
            If Session("LoginTypeAngka") = "10" Then
                If _LevelUser = "Administrator" Or _LevelUser = "Account User Admin" Then
                    Throw New Exception("Insert Level User " & _LevelUser & " Access Denied")
                Else

                    _strloq = "insert into msUser (USERNAME, PASSWORD, NAME, LEVELUSER, ORGANIZATION, EMAIL_ADDRESS, USERCREATE, KIRIMEMAIL, NA) " & _
                              "values('" & _UserName & "', '" & _Password & "', '" & _Name & "', '" & _LevelUser & "', '" & _Organization & "', '" & _EmailAddress & "', '" & Session("username") & "', '" & _KirimEmailNotif & "', '" & _StatuNotif & "')"
                    If (ValidasiExistingUser(_UserName)) = False Then
                        If IsValidPasswordFormat(_Password) = True Then
                            If IsValidEmailFormat(_EmailAddress) = True Then
                                Dim str As String = "insert into msUser (USERNAME, PASSWORD, NAME, LEVELUSER, ORGANIZATION, EMAIL_ADDRESS, USERCREATE, KIRIMEMAIL, NA,INBOUND,EMAIL,CHAT,WHATSAPP,FACEBOOK,TWITTER,INSTAGRAM,MAX_INBOUND,MAX_EMAIL,MAX_CHAT,MAX_WHATSAPP,MAX_FACEBOOK,MAX_TWITTER,MAX_INSTAGRAM) values(@USERNAME, @PASSWORD, @NAME, @LEVELUSER, @ORGANIZATION, @EMAIL_ADDRESS, '" & Session("username") & "', @KIRIMEMAIL, @NA,@INBOUND,@EMAIL,@CHAT,@WHATSAPP,@FACEBOOK,@TWITTER,@INSTAGRAM,@MAX_INBOUND,@MAX_EMAIL,@MAX_CHAT,@MAX_WHATSAPP,@MAX_FACEBOOK,@MAX_TWITTER,@MAX_INSTAGRAM)"
                                Try
                                    dsUser.InsertCommand = str
                                    _InsertActivity.InsertActivityManagementUser(_Action, _UserName, _UserName, _Name, _Password, _LevelUser, _Organization, _EmailAddress, _KirimEmailNotif, _StatuNotif, _StatusUser, _UserCreate)
                                Catch ex As Exception
                                    _ClasLoq.LogError(_strTime, ex, str)
                                End Try
                            Else
                                e.Cancel = True
                                Throw New Exception(_UserName & " Format email is not valid")
                            End If
                        Else
                            e.Cancel = True
                            Throw New Exception(_UserName & " Format password is not valid (Format password 8 Karakter terdiri dari huruf besar, huruf kecil, angka dan special characters)")
                        End If
                    Else
                        _ClasLoq.LogSuccess(_strTime, _strloq)
                        e.Cancel = True
                        Throw New Exception(_UserName & " Username already exits")
                    End If
                End If
            Else
                _strloq = "insert into msUser (USERNAME, PASSWORD, NAME, LEVELUSER,  ORGANIZATION, EMAIL_ADDRESS, USERCREATE, KIRIMEMAIL, NA) " &
                          "values('" & _UserName & "', '" & _Password & "', '" & _Name & "', '" & _LevelUser & "', '" & _Organization & "', '" & _EmailAddress & "', '" & Session("username") & "', '" & _KirimEmailNotif & "', '" & _StatuNotif & "')"
                If (ValidasiExistingUser(_UserName)) = False Then
                    If IsValidPasswordFormat(_Password) = True Then
                        If IsValidEmailFormat(_EmailAddress) = True Then
                            Dim str As String = "insert into msUser (USERNAME, PASSWORD, NAME, LEVELUSER, ORGANIZATION, EMAIL_ADDRESS, USERCREATE, KIRIMEMAIL, NA,INBOUND,EMAIL,CHAT,WHATSAPP,FACEBOOK,TWITTER,INSTAGRAM,MAX_INBOUND,MAX_EMAIL,MAX_CHAT,MAX_WHATSAPP,MAX_FACEBOOK,MAX_TWITTER,MAX_INSTAGRAM) values(@USERNAME, @PASSWORD, @NAME, @LEVELUSER, @ORGANIZATION, @EMAIL_ADDRESS, '" & Session("username") & "', @KIRIMEMAIL, @NA,@INBOUND,@EMAIL,@CHAT,@WHATSAPP,@FACEBOOK,@TWITTER,@INSTAGRAM,@MAX_INBOUND,@MAX_EMAIL,@MAX_CHAT,@MAX_WHATSAPP,@MAX_FACEBOOK,@MAX_TWITTER,@MAX_INSTAGRAM)"
                            dsUser.InsertCommand = str
                            _ClasLoq.LogSuccess(_strTime, _strloq)
                            _InsertActivity.InsertActivityManagementUser(_Action, _UserName, _UserName, _Name, _Password, _LevelUser, _Organization, _EmailAddress, _KirimEmailNotif, _StatuNotif, _StatusUser, _UserCreate)

                        Else
                            e.Cancel = True
                            Throw New Exception(_UserName & " Format email is not valid")
                        End If
                    Else
                        e.Cancel = True
                        Throw New Exception(_UserName & " Format password is not valid (Format password 8 Karakter terdiri dari huruf besar, huruf kecil, angka dan special characters)")
                    End If
                Else
                    _ClasLoq.LogSuccess(_strTime, _strloq)
                    e.Cancel = True
                    Throw New Exception(_UserName & " Username already exits")
                End If
            End If
        End If
    End Sub
    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim _UserNameExits As String = e.OldValues("USERNAME")
        Dim _UserName As String = e.NewValues("USERNAME")
        Dim _Password As String = e.NewValues("PASSWORD")
        Dim _Name As String = e.NewValues("NAME")
        Dim _LevelUser As String = e.NewValues("LEVELUSER")
        Dim _Organization As String = e.NewValues("ORGANIZATION")
        Dim _EmailAddress As String = e.NewValues("EMAIL_ADDRESS")
        Dim _EmailAddressExits As String = e.OldValues("EMAIL_ADDRESS")
        Dim _KirimEmailNotif As String = e.NewValues("KIRIMEMAIL")
        Dim _StatuNotif As String = e.NewValues("NA")
        Dim _UserID As String = e.Keys("USERID")
        Dim _StatusUser As String = e.NewValues("STATUS_USER")
        Dim _strloq As String = String.Empty
        Dim _LevelUserSelect As String = e.OldValues("LEVELUSER")
        Dim _Action As String = "Edit"
        Dim _UserCreate As String = Session("UserName")

        If (_UserName = _UserNameExits = True) Then
        Else
            If (ValidasiInputObjectColumn(_Password) = True) Then
                e.Cancel = True
                Throw New Exception("UserName And Password cannot be changed")
            Else
                e.Cancel = True
                Throw New Exception("UserName cannot be changed")
            End If
        End If
        If (ValidasiInputObjectColumn(_Password) = True) Then
            e.Cancel = True
            Throw New Exception("Password cannot be changed ")
        Else

        End If
        If (ValidasiInputObject(_EmailAddress) = True) Then
            If IsValidEmailFormat(_EmailAddress) = True Then
            Else
                e.Cancel = True
                Throw New Exception("Format email address is not valid")
            End If
        Else
            e.Cancel = True
            Throw New Exception("Email Address is empty")
        End If
        If Session("LoginTypeAngka") = "10" Then
            If _LevelUserSelect = "Administrator" Or _LevelUserSelect = "Account User Admin" Then
                Throw New Exception("Edit Level User " & _LevelUserSelect & " Access Denied")
            Else

                _strloq = "update msUser set NAME='" & _Name & "', LEVELUSER='" & _LevelUser & "', ORGANIZATION='" & _Organization & "', EMAIL_ADDRESS='" & _EmailAddress & "', USERCREATE='" & Session("username") & "', KIRIMEMAIL='" & _KirimEmailNotif & "', NA='" & _StatuNotif & "', STATUS_USER='" & _StatusUser & "' where USERID='" & _UserID & "'"

                Dim str As String = "update msUser set NAME=@NAME, LEVELUSER=@LEVELUSER, ORGANIZATION=@ORGANIZATION, EMAIL_ADDRESS=@EMAIL_ADDRESS, USERCREATE='" & Session("username") & "', KIRIMEMAIL=@KIRIMEMAIL, NA=@NA, STATUS_USER=@STATUS_USER, INBOUND=@INBOUND, EMAIL=@EMAIL,CHAT=@CHAT,WHATSAPP=@WHATSAPP,FACEBOOK=@FACEBOOK,TWITTER=@TWITTER,INSTAGRAM=@INSTAGRAM,MAX_INBOUND=@MAX_INBOUND, MAX_EMAIL=@MAX_EMAIL,MAX_CHAT=@MAX_CHAT,MAX_WHATSAPP=@MAX_WHATSAPP,MAX_FACEBOOK=@MAX_FACEBOOK,MAX_TWITTER=@MAX_TWITTER,MAX_INSTAGRAM=@MAX_INSTAGRAM where USERID=@USERID"
                If (ValidasiUpdateExistingUser(_UserID, _UserName)) = False Then
                    If _EmailAddress = _EmailAddressExits Then
                        dsUser.UpdateCommand = str
                        _ClasLoq.LogSuccess(_strTime, _strloq)
                        _InsertActivity.UpdateActivityManagementUser(_Action, _UserID, _UserName, _Name, _LevelUser, _Organization, _EmailAddress, _KirimEmailNotif, _StatuNotif, _StatusUser, _UserCreate)

                    Else
                        If (IsValidEmailFormat(_EmailAddress)) = True Then
                            dsUser.UpdateCommand = str
                            _ClasLoq.LogSuccess(_strTime, _strloq)
                            _InsertActivity.UpdateActivityManagementUser(_Action, _UserID, _UserName, _Name, _LevelUser, _Organization, _EmailAddress, _KirimEmailNotif, _StatuNotif, _StatusUser, _UserCreate)

                        Else
                        End If
                    End If
                Else
                    _ClasLoq.LogSuccess(_strTime, _strloq)
                    e.Cancel = True
                    Throw New Exception(_UserName & " Username already exits")
                End If
            End If
        Else
            _strloq = "update msUser set NAME='" & _Name & "', LEVELUSER='" & _LevelUser & "', ORGANIZATION='" & _Organization & "', EMAIL_ADDRESS='" & _EmailAddress & "', USERCREATE='" & Session("username") & "', KIRIMEMAIL='" & _KirimEmailNotif & "', NA='" & _StatuNotif & "', STATUS_USER='" & _StatusUser & "' where USERID='" & _UserID & "'"
            Dim str As String = "update msUser set NAME=@NAME, LEVELUSER=@LEVELUSER, ORGANIZATION=@ORGANIZATION, EMAIL_ADDRESS=@EMAIL_ADDRESS, USERCREATE='" & Session("username") & "', KIRIMEMAIL=@KIRIMEMAIL, NA=@NA, STATUS_USER=@STATUS_USER, INBOUND=@INBOUND, EMAIL=@EMAIL,CHAT=@CHAT,WHATSAPP=@WHATSAPP,FACEBOOK=@FACEBOOK,TWITTER=@TWITTER,INSTAGRAM=@INSTAGRAM,MAX_INBOUND=@MAX_INBOUND, MAX_EMAIL=@MAX_EMAIL,MAX_CHAT=@MAX_CHAT,MAX_WHATSAPP=@MAX_WHATSAPP,MAX_FACEBOOK=@MAX_FACEBOOK,MAX_TWITTER=@MAX_TWITTER,MAX_INSTAGRAM=@MAX_INSTAGRAM where USERID=@USERID"
            If (ValidasiUpdateExistingUser(_UserID, _UserName)) = False Then
                If _EmailAddress = _EmailAddressExits Then
                    dsUser.UpdateCommand = str
                    _ClasLoq.LogSuccess(_strTime, _strloq)
                    _InsertActivity.UpdateActivityManagementUser(_Action, _UserID, _UserName, _Name, _LevelUser, _Organization, _EmailAddress, _KirimEmailNotif, _StatuNotif, _StatusUser, _UserCreate)

                Else
                    If (IsValidEmailFormat(_EmailAddress)) = True Then
                        dsUser.UpdateCommand = str
                        _ClasLoq.LogSuccess(_strTime, _strloq)
                        _InsertActivity.UpdateActivityManagementUser(_Action, _UserID, _UserName, _Name, _LevelUser, _Organization, _EmailAddress, _KirimEmailNotif, _StatuNotif, _StatusUser, _UserCreate)

                    Else
                    End If
                End If
            Else
                _ClasLoq.LogSuccess(_strTime, _strloq)
                e.Cancel = True
                Throw New Exception(_UserName & " Username already exits")
            End If
        End If
    End Sub
    Private Sub updateAlert()
        Try
            upPage = "update user1 set Activity='N'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            upPage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            upPage = "update user2 set Activity='N'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            upPage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Function ValidasiExistingUser(ByVal _Value As String)
        Dim _strUser As String = "select * from msUser where username='" & _Value & "'"
        readUser = Proses.ExecuteReader(_strUser)
        If readUser.HasRows() Then
            readUser.Read()
            Return True
        Else
            Return False
        End If
        readUser.Close()
    End Function
    Function ValidasiUpdateExistingUser(ByVal _UserID As String, ByVal _Value As String)
        Dim _strUser As String = "select * from msUser where username='" & _Value & "'"
        readUser = Proses.ExecuteReader(_strUser)
        If readUser.HasRows() Then
            readUser.Read()
            If _UserID = readUser("USERID").ToString Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        readUser.Close()
    End Function
    Private Sub btn_Export_Click(sender As Object, e As EventArgs) Handles btn_Export.Click
        ASPxGridView1.Columns("PASSWORD").Visible = False
        Dim casses As String = ddList.SelectedValue
        Select Case casses
            Case "xlsx"
                ASPxGridViewExporter1.WriteXlsxToResponse("DataUserApplication_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "xls"
                ASPxGridViewExporter1.WriteXlsToResponse("DataUserApplication_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "rtf"
                ASPxGridViewExporter1.Landscape = True
                ASPxGridViewExporter1.LeftMargin = 35
                ASPxGridViewExporter1.RightMargin = 30
                ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                ASPxGridViewExporter1.MaxColumnWidth = 108
                ASPxGridViewExporter1.WriteRtfToResponse("DataUserApplication_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "pdf"
                ASPxGridViewExporter1.Landscape = True
                ASPxGridViewExporter1.LeftMargin = 35
                ASPxGridViewExporter1.RightMargin = 30
                ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                ASPxGridViewExporter1.MaxColumnWidth = 108
                ASPxGridViewExporter1.WritePdfToResponse("DataUserApplication_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "csv"
                ASPxGridViewExporter1.WriteCsvToResponse("DataUserApplication_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
        End Select
    End Sub
    Function IsValidPasswordFormat(ByVal s As String) As Boolean
        Try
            Return Regex.IsMatch(s, "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}")
        Catch
            Return False
        End Try
        Return True
    End Function
    Function IsValidEmailFormat(ByVal s As String) As Boolean
        Try
            Return Regex.IsMatch(s, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$*([;])*")
        Catch
            Return False
        End Try
        Return True
    End Function
    Function ValidasilicenseUser()
        Dim LicResults As Integer
        Dim TypeApplication As String = "DC"
        Dim _strUser As String = "Exec Temp_Application '" & TypeApplication & "'"
        readUser = Proses.ExecuteReader(_strUser)
        If readUser.HasRows() Then
            readUser.Read()
            LicResults = readUser("Results").ToString()
        Else
        End If
        readUser.Close()
        Return LicResults
    End Function
    Function _GetUserID(ByVal UserName As String)
        Dim _UseridSelected As Integer
        Dim _strUser As String = "Select UserID From Msuser Where UserName='" & UserName & "'"
        readerUser = Proses.ExecuteReader(_strUser)
        If readerUser.HasRows() Then
            readerUser.Read()
            Return readerUser("UserID").ToString()
            _ClasLoq.LogSuccess(_strTime, _strUser)
            _ClasLoq.LogSuccess(_strTime, _UseridSelected)
        Else
            Return "-"
            _ClasLoq.LogSuccess(_strTime, _strUser)
            _ClasLoq.LogSuccess(_strTime, _UseridSelected)
        End If
        readerUser.Close()
        'Dim _strUser As String
        '_strUser = "Select UserID From Msuser Where UserName='" & UserName & "'"
        'readUser = strExecute.ExecuteReader(strSql)
        'Try
        '    If sqldr.HasRows() Then
        '        sqldr.Read()
        '        Return sqldr("CustomerID").ToString
        '    Else
        '        Return ""
        '    End If
        '    sqldr.Close()
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
    End Function
    Function ValidasiInputObject(ByVal _FieldName As String)
        If IsNothing(_FieldName) Then
            Return False
        Else
            Return True
        End If
    End Function
    Function ValidasiInputObjectColumn(ByVal _FieldName As String)
        If IsNothing(_FieldName) Then
            Return False
        Else
            Return True
        End If
    End Function
End Class