﻿Imports DevExpress.Web.Data
Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports DevExpress.Web
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxHtmlEditor
Imports DevExpress.Web.ASPxEditors
Public Class m_reason_email
    Inherits System.Web.UI.Page

    Dim proses As New ClsConn
    Dim _upage As String
    Dim Sqldr As SqlDataReader
    Dim _strSql As String
    Dim _strScript As String = String.Empty
    Dim _strlogTime As String = DateTime.Now.ToString("yyyy")
    Dim _Number As String = String.Empty
    Dim _LastNumber As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _updatePage()
        ds_Data.UpdateParameters("Username").DefaultValue = Session("username")
    End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub ASPxGridView1_RowInserting(sender As Object, e As ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        _strSql = "select SUBSTRING(ReasonCodeID,0,6) as _Number from Temp_EmailReasonCode where ReasonCodeID is not null order by id desc"
        Try
            Sqldr = proses.ExecuteReader(_strSql)
            If Sqldr.HasRows Then
                Sqldr.Read()
                _Number = Sqldr("_Number").ToString
            Else
            End If
            Sqldr.Close()
            proses.LogSuccess(_strlogTime, " _Number Temp_EmailReasonCode - " & _strSql)
        Catch ex As Exception
            proses.LogError(_strlogTime, ex, " _Number Temp_EmailReasonCode - " & _strSql)
            Response.Write(ex.Message)
        End Try

        If _Number = "" Then
            _strScript = "select  _LastNumber = Right(100000 + COUNT(ID) + 0, 5)  from Temp_EmailReasonCode where ReasonCodeID is not null "
        Else
            _strScript = "select  _LastNumber = Right(10000" & _Number & " + 1 + 0, 5)  from Temp_EmailReasonCode where ReasonCodeID is not null"
        End If
        Try
            Sqldr = proses.ExecuteReader(_strScript)
            If Sqldr.HasRows Then
                Sqldr.Read()
                _LastNumber = Sqldr("_LastNumber").ToString
            Else
            End If
            Sqldr.Close()
            proses.LogSuccess(_strlogTime, "_LastNumber Temp_EmailReasonCode - " & _strScript)
        Catch ex As Exception
            proses.LogError(_strlogTime, ex, "_LastNumber Temp_EmailReasonCode - " & _strScript)
            Response.Write(ex.Message)
        End Try

        Dim grid As ASPxGridView = TryCast(sender, ASPxGridView)
        Dim htmlEditor As ASPxHtmlEditor = TryCast(ASPxGridView1.FindEditFormTemplateControl("html_editor_Body"), ASPxHtmlEditor)
        e.NewValues("BodyEmail") = htmlEditor.Html
        Dim cmbStatus As ASPxComboBox = TryCast(ASPxGridView1.FindEditFormTemplateControl("cmbStatus"), ASPxComboBox)
        e.NewValues("NA") = cmbStatus.Value
        Dim txtSubjectNya As ASPxTextBox = TryCast(ASPxGridView1.FindEditFormTemplateControl("txtSubject"), ASPxTextBox)
        e.NewValues("SubjectEmail") = txtSubjectNya.Text

        Try
            _strScript = "insert into Temp_EmailReasonCode (ReasonCodeID, SubjectEmail, BodyEmail, UserCreate) values('" & _LastNumber & "', '" & txtSubjectNya.Text & "', '" & htmlEditor.Html & "', '" & Session("UserName") & "')"
            ds_Data.InsertCommand = _strScript
            proses.LogSuccess(_strlogTime, "insert Temp_EmailReasonCode - " & _strScript)
        Catch ex As Exception
            proses.LogError(_strlogTime, ex, "insert Temp_EmailReasonCode - " & _strScript)
            Response.Write(ex.Message)
        End Try     
    End Sub
    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim grid As ASPxGridView = TryCast(sender, ASPxGridView)
        Dim htmlEditor As ASPxHtmlEditor = TryCast(ASPxGridView1.FindEditFormTemplateControl("html_editor_Body"), ASPxHtmlEditor)
        e.NewValues("BodyEmail") = htmlEditor.Html
        Dim cmbStatus As ASPxComboBox = TryCast(ASPxGridView1.FindEditFormTemplateControl("cmbStatus"), ASPxComboBox)
        e.NewValues("NA") = cmbStatus.Value
        Dim txtSubjectNya As ASPxTextBox = TryCast(ASPxGridView1.FindEditFormTemplateControl("txtSubject"), ASPxTextBox)
        e.NewValues("SubjectEmail") = txtSubjectNya.Text
    End Sub
End Class