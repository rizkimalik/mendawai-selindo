﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Public Class mt_user
    Inherits System.Web.UI.Page

    Dim ceknull As String
    Dim sqlcon, con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As SqlCommand
    Dim sqldr As SqlDataReader
    Dim Proses As New ClsConn
    Dim upPage As String = String.Empty
    Dim strSql As String = String.Empty
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ds_user.SelectCommand = "SELECT * FROM MTROLEUser order by NumberNya Asc"
        updateAlert()
        sql_sub_menu.DeleteCommand = "delete from user4 where SUBMENUID=@SubMenuID and LEVELUSERID='" & Session("ROLEID") & "'"
    End Sub
    Protected Sub onemenu_select(ByVal sender As Object, ByVal e As EventArgs)
        Session("ROLEID") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        ds_one_menu.SelectCommand = "select DISTINCT USER4.MenuID, USER1.MenuName from user4 LEFT OUTER JOIN User1 ON.USER4.MenuID = USER1.MenuID where USER4.leveluserid='" & Session("ROLEID") & "'"
        Try
            strSql = "select * from MTROLEUser where ROLE_ID='" & Session("ROLEID") & "'"
            sqldr = Proses.ExecuteReader(strSql)
            If sqldr.HasRows Then
                sqldr.Read()
                Session("Role_user") = sqldr("Role_user").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        sql_sub_menu.InsertCommand = "INSERT INTO USER4 (USERID, MENUID, SUBMENUID, LEVELUSERID, USERCREATE) VALUES ('" & Session("Role_user") & "','" & Session("MenuID") & "', @SubMenuName, '" & Session("ROLEID") & "','" & Session("username") & "')"
    End Sub
    Protected Sub GridList_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("MenuID") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_subMenu_dr.SelectCommand = "SELECT * FROM USER2 WHERE MENUID='" & Session("MenuID") & "'"
        sql_sub_menu.SelectCommand = "select DISTINCT user2.MenuID, USER4.SubMenuID, USER2.SubMenuName from user4 LEFT OUTER JOIN User2 ON.USER4.SubMenuID = USER2.SubMenuID where USER4.LEVELUSERID='" & Session("ROLEID") & "' And USER2.MenuID='" & Session("MenuID") & "'"
    End Sub
    Protected Sub gv_menu_tree_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("SubMenuID") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_menu_tree.SelectCommand = "select DISTINCT USER3.SubMenuIDTree, USER3.MenuTreeName from user4 LEFT OUTER JOIN USER3 ON.USER4.MenuIDTree = USER3.SubMenuIDTree where USER3.SubMenuID='" & Session("SubMenuID") & "'"
        ds_user_3.SelectCommand = "Select * from user3 where SubMenuID='" & Session("SubMenuID") & "'"

        Dim LevelUserID As String = String.Empty
        Try
            strSql = "select LevelUserID from user4 where UserID='" & Session("username") & "'"
            sqldr = Proses.ExecuteReader(strSql)
            If sqldr.HasRows Then
                sqldr.Read()
                LevelUserID = sqldr("LevelUserID").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        sql_menu_tree.InsertCommand = "INSERT INTO USER4 (USERID, MENUID, SUBMENUID, MENUIDTREE, LEVELUSERID) VALUES ('" & Session("username") & "','" & Session("MenuID") & "', '" & Session("SubMenuID") & "' , @MenuTreeName,'" & LevelUserID & "')"
    End Sub
    Private Sub updateAlert()
        Try
            upPage = "update user1 set Activity='N'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            upPage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            upPage = "update user2 set Activity='N'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            upPage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            Proses.ExecuteNonQuery(upPage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ds_one_menu_Load(sender As Object, e As EventArgs) Handles ds_one_menu.Load
        ds_one_menu.InsertCommand = "INSERT INTO USER4 (USERID, MENUID, LEVELUSERID, USERCREATE) VALUES ('" & Session("Role_user") & "', @MenuName , '" & Session("ROLEID") & "','" & Session("username") & "')"
        ds_one_menu.DeleteCommand = "DELETE FROM USER4 WHERE MENUID=@MenuID AND LEVELUSERID='" & Session("ROLEID") & "'"
    End Sub

End Class