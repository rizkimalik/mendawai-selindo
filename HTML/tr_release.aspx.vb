﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class tr_release
    Inherits System.Web.UI.Page

    Dim Proses, strExecute As New ClsConn
    Dim sqldr As SqlDataReader
    Dim Data, strSql As String
    Dim _upage As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ds_Query.SelectCommand = "exec Tr_Release_Data '" & Session("LoginTypeAngka") & "'"
        EscalationLayer()
        TrxUserName.Value = Session("username")
        _updatePage()
    End Sub
    Private Sub EscalationLayer()
        ltrEscalationLayer.Text &= ""
        strSql = "exec Temp_SP_Workflow '" & Session("LoginTypeAngka") & "'"
        Try
            sqldr = Proses.ExecuteReader(strSql)
            While sqldr.Read()
                Data &= " <option value='" & sqldr("LayerUser").ToString & "' >Layer " & sqldr("LayerUser").ToString & "</option>"
            End While
            sqldr.Close()
            ltrEscalationLayer.Text = Data
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ds_Query_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles ds_Query.Selecting
        e.Command.CommandTimeout = 480
    End Sub
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        ds_Query.SelectCommand = "exec Tr_Release_Data '" & Session("LoginTypeAngka") & "'"
    End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class