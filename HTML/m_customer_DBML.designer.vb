﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Data.Linq
Imports System.Data.Linq.Mapping
Imports System.Linq
Imports System.Linq.Expressions
Imports System.Reflection


<Global.System.Data.Linq.Mapping.DatabaseAttribute(Name:="2020HelpdeskShopee")>  _
Partial Public Class m_customer_DBMLDataContext
	Inherits System.Data.Linq.DataContext
	
	Private Shared mappingSource As System.Data.Linq.Mapping.MappingSource = New AttributeMappingSource()
	
  #Region "Extensibility Method Definitions"
  Partial Private Sub OnCreated()
  End Sub
  #End Region
	
	Public Sub New()
		MyBase.New(Global.System.Configuration.ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As String)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As System.Data.IDbConnection)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As String, ByVal mappingSource As System.Data.Linq.Mapping.MappingSource)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As System.Data.IDbConnection, ByVal mappingSource As System.Data.Linq.Mapping.MappingSource)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	<Global.System.Data.Linq.Mapping.FunctionAttribute(Name:="dbo.SP_Temp_SearchingChannel")>  _
	Public Function SP_Temp_SearchingChannel() As ISingleResult(Of SP_Temp_SearchingChannelResult)
		Dim result As IExecuteResult = Me.ExecuteMethodCall(Me, CType(MethodInfo.GetCurrentMethod,MethodInfo))
		Return CType(result.ReturnValue,ISingleResult(Of SP_Temp_SearchingChannelResult))
	End Function
End Class

Partial Public Class SP_Temp_SearchingChannelResult
	
	Private _CustomerID As String
	
	Private _Name As String
	
	Private _ValueChannel As String
	
	Private _FlagChannel As String
	
	Private _CIF As String
	
	Private _NIK As String
	
	Public Sub New()
		MyBase.New
	End Sub
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_CustomerID", DbType:="VarChar(150) NOT NULL", CanBeNull:=false)>  _
	Public Property CustomerID() As String
		Get
			Return Me._CustomerID
		End Get
		Set
			If (String.Equals(Me._CustomerID, value) = false) Then
				Me._CustomerID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Name", DbType:="VarChar(500)")>  _
	Public Property Name() As String
		Get
			Return Me._Name
		End Get
		Set
			If (String.Equals(Me._Name, value) = false) Then
				Me._Name = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ValueChannel", DbType:="VarChar(200)")>  _
	Public Property ValueChannel() As String
		Get
			Return Me._ValueChannel
		End Get
		Set
			If (String.Equals(Me._ValueChannel, value) = false) Then
				Me._ValueChannel = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_FlagChannel", DbType:="VarChar(30)")>  _
	Public Property FlagChannel() As String
		Get
			Return Me._FlagChannel
		End Get
		Set
			If (String.Equals(Me._FlagChannel, value) = false) Then
				Me._FlagChannel = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_CIF", DbType:="VarChar(100)")>  _
	Public Property CIF() As String
		Get
			Return Me._CIF
		End Get
		Set
			If (String.Equals(Me._CIF, value) = false) Then
				Me._CIF = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_NIK", DbType:="VarChar(100)")>  _
	Public Property NIK() As String
		Get
			Return Me._NIK
		End Get
		Set
			If (String.Equals(Me._NIK, value) = false) Then
				Me._NIK = value
			End If
		End Set
	End Property
End Class
