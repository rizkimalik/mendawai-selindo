﻿Public Class tr_thread
    Inherits System.Web.UI.Page

    Dim proses As New ClsConn
    Dim _upage As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginType") = "Supervisor" Or Session("LoginType") = "Administrator" Then
            Try
                Dim strSql As New T_Thread_DBMLDataContext
                strSql.CommandTimeout = 480
                ASPxGridView1.DataSource = strSql.TR_DataThread("" & Session("LoginType") & "", "" & Session("UnitKerja") & "").ToList()
                ASPxGridView1.DataBind()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        Else
            Try
                Dim strSql As New T_Thread_DBMLDataContext
                strSql.CommandTimeout = 480
                ASPxGridView1.DataSource = strSql.TR_DataThread("" & Session("LoginType") & "", "" & Session("username") & "").ToList()
                ASPxGridView1.DataBind()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
        _updatePage()
        TrxUsername.Value = Session("username")
    End Sub
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        If Session("LoginType") = "Supervisor" Or Session("LoginType") = "Administrator" Then
            Try
                Dim strSql As New T_Thread_DBMLDataContext
                strSql.CommandTimeout = 480
                ASPxGridView1.DataSource = strSql.TR_DataThread("" & Session("LoginType") & "", "" & Session("UnitKerja") & "").ToList()
                ASPxGridView1.DataBind()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        Else
            Try
                Dim strSql As New T_Thread_DBMLDataContext
                strSql.CommandTimeout = 480
                ASPxGridView1.DataSource = strSql.TR_DataThread("" & Session("LoginType") & "", "" & Session("username") & "").ToList()
                ASPxGridView1.DataBind()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
    End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class