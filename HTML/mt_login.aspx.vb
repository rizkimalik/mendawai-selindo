﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class mt_login
    Inherits System.Web.UI.Page

    Dim strSql As String = String.Empty
    Dim sqlDr As SqlDataReader
    Dim Proses As New ClsConn
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As New SqlCommand
    Dim execute As New ClsConn
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TrxUserName.Value = Session("UserName")
        CallStatus()
        strSql = "select count(*) as login from msuser where login='1'"
        Try
            sqlDr = Proses.ExecuteReader(strSql)
            If sqlDr.HasRows Then
                sqlDr.Read()
                lblogin.Text = sqlDr("login").ToString
            End If
            sqlDr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        strSql = "select count(*) as notlogin from msuser where login='0'"
        Try
            sqlDr = Proses.ExecuteReader(strSql)
            If sqlDr.HasRows Then
                sqlDr.Read()
                lblnotlogin.Text = sqlDr("notlogin").ToString
            End If
            sqlDr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        'strSql = "select count(*) as aux from msuser where IdAUX<>'9'"
        'Try
        '    sqlDr = Proses.ExecuteReader(strSql)
        '    If sqlDr.HasRows Then
        '        sqlDr.Read()
        '        lbaux.Text = sqlDr("aux").ToString
        '    End If
        '    sqlDr.Close()
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
        updateAlert()
    End Sub
    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnRelease_Click(sender As Object, e As EventArgs)
        Dim USERID As String = Convert.ToInt32((sender).CommandArgument)
        Try
            strSql = "update msUser set login='0', IDAUX='9', DescAUX='READY' where USERID='" & USERID & "'"
            execute.ExecuteNonQuery(strSql)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Response.Redirect("mt_login.aspx")
    End Sub
    Private Sub CallStatus()
        If Request.QueryString("id") = "1" Then
            ds_login.SelectCommand = "select * from msuser where login='1'"
        ElseIf Request.QueryString("id") = "2" Then
            ds_login.SelectCommand = "select * from msuser where login='0'"
        ElseIf Request.QueryString("id") = "3" Then
            ds_login.SelectCommand = "select * from msuser where IDAux<>'9'"
        Else
            ds_login.SelectCommand = "select * from msuser"
        End If
    End Sub
End Class