﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="hiStoryTransaction.aspx.vb" Inherits="ICC.hiStoryTransaction" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="module" src="scripts/history_transaction.js"></script>

    <script type="text/javascript">
        function CloseGridLookup() {
            gridLookup.ConfirmCurrentSelection();
            gridLookup.HideDropDown();
        }
    </script>
    <script>
        function ShowUpdateTransaction(NoTicket) {
            $("#MainContent_hd_ticketid").val(NoTicket);
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/Select_Data_TransactionTicket",
                data: "{ filterData: '" + $("#MainContent_hd_ticketid").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var tblTickets = "";

                    for (i = 0; i < json.length; i++) {


                        if ((json[i].TrxCategoryName) === 'Complaint') {
                            $("#divCategoryType").css("display", "block");
                        } else {
                            $("#divCategoryType").css("display", "none");
                        }

                        var TglKejadian = ("" + json[i].TrxTahun + "-" + json[i].TrxBulan + "-" + json[i].TrxHari + "");
                        var DateKejadian = new Date(TglKejadian);
                        BTN_datePengaduan.SetDate(DateKejadian);
                        //$("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_frameattachment").src = "uploadcontrol.aspx?idticketutama=" + json[i].TrxTicketNumber + "";
                        document.getElementById('MainContent_ASPxPopupControl1_ASPxCallbackPanel1_frameattachment').src = "uploadcontrol.aspx?idticketutama=" + json[i].TrxTicketNumber + "";
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxChannel_I").val(json[i].TrxTicketSourceName);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_cmbPenyebab_I").val(json[i].TrxPenyebab);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtPenerimaPengaduan_I").val(json[i].TrxPenerimaPengaduan);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_cmbStatusPelapor_I").val(json[i].TrxStatusPelapor);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_CmbCountry_I").val(json[i].TrxCategoryName);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox1_I").val(json[i].TrxLevel1Name);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmb_level2_I").val(json[i].TrxLevel2Name);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_CmbCity_I").val(json[i].TrxLevel3Name);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxDescription_I").val(json[i].TrxDetailComplaint);
                        $("#cmbStatusTicket").val(json[i].TrxStatus);
                        $("#hd_sla").val(json[i].TrxSLA);
                        $("#MainContent_hd_ticketid").val(json[i].TrxTicketNumber);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbskalaprioritas_I").val(json[i].TrxSkalaPrioritas);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbjenisnasabah_I").val(json[i].TrxJenisNasabah);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtNomorRekening_I").val(json[i].TrxNomorRekening);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbsumberinformmasi_I").val(json[i].TrxSumberInformasi);
                        $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxChannel_I").val(json[i].TrxTicketSourceName);

                        ASPxPopupControl1.Show();
                        ASPxCallbackPanel4.PerformCallback();
                        GetDetailTransaction(json[i].TrxTicketNumber, json[i].TrxCategoryName);

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }

        function GetDetailTransaction(TrxTicketNumber, TrxCategoryName) {
            var messageDiv = $('#DivLoadSLA');
            var TrxLoginTypeAngka = $("#MainContent_TrxLoginTypeAngka").val()
            $.ajax({
                type: 'GET',
                async: false,
                url: "AjaxPages/level4.aspx?jenis=edit&id=" + TrxTicketNumber + "&Category=" + TrxCategoryName + "&LoginAngka=" + TrxLoginTypeAngka,
                cache: false,
                success: function (result) {
                    //alert("ss" + result)
                    messageDiv.empty();
                    messageDiv.append(result);
                    messageDiv.append("<br />");
                    //messageDiv.append("counter = " + counter);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }
            });
        }
    </script>
    <script type="text/javascript">
        function ASPxGridView1_SelectionChanged(s, e) {
            s.GetSelectedFieldValues("TicketNumber", GetSelectedFieldValuesCallback);
        }
        function GetSelectedFieldValuesCallback(values) {
            $('#MainContent_TrxTicketNumber').val(values);
        }
    </script>
    <script>
        function ShowParentChild(values) {
            $("#MainContent_hd_ticketid").val(values);
            $("#dx_LabelTicketNumber").text(values);
            //ASPxCallbackPanel2.PerformCallback();
            ASPxGridView1.Refresh();
            ASPxPopupControl2.Show();
        }
        function showParent() {
            ASPxGridView3.Refresh();
        }
    </script>
    <script>
        function execParentNumber() {
            var TrxTicketNumberFrom = $("#MainContent_hd_ticketid").val();
            var TrxTicketNumberTo = $("#MainContent_TrxTicketNumber").val();
            var TrxUsername = $("#MainContent_TrxUserName").val();
            var TrxReason = $("#dxmReason_I").val();
            if (TrxTicketNumberTo === '') {
                alert("Please, Select your data transaction")
                return false;
            }
            if (TrxReason === 'Reason Parent Ticket') {
                alert("Reason parent is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/ParentChildNumberID",
                    data: "{ TrxTicketNumberTo: '" + TrxTicketNumberTo + "', TrxUserName: '" + TrxUsername + "', TrxTicketNumberFrom: '" + TrxTicketNumberFrom + "', TrxReason: '" + encodeData(TrxReason) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                $("#MainContent_TrxTicketNumber").val("");
                                $("#dxmReason_I").val("");
                                alert(json[i].msgSystem);
                                //ASPxPopupControl2.Hide();
                                window.location.reload();
                            } else {
                                alert(json[i].msgSystem);
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
        function closeReloadPage() {
            window.location.reload()
        }
    </script>
    <script>
        function ShowUpdateParentID(v) {
            $("#MainContent_TrxTicketNumberUpdate").val(v);
            var TrxUsername = $("#MainContent_TrxUserName").val();
            var TrxTicketNumberUpdate = $("#MainContent_TrxTicketNumberUpdate").val();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/DeleteParentChildNumberID",
                    data: "{ TrxTicketNumber: '" + TrxTicketNumberUpdate + "', TrxUsername: '" + TrxUsername + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem);
                                ASPxPopupControl2.Hide();
                            } else {
                                alert(json[i].msgSystem);
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
    </script>
    <script>
        function encodeData(s) {
            return encodeURIComponent(s).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hd_ticketid" runat="server" />
    <asp:HiddenField ID="TrxLoginTypeAngka" runat="server" />
    <asp:HiddenField ID="TrxUserName" runat="server" />
    <asp:HiddenField ID="TrxTicketNumber" runat="server" />
    <asp:HiddenField ID="TrxTicketNumberUpdate" runat="server" />
    <h4 class="headline">Data History Transaction
			<span class="line bg-warning"></span>
    </h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-tab clearfix">
                <ul class="tab-bar">
                    <li class="active"><a href="#Tab1" data-toggle="tab"><i class="fa fa-file-text"></i>&nbsp;<strong>Data History Transaction</strong></a></li>
                    <li><a href="#Tab2" data-toggle="tab"><i class="fa fa-cogs"></i>&nbsp;<strong>Data Setting History Transaction</strong></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="Tab1" style="margin-left: -14px; margin-bottom: -15px;">
                        <div id="dxDataHistoryTransaction"></div>

                        <%-- <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" Theme="Default" ShowHeader="false">
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server">
                                    <dx:ASPxFilterControl ID="ASPxFilterControl1" runat="server" ClientInstanceName="filter" Width="100%" Theme="Metropolis">
                                        <Columns>
                                            <dx:FilterControlColumn PropertyName="CustomerID" />
                                            <dx:FilterControlColumn PropertyName="Name" />
                                            <dx:FilterControlColumn PropertyName="TicketNumber" />
                                            <dx:FilterControlColumn PropertyName="GenesysNumber" DisplayName="Interaction ID" />
                                            <dx:FilterControlColumn PropertyName="ThreadID" />
                                            <dx:FilterControlColumn PropertyName="NomorRekening" />
                                            <dx:FilterControlColumn PropertyName="HP" />
                                            <dx:FilterControlColumn PropertyName="EMAIL" />
                                            <dx:FilterControlColumn PropertyName="CIF" />
                                            <dx:FilterControlColumn PropertyName="NIK" />
                                            <dx:FilterControlColumn PropertyName="TicketSourceName" DisplayName="Channel" />
                                            <dx:FilterControlColumn PropertyName="AccountInbound" DisplayName="Account" />
                                            <dx:FilterControlColumn PropertyName="SubCategory3Name" DisplayName="Reason" />
                                            <dx:FilterControlColumn PropertyName="Status" />
                                            <dx:FilterControlColumn PropertyName="DetailComplaint" DisplayName="User Issue Remark" />
                                            <dx:FilterControlColumn PropertyName="UserCreate" />
                                            <dx:FilterControlDateColumn PropertyName="DateCreate" ColumnType="DateTime" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd" PropertiesDateEdit-DisplayFormatInEditMode="true" />
                                        </Columns>
                                        <ClientSideEvents Applied="function(s, e) { grid.ApplyFilter(e.filterExpression);}" />
                                    </dx:ASPxFilterControl>
                                    <table style="margin-bottom: -5px;">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton runat="server" ID="btnApply" Text="Submit" AutoPostBack="false" UseSubmitBehavior="false" Width="100px" Style="margin: 12px auto 0; display: block;" Theme="Metropolis" Font-Bold="true">
                                                    <ClientSideEvents Click="function() { filter.Apply(); }" />
                                                </dx:ASPxButton>
                                            </td>
                                            <td></td>
                                            <td>
                                                <dx:ASPxButton runat="server" ID="btnClear" Text="Clear" AutoPostBack="False" Width="100px" UseSubmitBehavior="False" Style="margin: 12px auto 0; display: block;" Theme="Metropolis" Font-Bold="true">
                                                    <ClientSideEvents Click="function() { filter.Clear(); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                        KeyFieldName="TicketNumber" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="15">
                                        <SettingsPager>
                                            <AllButton Text="All">
                                            </AllButton>
                                            <NextPageButton Text="Next &gt;">
                                            </NextPageButton>
                                            <PrevPageButton Text="&lt; Prev">
                                            </PrevPageButton>
                                            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                        </SettingsPager>
                                        <SettingsEditing Mode="Inline" />
                                        <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                                                <DataItemTemplate>
                                                    <a href="#" onclick="ShowUpdateTransaction('<%# Eval("TicketNumber")%>')">
                                                        <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="Image1" runat="server" />
                                                    </a>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="TicketSourceName" Caption="Channel" Width="70" />
                                            <dx:GridViewDataTextColumn FieldName="TicketNumber" Width="110" />
                                            <dx:GridViewDataTextColumn FieldName="CustomerID" Width="110" />
                                            <dx:GridViewDataTextColumn FieldName="Name" Width="200" />
                                            <dx:GridViewDataTextColumn FieldName="GenesysNumber" Caption="Interaction ID" Width="200" />
                                            <dx:GridViewDataTextColumn FieldName="ThreadID" Width="150" />
                                            <dx:GridViewDataTextColumn FieldName="NomorRekening" Width="150" Caption="Account Number" />
                                            <dx:GridViewDataTextColumn FieldName="HP" Width="150" Caption="Phone Number" />
                                            <dx:GridViewDataTextColumn FieldName="EMAIL" Width="150" Caption="Email Address" />
                                            <dx:GridViewDataTextColumn FieldName="CIF" Width="150" />
                                            <dx:GridViewDataTextColumn FieldName="NIK" Width="150" />
                                            <dx:GridViewDataTextColumn FieldName="AccountInbound" Caption="Account" Width="150" />
                                            <dx:GridViewDataTextColumn FieldName="SubCategory3Name" Caption="Reason" Width="300" />
                                            <dx:GridViewDataTextColumn FieldName="DetailComplaint" Caption="User Issue Remark" Width="300" />
                                            <dx:GridViewDataTextColumn FieldName="ResponComplaint" Caption="Agent Response" Width="300" />
                                            <dx:GridViewDataTextColumn FieldName="Status" Width="160" />
                                            <dx:GridViewDataTextColumn Caption="Parent Case Number" FieldName="ParentNumberID" Width="150"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="UserCreate" Width="150" />
                                            <dx:GridViewDataTextColumn FieldName="DateCreate" Width="150" />
                                            <dx:GridViewDataTextColumn FieldName="LastResponseBy" Caption="Last Response By" Width="150" />
                                            <dx:GridViewDataTextColumn FieldName="LastResponseDate" Caption="Last Response Date" Width="150" />
                                            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="24" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                Width="50px" ToolTip="Parent Child Ticket">
                                                <DataItemTemplate>
                                                    <a href="#" id="ParentID" onclick="ShowParentChild('<%# Eval("TicketNumber")%>')">
                                                        <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" />
                                                    </a>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxRoundPanel> --%>
                    </div>
                    <div class="tab-pane fade" id="Tab2" style="margin-left: -14px; margin-bottom: -15px;">
                        <!-- <div id="dxSettingHistoryTransaction"></div> -->

                        <dx:ASPxGridView ID="GridView" ClientInstanceName="GridView" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="ds_query" KeyFieldName="ID"
                            Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                            <SettingsPager>
                                <AllButton Text="All">
                                </AllButton>
                                <NextPageButton Text="Next &gt;">
                                </NextPageButton>
                                <PrevPageButton Text="&lt; Prev">
                                </PrevPageButton>
                                <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false"
                                ShowGroupPanel="false" />
                            <SettingsBehavior ConfirmDelete="true" />
                            <Columns>
                                <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                    ButtonType="Image" FixedStyle="Left" Width="30px">
                                    <EditButton Visible="true">
                                        <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                                    </EditButton>
                                    <NewButton Visible="true">
                                        <Image ToolTip="New" Url="img/Icon/Apps-text-editor-icon2.png" />
                                    </NewButton>
                                    <DeleteButton Visible="false">
                                        <Image ToolTip="Delete" Url="img/Icon/Actions-edit-clear-icon2.png" />
                                    </DeleteButton>
                                    <CancelButton Visible="true">
                                        <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                        </Image>
                                    </CancelButton>
                                    <UpdateButton Visible="true">
                                        <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                    </UpdateButton>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataComboBoxColumn Caption="Day" FieldName="Day" VisibleIndex="2">
                                    <PropertiesComboBox>
                                        <Items>
                                            <dx:ListEditItem Text="5" Value="5" />
                                            <dx:ListEditItem Text="10" Value="10" />
                                            <dx:ListEditItem Text="15" Value="15" />
                                            <dx:ListEditItem Text="20" Value="20" />
                                            <dx:ListEditItem Text="25" Value="25" />
                                            <dx:ListEditItem Text="30" Value="30" />
                                        </Items>
                                    </PropertiesComboBox>
                                </dx:GridViewDataComboBoxColumn>
                                <dx:GridViewDataDateColumn FieldName="FilterDate" Caption="Start Filter Date" VisibleIndex="3"></dx:GridViewDataDateColumn>
                            </Columns>
                            <Settings ShowGroupPanel="True" />
                        </dx:ASPxGridView>

                        
                        <asp:SqlDataSource ID="ds_query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                            DeleteCommand="DELETE FROM [Temp_SettingHiStoryTransaction] WHERE [ID] = @ID"
                            SelectCommand="select * from Temp_SettingHiStoryTransaction where CreatedBy=@username and Type='History'"
                            UpdateCommand="UPDATE [Temp_SettingHiStoryTransaction] SET [Day] = @Day, [FilterDate] = @FilterDate, Type='History', [CreatedBy]=@username WHERE [ID] = @ID">
                            <SelectParameters>
                                <asp:Parameter Name="username" Type="String" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:Parameter Name="ID" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Day" Type="String" />
                                <asp:Parameter Name="FilterDate" Type="String" />
                                <asp:Parameter Name="username" Type="String" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Day" Type="String" />
                                <asp:Parameter Name="FilterDate" Type="String" />
                                <asp:Parameter Name="username" Type="String" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="600px" AllowResize="true" Width="1090px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="None"
        ShowFooter="True" HeaderText="Form History Transaction" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="1105px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="panel-tab clearfix">
                                <ul class="tab-bar">
                                    <li class="active"><a href="#dataTicket" data-toggle="tab"><i class="fa fa-file-text"></i>&nbsp;<strong>Data Transaction Ticket</strong></a></li>
                                    <li><a href="#dataInteraction" data-toggle="tab"><i class="fa fa-group"></i>&nbsp;<strong>Data Interaction Ticket</strong></a></li>
                                    <li><a href="#dataattchment" data-toggle="tab"><i class="fa fa-hdd-o"></i>&nbsp;<strong>Data Attachment Ticket</strong></a></li>
                                </ul>
                            </div>
                            <div class="panel-body" style="margin-left: -10px;">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="dataTicket">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Date Created</label>
                                                            <dx:ASPxDateEdit AutoPostBack="false" ID="BTN_datePengaduan" ClientInstanceName="BTN_datePengaduan" runat="server" Cursor="not-allowed"
                                                                CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd" DisplayFormatString="yyyy-MM-dd" ReadOnly="true">
                                                            </dx:ASPxDateEdit>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6"></div>
                                                    <div class="col-md-3">
                                                        <label>Channel</label>
                                                        <dx:ASPxComboBox ID="ASPxChannel" ClientInstanceName="ASPxChannel" runat="server" Theme="Metropolis" Height="30px"
                                                            ItemStyle-HoverStyle-BackColor="#0076c4" AutoPostBack="false" Enabled="false" Cursor="not-allowed"
                                                            Width="100%">
                                                            <ItemStyle>
                                                                <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                                                </HoverStyle>
                                                            </ItemStyle>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Category</label>
                                                            <dx:ASPxComboBox runat="server" ID="CmbCountry" DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith" Theme="Metropolis" Height="30px"
                                                                DataSourceID="sql_country" TextField="Name" ReadOnly="true" Enabled="false" ValueField="CategoryID"
                                                                EnableSynchronization="False" Width="100%" Cursor="not-allowed">
                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sql_transaction_type" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                                                            <asp:SqlDataSource ID="sql_country" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                                SelectCommand="select * from mCategory"></asp:SqlDataSource>
                                                            <asp:SqlDataSource ID="sql_city" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                                SelectCommand="select SubCategory3ID, SubName, SLA from mSubCategoryLv3 WHERE CategoryID =@CategoryID">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="CategoryID" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Enquiry Type</label>
                                                            <dx:ASPxComboBox runat="server" ID="ASPxComboBox1" ClientInstanceName="ASPxComboBox1" ReadOnly="false" Theme="Metropolis" Height="30px"
                                                                DropDownStyle="DropDown" DataSourceID="sql_ASPxComboBox1" TextField="SubName" Width="100%" Enabled="false"
                                                                ValueField="SubCategory2ID" IncrementalFilteringMode="StartsWith" EnableSynchronization="False" Cursor="not-allowed">
                                                                <ClientSideEvents EndCallback="OnEndCallbackTiga" />
                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {popupsla(s.GetSelectedItem().texts[0]);}" />
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sql_ASPxComboBox1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mSubCategoryLv2 WHERE SubCategory2ID=@SubCategory2ID">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="SubCategory2ID" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Enquiry Detail</label>
                                                            <dx:ASPxComboBox runat="server" ID="cmb_level2" ClientInstanceName="cmb_level2" ReadOnly="false" Theme="Metropolis" Height="30px"
                                                                DropDownStyle="DropDown" DataSourceID="SqlDataSource6" TextField="SubName" Width="100%" Enabled="false"
                                                                ValueField="SubCategory2ID" IncrementalFilteringMode="StartsWith" EnableSynchronization="False" Cursor="not-allowed">
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mSubCategoryLv2 WHERE SubCategory2ID=@SubCategory2ID">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="SubCategory2ID" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Reason</label>
                                                            <dx:ASPxComboBox runat="server" ID="CmbCity" ClientInstanceName="cmbCity" Theme="Metropolis" Height="30px"
                                                                DropDownStyle="DropDown" DataSourceID="sql_city" TextField="SubName" Width="100%" Enabled="false"
                                                                ValueField="SubCategory3ID" IncrementalFilteringMode="StartsWith" EnableSynchronization="False" Cursor="not-allowed">
                                                                <ClientSideEvents EndCallback="OnEndCallback" />
                                                                <%--<ClientSideEvents SelectedIndexChanged="function(s, e) {OnSlaChanged(s.GetSelectedItem().texts);}" />--%>
                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {OnSlaChanged(s.GetSelectedItem().value);}" />
                                                            </dx:ASPxComboBox>
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                
                                                <div class="row">
                                                    <div id="DivLoadSLA" name="DivLoadSLA"></div>
                                                    <div id="Div1" style="height: 0px; visibility: hidden" runat="server" visible="false">
                                                        <div class="col-md-3 col-sm-3">
                                                            <div class="form-group">
                                                                <label>Ticket Status</label>

                                                                <dx:ASPxComboBox ID="cmb_status" ClientInstanceName="cmb_status" runat="server" TextField="status" CssClass="form-control input-sm"
                                                                    ValueField="status" ItemStyle-HoverStyle-BackColor="#F37021" DataSourceID="sql_cmb_status" AutoPostBack="false" ReadOnly="false"
                                                                    Width="100%" IncrementalFilteringMode="Contains">

                                                                <%--<dx:ASPxComboBox ID="cmb_status" ClientInstanceName="cmb_status" runat="server" TextField="status" Theme="Metropolis"
                                                                    ValueField="status" ItemStyle-HoverStyle-BackColor="#F37021" DataSourceID="sql_cmb_status"
                                                                    Width="100%" IncrementalFilteringMode="Contains">--%>
                                                                    <%--<Columns>
                                                                        <dx:ListBoxColumn Caption="Status" FieldName="status" />
                                                                    </Columns>--%>
                                                                    <ItemStyle>
                                                                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                                                        </HoverStyle>
                                                                    </ItemStyle>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sql_cmb_status" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mstatus order by Urutan ASC"></asp:SqlDataSource>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label>Unit</label>
                                                            <asp:TextBox CssClass="form-control" ID="txt_channel" runat="server" Enabled="false"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label>Enginer</label>
                                                            <asp:TextBox CssClass="form-control" ID="txt_update_enginer" runat="server" Enabled="false">                                      
                                                            </asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label>SLA</label>
                                                            <asp:TextBox CssClass="form-control" ID="txt_sla" runat="server" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <dx:ASPxMemo ID="ASPxDescription" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="Detail Complaint" ReadOnly="true" Enabled="false" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <dx:ASPxMemo ID="ASPxResponse" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="Agent Response" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#OptionalDataUpdate">
                                                                Optional Data
                                                                <span class="badge badge-danger pull-right"><i class="fa fa-plus"></i></span>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="OptionalDataUpdate" class="panel-collapse collapse" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="form-group">
                                                                        <label>Priority Scale</label>
                                                                        <dx:ASPxComboBox ID="cmbskalaprioritas" ClientInstanceName="cmbskalaprioritas" runat="server"           CssClass="form-control input-sm"
                                                                            ItemStyle-HoverStyle-BackColor="#EE4D2D" AutoPostBack="false" Theme="Default"
                                                                            Width="100%" ReadOnly="true">
                                                                            <ItemStyle>
                                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                                </HoverStyle>
                                                                            </ItemStyle>
                                                                        </dx:ASPxComboBox>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="form-group">
                                                                        <label>Source of Information / Channel</label>
                                                                        <dx:ASPxComboBox ID="cmbsumberinformmasi" ClientInstanceName="cmbsumberinformmasi" runat="server"
                                                                            CssClass="form-control input-sm" ValueField="SumberInformasi" ItemStyle-HoverStyle-BackColor="#EE4D2D" AutoPostBack="false" Width="100%" ReadOnly="true">
                                                                            <ItemStyle>
                                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                                </HoverStyle>
                                                                            </ItemStyle>
                                                                        </dx:ASPxComboBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="dataInteraction">
                                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel4" runat="server" Width="1030px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel4">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="ASPxGridView4" ClientInstanceName="ASPxGridView4" runat="server" KeyFieldName="ID"
                                                        DataSourceID="dsInteraction" Width="1095px" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                        SettingsPager-PageSize="15">
                                                        <SettingsPager>
                                                            <AllButton Text="All">
                                                            </AllButton>
                                                            <NextPageButton Text="Next &gt;">
                                                            </NextPageButton>
                                                            <PrevPageButton Text="&lt; Prev">
                                                            </PrevPageButton>
                                                            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                        </SettingsPager>
                                                        <SettingsEditing Mode="Inline" />
                                                        <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="false"
                                                            ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                                        <SettingsBehavior ConfirmDelete="true" />
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                                                                PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Width="200px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Width="200px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Response" FieldName="ResponseComplaint" Width="300px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Dispatch To" FieldName="DispatchTicket" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Dispatch To Layer" FieldName="DispatchToLayer" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Dispatch To Dept" FieldName="ORGANIZATION_NAME" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Created by" FieldName="AgentCreate" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Date Create" FieldName="DateCreate" Width="130px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" Width="160"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Type" FieldName="InteractionType" Width="70px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Document" VisibleIndex="11" CellStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center" Width="70px">
                                                                <DataItemTemplate>
                                                                    <a target="_blank" href="uConnector/uConnector_Files/HTML/<%# Eval("GenesysID")%>/<%# Eval("GenesysID")%>.HTML">
                                                                        <%# Eval("GenesysID")%>
                                                                    </a>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsInteraction" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxCallbackPanel>
                                    </div>
                                    <div class="tab-pane fade" id="dataattchment" style="height: 400px; overflow: hidden;">
                                        <iframe id="frameattachment" style="width: 100%; height: 400px; border: none; overflow: hidden;" runat="server"></iframe>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="660px" AllowResize="true" Width="1050px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="Vertical"
        ShowFooter="True" HeaderText="Form Parent Child Ticket" FooterText="" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s, e) { closeReloadPage(); }">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel2" runat="server" Width="1000px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel2">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="panel-tab clearfix">
                                <ul class="tab-bar">
                                    <li class="active">
                                        <a href="#Tab11" data-toggle="tab"><i class="fa fa-file-text"></i>&nbsp;<strong>Data Transaction Ticket</strong>
                                        <span class="badge badge-warning" style="background-color: #FF8800;">
                                            <dx:ASPxLabel ID="dx_LabelTicketNumber" runat="server" Theme="Metropolis" Font-Size="10px" ForeColor="White" ClientIDMode="Static"></dx:ASPxLabel>
                                        </span>
                                    </a>
                                    </li>
                                    <li><a href="#Tab12" data-toggle="tab"><i class="fa fa-cogs"></i>&nbsp;<strong>Data Setting Query Ticket</strong></a></li>
                                    <li><a href="#Tab13" onclick="showParent()" data-toggle="tab"><i class="fa fa-sitemap"></i>&nbsp;<strong>Data Parent Child Ticket</strong></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="Tab11">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridLookup ID="ASPxGridLookup1" runat="server" SelectionMode="Multiple" ClientInstanceName="gridLookup" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                    KeyFieldName="TicketNumber" Width="100%" TextFormatString="{0}" MultiTextSeparator="," Theme="Metropolis" Height="30px" Visible="false">
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                        <dx:GridViewDataColumn FieldName="TicketNumber" Settings-AutoFilterCondition="Contains" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="X-Small" CellStyle-Font-Size="X-Small" />
                                                        <dx:GridViewDataColumn FieldName="TicketSourceName" Caption="Channel" Width="150" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="X-Small" CellStyle-Font-Size="X-Small" />
                                                        <dx:GridViewDataColumn FieldName="GenesysNumber" Caption="Interaction ID" Width="200" Settings-AutoFilterCondition="Contains" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="X-Small" CellStyle-Font-Size="X-Small" />
                                                        <dx:GridViewDataColumn FieldName="ThreadID" Width="150" Settings-AutoFilterCondition="Contains" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="X-Small" CellStyle-Font-Size="X-Small" />
                                                        <dx:GridViewDataColumn FieldName="CustomerID" Width="150" Settings-AutoFilterCondition="Contains" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="X-Small" CellStyle-Font-Size="X-Small" />
                                                        <dx:GridViewDataColumn FieldName="Name" Width="200px" Settings-AutoFilterCondition="Contains" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="X-Small" CellStyle-Font-Size="X-Small" />
                                                        <dx:GridViewDataColumn FieldName="NomorRekening" Width="150" Caption="Account Number" Settings-AutoFilterCondition="Contains" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="X-Small" CellStyle-Font-Size="X-Small" />
                                                    </Columns>
                                                    <GridViewProperties>
                                                        <Templates>
                                                            <StatusBar>
                                                                <table class="OptionsTable" style="float: right">
                                                                    <tr>
                                                                        <td onclick="return _aspxCancelBubble(event)">
                                                                            <dx:ASPxButton ID="Close" runat="server" AutoPostBack="false" Text="Close" Theme="Metropolis" ClientSideEvents-Click="CloseGridLookup" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </StatusBar>
                                                        </Templates>
                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                                <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                    KeyFieldName="TicketNumber" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="10">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <SettingsBehavior EnableRowHotTrack="true" AllowSelectByRowClick="true" ConfirmDelete="true" AllowSelectSingleRowOnly="true" />
                                                    <Settings ShowFilterRow="True" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="30px" CellStyle-HorizontalAlign="Center"></dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn FieldName="TicketNumber" Width="150" VisibleIndex="1" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketSourceName" Caption="Channel" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="GenesysNumber" Caption="Interaction ID" Width="200" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="ThreadID" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="CustomerID" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="Name" Width="200" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="NomorRekening" Width="150" Caption="Account Number" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="HP" Width="150" Caption="Phone Number" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="EMAIL" Width="150" Caption="Email Address" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="CIF" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="NIK" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="AccountInbound" Caption="Account" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="SubCategory3Name" Caption="Reason" Width="300" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="DetailComplaint" Caption="User Issue Remark" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="Status" Width="160px" />
                                                        <dx:GridViewDataTextColumn FieldName="UserCreate" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateCreate" Width="150" />
                                                    </Columns>
                                                    <ClientSideEvents SelectionChanged="ASPxGridView1_SelectionChanged" />
                                                </dx:ASPxGridView>
                                                <br />
                                                <dx:ASPxMemo ID="dxmReason" runat="server" Theme="Metropolis" Height="120" Width="100%" NullText="Reason Parent Ticket" ClientIDMode="Static" placeholder="Reason Parent Child Ticket"></dx:ASPxMemo>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="ASPxButton1" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                            <ClientSideEvents Click="function(s, e) { execParentNumber(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="ASPxButton2" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                            <%--<ClientSideEvents Click="function(s, e) { execCancelParentNumber(); }" />--%>
                                                            <ClientSideEvents Click="function(s, e) { closeReloadPage(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Tab12">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="ID"
                                                    Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false"
                                                        ShowGroupPanel="false" />
                                                    <SettingsBehavior ConfirmDelete="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                                            ButtonType="Image" FixedStyle="Left" Width="30px">
                                                            <EditButton Visible="true">
                                                                <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                                                            </EditButton>
                                                            <NewButton Visible="true">
                                                                <Image ToolTip="New" Url="img/Icon/Apps-text-editor-icon2.png" />
                                                            </NewButton>
                                                            <DeleteButton Visible="false">
                                                                <Image ToolTip="Delete" Url="img/Icon/Actions-edit-clear-icon2.png" />
                                                            </DeleteButton>
                                                            <CancelButton Visible="true">
                                                                <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                                                </Image>
                                                            </CancelButton>
                                                            <UpdateButton Visible="true">
                                                                <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                                            </UpdateButton>
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Day" FieldName="Day" VisibleIndex="2">
                                                            <PropertiesComboBox>
                                                                <Items>
                                                                    <dx:ListEditItem Text="5" Value="5" />
                                                                    <dx:ListEditItem Text="10" Value="10" />
                                                                    <dx:ListEditItem Text="15" Value="15" />
                                                                    <dx:ListEditItem Text="20" Value="20" />
                                                                    <dx:ListEditItem Text="25" Value="25" />
                                                                    <dx:ListEditItem Text="30" Value="30" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataDateColumn FieldName="FilterDate" Caption="Start Filter Date" VisibleIndex="3" Width="150px"></dx:GridViewDataDateColumn>
                                                    </Columns>
                                                    <Settings ShowGroupPanel="True" />
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                    DeleteCommand="DELETE FROM [Temp_SettingHiStoryTransaction] WHERE [ID] = @ID"
                                                    SelectCommand="select * from Temp_SettingHiStoryTransaction where CreatedBy=@username and Type='ParentChild'"
                                                    UpdateCommand="UPDATE [Temp_SettingHiStoryTransaction] SET [Day] = @Day, [FilterDate] = @FilterDate, Type='ParentChild', [CreatedBy]=@username WHERE [ID] = @ID">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </SelectParameters>
                                                    <DeleteParameters>
                                                        <asp:Parameter Name="ID" Type="Int32" />
                                                    </DeleteParameters>
                                                    <InsertParameters>
                                                        <asp:Parameter Name="Day" Type="String" />
                                                        <asp:Parameter Name="FilterDate" Type="String" />
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </InsertParameters>
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="Day" Type="String" />
                                                        <asp:Parameter Name="FilterDate" Type="String" />
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </UpdateParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Tab13">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="ASPxGridView3" ClientInstanceName="ASPxGridView3" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                    KeyFieldName="ParentNumberID" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="10"
                                                    OnCustomColumnGroup="ASPxGridView3_CustomColumnGroup" OnCustomGroupDisplayText="ASPxGridView3_CustomGroupDisplayText"
                                                    OnCustomColumnSort="ASPxGridView3_CustomColumnSort">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <SettingsBehavior EnableRowHotTrack="true" AllowSelectByRowClick="true" ConfirmDelete="true" />
                                                    <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                                                            <DataItemTemplate>
                                                                <a href="#" onclick="ShowUpdateParentID('<%# Eval("TicketNumber")%>')">
                                                                    <asp:Image ImageUrl="img/icon/Actions-edit-clear-icon2.png" ID="Image1" runat="server" />
                                                                </a>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberID" Caption="Parent Case Number" Width="150" GroupIndex="1" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketNumber" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketSourceName" Caption="Channel" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="GenesysID" Caption="Interaction ID" Width="200" />
                                                        <dx:GridViewDataTextColumn FieldName="ThreadID" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="NIK" Caption="Customer ID" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="Name" Width="200" />
                                                        <dx:GridViewDataTextColumn FieldName="NomorRekening" Width="150" Caption="Account Number" />
                                                        <dx:GridViewDataTextColumn FieldName="HP" Width="150" Caption="Phone Number" />
                                                        <dx:GridViewDataTextColumn FieldName="Email" Width="150" Caption="Email Address" />
                                                        <dx:GridViewDataTextColumn FieldName="CIF" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="NoKTP" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="SubCategory3Name" Caption="Reason" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="DetailComplaint" Caption="User Issue Remark" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="ResponseComplaint" Caption="Agent Response" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentReason" Caption="Parent Child Reason" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="StatusInteraction" Caption="Status" Width="160px" />
                                                        <dx:GridViewDataTextColumn FieldName="UserCreate" Caption="Created Ticket By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateCreate" Caption="Created Date Ticket" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="AgentCreate" Caption="Created Interaction By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateInteraction" Caption="Created Date Interaction" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberCreated" Caption="Created Ticket Parent By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberDate" Caption="Created Date Parent" Width="150" />
                                                    </Columns>
                                                    <SettingsBehavior AutoExpandAllGroups="true" />
                                                    <Settings ShowGroupedColumns="True" />
                                                </dx:ASPxGridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
