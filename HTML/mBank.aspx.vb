﻿Imports System.Data
Imports System.Data.SqlClient
Public Class mBank
    Inherits System.Web.UI.Page

    Dim sqlQuery As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sql_mbank.SelectCommand = "SELECT * FROM mBank"
    End Sub

    Private Sub GridView_mBank_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles GridView_mBank.RowInserting
        Dim BankName, Status As String
        BankName = e.NewValues("BankName").ToString()
        Status = e.NewValues("NA").ToString()

        Try
            sqlQuery = "INSERT INTO mBank(BankName, NA) values('" & BankName & "', '" & Status & "') "
            sql_mbank.InsertCommand = sqlQuery
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GridView_mBank_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridView_mBank.RowUpdating
        Dim ID, BankName, Status As String
        ID = e.Keys("ID")
        BankName = e.NewValues("BankName").ToString()
        Status = e.NewValues("NA").ToString()

        Try
            sqlQuery = "UPDATE mBank SET BankName='" & BankName & "',NA='" & Status & "' WHERE ID='" & ID & "'"
            sql_mbank.UpdateCommand = sqlQuery
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

End Class