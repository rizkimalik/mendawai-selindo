﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="calltype_one.aspx.vb" Inherits="ICC.calltype_one" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Data Enquiry Type
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="gv_calltype_one" runat="server" KeyFieldName="ID"
        DataSourceID="sql_calltype_one" Width="100%" Theme="Metropolis"
        Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
        </SettingsPager>
        <SettingsPager PageSize="15" />
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
            ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="100px">
                <EditButton Visible="True">
                    <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="True">
                    <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="false">
                    <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton>
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton>
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="ID" FieldName="SubCategory1ID" ReadOnly="True" Visible="false" PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray" VisibleIndex="0" Width="50px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Category" FieldName="Name" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                <PropertiesComboBox TextFormatString="{1}" TextField="Name" ValueField="CategoryID" DataSourceID="sql_transaction_type">
                    <Columns>
                        <dx:ListBoxColumn Caption="ID" FieldName="CategoryID" Width="100px" />
                        <dx:ListBoxColumn Caption="Category" FieldName="Name" Width="180px" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn Caption="Enquiry Type" FieldName="SubName" Width="350px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" Width="100px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Active" Value="Y" />
                        <dx:ListEditItem Text="In Active" Value="N" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sql_calltype_one" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_transaction_type" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
</asp:Content>
