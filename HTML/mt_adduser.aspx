﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="mt_adduser.aspx.vb" Inherits="ICC.mt_adduser" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="Server">
    <script>
        function ShowReset(value, leveluser) {
            $("#TrxUserName").val(value)
            var TypeLevelUser = leveluser
            if ($("#MainContent_TrxLoginType").val() == "10") {
                console.log("TrxLoginType " + $("#MainContent_TrxLoginType").val() + " TypeLevelUser " + TypeLevelUser + "")
                if (TypeLevelUser == "Administrator" || TypeLevelUser == "Account User Admin") {
                    alert("Reset Password Access Denied")
                    return false;
                } else {
                    ASPxPopupControl1.Show();
                }
            } else {
                ASPxPopupControl1.Show();
            }

        }
        function execReset() {
            var TrxAutoPassword = $("#TxtPassword_I").val();
            var TrxUserName = $("#TrxUserName").val();
            var TrxAutoResetPassword = "AutoResetPassword";
            var TrxUserCreated = $("#TrxUserCreated").val();
            if (TrxAutoPassword != '') {
                var passwordformat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;
                if (TrxAutoPassword.match(passwordformat)) {

                }
                else {
                    alert("Format password is not valid, Format password is not valid (Format password 8 Karakter terdiri dari huruf besar, huruf kecil, angka dan special characters)");
                    return false;
                }

            } else {
                alert("Password is empty");
                return false
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/ChangePassword",
                    data: "{ TrxPassword: '" + TrxAutoPassword + "', TrxNewPassword: '" + TrxAutoResetPassword + "', TrxUserName: '" + TrxUserName + "', TrxUserCreated: '" + TrxUserCreated + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem);
                                $("#TxtPassword_I").val("");
                                ASPxPopupControl1.Hide();
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;

        }
        function execCancelReset() {
            ASPxPopupControl1.Hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="TrxUserName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="TrxUserCreated" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="TrxLoginType" runat="server" />
    <h4 class="headline">Data User Application
			<span class="line bg-warning" style="background-color: #f37021;"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Theme="Metropolis"
        DataSourceID="dsUser" KeyFieldName="USERID" Width="100%" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
        </SettingsPager>
        <SettingsPager PageSize="15" />
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" VisibleIndex="0" Width="130px" HeaderStyle-HorizontalAlign="Center" Visible="true">
                <NewButton Visible="true">
                    <Image Url="img/Icon/Apps-text-editor-icon2.png"></Image>
                </NewButton>
                <DeleteButton Visible="true">
                    <Image Url="img/Icon/Actions-edit-clear-icon2.png"></Image>
                </DeleteButton>
                <EditButton Visible="true">
                    <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                </EditButton>
                <CancelButton Visible="true">
                    <Image ToolTip="Cancel" Url="img/Icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton Visible="true">
                    <Image ToolTip="Update" Url="img/Icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="USERID" FieldName="USERID" ReadOnly="TRUE" Visible="false"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="* Username" FieldName="USERNAME" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="200px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="* Password" FieldName="PASSWORD" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="200px" PropertiesTextEdit-Password="true">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="* Name" FieldName="NAME" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="200px"></dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="* Email Address" FieldName="EMAIL_ADDRESS" Settings-AutoFilterCondition="Contains" Width="300px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="* Level User" FieldName="LEVELUSER" HeaderStyle-HorizontalAlign="left" Width="200px">
                <PropertiesComboBox TextField="Role_user" ValueField="Role_user" EnableSynchronization="False"
                    TextFormatString="{1}" IncrementalFilteringMode="Contains" DataSourceID="dsLevelUser">
                    <Columns>
                        <dx:ListBoxColumn Caption="No" FieldName="NumberNya" Width="50px" />
                        <dx:ListBoxColumn Caption="Level User" FieldName="Role_user" Width="200px" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="* Department" FieldName="ORGANIZATION" HeaderStyle-HorizontalAlign="left" Width="300px">
                <PropertiesComboBox TextField="ORGANIZATION_NAME" ValueField="ORGANIZATION_ID" EnableSynchronization="False"
                    TextFormatString="{1}" IncrementalFilteringMode="Contains" DataSourceID="dsUnitCase">
                    <Columns>
                        <dx:ListBoxColumn Caption="ID" FieldName="ORGANIZATION_ID" Width="50px" />
                        <dx:ListBoxColumn Caption="Department" FieldName="ORGANIZATION_NAME" Width="200px" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>           
            <dx:GridViewDataComboBoxColumn Caption="* Status Notif" FieldName="NA" Width="100px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="Y" Selected="true" />
                        <dx:ListEditItem Text="No" Value="N" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="* Kirim Email Notif" FieldName="KIRIMEMAIL" Width="120px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="YES" Selected="true" />
                        <dx:ListEditItem Text="No" Value="NO" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn Caption="Voip" FieldName="INBOUND" Width="80px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="True" Selected="true" />
                        <dx:ListEditItem Text="No" Value="False" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Email" FieldName="EMAIL" Width="80px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="True" Selected="true" />
                        <dx:ListEditItem Text="No" Value="False" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Chat" FieldName="CHAT" Width="80px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="True" Selected="true" />
                        <dx:ListEditItem Text="No" Value="False" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Whatsapp" FieldName="WHATSAPP" Width="80px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="True" Selected="true" />
                        <dx:ListEditItem Text="No" Value="False" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Facebook" FieldName="FACEBOOK" Width="80px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="True" Selected="true" />
                        <dx:ListEditItem Text="No" Value="False" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Twitter" FieldName="TWITTER" Width="80px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="True" Selected="true" />
                        <dx:ListEditItem Text="No" Value="False" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Instagram" FieldName="INSTAGRAM" Width="80px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="True" Selected="true" />
                        <dx:ListEditItem Text="No" Value="False" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataTextColumn Caption="Max Voip" FieldName="MAX_INBOUND" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="80px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Max Email" FieldName="MAX_EMAIL" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="80px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Max Chat" FieldName="MAX_CHAT" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="80px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Max Whatsapp" FieldName="MAX_WHATSAPP" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="80px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Max Facebook" FieldName="MAX_FACEBOOK" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="80px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Max Twitter" FieldName="MAX_TWITTER" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="80px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Max Instagram" FieldName="MAX_INSTAGRAM" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="80px"></dx:GridViewDataTextColumn>

            <dx:GridViewDataComboBoxColumn Caption="* Status User" FieldName="STATUS_USER" Width="100px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Active" Value="Y" Selected="true" />
                        <dx:ListEditItem Text="No Active" Value="N" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn Caption="Action Reset Password" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="100px" ToolTip="Reset Password">
                <DataItemTemplate>
                    <a href="#" id="ResetPassword" onclick="ShowReset('<%# Eval("USERNAME")%>','<%# Eval("LEVELUSER")%>')">
                        <%--<asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" />--%>Reset Password
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <asp:ListItem Value="rtf" Text="RTF" />
                <asp:ListItem Value="csv" Text="CSV" />
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <dx:ASPxButton ID="btn_Export" runat="server" Text="Export" Theme="Metropolis" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="true" Width="500px"
        closeonescape="true" Height="200px"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="true" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Reset Password" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="900px" ShowLoadingPanel="true" ClientInstanceName="ASPxCallbackPanel1" Theme="SoftOrange">
                    <PanelCollection>
                        <dx:PanelContent>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Input Default Password</label>
                                    <dx:ASPxTextBox runat="server" ID="TxtPassword" Theme="Metropolis" Width="100%" Height="30px" ClientIDMode="Static" Password="true"></dx:ASPxTextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2">
                                    <dx:ASPxButton ID="ASPxButtonSubmit" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                        <ClientSideEvents Click="function(s, e) { execReset(); }" />
                                    </dx:ASPxButton>
                                </div>
                                <div class="col-md-2">
                                    <dx:ASPxButton ID="ASPxButtonCancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                        <ClientSideEvents Click="function(s, e) { execCancelReset(); }" />
                                    </dx:ASPxButton>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1"></dx:ASPxGridViewExporter>
    <asp:SqlDataSource ID="dsUser" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsLevelUser" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM MTROLEUser order by NumberNya Asc"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsUnitCase" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM mOrganization"></asp:SqlDataSource>
</asp:Content>
