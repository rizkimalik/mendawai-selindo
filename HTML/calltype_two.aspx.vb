﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxGridLookup
Imports DevExpress.Web.ASPxClasses
Partial Class calltype_two
    Inherits System.Web.UI.Page
    Dim Com As SqlCommand
    Dim Dr As SqlDataReader
    Dim SelTicket, SelTicket1, Categori As String
    Dim ConnectionTest As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Comm, sqlcom As SqlCommand
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim loq As New cls_globe
    Dim SourceJenisTransaksi As String = String.Empty
    Dim _strScript As String = String.Empty
    Dim _strlogTime As String = DateTime.Now.ToString("yyyy")
    Dim Proses As New ClsConn
    Dim _strSql As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("NamaForm") = IO.Path.GetFileName(Request.Path)
        dsSubject.SelectCommand = "select b.IDHandling, a.Name as JenisTransaksi,b.SubName as SubjectTable,c.SubName as UnitKerja,b.NA,b.ID from mCategory a left outer join mSubCategoryLv2 b on a.CategoryID = b.CategoryID left outer join mSubCategoryLv1 as c on b.SubCategory1ID=c.SubCategory1ID where c.SubName <> ''"
        dsmCategory.SelectCommand = "select * from mCategory Where NA='Y'"
        updateAlert()
    End Sub
    Protected Sub ASPxGridLookupTagIDs_OnInit(ByVal sender As Object, ByVal e As EventArgs)
        Dim lookup = CType(sender, ASPxGridLookup)
        'lookup.DataSource = Session("TagsData")
        Dim container = CType(lookup.NamingContainer, GridViewEditItemTemplateContainer)
        If container.Grid.IsNewRowEditing Then Return
        Dim tagIDs = CStr(container.Grid.GetRowValues(container.VisibleIndex, container.Column.FieldName))
        Dim aa As Integer
        If tagIDs IsNot Nothing Then
            'lookup.GridView.Selection.SelectRowByKey(12)
            'lookup.GridView.Selection.SelectRowByKey(13)
            For Each tagID In tagIDs.Split(","c)
                lookup.GridView.Selection.SelectRowByKey(tagIDs)
            Next
        End If
    End Sub
    Protected Sub ASPxListBox1_DataBound(ByVal sender As Object, ByVal e As EventArgs)
        Dim listBox = DirectCast(sender, ASPxListBox)

        Dim editingRowVisibleIndex As Integer = ASPxGridView1.EditingRowVisibleIndex
        Dim rowValue As String = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "IDHandling").ToString()
        Dim rowValueItems() As String = rowValue.Split(","c)

        Dim rowValueItemsAsList As New List(Of String)()
        rowValueItemsAsList.AddRange(rowValueItems)

        For Each item As ListEditItem In listBox.Items
            If rowValueItemsAsList.Contains(item.Value.ToString()) Then
                item.Selected = True
            End If
        Next item
    End Sub
    Private Sub cmbCombo2_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)
        FillComboUnitKerja(TryCast(source, ASPxComboBox), e.Parameter, dsmSubCategoryLv1)
    End Sub
    Protected Sub InitializeComboUnitKerja(ByVal e As ASPxGridViewEditorEventArgs, ByVal parentComboName As String, ByVal source As SqlDataSource, ByVal callBackHandler As CallbackEventHandlerBase)

        Dim id As String = String.Empty
        If (Not ASPxGridView1.IsNewRowEditing) Then
            Dim val As Object = ASPxGridView1.GetRowValuesByKeyValue(e.KeyValue, parentComboName)
            If (val Is Nothing OrElse val Is DBNull.Value) Then
                id = Nothing
            Else
                id = val.ToString()
            End If
        End If
        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        If combo IsNot Nothing Then
            ' unbind combo
            combo.DataSourceID = Nothing
            FillComboUnitKerja(combo, id, source)
            AddHandler combo.Callback, callBackHandler
        End If
        Return
    End Sub
    Protected Sub FillComboUnitKerja(ByVal cmb As ASPxComboBox, ByVal id As String, ByVal source As SqlDataSource)
        cmb.Items.Clear()
        ' trap null selection
        If String.IsNullOrEmpty(id) Then
            Return
        End If

        ' get the values
        source.SelectParameters(0).DefaultValue = id
        Dim view As DataView = CType(source.Select(DataSourceSelectArguments.Empty), DataView)
        For Each row As DataRowView In view
            cmb.Items.Add(row(3).ToString(), row(2))
        Next row
    End Sub
    Protected Sub ASPxGridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles ASPxGridView1.CellEditorInitialize
        dsmSubCategoryLv1.SelectCommand = "select * from mSubCategoryLv1 Where categoryid=@categoryid and NA='Y'"
        Select Case e.Column.FieldName
            'Case "JenisTransaksi"
            '    InitializeCombo(e, "CategoryID", dsmCategory, AddressOf cmbCombo2_OnCallback)
            Case "UnitKerja"
                InitializeComboUnitKerja(e, "CategoryID", dsmSubCategoryLv1, AddressOf cmbCombo2_OnCallback)
            Case Else
        End Select
        If ASPxGridView1.IsNewRowEditing Then
            If e.Column.FieldName = "NA" Then
                e.Editor.Value = "Y"
            End If
        Else
        End If     
    End Sub
    'Protected Sub ASPxGridView1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASPxGridView1.Init
    '    dsSubject.SelectCommand = "select b.IDHandling, a.Name as JenisTransaksi,b.SubName as SubjectTable,c.SubName as UnitKerja,b.NA,b.ID from mCategory a left outer join mSubCategoryLv2 b on a.CategoryID = b.CategoryID left outer join mSubCategoryLv1 as c on b.SubCategory1ID=c.SubCategory1ID where c.SubName <> ''"
    'End Sub
    'Protected Sub ASPxGridView1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASPxGridView1.Load
    'End Sub
    Protected Sub ASPxGridView1_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        '====Untuk cek Jenis Transaksi sudah ada atau belum
        Dim _NA As String = e.NewValues("NA")
        Dim cekJenisTransaksi As String
        If e.NewValues("JenisTransaksi") = Nothing Then
            cekJenisTransaksi = ""
        Else
            cekJenisTransaksi = e.NewValues("JenisTransaksi").ToString()
        End If
        Dim cekUnitKerja, cekSubjectTable As String
        If e.NewValues("UnitKerja") = Nothing Then
            cekUnitKerja = ""
        Else
            cekUnitKerja = e.NewValues("UnitKerja").ToString()
        End If
        cekSubjectTable = e.NewValues("SubjectTable").ToString()
        ''End Check

        Dim SourceJenisTransaksi As String
        SourceJenisTransaksi = "Select COUNT (SubName) as CekTransaksi from mSubCategoryLv2 where CategoryID='" & cekJenisTransaksi & "' and SubCategory1ID='" & cekUnitKerja & "' and SubName='" & cekSubjectTable & "'"
        Comm = New SqlCommand(SourceJenisTransaksi, Connection)
        Try
            Connection.Open()
            Dr = Comm.ExecuteReader()
            Dr.Read()
            SourceJenisTransaksi = Dr("CekTransaksi").ToString
            Dr.Close()
            Connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        If SourceJenisTransaksi > 0 Then
            e.Cancel = True
            Throw New Exception(cekSubjectTable & " already exists")
        Else
            e.Cancel = False
        End If

        Dim nourutAkhir, angkaTerakhir, GenerateNoID As String
        Dim querySelectData As String = "select SUBSTRING(SubCategory2ID,5,5) as nourutAkhir from mSubCategoryLv2 order by ID desc"
        Comm = New SqlCommand(querySelectData, Connection)
        Try
            Connection.Open()
            Dr = Comm.ExecuteReader()
            If Dr.Read() Then
                nourutAkhir = Dr("nourutAkhir").ToString
            End If
            Dr.Close()
            Connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
       
        If nourutAkhir = "" Then
            querySelectData = "select  AngkaTerakhir = Right(100000 + COUNT(ID) + 0, 5)  from mSubCategoryLv2"
        Else
            querySelectData = "select  AngkaTerakhir = Right(100" & nourutAkhir & " + 1 + 0, 5)  from mSubCategoryLv2"
        End If

        Try
            Comm = New SqlCommand(querySelectData, Connection)
            Connection.Open()
            Dr = Comm.ExecuteReader()
            If Dr.Read() Then
                angkaTerakhir = Dr("AngkaTerakhir").ToString
            End If
            Dr.Close()
            Connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Try
            GenerateNoID = "CT2-" & angkaTerakhir & ""
            _strSql = "insert into mSubCategoryLv2 (CategoryID,SubCategory1ID,SubCategory2ID,SubName,NA) values ('" & cekJenisTransaksi & "', '" & cekUnitKerja & "','" & GenerateNoID & "', '" & cekSubjectTable & "', '" & _NA & "')"
            dsSubject.InsertCommand = _strSql
            Proses.LogSuccess(_strlogTime, " _Number mSubCategoryLv2 - " & _strSql)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, " _Number mSubCategoryLv2 - " & _strSql)
        End Try
    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim cekJenisTransaksi, cekSubjectTable, cekUnitKerja As String
        Dim OldcekJenisTransaksi, OldcekSubjectTable, OldcekUnitKerja As String

        'cekJenisTransaksi = e.NewValues("JenisTransaksi").ToString()
        cekSubjectTable = e.NewValues("SubjectTable").ToString()
        cekUnitKerja = e.NewValues("UnitKerja").ToString()

        'OldcekJenisTransaksi = e.OldValues("JenisTransaksi").ToString()
        OldcekSubjectTable = e.OldValues("SubjectTable").ToString()
        OldcekUnitKerja = e.OldValues("UnitKerja").ToString()

        ''Init Multiple ListBox
        'If OldcekJenisTransaksi <> cekJenisTransaksi And cekSubjectTable <> OldcekSubjectTable And cekUnitKerja <> OldcekUnitKerja Then
        If OldcekUnitKerja <> cekUnitKerja And cekSubjectTable <> OldcekSubjectTable Then
            Dim SourceJenisTransaksi As String
            SourceJenisTransaksi = "select COUNT(a.SubName) as CekTransaksi from mSubCategoryLv2 a left outer join mSubCategoryLv1 b on a.SubCategory1ID=b.SubCategory1ID where b.SubCategory1ID='" & cekUnitKerja & "' and a.SubName='" & cekSubjectTable & "'"
            Comm = New SqlCommand(SourceJenisTransaksi, Connection)
            Connection.Open()
            Dr = Comm.ExecuteReader()
            Dr.Read()
            SourceJenisTransaksi = Dr("CekTransaksi").ToString
            Dr.Close()
            Connection.Close()

            If SourceJenisTransaksi > 0 Then
                e.Cancel = True
                Throw New Exception(cekSubjectTable & " already exists")
            Else
                e.Cancel = False
                'dsSubject.UpdateCommand = "UPDATE mSubCategoryLv2 SET CategoryID=@JenisTransaksi,SubCategory1ID=@UnitKerja,SubName=@SubjectTable,  NA=@NA where ID=@ID"
                dsSubject.UpdateCommand = "UPDATE mSubCategoryLv2 SET SubName=@SubjectTable  NA=@NA where ID=@ID"
            End If

        ElseIf cekSubjectTable <> OldcekSubjectTable Then

            SourceJenisTransaksi = "select COUNT(a.SubName) as CekTransaksi from mSubCategoryLv2 a left outer join mSubCategoryLv1 b on a.SubCategory1ID=b.SubCategory1ID where b.SubName='" & cekUnitKerja & "' and a.SubName='" & cekSubjectTable & "'"
            Try
                Comm = New SqlCommand(SourceJenisTransaksi, Connection)
                Connection.Open()
                Dr = Comm.ExecuteReader()
                Dr.Read()
                SourceJenisTransaksi = Dr("CekTransaksi").ToString
                Dr.Close()
                Connection.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
           
            If SourceJenisTransaksi > 0 Then
                e.Cancel = True
                Throw New Exception(cekSubjectTable & " already exists")
            Else
                e.Cancel = False
                dsSubject.UpdateCommand = "UPDATE mSubCategoryLv2 SET SubName=@SubjectTable,  NA=@NA where ID=@ID"
            End If

        ElseIf OldcekUnitKerja <> cekUnitKerja Then
            dsSubject.UpdateCommand = "UPDATE mSubCategoryLv2 SET CategoryID=@JenisTransaksi,SubCategory1ID=@UnitKerja,SubName=@SubjectTable,  NA=@NA where ID=@ID"
        Else
            e.Cancel = False
            dsSubject.UpdateCommand = "UPDATE mSubCategoryLv2 SET SubName=@SubjectTable,  NA=@NA where ID=@ID"
        End If
    End Sub
    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
