<div class="position-relative">
    <div class="chat-input-section p-3 p-lg-4">

        <form id="chatinput-form" enctype="multipart/form-data">
            <div class="row g-0 align-items-center">
                <div class="file_Upload"></div>
                <div class="col-auto">
                    <div class="chat-input-links me-md-2">
                        <div class="links-list-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="File Document">
                            <input type="file" id="attachment" accept="image/*, .doc, .pdf, .xls, .xlsx" class="hide" />
                            <button type="button" onclick="document.getElementById('attachment').click()" class="btn btn-link text-decoration-none btn-lg waves-effect" aria-expanded="false" data-bs-toggle="collapse" data-bs-target="#chatinputmorecollapse" aria-expanded="false" aria-controls="chatinputmorecollapse">
                                <i class="bx bx-paperclip align-middle"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="position-relative">
                        <div class="chat-input-feedback">
                            Please Enter a Message
                        </div>
                        <input autocomplete="off" type="text" class="form-control form-control-lg chat-input" autofocus id="chat-input" placeholder="Type your message...">
                    </div>
                </div>
                <div class="col-auto">
                    <div class="chat-input-links ms-2 gap-md-1">
                        <div class="links-list-item">
                            <button type="button" class="btn btn-primary btn-lg chat-send waves-effect waves-light">
                                <i class="bx bxs-send align-middle" id="submit-btn"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="chat-input-collapse chat-input-collapse1 collapse" id="file_show">
            <div class="card mb-0">
                <div class="card-body py-2">
                    <div class="d-flex align-items-center attached-file">
                        <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                            <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                                <i class="bx bxs-file-image"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 overflow-hidden">
                            <div class="text-start">
                                <h5 class="font-size-14 mb-1" id="filename"></h5>
                                <p class="text-muted text-truncate font-size-13 mb-0" id="filesize"></p>
                            </div>
                        </div>
                        <div class="flex-shrink-0 ms-4">
                            <div class="d-flex gap-2 font-size-20 d-flex align-items-start">
                                <a href="#" class="text-muted" id="file_remove">
                                    <i class="bx bx-x-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>