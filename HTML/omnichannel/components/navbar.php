<div class="p-3 p-lg-4 user-chat-topbar" style="height: 89px;">
    <div class="row align-items-center" id="navbar-profile">
        <div class="col-sm-6 col-6">
            <div class="d-flex align-items-center">
                <div class="flex-grow-1 overflow-hidden">
                    <div class="d-flex align-items-center">
                        <div class="flex-shrink-0 avatar-sm ms-0 me-3 hide" id="img-profile">
                            <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                <i class="bx bxs-id-card"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 overflow-hidden">
                            <h6 class="text-truncate mb-0 font-size-18">
                                <a href="#" class="user-profile-show text-reset"><span id="active-name"></span></a>
                            </h6>
                            <p class="text-truncate text-muted mb-0" id="active-status"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-6">
            <ul class="list-inline user-chat-nav text-end mb-0">
                <!-- <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                    <button type="button" class="btn nav-btn call-panel-show" data-bs-toggle="tooltip" data-bs-trigger="hover"
                        data-bs-placement="bottom" title="Call Data" id="btn-call">
                        <i class='bx bxs-phone-call text-success'></i>
                    </button>
                </li> -->
                <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                    <button type="button" class="btn nav-btn user-profile-show" data-bs-toggle="tooltip"
                        data-bs-trigger="hover" data-bs-placement="bottom" title="View Profile">
                        <i class='bx bx-user-circle text-primary'></i>
                    </button>
                </li>

                <li class="list-inline-item">
                    <div class="dropdown">
                        <button class="btn nav-btn dropdown-toggle" type="button" data-bs-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class='bx bx-dots-vertical-rounded'></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end">
                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none user-profile-show"
                                href="#">View Profile <i class="bx bx-user-circle text-primary"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none call-panel-show"
                                href="#">Call Data <i class="bx bxs-phone-call text-success"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Archive
                                <i class="bx bx-archive text-warning"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#"
                                id="btn-endchat">End Chat <i class="bx bx-message-square-x text-danger"></i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div>