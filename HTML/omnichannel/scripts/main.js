/* section global parameter */
// const url_api = `http://localhost:3001`; // from config.js
const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
const formSend = document.getElementById("chatinput-form");
const message = document.getElementById("chat-input");
const attachment = document.getElementById("attachment");
const conversation = document.getElementById("chat-conversation-list");
const submitbtn = document.getElementById("submit-btn");
const btn_endchat = document.getElementById("btn-endchat");
const customer_list = document.getElementById("customer-list");
const img_profile = document.getElementById("img-profile");
const active_name = document.getElementById("active-name");
const active_status = document.getElementById("active-status");
const total_live = document.getElementById("total-live");
const display_agent = document.getElementById("display-agent");
const display_status = document.getElementById("display-status");
const navbar_profile = document.getElementById("navbar-profile");
display_status.innerHTML = '<small class="badge badge-soft-warning rounded p-1 text-truncate mb-4">Agent not ready.</small>';
/* section end global parameter */

/* section socket */
const socket = io(url_api);
socket.auth = {
    username: params.username,
    flag_to: 'agent',
}
socket.connect();
socket.on('connect', function () {
    console.info(new Date().toLocaleString() + ' : id = ' + socket.id);
    // AskPermission();
    onLoginAgent(socket.id);
    setShowHideToolbar(true);
    connectSocket(socket); //?socket call webrtc
    display_status.innerHTML = '<small class="badge badge-soft-primary rounded p-1 text-truncate mb-4">Ready to chat.</small>';
});

socket.on('send-message-customer', async function (data) {
    // push from blending
    await incomingMessage(data);
});

socket.on('return-message-customer', async function (data) {
    await incomingMessage(data);
});

socket.on('return-message-whatsapp', async function (data) {
    await incomingMessage(data);
});

socket.on('return-directmessage-twitter', async function (data) {
    await incomingMessage(data);
});

socket.on('return-typing', (res) => {
    active_status.innerHTML = res.typing ? "<small>typing...</small>" : '<small class="text-muted d-block"><i class="bx bxs-circle text-success font-size-10 align-middle"></i> online</small>';
});

socket.on('return-reconnect', async (res) => {
    // console.log(res)
    await getCustomerList(); //load customer
});

/* section end socket */

/* section form send message */
submitbtn.addEventListener("click", async (e) => {
    e.preventDefault();
    await onSendMessage();
});

message.addEventListener("keypress", async (e) => {
    if (e.key === 'Enter') {
        e.preventDefault();
        await onSendMessage();
    }
    else {
        const ActiveCustomer = localStorage.getItem('ActiveCustomer');
        const data = JSON.parse(ActiveCustomer);

        let values = {
            uuid_customer: data.uuid_customer,
            uuid_agent: socket.id,
            email: data.email,
            agent_handle: data.agent_handle,
            flag_to: 'agent',
            typing: true
        }
        socket.emit('typing', values);
    }
});

btn_endchat.addEventListener("click", (e) => {
    e.preventDefault();
    Swal.fire({
        title: 'End Chat',
        text: 'Do you want to close the chat?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'End Chat',
    }).then((result) => {
        if (result.isConfirmed) {
            onEndChat();
            Swal.fire('Success!', '', 'success')
        }
    })
});

attachment.addEventListener('change', (e) => {
    const file = e.target.files[0];
    if (file.size > 3000000) {
        alert('Max size file 3mb.');
    } else {
        document.getElementById('file_show').classList.add('show');
        document.getElementById('filename').innerHTML = file.name;
        document.getElementById('filesize').innerHTML = file.size / 1000 + ' kb';
    }
});

document.getElementById('file_remove').addEventListener('click', (e) => {
    e.preventDefault();
    attachment.value = '';
    document.getElementById('file_show').classList.remove('show');
});
/* section end form send message */

/* section function get data */
async function onLoginAgent(socket_id) {
    const response = await fetch(`${url_api}/omnichannel/join_chat`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            uuid: socket_id,
            username: params.username,
            flag_to: 'agent'
        })
    });

    const json = await response.json();
    if (json.status === 200) {
        localStorage.removeItem('ActiveCustomer');
        display_agent.innerHTML = params.username;
        await getCustomerList()
    }
    else {
        alert('Login Failed.')
    }
}

async function onSendMessage() {
    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
    const data = JSON.parse(ActiveCustomer);
    
    let message_type = 'text';
    let attachment_file = '';
    if (attachment.files[0]) {
        let generate_filename = attachment.files[0] ? (Date.now() + '.' + (attachment.files[0].name).split('.').pop()) : '';
        message_type = (attachment.files[0].type).split('/')[0] === 'image' ? 'image' : 'document';
        attachment_file = [{
            attachment: attachment.files[0],
            file_origin: attachment.files[0]?.name,
            file_name: generate_filename,
            file_size: attachment.files[0]?.size,
            file_url: attachment.files[0] ? `${urlAttachment}/${data.channel}/${generate_filename}` : '',
        }];
    }

    let values = {
        username: data.name,
        chat_id: data.chat_id,
        user_id: data.user_id,
        customer_id: data.customer_id,
        message: message.value,
        message_type: message_type,
        name: params.username,
        email: data.email,
        channel: data.channel,
        flag_to: 'agent',
        agent_handle: data.agent_handle,
        page_id: data.page_id,
        date_create: new Date(),
        uuid_customer: data.uuid_customer,
        uuid_agent: socket.id,
        attachment: attachment_file,
        typing: false
    }

    if (message.value) {
        if (data.channel === 'Chat') {
            socket.emit('send-message-agent', values);
            showMessageHTML(values);
        }
        else if (data.channel === 'Whatsapp') {
            socket.emit('send-message-whatsapp', values);
            showMessageHTML(values);
            // await onSendMessageWhatsapp(values);
        }
        else if (data.channel === 'Twitter_DM') {
            socket.emit('send-directmessage-twitter', values);
            showMessageHTML(values);
        }
        socket.emit('typing', values);
    }
    removeAttachment();

    return false;
}

async function incomingMessage(data) {
    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
    // ShowNotification(data.name, data.message);
    await getCustomerList(); //load customer

    if (ActiveCustomer) {
        const { customer_id } = JSON.parse(ActiveCustomer);
        if (data.customer_id == customer_id) {
            active_status.innerHTML = data.typing ? "<small>typing...</small>" : "<small>online</small>";
            let showFileHtml = data.attachment ? showAttachment(data.attachment) : '';

            conversation.innerHTML +=
                `<li class="chat-list left">
                    <div class="conversation-list">
                        <div class="chat-avatar">
                            <div class="avatar-sm">
                                <div class="avatar-title text-primary bg-soft-primary rounded-circle border">
                                    <i class="bx bxs-id-card"></i>
                                </div>
                            </div>
                        </div>
                        
                        <div class="user-chat-content">
                            <div class="ctext-wrap">
                                <div class="ctext-wrap-content">
                                    ${showFileHtml}
                                    <p class="mb-0 ctext-content">${data.message}</p>
                                </div>
                            </div>
                            <div class="conversation-name">
                                <small class="text-muted time">${h()}</small> 
                                <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span>
                            </div>
                        </div>
                    </div>
                </li>`;
        }
    }
}

async function getCustomerList() {
    const response = await fetch(`${url_api}/omnichannel/list_customers?agent=${params.username}`, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        },
    });

    const json = await response.json();
    const data = json.data;

    if (json.status === 200) {
        let html = '';
        let active = '';
        const ActiveCustomer = localStorage.getItem('ActiveCustomer');
        const actv = JSON.parse(ActiveCustomer);

        for (let i = 0; i < data.length; i++) {
            if (actv) {
                active = data[i].chat_id === actv.chat_id ? 'active' : '';
            }

            let icon = '';
            // let status_connected = data[i].connected ? '<span class="user-status"></span>' : '';
            let status_connected = '<span class="user-status"></span>';
            let total_chat = data[i].total_chat ? data[i].total_chat : '<i class="bx bx-check-double"></i>';
            let page_name = data[i].page_name ? data[i].page_name : 'Mendawai';

            if (data[i].channel === 'Chat') {
                icon = 'assets/img/icon/chat.png';
            }
            else if (data[i].channel === 'Whatsapp') {
                icon = 'assets/img/icon/whatsapp.png';
            }
            else if (data[i].channel.split('_')[0] === 'Twitter') {
                icon = 'assets/img/icon/twitter.png';
            }

            html += `<li class="border-bottom ${active}" id="chatid-${data[i].chat_id}"> 
                        <a href="javascript: void(0);">
                            <div class="d-flex align-items-center my-2">
                                <div class="chat-user-img online align-self-center me-2 ms-0">
                                    <div class="avatar-title bg-soft-dark rounded-circle">
                                        <img src="${icon}" class="rounded-circle avatar-sm" alt="profile">
                                    </div>
                                    ${status_connected}
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                    <span class="my-1">${data[i].name}</span>
                                    <p class="text-truncate my-1">${data[i].user_id}</p>
                                    <span class="badge bg-soft-primary text-primary rounded my-1">${data[i].channel}</span>
                                </div>
                                <div class="d-flex flex-column align-items-end">
                                    <p class="text-mute font-size-12 my-1">${data[i].datetime}</p>
                                    <span class="badge badge-soft-dark rounded p-1 my-1">${total_chat}</span>
                                    <p class="text-truncate font-size-12 my-1">${page_name}</p>
                                </div>
                            </div>
                        </a> 
                    </li>`;
        }
        total_live.innerHTML = data.length;
        customer_list.innerHTML = '';
        customer_list.innerHTML += html;

        //? On Select Customer
        data.map((raw, index) => {
            document.getElementById(`chatid-${raw.chat_id}`)
                .addEventListener('click', (e) => {
                    e.preventDefault()
                    onSelectCustomer(raw);
                });
        });
    }
    else {
        console.log(json);
    }
}

const onSelectCustomer = async (data) => {
    localStorage.removeItem('ActiveCustomer'); //clear
    localStorage.setItem('ActiveCustomer', JSON.stringify(data));
    active_name.innerHTML = `${data.name}`;
    active_status.innerHTML = `<small class="text-muted"><i class="bx bxs-circle text-success font-size-11 align-middle"></i> online</small> - <small class="text-muted font-size-11">ID:${data.chat_id}</small>`;
    // active_status.innerHTML = data.connected
    //     ? '<small class="text-muted d-block"><i class="bx bxs-circle text-success font-size-10 align-middle"></i> online</small>'
    //     : '<small class="text-muted d-block"><i class="bx bxs-circle text-danger font-size-10 align-middle"></i> offline</small>';
    img_profile.classList.remove('hide');
    setShowHideToolbar(false);
    await getCustomerList();
    await showCustomerProfile(data);
    getConversationData(data.chat_id, data.customer_id);
    await getCallHistory(); // from call_webrtc.js
}

async function showCustomerProfile(data) {
    document.getElementById('channel').innerHTML = data.channel;
    document.getElementById('chat_id').innerHTML = data.chat_id;
    document.getElementById('customer_id').innerHTML = data.customer_id;
    document.getElementById('customer_name').innerHTML = data.name;
    document.getElementById('user_id').innerHTML = data.user_id;
    document.getElementById('page_name').innerHTML = data.page_name ? data.page_name : 'Mendawai';
}

async function getConversationData(chat_id, customer_id) {
    const response = await fetch(`${url_api}/omnichannel/conversation_chats`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            chat_id: chat_id,
            customer_id: customer_id,
        })
    });

    const json = await response.json();
    const data = json.data;
    if (json.status === 200) {
        let html = '';
        for (let i = 0; i < data.length; i++) {
            const read_status = data[i].flag_notif ? '<i class="bx bx-check-double"></i>' : '<i class="bx bx-check"></i>';
            let showFileHtml = data[i].attachment ? showAttachment(data[i].attachment) : '';

            if (data[i].flag_to === 'customer') {
                html += `<li class="chat-list left">
                        <div class="conversation-list">
                            <div class="chat-avatar">
                                <div class="avatar-sm">
                                    <div class="avatar-title text-primary bg-soft-primary rounded-circle border">
                                        <i class="bx bxs-id-card"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="user-chat-content">
                                <div class="ctext-wrap">
                                    <div class="ctext-wrap-content">
                                        ${showFileHtml}
                                        <p class="mb-0 ctext-content">${data[i].message}</p>
                                    </div>
                                </div>
                                <div class="conversation-name">
                                    <small class="text-muted time">${data[i].date_create}</small> 
                                    <span class="text-success check-message-icon">${read_status}</span>
                                </div>
                            </div>
                        </div>
                    </li>`;
            }
            else {
                html += `<li class="chat-list right">
                    <div class="conversation-list">
                        <div class="user-chat-content">
                            <div class="ctext-wrap">
                                <div class="ctext-wrap-content">
                                    ${showFileHtml}
                                    <p class="mb-0 ctext-content">${data[i].message}</p>
                                </div>
                            </div>
                            <div class="conversation-name">
                                <small class="text-muted time">${data[i].date_create}</small> 
                                <span class="text-success check-message-icon">${read_status}</span>
                            </div>
                        </div>
                    </div>
                </li>`;
            }

        }
        conversation.innerHTML = '';
        conversation.innerHTML += html;
    }
}

const onEndChat = async () => {
    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
    const data = JSON.parse(ActiveCustomer);

    let values = {
        username: data.name,
        chat_id: data.chat_id,
        user_id: data.user_id,
        customer_id: data.customer_id,
        message: 'Terima kasih, session telah berakhir.',
        message_type: 'text',
        name: params.username,
        email: data.email,
        channel: data.channel,
        flag_to: 'agent',
        agent_handle: data.agent_handle,
        date_create: new Date(),
        uuid_customer: data.uuid_customer,
        uuid_agent: socket.id,
        attachment: '',
        file_origin: '',
        file_name: '',
        file_size: '',
        file_url: '',
        typing: false
    }
    socket.emit('send-message-agent', values); // send message session end

    const response = await fetch(`${url_api}/omnichannel/end_chat`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            chat_id: data.chat_id,
            customer_id: data.customer_id,
        })
    });
    const json = await response.json();
    if (json.status === 200) {
        await InsertThread(data);
        await onCreateTicket();
        localStorage.removeItem('ActiveCustomer');
        active_name.innerHTML = '';
        active_status.innerHTML = '';
        conversation.innerHTML = '';
        setShowHideToolbar(true);
        await getCustomerList();
    }
}

const showMessageHTML = (values) => {
    let showFileHtml = values.attachment ? showAttachment(values.attachment) : '';
    conversation.innerHTML +=
        `<li class="chat-list right">
            <div class="conversation-list">
                <div class="user-chat-content">
                    <div class="ctext-wrap">
                        <div class="ctext-wrap-content">
                            ${showFileHtml}
                            <p class="mb-0 ctext-content">${message.value}</p>
                        </div>
                        <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                            <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                        </div>
                    </div>
                    <div class="conversation-name">
                        <small class="text-muted time">${h()}</small> 
                        <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>
                    </div>
                </div>
            </div>
        </li>`;
    message.value = "";
}

const onSendMessageWhatsapp = async (values) => {
    const response = await fetch(`${url_api}/omnichannel/whatsapp/sendmessage`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(values),
        redirect: 'follow'
    });

    const result = await response.json();
    if (result.status === 200) {
        console.log(result);
        showMessageHTML(values);
    }
}

async function onCreateTicket() {
    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
    const data = JSON.parse(ActiveCustomer);
    window.open(`../mainframe.aspx?agentid=${params.username}&channel=${data.channel}&genesysid=${data.chat_id}&customerid=${data.customer_id}&account=${data.email}&accountid=${data.customer_id}&subject=&phoneChat=${data.hp}&emailaddress=${data.email}&source=inbound&threadid=-`);
}

const InsertThread = async (values) => {
    const response = await fetch(`${url_api}/thread/insert`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(values),
        redirect: 'follow'
    });
    const result = await response.json();
    console.log(result);
}

const setShowHideToolbar = (value) => {
    if (value === true) {
        formSend.classList.add('hide');
        navbar_profile.classList.add('hide');
    } else {
        formSend.classList.remove('hide');
        navbar_profile.classList.remove('hide');
    }
}

const showAttachment = (attachment) => {
    let html = '';
    html += '<div class="d-flex align-items-center">';
    for (let i = 0; i < attachment.length; i++) {
        html += `<div class="p-3 border-primary border rounded-3 m-2">
            <div class="d-flex align-items-center attached-file">
                <div class="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                    <div class="avatar-title bg-soft-primary text-primary rounded-circle font-size-20"> <i
                            class="bx bx-paperclip align-middle"></i> </div>
                </div>
                <div class="flex-grow-1 overflow-hidden">
                    <div class="text-start">
                        <h5 class="font-size-14 mb-1">${attachment[i].file_name}</h5>
                        <p class="text-muted text-truncate font-size-13 mb-0">${attachment[i].file_size / 1000} kb</p>
                    </div>
                </div>
                <div class="flex-shrink-0 ms-4">
                    <div class="d-flex gap-2 font-size-20 d-flex align-items-start">
                        <div> <a href="${attachment[i].file_url}" target="_blank" rel="noopener noreferrer" class="text-muted"> <i class="bx bxs-download"></i> </a> </div>
                    </div>
                </div>
            </div>
        </div>`;

    }
    html += '</div>';

    return (html);
}

const removeAttachment = () => {
    attachment.value = '';
    document.getElementById('file_show').classList.remove('show');
}

function h() {
    var e = 12 <= (new Date).getHours() ? "pm" : "am",
        t = 12 < (new Date).getHours() ? (new Date).getHours() % 12 : (new Date).getHours(),
        a = (new Date).getMinutes() < 10 ? "0" + (new Date).getMinutes() : (new Date).getMinutes();
    return t < 10 ? "0" + t + ":" + a + " " + e : t + ":" + a + " " + e
}
    /* section end function get data */
