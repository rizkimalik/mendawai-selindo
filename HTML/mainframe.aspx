﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="mainframe.aspx.vb" Inherits="ICC.mainframe" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxFormLayout" Assembly="DevExpress.Web.v13.2" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v13.2" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxCallbackPanel" Assembly="DevExpress.Web.v13.2" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPanel" Assembly="DevExpress.Web.v13.2" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.v13.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">

    <script type="module" src="scripts/mainframe.js"></script>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            // sample InteractionID
            if (document.getElementById('MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I').value == '') {
                document.getElementById('MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I').value = Date.now();
            }
          

            function getActiveTab() {
                var data = localStorage.getItem("ActiveTab");
                var linkAddCustomer = document.querySelector('.linkAddCustomer');
                var linkDataCustomer = document.querySelector('.linkDataCustomer');

                if (data == "TabAddCustomer") {
                    linkAddCustomer.addEventListener("click", function () {
                        console.log('tab add customer')
                    });
                    document.querySelector(".AddCustomer").classList.add("active")
                    document.querySelector(".TabAddCustomer").classList.add("in", "active")
                    document.querySelector(".DataCustomer").classList.remove("active")
                    document.querySelector(".TabPreviewCustomer").classList.remove("in", "active")
                }
                else if (data == "TabPreviewCustomer") {
                    linkDataCustomer.addEventListener("click", function () {
                        console.log('tab data customer')
                    })
                    document.querySelector(".AddCustomer").classList.remove("active")
                    document.querySelector(".TabAddCustomer").classList.remove("in", "active")
                    document.querySelector(".DataCustomer").classList.add("active")
                    document.querySelector(".TabPreviewCustomer").classList.add("in", "active")
                }
            }
            //getActiveTab();
        });

        function openActiveTab(event, TabName) {
            var i, tablinks;
            localStorage.setItem("ActiveTab", TabName);

            if (TabName == "TabAddCustomer") {
                document.querySelector(".TabAddCustomer").classList.add("in", "active")
                document.querySelector(".TabPreviewCustomer").classList.remove("in", "active")
            }
            else if (TabName == "TabPreviewCustomer") {
                document.querySelector(".TabAddCustomer").classList.remove("in", "active")
                document.querySelector(".TabPreviewCustomer").classList.add("in", "active")
            }

            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            event.currentTarget.className += " active";
        }

        function getWS_CategoryType(value) {
            var selectedText = $("#cmbTransaksi").find("option:selected").text();
            var selectedValue = $("#cmbTransaksi").val();
            console.log("Selected Text: " + selectedText + " Value: " + selectedValue);

            var cmbDataSourceEnquiry = $('#ASPxComboBox1');
            var jsonText = JSON.stringify({ tableType: 'AllWhereData', tableName: "mSubCategoryLv1", paramQuery: "where NA='Y' and CategoryID='" + selectedValue + "'" });
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetWhereRecords",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x, resultSourceEnquiry = "";

                    cmbDataSourceEnquiry.empty();
                    cmbDataSourceEnquiry.append('<option value="">Select</option>');
                    for (i = 0; i < json.length; i++) {
                        //alert();
                        //alert();
                        //alert(json[i].UserCreate);               
                        resultSourceEnquiry = '<option value="' + json[i].SubCategory1ID + '">' + json[i].SubName + '</option>';
                        cmbDataSourceEnquiry.append(resultSourceEnquiry);

                        if (selectedValue == "CAT-10002") {
                            var cmbTypeOfComplaint = $('#cmbChannelTicket');
                            var jsonText = JSON.stringify({ tableType: 'AllWhereData', tableName: "Temp_ExtendSubCategory", paramQuery: "where NA='Y'" });
                            $.ajax({
                                type: "POST",
                                url: "WebServiceTransaction.asmx/GetWhereRecords",
                                data: jsonText,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    var json = JSON.parse(data.d);
                                    var i, x, resultTypeOfComplaint = "";

                                    cmbTypeOfComplaint.empty();
                                    cmbTypeOfComplaint.append('<option value="">Select</option>');
                                    for (i = 0; i < json.length; i++) {
                                        
                                        resultTypeOfComplaint = '<option value="' + json[i].ID + '">' + json[i].NameExtend + '</option>';
                                        cmbTypeOfComplaint.append(resultTypeOfComplaint);
                                        $('#cmbChannelTicket').attr('disabled', false);
                                    }

                                },
                                error: function (xmlHttpRequest, textStatus, errorThrown) {
                                    console.log(xmlHttpRequest.responseText);
                                    console.log(textStatus);
                                    console.log(errorThrown);
                                }
                            })
                        } else {
                            //$('#cmbChannelTicket').css("display", "none")
                            $('#cmbChannelTicket').attr('disabled', true);
                        }

                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
        function getWS_CategoryTypeDetail(value) {
            var selectedText = $("#ASPxComboBox1").find("option:selected").text();
            var selectedValue = $("#ASPxComboBox1").val();
            console.log("Selected Text: " + selectedText + " Value: " + selectedValue);

            var cmbDataSourceEnquiryDetail = $('#ASPxComboBox2');
            var jsonText = JSON.stringify({ tableType: 'AllWhereData', tableName: "mSubCategoryLv2", paramQuery: "where NA='Y' and SubCategory1ID='" + selectedValue + "'" });
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetWhereRecords",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x, resultSourceEnquiryDetail = "";
                    cmbDataSourceEnquiryDetail.empty();
                    cmbDataSourceEnquiryDetail.append('<option value="">Select</option>');
                    for (i = 0; i < json.length; i++) {
                        resultSourceEnquiryDetail = '<option value="' + json[i].SubCategory2ID + '">' + json[i].SubName + '</option>';
                        cmbDataSourceEnquiryDetail.append(resultSourceEnquiryDetail);

                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
        function getWS_CategoryTypeReason(value) {
            var selectedText = $("#ASPxComboBox2").find("option:selected").text();
            var selectedValue = $("#ASPxComboBox2").val();
            console.log("Selected Text: " + selectedText + " Value: " + selectedValue);

            var cmbDataSourceEnquiryReason = $('#cmbProblem3');
            var jsonText = JSON.stringify({ tableType: 'AllWhereData', tableName: "mSubCategoryLv3", paramQuery: "where NA='Y' and SubCategory2ID='" + selectedValue + "' order by subname ASC" });
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetWhereRecords",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x, resultSourceEnquiryReason = "";
                    cmbDataSourceEnquiryReason.empty();
                    cmbDataSourceEnquiryReason.append('<option value="">Select</option>');
                    for (i = 0; i < json.length; i++) {

                        resultSourceEnquiryReason = '<option value="' + json[i].SubCategory3ID + '">' + json[i].SubName + '</option>';
                        cmbDataSourceEnquiryReason.append(resultSourceEnquiryReason);

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
        function getWS_SLAReason(value) {
            var selectedText = $("#cmbProblem3").find("option:selected").text();
            var selectedValue = $("#cmbProblem3").val();
            console.log("getWS_SLAReason: " + selectedText + " Value: " + selectedValue);

            //var slaSpanData = $('#hd_sla');
            var jsonText = JSON.stringify({ tableType: 'AllWhereData', tableName: "mSubCategoryLv3", paramQuery: "where SubCategory3ID='" + selectedValue + "' And NA='Y'" });
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetWhereRecords",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x, resultSourceEnquiryReason = "";

                    for (i = 0; i < json.length; i++) {

                        $("#hd_sla").val(json[i].SLA);
                        //$("#cmbTujuanEskalasi").val()
                        //$("#cmbTujuanEskalasi").find("option:selected").text();
                        $("#cmbTujuanEskalasi").val(json[i].TujuanEskalasi);

                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })

            //Combo Status
            var cmbStatusTicket = $('#cmbStatusTicket');
            var jsonText = JSON.stringify({ tableType: 'AllWhereData', tableName: "mStatus", paramQuery: "WHERE NA='Y' order by Urutan ASC" });
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetWhereRecords",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x, resultStatusTicket = "";

                    cmbStatusTicket.empty();
                    cmbStatusTicket.append('<option value="">Select</option>');
                    for (i = 0; i < json.length; i++) {

                        resultStatusTicket = '<option value="' + json[i].status + '">' + json[i].status + '</option>';
                        cmbStatusTicket.append(resultStatusTicket);

                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })

            //Combo Escalation Unit
            var cmbTujuanEskalasi = $('#cmbTujuanEskalasi');
            var jsonText = JSON.stringify({ tableType: 'AllWhereData', tableName: "MORGANIZATION", paramQuery: "where flag=0 order by ORGANIZATION_NAME ASC" });
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetWhereRecords",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x, resultTujuanEskalasi = "";

                    cmbTujuanEskalasi.empty();
                    cmbTujuanEskalasi.append('<option value="">Select</option>');
                    for (i = 0; i < json.length; i++) {

                        resultTujuanEskalasi = '<option value="' + json[i].ORGANIZATION_ID + '">' + json[i].ORGANIZATION_NAME + '</option>';
                        cmbTujuanEskalasi.append(resultTujuanEskalasi);

                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })


        }
   
        function onclickDefault() {
            var TicketNumber = $("#MainContent_hd_ticketid").val();
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/Select_Data_TransactionTicket",
                data: "{ filterData: '" + TicketNumber + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var tblTickets = "";

                    for (i = 0; i < json.length; i++) {

                        $("#MainContent_ASPxPopupControl2_TxtAmountUpdate_I").val(json[i].TrxAmountDecimal);

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
  
        function onclickAmount() {
            $('#MainContent_ASPxPopupControl7_TxtInputAmount_I').val("");
            ASPxPopupControl7.Show();
        }

        function price_InitAndKeyUpdate(s, e) {
            var rupeeIndian = Intl.NumberFormat("en-ID", {
                style: "currency",
                currency: "IDR",
            });
            var idr = rupeeIndian.format($('#MainContent_ASPxPopupControl7_TxtInputAmount_I').val());
            //alert(idr)
            var mon = idr.split('IDR').join('');
            $('#MainContent_ASPxPopupControl2_TxtAmountUpdate_I').val(mon);
            ASPxPopupControl7.Hide();
            //console.log("Dollars: " + rupeeIndian.format($('#MainContent_Amount_I').val()));
        }

        
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "");
        }
 
        function f_SearchingCustomer() {
            ASPxPopupControl5.Show();
        }
        function OnRowClick(s, e) {
            ASPxGridView5.GetRowValues(ASPxGridView5.GetFocusedRowIndex(), 'CustomerID;Name;Email;HP;Alamat;Birth;JenisKelamin', OnGetRowValuesCustomer);
        }
        function OnGetRowValuesCustomer(values) {
            //document.getElementById('MainContent_hd_Customer_Detail').value = values
            $("#MainContent_hd_Customer_Detail").val(values)
            var data = $("#MainContent_hd_Customer_Detail").val();
            var CustomerID = data.split(',')[0];
            var Name = data.split(',')[1];
            var Email = data.split(',')[2];
            var HP = data.split(',')[3];
            var Alamat = data.split(',')[4];
            var Birth = data.split(',')[5];
            var Gender = data.split(',')[6];
            var Tlahir = new Date(Birth)
            var ConvertTlahir = Tlahir.getFullYear() + '-' + (Tlahir.getMonth() + 1) + '-' + Tlahir.getDate();
            var vCustomer = $("#MainContent_hd_Customer_Detail").val();
            RedirectThreadToMainframe(CustomerID);
            //alert(CustomerID);
            if (vCustomer === '') {
                alert("Please select data customer")
                ASPxPopupControl5.Hide();
            }
            else {
                $("#MainContent_hd_customerid").val(CustomerID)
                $("#MainContent_ASPxSplitter1_formLayout_TxtName_I").val(Name)
                $("#MainContent_ASPxSplitter1_formLayout_TxtPhone_I").val(HP)
                $("#MainContent_ASPxSplitter1_formLayout_cmbGender_I").val(Gender)
                $("#MainContent_ASPxSplitter1_formLayout_TxtEmail_I").val(Email)
                $("#MainContent_ASPxSplitter1_formLayout_TxtAddress_I").val(Alamat)
                $("#MainContent_ASPxSplitter1_formLayout_dtBirth_I").val(ConvertTlahir)

                ASPxPopupControl5.Hide();
                ASPxCallbackPanel5.PerformCallback();
            }
        }
  
        function execShowAddCustomer() {
            var accountnya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtAccount_I").val();
            if ($("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtType_I").val() == "voice") {
                $("#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerPhone_I").val(accountnya);
            } else if ($("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtType_I").val() == "email") {
                $("#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerEmail_I").val(accountnya);
            }
            ASPxPopupControl5.Hide();
            ASPxPopupControl4.Show();
        }
 
        function RedirectThreadToMainframe(value) {
            var TrxCustomerID = value;
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/UpdateThreadMultipleAccounut",
                    data: "{ TrxCustomerID: '" + TrxCustomerID + "', TrxGenesysID: '" + TrxGenesysID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        readCustomer(value);
                        ASPxPopupControl5.Hide();
                        //Restu update test Redirect//
                        var TrxChannelNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtType_I").val();
                        var TrxCustomerIDNya = $("#MainContent_hd_customerid").val();
                        var TrxGenesysIDNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
                        var TxtThreadIDNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtThreadID_I").val();
                        var TxtAccountNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtAccount_I").val();
                        var TxtContactIDNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtAccountID_I").val();
                        var TxtSubjectNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtSubject_I").val();
                        window.location.href = "mainframe.aspx?channel=" + TrxChannelNya + "&threadid=" + TxtThreadIDNya + "&genesysid=" + TrxGenesysIDNya + "&account=" + TxtAccountNya + "&accountid=" + TxtContactIDNya + "&subject=" + TxtSubjectNya + "&source=thread&customerid=" + TrxCustomerID + "";
                        //END
                    },
                    error: function (r) {
                        alert(r.responseText);
                    },
                    failure: function (r) {
                        alert(r.responseText);
                    }
                })
            } else
                return false;
        }
  
        function RedirectThread(value) {
            var TrxCustomerID = value;
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/UpdateThreadMultipleAccounut",
                    data: "{ TrxCustomerID: '" + TrxCustomerID + "', TrxGenesysID: '" + TrxGenesysID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        readCustomer(value);
                        ASPxPopupControl4.Hide();
                    },
                    error: function (r) {
                        alert(r.responseText);
                    },
                    failure: function (r) {
                        alert(r.responseText);
                    }
                })
            } else
                return false;
        }
  
        function showEmail() {
            ASPxPopupControl3.Show();
            ASPxCallbackPanel7.PerformCallback();
        }
  
        function Check(s, e) {
            //alert(s.GetChecked())
            if (s.GetChecked()) {
                $("#MainContent_hd_NomorRekening").val("0");
                ASPxCallbackPanel6.PerformCallback();
            } else {
                alert($("#MainContent_hd_NomorRekening").val())
                ASPxCallbackPanel6.PerformCallback();
            }
        }
        function OnCheckedChanged(s, e) {
            //alert(s.GetChecked())
            if (s.GetChecked()) {
                if ($("#MainContent_hd_Customer_Detail").val() == "") {
                    alert("Please, select data customer")
                    return false
                } else {
                    $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelapor_I").val($("#MainContent_ASPxSplitter1_formLayout_TxtName_I").val());
                    $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporEmail_I").val($("#MainContent_ASPxSplitter1_formLayout_TxtEmail_I").val());
                    $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporPhone_I").val($("#MainContent_ASPxSplitter1_formLayout_TxtPhone_I").val());
                    $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporAddress_I").val($("#MainContent_ASPxSplitter1_formLayout_TxtAddress_I").val());
                }
            } else {
                $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelapor_I").val("");
                $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporEmail_I").val("");
                $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporPhone_I").val("");
                $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporAddress_I").val("");
            }
        }
    
        function OnRowClickAccountNumber(s, e) {
            ASPxGridView1.GetRowValues(ASPxGridView1.GetFocusedRowIndex(), 'NomorRekening', OnGetRowValuesNomorRekening);
        }
        function OnGetRowValuesNomorRekening(values) {
            //alert(values)
            $("#MainContent_hd_NomorRekening").val(values);
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_txtNomorRekening_I").val($("#MainContent_hd_NomorRekening").val());
            if ($("#MainContent_hd_NomorRekening").val() === '') {
                alert("Please select nomor rekening")
            }
            else {
                ASPxCallbackPanel6.PerformCallback();
            }
        }

    // <%--Pop up Data Transaksi--%>
        function ShowLoginWindow() {
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var TrxThreadID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtThreadID_I").val();
            var TrxPelapor = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelapor_I").val();
            var TrxPelaporEmail = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporEmail_I").val();
            var TrxPelaporPhone = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporPhone_I").val();
            var TrxPelaporAddress = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporAddress_I").val();

            if (TrxGenesysID === '') {
                alert("Interaction ID is empty")
                return false;
            }
            if (TrxThreadID === '') {
                alert("Thread ID is empty")
                return false;
            }
            if (TrxPelapor === '') {
                alert("Pelapor Name is empty")
                return false;
            }
            //if (TrxPelaporEmail === '') {
            //    alert("Pelapor Email is empty")
            //    return false;
            //}
            //if (TrxPelaporPhone === '') {
            //    alert("Pelapor Phone is empty")
            //    return false;
            //}
            if (TrxPelaporEmail != '') {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (TrxPelaporEmail.match(mailformat)) {

                }
                else {
                    alert("Format email address not valid");
                    return false;
                }
            }
            if (TrxPelaporPhone != '') {
                var numberNya = /^[0-9]+$/;
                if (TrxPelaporPhone.match(numberNya)) {
                    var PhoneLengt = TrxPelaporPhone.toString().length;
                    if (PhoneLengt > '6') {

                    } else {
                        alert("Format phone number is not valid")
                        return false;
                    }
                } else {
                    alert("Phone Number format is numeric")
                    return false;
                }
            }
            if (TrxPelaporAddress === '') {
                alert("Pelapor Address is empty")
                return false;
            }
            if ($("#MainContent_hd_NomorRekening").val() === '') {
                $("#MainContent_hd_NomorRekening").val("0")
                $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_txtNomorRekening_I").val("0")
            }
            var id = $("#MainContent_hd_customerid").val()
            if (id === '') {
                alert("Please select customer")
                ASPxPopupControl1.Hide();
            } else {
                ASPxPopupControl1.Show();
                $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxButton3').hide();
            }
            //combobox.SetValue("someValue");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox1_I").val("--Select--");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox2_I").val("--Select--");
        }

    // <%--Encode Data--%>
        function encodeData(s) {
            return encodeURIComponent(s).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
        }

    // <%--Insert Data Transaksi--%>
        function execSimpanTransaction() {
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxCustomerID = $("#MainContent_hd_customerid").val();
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var TxtThreadID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var TxtAccount = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtAccount_I").val();
            var TxtContactID = '';
            var TrxPelapor = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelapor_I").val();
            var TrxPelaporEmail = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporEmail_I").val();
            var TrxPelaporPhone = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporPhone_I").val();
            var TrxPelaporAddress = $("#MainContent_ASPxSplitter1_ASPxFormLayout1_TxtPelaporAddress_I").val();
            var TrxKejadian = $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_datePengaduan_I").val();
            var TrxPenerimaPengaduan = $("#MainContent_hd_username").val();
            var TrxSkalaPrioritas = $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbskalaprioritas_I").val();
            var TrxStatusPelapor = '';
            var TrxPenyebab = '';
            var TrxJenisNasabah = '';
            var TrxNomorRekening = '';
            var TrxSumberInformasi = $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbsumberinformasi_I").val();
			var TrxCategory = document.getElementById('cmbTransaksi').value;
			var TrxLevel1 = document.getElementById('MainContent_hd_subcategory1id').value;
            var TrxLevel2 = document.getElementById('MainContent_hd_subcategory2id').value;
            var TrxLevel3 = document.getElementById('MainContent_hd_Category3ID').value;
            var TrxComplaint = $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxMemo2_I").val();
            var TrxResponse = $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxMemo3_I").val();
            var TrxChannel = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtType_I").val();
            var TrxStatus = $("#cmbStatusTicket").val();
            var TrxEskalasi = $("#cmbTujuanEskalasi").val();
            var TrxSLA = $("#hd_sla").val();
            var TrxLayer = $("#cmbEscalationNew").val();

            if (TrxLevel1 === '') {
                alert("Enquiry Type value is empty.");
                hide();
                return false;
            }

            if (TrxLevel2 === '') {
                alert("Enquiry Detail value is empty.");
                hide();
                return false;
            }

            if (TrxLevel3 === '') {
                alert("Reason value is empty.");
                hide();
                return false;
            }

            //Shopee Extend SLA            
            if (TrxPelaporEmail != '') {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (TrxPelaporEmail.match(mailformat)) {

                }
                else {
                    alert("Format email address not valid");
                    return false;
                }
            }
            if (TrxPelaporPhone != '') {
                var numberNya = /^[0-9]+$/;
                if (TrxPelaporPhone.match(numberNya)) {
                    var PhoneLengt = TrxPelaporPhone.toString().length;
                    if (PhoneLengt > '6') {

                    } else {
                        alert("Format phone number is not valid")
                        return false;
                    }
                } else {
                    alert("Phone Number format is numeric")
                    return false;
                }
            }
           
            if (TrxLayer == "0") {
                TrxLayer = "0"
            }
            
            if ($("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_datePengaduan_I").val() === '') {
                alert("Date of Incident is empty")
                return false;
            }
           
            if ($("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_cmbStatusPelapor_I").val() === '') {
                alert("User Status is empty")
                return false;
            }
          
            if ($("#cmbTransaksi").val() === '0') {
                alert("Category is empty")
                return false;
            }
            //if ($("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox1_I").val() === '0') {
            if ($("#ASPxComboBox1").val() === '0' || $("#ASPxComboBox1").val() === '' || $("#ASPxComboBox1").val() === 'Select') {
                alert("Enquiry Type is empty")
                return false;
            }

            //if ($("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox2_I").val() === '0') {
            if ($("#ASPxComboBox2").val() === '0' || $("#ASPxComboBox2").val() === '' || $("#ASPxComboBox2").val() === 'Select') {
                alert("Enquiry Detail is empty")
                return false;
            }
            if ($("#cmbProblem3").val() === '0' || $("#cmbProblem3").val() === '' || $("#cmbProblem3").val() === 'Select') {
                alert("Reason is empty")
                return false;
            }
            if ($("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxMemo2_I").val() === 'User Issue Remark') {
                alert("User issue remark is empty")
                return false;
            }
            if ($("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxMemo3_I").val() === 'Agent Response') {
                alert("Agent Response is empty")
                return false;
            }
            if ($("#cmbTransaksi").val() === 'CAT-10002') {
                if ($("#cmbChannelTicket").val() === '0' || $("#cmbChannelTicket").val() === '' || $("#cmbChannelTicket").val() === 'Select') {
                    alert("Type of Complaint is empty")
                    return false;
                }
            }
            if ($("#cmbStatusTicket").val() === '') {
                alert("Ticket Status is empty")
                return false;
            }
            if ($("#cmbTujuanEskalasi").val() === '') {
                alert("Eskalation unit is empty")
                return false;
            }
            if ($("#hd_sla").val() === '') {
                alert("SLA is empty")
                return false;
            }
         
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/Insert_TransactionTicket",
                    data: "{ TrxUsername: '" + TrxUsername + "', TrxCustomerID: '" + TrxCustomerID + "',TxtThreadID: '" + TxtThreadID + "',TxtAccount: '" + TxtAccount + "',TrxPelapor: '" + encodeData(TrxPelapor) + "',TrxPelaporEmail: '" + TrxPelaporEmail + "',TrxPelaporPhone: '" + TrxPelaporPhone + "',TrxPelaporAddress: '" + encodeData(TrxPelaporAddress) + "',TrxKejadian: '" + TrxKejadian + "',TrxPenyebab: '" + TrxPenyebab + "',TrxPenerimaPengaduan: '" + TrxPenerimaPengaduan + "',TrxStatusPelapor: '" + TrxStatusPelapor + "',TrxSkalaPrioritas: '" + TrxSkalaPrioritas + "',TrxJenisNasabah: '" + TrxJenisNasabah + "',TrxNomorRekening: '" + TrxNomorRekening + "',TrxSumberInformasi: '" + TrxSumberInformasi + "',TrxCategory: '"+TrxCategory+"',TrxLevel1: '" + TrxLevel1 + "',TrxLevel2: '" + TrxLevel2 + "',TrxLevel3: '" + TrxLevel3 + "',TrxComplaint: '" + encodeData(TrxComplaint) + "',TrxResponse: '" + encodeData(TrxResponse) + "',TrxChannel: '" + TrxChannel + "',TrxStatus: '" + TrxStatus + "',TrxEskalasi: '" + TrxEskalasi + "',TrxSLA: '" + TrxSLA + "',TrxExtendCategory: '',TrxLayer: '" + TrxLayer + "',TrxThreadID:'" + TxtThreadID + "', TrxGenesysID:'" + TrxGenesysID + "', TxtContactID:'" + TxtContactID + "', TrxAmount:'0'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert("Ticket has been save");
                                $('#MainContent_hd_ticketid').val(json[i].TrxTicketNumber);
                                $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxButton1').hide();
                                $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxButton3').show();
                                ASPxCallbackPanel3.PerformCallback()
                                document.getElementById('MainContent_ASPxPopupControl1_ASPxCallbackPanel1_IframeInsertTicket').src = "uploadcontrol.aspx?idticketutama=" + json[i].TrxTicketNumber + "";
                            } else {
                                alert(json[i].TrxmsgSystem)
                                return false;
                            }

                        }
                    },

                })
            } else
                return false;
        }
        function execCancelTransaction() {
            $('#cmbTransaksi').empty()
            ASPxCallbackPanel1.PerformCallback()
            ASPxPopupControl1.Hide();
        }
        function execAddTransaction() {
            alert("Add New Transaction")
            $('#MainContent_hd_ticketid').val("");
            document.getElementById('<%= ASPxGridView3.ClientID%>').style.display = 'none';
            document.getElementById('MainContent_ASPxPopupControl1_ASPxCallbackPanel1_IframeInsertTicket').src = "";
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_cmbPenyebab_I").val("");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_cmbStatusPelapor_I").val("");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbskalaprioritas_I").val("");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbjenisnasabah_I").val("");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_txtNomorRekening_I").val("");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbsumberinformasi_I").val("");
            $("#cmbTransaksi").val("");
            $("#cmbProblem").val("");
            $("#MainContent_hd_subcategory2id").val("");
            $("#cmbProblem3").val("");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxMemo2_I").val("");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxMemo3_I").val("");
            $("#cmbChannelTicket").val("");
            $("#cmbStatusTicket").val("");
            $("#cmbTujuanEskalasi").val("");
            $("#hd_sla").val("");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxButton3').hide();
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxButton1').show();
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_BTN_datePengaduan_I').val()
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox2_I').val("")
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox1_I').val("")
        }
        function execPublishTransaction() {
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxCustomerID = $("#MainContent_hd_customerid").val();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/Update_Publish_TransactionTicket",
                    data: "{ TrxUsername: '" + TrxUsername + "', TrxCustomerID: '" + TrxCustomerID + "', TrxGenesysID: '" + TrxGenesysID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        //alert(r.d);
                        window.location.href = "new_inbox.aspx?idpage=1012&status=inbox";
                    },
                    error: function (r) {
                        alert(r.responseText);
                    },
                    failure: function (r) {
                        alert(r.responseText);
                    }
                })
            } else
                return false;
        }

    // <%--Show & Update Data Transaksi--%>
        function ShowUpdateTransaction(text) {
            $("#MainContent_hd_ticketid").val(text);
            var TicketNumber = $("#MainContent_hd_ticketid").val();
            var TrxUserName = $("#MainContent_TrxUserName").val();
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/Select_Data_TransactionTicket",
                data: "{ filterData: '" + TicketNumber + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var tblTickets = "";

                    for (i = 0; i < json.length; i++) {
                        if (json[i].TrxStatus === 'Closed') {
                            ASPxUpdateTransaction.SetVisible(false);
                            ASPxCancelTransaction.SetVisible(false);
                        }
                        //else {
                        //    if (json[i].TrxReleaseUser == 'No') {

                        //    } else {
                        //        if (TrxUserName != json[i].TrxReleaseUser) {
                        //            ASPxUpdateTransaction.SetVisible(false);
                        //            ASPxCancelTransaction.SetVisible(false);
                        //        } 
                        //    }                          
                        if ((json[i].TrxCategoryName) === 'Complaint') {
                            $("#divCategoryType").css("display", "block");
                        } else {
                            $("#divCategoryType").css("display", "none");
                        }

                        var TglKejadian = ("" + json[i].TrxTahun + "-" + json[i].TrxBulan + "-" + json[i].TrxHari + "");
                        var DateKejadian = new Date(TglKejadian);
                        DTUpdateTanggalKejadian.SetDate(DateKejadian);

                        //$("#MainContent_ASPxPopupControl2_DTUpdateTanggalKejadian_I").val(json[i].TrxKejadian);
                        $("#MainContent_ASPxPopupControl2_TxtAmountUpdate_I").val(json[i].TrxAmountDecimal);
                        $("#MainContent_ASPxPopupControl2_CmbUpdatePenyebab_I").val(json[i].TrxPenyebab);
                        // $("#MainContent_ASPxPopupControl2_TxtUpdatePenerimaPengaduan_I").val(json[i].TrxPenerimaPengaduan);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateStatusPelapor_I").val(json[i].TrxStatusPelapor);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateSkalaPrioritas_I").val(json[i].TrxSkalaPrioritas);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateJenisNasabah_I").val(json[i].TrxJenisNasabah);
                        $("#MainContent_ASPxPopupControl2_TxtUpdateNomorRekening_I").val(json[i].TrxNomorRekening);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateSumberInformasi_I").val(json[i].TrxSumberInformasi);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateCategory_I").val(json[i].TrxCategoryName);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateLevel1_I").val(json[i].TrxLevel1Name);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateLevel2_I").val(json[i].TrxLevel2Name);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateLevel3_I").val(json[i].TrxLevel3Name);
                        $("#MainContent_ASPxPopupControl2_MmUpdateComplaint_I").val(json[i].TrxDetailComplaint);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateStatus_I").val(json[i].TrxStatus);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateEscalationUnit_I").val(json[i].TrxEskalasiUnit);
                        $("#MainContent_ASPxPopupControl2_TxtUpdateSLA_I").val(json[i].TrxSLA);
                        $("#MainContent_ASPxPopupControl2_ASPxChannel_I").val(json[i].TrxTicketSourceName);
                        $("#MainContent_ASPxPopupControl2_CmbUpdateChannel_I").val(json[i].TrxExtendName);
                        $("#MainContent_TrxDivisi").val(json[i].TrxDivisi);

                        ASPxPopupControl2.Show();
                        ASPxCallbackPanel4.PerformCallback();
                        ASPxCallbackPanel10.PerformCallback();
                        loaddataiframe(TicketNumber);

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
        function execUpdateTransaction() {
            var TrxTicketNumber = $("#MainContent_hd_ticketid").val();
            var TrxResponse = $("#MainContent_ASPxPopupControl2_MmUpdateResponse_I").val();
            var TrxStatus = $("#MainContent_ASPxPopupControl2_CmbUpdateStatus_I").val();
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxChannel = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtType_I").val();
            var TrxThreadID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var EscalasiUnit = $("#MainContent_ASPxPopupControl2_CmbUpdateEscalationUnit_I").val();
       
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/SelectOrganizationCase",
                data: "{ filterData: '" + EscalasiUnit + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var tblTickets = "";

                    for (i = 0; i < json.length; i++) {
                        if (json[i].Result === 'True') {
                            $("#MainContent_TrxDivisi").val(json[i].OrganizationID);

                            var TrxEscalasiUnit = $("#MainContent_TrxDivisi").val();
                            if (TrxResponse === 'Agent Response') {
                                alert("Agent Response is empty")
                                return false;
                            }
                            if (confirm("Do you want to process?")) {
                                $.ajax({
                                    type: "POST",
                                    url: "WebServiceTransaction.asmx/Update_TransactionTicket",
                                    data: "{ TrxTicketNumber: '" + TrxTicketNumber + "',TrxResponse: '" + encodeData(TrxResponse) + "',TrxStatus: '" + TrxStatus + "',TrxUsername: '" + TrxUsername + "',TrxChannel: '" + TrxChannel + "', TrxThreadID: '" + TrxThreadID + "', TrxGenesysID: '" + TrxGenesysID + "', TrxEscalasiUnit:'" + TrxEscalasiUnit + "', TrxAmount:'0'}",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (data) {
                                        var json = JSON.parse(data.d);
                                        var i, x = "";
                                        var tblTickets = "";

                                        for (i = 0; i < json.length; i++) {
                                            if (json[i].Result === 'True') {
                                                alert("Ticket has been update");
                                                ASPxPopupControl2.Hide();
                                            } else {
                                                alert(json[i].msgSystem)
                                                return false;
                                            }

                                        }
                                    },
                                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                                        console.log(xmlHttpRequest.responseText, textStatus, errorThrown);
                                    }
                                })
                            }
                            else
                                return false;


                        } else {
                            return false;
                        }

                    }
                },
            })

        }
        function execEscalation() {
            var TrxTicketNumber = $("#MainContent_hd_ticketid").val();
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxLayer = $("#cmbEscalationToLayer").val();
            var TrxStatus = $("#MainContent_ASPxPopupControl2_CmbUpdateStatus_I").val();
            var TrxEscalasiUnit = $("#MainContent_TrxDivisi").val();
            var TrxChannel = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtType_I").val();
            var TrxResponse = $("#MainContent_ASPxPopupControl2_ASPxCallbackPanel10_TxtEscalationReason_I").val();
            var TrxThreadID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
         
            if (TrxLayer === '0') {
                alert("Escalation to layer is empty")
                return false;
            }
            if (TrxResponse === '') {
                alert("Escalation Reason is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/EscalationTransactionTicket",
                    data: "{ TrxTicketNumber: '" + TrxTicketNumber + "', TrxResponse: '" + encodeData(TrxResponse) + "', TrxLayer: '" + TrxLayer + "', TrxStatus: '" + TrxStatus + "',TrxUsername: '" + TrxUsername + "',TrxChannel: '" + TrxChannel + "', TrxThreadID: '" + TrxThreadID + "', TrxGenesysID: '" + TrxGenesysID + "', TrxEscalasiUnit:'" + TrxEscalasiUnit + "', TrxAmount:'0'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert("Ticket has been escalation successfully");
                                ASPxPopupControl2.Hide();
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
        function execCancelTransactionUpdate() {
            ASPxPopupControl2.Hide();
        }

    // <%--Load Data Attachment--%>
        function loaddataiframe(id) {
            document.getElementById('MainContent_ASPxPopupControl2_frameattachment').src = "uploadcontrol.aspx?idticketutama=" + id + "";
        }

    // <%--Load Data Category--%>
        function FuncGetWrittenVerbal(id) {
            $("#MainContent_hd_IdWrittenVerbal").val(id);
        }
        function loadCategory() {
            var messageDiv = $('#contentLoadProblem');
            var cmbTrxID = $('#cmbTransaksi option:selected').val();
            //GET ExtendCategorySLA
            var contentExtendCategoryDiv = $('#contentExtendCategory');;
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetExtendCategory",
                data: "{paramQuery: '" + cmbTrxID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";
                    var IDExtendCatgeory;
                    var NameExtendCatgeory;
                    for (i = 0; i < json.length; i++) {
                        IDExtendCatgeory = json[i].ID;
                        NameExtendCatgeory = json[i].NameExtend;
                   
                    }
                    if (cmbTrxID == "CAT-10002") {
                        //$('#simpleModal').modal('show');                       
                        $('#ContentPlaceHolder1_selectSupport').append('<option value="selected">--Select--</option>')
                        $('#divCategoryType').css("display", "block");

                    } else {
                        $('#divCategoryType').css("display", "none");
                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })

            $.ajax({
                type: 'GET',
                async: false,
                url: "AjaxPages/level3.aspx?idinsert=" + cmbTrxID,
                cache: false,
                success: function (result) {
                    //alert(result)
                    messageDiv.empty();
                    messageDiv.append(result);
                    messageDiv.append("<br />");
                    //messageDiv.append("counter = " + counter);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }
            });

            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox1_I").val("--Select--");
            $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox2_I").val("--Select--");
        }

    // <%--Load Data Level 1--%>
        function loadSelectedProblem() {
            var messageDiv = $('#cmbleveldua');
            var cmbTrxID = $('#cmbTransaksi option:selected').val();
            alert("ss" + cmbTrxID);
            $.ajax({
                type: 'GET',
                async: false,
                url: "AjaxPages/level2.aspx?id=" + cmbTrxID,
                cache: false,
                success: function (result) {
                    //alert(result)
                    messageDiv.empty();
                    messageDiv.append(result);
                    messageDiv.append("<br />");
                    //messageDiv.append("counter = " + counter);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }
            });
        }

    // <!-- <%--Load Data Level 3--%> -->
        function loadSelectedProblemtiga() {
            var messageDiv = $('#DivLoadSLA');
            var cmbTrxID = $('#cmbProblem3 option:selected').val();
            var cmbTrxID3 = cmbTrxID.split(';')[0];
            var cmbTrxID2 = cmbTrxID.split(';')[1];
            var cmbTrxID1 = cmbTrxID.split(';')[2];
            if (document.getElementById('MainContent_hd_source').value === '') {
                var channel = "EmailTrue";
            } else {
                var channel = document.getElementById('MainContent_hd_source').value;
            }
            var Category = $('#cmbTransaksi option:selected').val()
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/SelectKategoriPermasalahan",
                data: "{filterData: '" + cmbTrxID3 + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";
                    for (i = 0; i < json.length; i++) {
                        $("#MainContent_hd_subcategory1id").val(json[i].CategoriLevel1ID);
                        $("#MainContent_hd_subcategory2id").val(json[i].CategoriLevel2ID);
                        $("#MainContent_hd_Category3ID").val(cmbTrxID3);
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })

            if ($("#cmbProblem3").val() != "0") {
                $.ajax({
                    type: 'GET',
                    async: false,
                    url: "AjaxPages/level4.aspx?id=" + cmbTrxID3 + "&channel=" + channel + "&Category=" + Category,
                    cache: false,
                    success: function (result) {
                        messageDiv.empty();
                        messageDiv.append(result);
                        messageDiv.append("<br />");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        counter++;
                        messageDiv.empty();
                        messageDiv.append("thrown error: " + thrownError);
                        messageDiv.append("<br />");
                        messageDiv.append("status text: " + xhr.statusText);
                        messageDiv.append("<br />");
                        messageDiv.append("counter = " + counter);
                    }
                });
            } else {
                $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox1_I").val("--Select--");
                $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_ASPxComboBox2_I").val("--Select--");
            }
        }
    
        function loadCategoryType() {
            var cmbTrxID = $('#cmbChannelTicket option:selected').val();
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/GetExtendCategorySLA",
                data: "{filterData: '" + cmbTrxID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";
                    //for (i = 0; i < json.length; i++) {
                    //    $("#hd_sla").val(json[i].TrxExtendSLA);
                    //}
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })

        }

    // <%--Pop up Data Customer--%>
        function ShowUpdateCustomer() {
            $('#MainContent_TrxAction').val("update");
            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_btnSubmit_CD').text("Update")
            showCustomer($('#MainContent_hd_customerid').val());
            ASPxPopupControl4.Show();
        }
   
        function execSubmit() {
            var TrxAction = $('#MainContent_TrxAction').val();
            //var TrxCustomerID = $('#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtAccountID_I').val()
            var TrxUserName = $('#MainContent_TrxUserName').val();
            var TrxName = $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCuxtomerName_I').val();
            var TrxEmail = $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerEmail_I').val();
            var TrxPhone = $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerPhone_I').val();
            var TrxGender = $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_ASPxComboBox3_I').val();
            var TrxBirth = $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_DtCustomerBirth_I').val();
            var TrxCIF = '';
            var TrxNIK = '';
            var TrxAddress = $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_ASPxMemo1_I').val();
            var TrxGenesysID = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtGenesysNumberID_I").val();
            var TrxChannelNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtType_I").val();
            var TxtThreadIDNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtThreadID_I").val();
            var TxtAccountNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtAccount_I").val();
            var TxtContactIDNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtAccountID_I").val();
            var TxtSubjectNya = $("#MainContent_ASPxSplitter1_ASPxFormLayout4_TxtSubject_I").val();

            if (TrxName === '') {
                alert("Name is empty")
                return false;
            }
            if (TrxEmail != '') {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (TrxEmail.match(mailformat)) {
                }
                else {
                    alert("Format email address not valid");
                    return false;
                }

            }
            if (TrxPhone != '') {
                var numberNya = /^[0-9]+$/;
                if (TrxPhone.match(numberNya)) {
                    var PhoneLengt = TrxPhone.toString().length;
                    if (PhoneLengt > '6') {

                    } else {
                        alert("Format phone number is not valid")
                        return false;
                    }
                } else {
                    alert("Phone Number is numeric")
                    return false;
                }
            }
            if (TrxGender == '--Select--' || TrxGender == '' || TrxGender == '0') {
                alert("Gender is empty")
                return false;
            }
            if (TrxCIF == '' || TrxCIF == '0') {

            } else {
                var numberNya = /^[0-9]+$/;
                if (TrxCIF.match(numberNya)) {
                    var CIFLengt = TrxCIF.toString().length;
                    if (CIFLengt == '10') {

                    } else {
                        alert("CIF Number format is 10 digit")
                        return false;
                    }
                } else {
                    alert("CIF Number is numeric")
                    return false;
                }
            }
            if (TrxNIK == '' || TrxNIK == '0') {

            } else {
                var numberNya = /^[0-9]+$/;
                if (TrxNIK.match(numberNya)) {
                    var NIKLengt = TrxNIK.toString().length;
                    if (NIKLengt == '16') {

                    } else {
                        alert("NIK Number format is 16 digit")
                        return false;
                    }
                } else {
                    alert("NIK is numeric")
                    return false;
                }
            }
            if (TrxAddress === '') {
                alert("Address is empty")
                return false;
            }

            if (confirm("Do you want to process?")) {
                if (TrxAction === 'insert') {
                    $.ajax({
                        type: "POST",
                        url: "WebServiceTransaction.asmx/InsertDataCustomer",
                        data: "{TrxCustomerID: '0', TrxName: '" + encodeData(TrxName) + "', TrxEmail: '" + TrxEmail + "', TrxPhone: '" + TrxPhone + "', TrxGender: '" + TrxGender + "', TrxBirth: '" + TrxBirth + "', TrxCIF: '" + TrxCIF + "', TrxNIK: '" + TrxNIK + "', TrxAddress: '" + encodeData(TrxAddress) + "', TrxUserName: '" + TrxUserName + "', TrxMenu: 'TrxMainframe', TrxGenesysID: '" + TrxGenesysID + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var json = JSON.parse(data.d);
                            var i, x = "";
                            var result = "";
                            for (i = 0; i < json.length; i++) {
                                if (json[i].Result == "True") {
                                    alert(json[i].msgSystem)
                                    ASPxPopupControl4.Hide();
                                    //readCustomer(json[i].CustomerID);
                                    window.location.href = "mainframe.aspx?channel=" + TrxChannelNya + "&threadid=" + TxtThreadIDNya + "&genesysid=" + TrxGenesysID + "&account=" + TxtAccountNya + "&accountid=" + TxtContactIDNya + "&subject=" + TxtSubjectNya + "&source=thread&customerid=" + (json[i].CustomerID) + "";
                                } else {
                                    alert(json[i].msgSystem)
                                    return false;
                                }

                            }
                        },
                        error: function (xmlHttpRequest, textStatus, errorThrown) {
                            console.log(xmlHttpRequest.responseText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    })

                } else if (TrxAction === 'update') {
                    var TrxCustomerID = $('#MainContent_hd_customerid').val()
                    $.ajax({
                        type: "POST",
                        url: "WebServiceTransaction.asmx/UpdateDataCustomer",
                        data: "{TrxCustomerID: '" + TrxCustomerID + "', TrxName: '" + encodeData(TrxName) + "', TrxEmail: '" + TrxEmail + "', TrxPhone: '" + TrxPhone + "', TrxGender: '" + TrxGender + "', TrxBirth: '" + TrxBirth + "', TrxCIF: '" + TrxCIF + "', TrxNIK: '" + TrxNIK + "', TrxAddress: '" + encodeData(TrxAddress) + "', TrxUserName: '" + TrxUserName + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var json = JSON.parse(data.d);
                            var i, x = "";
                            var result = "";
                            for (i = 0; i < json.length; i++) {
                                if (json[i].Result == "True") {
                                    alert(json[i].msgSystem)
                                    //readCustomer(TrxCustomerID)
                                    ASPxPopupControl4.Hide();
                                    window.location.href = "mainframe.aspx?channel=" + TrxChannelNya + "&threadid=" + TxtThreadIDNya + "&genesysid=" + TrxGenesysID + "&account=" + TxtAccountNya + "&accountid=" + TxtContactIDNya + "&subject=" + TxtSubjectNya + "&source=thread&customerid=" + TrxCustomerID + "";
                                } else {
                                    alert(json[i].msgSystem)
                                    return false;
                                }

                            }
                        },
                        error: function (xmlHttpRequest, textStatus, errorThrown) {
                            console.log(xmlHttpRequest.responseText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    })

                }

            }
            else
                return false;
        }
        function execCancel() {
            ASPxPopupControl1.Hide();
        }
        function execCancelCustomerEmpty() {
            ASPxPopupControl4.Hide();
        }
    
        function readCustomer(TrxCustomerID) {
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/SelectDataCustomer",
                data: "{TrxCustomerID: '" + TrxCustomerID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";
                    for (i = 0; i < json.length; i++) {
                        if (json[i].Result == "True") {
                            $('#MainContent_hd_customerid').val(json[i].CustomerID);
                            $("#MainContent_hd_Customer_Detail").val(json[i].CustomerID);

                            var Tlahir = ("" + json[i].CustomerTahunlahir + "-" + json[i].CustomerBulanlahir + "-" + json[i].CustomerHarilahir + "");
                            var dateLahir = new Date(Tlahir)
                            dtBirth.SetDate(dateLahir);
                            $('#MainContent_ASPxSplitter1_formLayout_TxtName_I').val(json[i].CustomerName)
                            $('#MainContent_ASPxSplitter1_formLayout_TxtEmail_I').val(json[i].CustomerEmail);
                            $('#MainContent_ASPxSplitter1_formLayout_TxtPhone_I').val(json[i].CustomerHP);
                            $('#MainContent_ASPxSplitter1_formLayout_cmbGender_I').val(json[i].CustomerGender);
                            $('#MainContent_ASPxSplitter1_formLayout_TxtCIF_I').val(json[i].CustomerCIF);
                            $('#MainContent_ASPxSplitter1_formLayout_TxtNIK_I').val(json[i].CustomerNIK);
                            $('#MainContent_ASPxSplitter1_formLayout_TxtAddress_I').val(json[i].CustomerAddress);

                        } else {

                        }
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
    
        function showCustomer(val) {
            $('#MainContent_hd_customerid').val(val);
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/SelectDataCustomer",
                data: "{TrxCustomerID: '" + $('#MainContent_hd_customerid').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";
                    for (i = 0; i < json.length; i++) {
                        if (json[i].Result == "True") {

                            var Tlahir = ("" + json[i].CustomerTahunlahir + "-" + json[i].CustomerBulanlahir + "-" + json[i].CustomerHarilahir + "");
                            var dateLahir = new Date(Tlahir);
                            DtCustomerBirth.SetDate(dateLahir);
                            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCuxtomerName_I').val(json[i].CustomerName)
                            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerEmail_I').val(json[i].CustomerEmail);
                            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerPhone_I').val(json[i].CustomerHP);
                            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_ASPxComboBox3_I').val(json[i].CustomerGender);
                            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_ASPxMemo1_I').val(json[i].CustomerAddress);
                            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerCIF_I').val(json[i].CustomerCIF);
                            $('#MainContent_ASPxPopupControl4_ASPxCallbackPanel8_TxtCustomerNIK_I').val(json[i].CustomerNIK);
                        } else {

                        }
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
  
        function ASPxGridView6_SelectionChanged(s, e) {
            s.GetSelectedFieldValues("TicketNumber", GetSelectedFieldValuesCallback);
        }
        function GetSelectedFieldValuesCallback(values) {
            $('#MainContent_TrxTicketNumber').val(values);
        }
   
        function ShowParentChild(values) {
            $("#MainContent_hd_ticketid").val(values);
            $("#dx_LabelTicketNumber").text(values);
            ASPxPopupControl6.Show();
            ASPxGridView6.Refresh();
        }
        function showParent() {
            ASPxGridView8.Refresh();
        }
    
        function execParentNumber() {
            var TrxTicketNumberFrom = $("#MainContent_hd_ticketid").val();
            var TrxTicketNumberTo = $("#MainContent_TrxTicketNumber").val();
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxReason = $("#dxmReason_I").val();
            if (TrxTicketNumberTo === '') {
                alert("Please, Select your data transaction")
                return false;
            }
            if (TrxReason === 'Reason Parent Ticket') {
                alert("Reason parent is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/ParentChildNumberID",
                    data: "{ TrxTicketNumberTo: '" + TrxTicketNumberTo + "', TrxUserName: '" + TrxUsername + "', TrxTicketNumberFrom: '" + TrxTicketNumberFrom + "', TrxReason: '" + encodeData(TrxReason) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                $("#MainContent_TrxTicketNumber").val("");
                                $("#dxmReason_I").val("");
                                alert(json[i].msgSystem);
                                ASPxPopupControl6.Hide();
                            } else {
                                alert(json[i].msgSystem);
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
    
        function execCancelParentNumber() {
            ASPxPopupControl6.Hide();
        }
    
        function ShowUpdateParentID(v) {
            $("#MainContent_TrxTicketNumberUpdate").val(v);
            var TrxUsername = $("#MainContent_TrxUserName").val();
            var TrxTicketNumberUpdate = $("#MainContent_TrxTicketNumberUpdate").val();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/DeleteParentChildNumberID",
                    data: "{ TrxTicketNumber: '" + TrxTicketNumberUpdate + "', TrxUsername: '" + TrxUsername + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem);
                                ASPxPopupControl6.Hide();
                            } else {
                                alert(json[i].msgSystem);
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="TrxAction" runat="server" />
    <asp:HiddenField ID="TrxUserName" runat="server" />
    <asp:HiddenField ID="TrxDivisi" runat="server" />
    <asp:HiddenField ID="hd_searching_journey" runat="server" />
    <asp:HiddenField ID="hd_bulan" runat="server" />
    <asp:HiddenField ID="hd_tahun" runat="server" />
    <asp:HiddenField ID="hd_source" runat="server" />
    <asp:HiddenField ID="hd_ivcid" runat="server" />
    <asp:HiddenField ID="hd_subcategory1id" runat="server" />
    <asp:HiddenField ID="hd_subcategory2id" runat="server" />
    <asp:HiddenField ID="hd_channel_id" runat="server" />
    <asp:HiddenField ID="hd_customerid" runat="server" />
    <asp:HiddenField ID="hd_ticketid" runat="server" />
    <asp:HiddenField ID="hdemail_customer" runat="server" />
    <asp:HiddenField ID="hd_Category3ID" runat="server" />
    <asp:HiddenField ID="hd_send_email" runat="server" />
    <asp:HiddenField ID="hd_Customer_Detail" runat="server" />
    <asp:HiddenField ID="hd_username" runat="server" />
    <asp:HiddenField ID="hd_NomorRekening" runat="server" />
    <asp:HiddenField ID="hd_IdWrittenVerbal" runat="server" />
    <asp:HiddenField ID="TrxNomorRekening" runat="server" />
    <asp:HiddenField ID="TrxTicketNumber" runat="server" />
    <asp:HiddenField ID="TrxTicketNumberUpdate" runat="server" />
    

    <div style="overflow-y: scroll; height: 400px;">
        <dx:ASPxButton ID="ASPxButton5" runat="server" Theme="Metropolis" Text="Save Services" Width="100%" AutoPostBack="false" Visible="false"
            HoverStyle-BackColor="#EE4D2D" Height="30px">
            <ClientSideEvents Click="function(s, e) { execServices('Save'); }" />
        </dx:ASPxButton>
        <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" Width="100%" FullscreenMode="true" Height="100%" ResizingMode="Live"
            AllowResize="true" ShowCollapseBackwardButton="true" ShowCollapseForwardButton="true" Orientation="Horizontal">
            <Panes>
                <dx:SplitterPane Size="25%" Name="listBoxContainer" ShowCollapseBackwardButton="True" AutoHeight="true" PaneStyle-Paddings-PaddingRight="0px">
                    <ContentCollection>
                        <dx:SplitterContentControl ID="SplitterContentControl1" runat="server" Width="100%">
                            <dx:ASPxFormLayout ID="formLayout" runat="server" AlignItemCaptionsInAllGroups="True" Width="100%" Theme="Metropolis">
                                <Items>
                                    <dx:LayoutGroup Caption="Customer Information" Width="100%" GroupBoxStyle-Caption-Font-Bold="true" GroupBoxStyle-Caption-Font-Size="14px" GroupBoxStyle-Caption-ForeColor="#EE4D2D">
                                        <Items>
                                            <dx:LayoutItem Caption="" FieldName="" ShowCaption="False">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer2" runat="server" SupportsDisabledAttribute="True">
                                                        <label style="font-weight: bold; font-size: 10px;">Name</label>
                                                        <dx:ASPxTextBox ID="TxtName" ClientInstanceName="TxtName" runat="server" Width="100%" Height="30px" Theme="Metropolis" ReadOnly="true" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Phone" FieldName="BirthDate" ShowCaption="False">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer4" runat="server" SupportsDisabledAttribute="True">
                                                        <label style="font-weight: bold; font-size: 10px;">Phone Number</label>
                                                        <dx:ASPxTextBox ID="TxtPhone" runat="server" Width="100%" Height="30px" Theme="Metropolis" ReadOnly="true" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Email" FieldName="Email" ShowCaption="False">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server" SupportsDisabledAttribute="True">
                                                        <label style="font-weight: bold; font-size: 10px;">Email Address</label>
                                                        <dx:ASPxTextBox ID="TxtEmail" runat="server" Width="100%" Height="30px" Theme="Metropolis" ReadOnly="true" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Gender" FieldName="BirthDate" ShowCaption="False">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer10" runat="server" SupportsDisabledAttribute="True">
                                                        <label style="font-weight: bold; font-size: 10px;">Gender</label>
                                                        <dx:ASPxComboBox ID="cmbGender" runat="server" Width="100%" Height="30px" Theme="Metropolis" ReadOnly="true">
                                                            <Items>
                                                                <dx:ListEditItem Text="Male" />
                                                                <dx:ListEditItem Text="Female" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Email" FieldName="BirthDate" ShowCaption="False">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer3" runat="server" SupportsDisabledAttribute="True">
                                                        <label style="font-weight: bold; font-size: 10px;">Address</label>
                                                        <dx:ASPxMemo ID="TxtAddress" runat="server" Width="100%" Height="100px" Theme="Metropolis" ReadOnly="true" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Email" FieldName="BirthDate" ShowCaption="False">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer9" runat="server" SupportsDisabledAttribute="True">
                                                        <dx:ASPxButton ID="EditCustomer" runat="server" Theme="Metropolis" Text="Data Customer" Width="100%" AutoPostBack="false" HoverStyle-BackColor="#EE4D2D">
                                                            <ClientSideEvents Click="function(s, e) { ShowUpdateCustomer(); }" />
                                                            <Image Url="img/icon/Text-Edit-icon2.png" Width="16px" Height="16px" />
                                                        </dx:ASPxButton>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
                <dx:SplitterPane Size="75%" ShowCollapseBackwardButton="True" AutoHeight="true" PaneStyle-Paddings-PaddingRight="0px">
                    <ContentCollection>
                        <dx:SplitterContentControl ID="SplitterContentControl2" runat="server">
                            <dx:ASPxFormLayout ID="ASPxFormLayout5" runat="server" Width="100%" Theme="Office2010Silver">
                                <Items>
                                    <dx:LayoutGroup Caption="BUTTON TOOLBAR" ShowCaption="False" ColCount="3" SettingsItemCaptions-Location="Top" GroupBoxStyle-Caption-Font-Bold="true" GroupBoxStyle-Caption-Font-Size="14px" GroupBoxStyle-Caption-ForeColor="#EE4D2D">
                                        <Items>
                                            <dx:LayoutItem CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" ShowCaption="False" ColSpan="3" HorizontalAlign="Right">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <dx:ASPxButton ID="AddTicket" runat="server" Theme="Metropolis" Text="Add Ticket" Width="100%" AutoPostBack="false" HoverStyle-BackColor="#EE4D2D">
                                                                    <ClientSideEvents Click="function(s, e) { ShowLoginWindow(); }" />
                                                                    <Image Url="img/icon/Apps-text-editor-icon2.png" Width="16px" Height="16px" />
                                                                </dx:ASPxButton>
                                                            </div>
                                                            <div class="col-md-6"></div>
                                                            <div class="col-md-3">
                                                                <a href="#" class="btn btn-sm btn-default pull-right" data-toggle="modal" onclick="showEmail()"><i class="fa fa-comments-o" title="Preview Document"></i> View Interaction</a>
                                                            </div>
                                                        </div>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>

                            <dx:ASPxFormLayout ID="ASPxFormLayout4" runat="server" Width="100%" Theme="Office2010Silver">
                                <Items>
                                    <dx:LayoutGroup Caption="Channel Interaction" ShowCaption="True" ColCount="3" SettingsItemCaptions-Location="Top" GroupBoxStyle-Caption-Font-Bold="true" GroupBoxStyle-Caption-Font-Size="14px" GroupBoxStyle-Caption-ForeColor="#EE4D2D">
                                        <Items>
                                            <dx:LayoutItem Caption="Channel" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" Width="30%">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="TxtType" runat="server" Width="100%" Height="30px" Theme="Metropolis" Text="Email" ReadOnly="False" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Interaction ID" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" Width="30%">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="TxtGenesysNumberID" runat="server" Width="100%" Height="30px" Theme="Metropolis" ReadOnly="False" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Account" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" Width="30%">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="TxtAccount" runat="server" Width="100%" Height="30px" Theme="Metropolis" ReadOnly="False" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Subject" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" ColSpan="3">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="TxtSubject" runat="server" Width="100%" Height="30px" Theme="Metropolis" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>
                            <br />
                            <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" Width="100%" Theme="Office2010Silver">
                                <Items>
                                    <dx:LayoutGroup Caption="Information From" ColCount="3" SettingsItemCaptions-Location="Top" GroupBoxStyle-Caption-Font-Bold="true" GroupBoxStyle-Caption-Font-Size="14px" GroupBoxStyle-Caption-ForeColor="#EE4D2D">
                                        <Items>
                                            <dx:LayoutItem CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" ShowCaption="False" ColSpan="3" HorizontalAlign="Right">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxCheckBox ID="ASPxCheckBox1" ClientInstanceName="ASPxCheckBox1" runat="server" AutoPostBack="false" Visible="true" Theme="Metropolis">
                                                            <ClientSideEvents CheckedChanged="OnCheckedChanged" />
                                                        </dx:ASPxCheckBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Name" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" Width="30%">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="TxtPelapor" runat="server" Width="100%" Height="30px" Theme="Metropolis" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Email Address" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" Width="30%">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="TxtPelaporEmail" runat="server" Width="100%" Height="30px" Theme="Metropolis">
                                                            <%--<ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                                                <RequiredField IsRequired="True" ErrorText="Required" />
                                                                <RegularExpression ErrorText="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*.\w+([-.]\w+)*" />
                                                            </ValidationSettings>--%>
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Phone Number" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px" Width="30%">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="TxtPelaporPhone" runat="server" Width="100%" Height="30px" Theme="Metropolis">
                                                            <ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                                                <RequiredField IsRequired="True" ErrorText="Required" />
                                                                <RegularExpression ErrorText="Please Enter Numbers Only" ValidationExpression="^\d+$" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Address" ColSpan="3" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Size="10px">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer5" runat="server">
                                                        <dx:ASPxMemo ID="TxtPelaporAddress" runat="server" Width="100%" Rows="3" Theme="Metropolis" />
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>
                            <br />
                            
                            <dx:ASPxFormLayout ID="ASPxFormLayout3" runat="server" Width="100%" Theme="Office2010Silver">
                                <Items>
                                    <dx:LayoutGroup Caption="History Transaction" ColCount="3" SettingsItemCaptions-Location="Top" GroupBoxStyle-Caption-Font-Bold="true" GroupBoxStyle-Caption-Font-Size="14px" GroupBoxStyle-Caption-ForeColor="#EE4D2D">
                                        <Items>
                                            <dx:LayoutItem Caption="Name" ShowCaption="False">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel6" runat="server" Width="100%" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel6">
                                                            <PanelCollection>
                                                                <dx:PanelContent>
                                                                    <div class="col-md-8"></div>
                                                                    <div class="col-md-4">
                                                                        <div class="text-right" style="margin-right: -20px;">
                                                                            <label style="font-size: x-small;">All History Transaction</label>
                                                                            <dx:ASPxCheckBox ID="ChkAllTransaction" ClientInstanceName="ChkAllTransaction" runat="server" AutoPostBack="false" Visible="true"
                                                                                Theme="Metropolis">
                                                                                <ClientSideEvents CheckedChanged="Check" />
                                                                            </dx:ASPxCheckBox>
                                                                        </div>
                                                                    </div>
                                                                    <dx:ASPxGridView ID="ASPxGridView2" Visible="true" ClientInstanceName="ASPxGridView2" runat="server" KeyFieldName="ID"
                                                                        SettingsPager-PageSize="10" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                                                                        <SettingsPager>
                                                                            <AllButton Text="All">
                                                                            </AllButton>
                                                                            <NextPageButton Text="Next &gt;">
                                                                            </NextPageButton>
                                                                            <PrevPageButton Text="&lt; Prev">
                                                                            </PrevPageButton>
                                                                            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                                        </SettingsPager>
                                                                        <SettingsEditing Mode="Inline" />
                                                                        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowGroupPanel="true"
                                                                            ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true" />
                                                                        <SettingsBehavior ConfirmDelete="true" />
                                                                        <Columns>
                                                                            <dx:GridViewDataTextColumn Caption="Action" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="60px">
                                                                                <DataItemTemplate>
                                                                                    <a href="#" onclick="ShowUpdateTransaction('<%# Eval("TicketNumber")%>')">
                                                                                        <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" ToolTip="Update Transaction" />
                                                                                    </a>
                                                                                </DataItemTemplate>
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                                                                                PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Settings-AutoFilterCondition="Contains" Width="200px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Channel" FieldName="TicketSourceName" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Category" FieldName="CategoryName" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Enquiry Type" FieldName="SubCategory1Name" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Enquiry Detail" FieldName="SubCategory2Name" Settings-AutoFilterCondition="Contains" Width="300px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Reason" FieldName="SubCategory3Name" Settings-AutoFilterCondition="Contains" Width="300px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" Settings-AutoFilterCondition="Contains" Width="160px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Parent Case Number" FieldName="ParentNumberID" HeaderStyle-HorizontalAlign="left" Width="130px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Date" FieldName="DateCreate" Settings-AutoFilterCondition="Contains" Width="130px"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="12" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px" ToolTip="Parent Child Ticket">
                                                                                <DataItemTemplate>
                                                                                    <a href="#" id="ParentID" onclick="ShowParentChild('<%# Eval("TicketNumber")%>')">
                                                                                        <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" />
                                                                                    </a>
                                                                                </DataItemTemplate>
                                                                            </dx:GridViewDataTextColumn>
                                                                        </Columns>
                                                                    </dx:ASPxGridView>
                                                                </dx:PanelContent>
                                                            </PanelCollection>
                                                        </dx:ASPxCallbackPanel>
                                                        <asp:SqlDataSource ID="dsHistory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from tTicket"></asp:SqlDataSource>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
            </Panes>
        </dx:ASPxSplitter>
    </div>

    <%--Pop up data Add New Transaction--%>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="true" Width="1100px"
        closeonescape="true" ShowPageScrollbarWhenModal="true"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="true" Theme="SoftOrange" Height="700px"
        ShowFooter="True" HeaderText="Form Add Data Transaction" FooterText="" AutoUpdatePosition="true" ScrollBars="Vertical">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="1030px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="panel-tab clearfix">
                                <ul class="tab-bar">
                                    <li class="active"><a href="#TabTicketing" data-toggle="tab"><i class="fa fa-file-text"></i>&nbsp;<strong>Data Transaction Ticket</strong></a></li>
                                    <li><a href="#TabAttachment" data-toggle="tab"><i class="fa fa-hdd-o"></i>&nbsp;<strong>Data Attachment Ticket</strong></a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="TabTicketing">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Date Created</label>
                                                        <dx:ASPxDateEdit AutoPostBack="false" ID="BTN_datePengaduan" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                                                            <ClientSideEvents Init="function(s,e){ s.SetDate(new Date());}" />
                                                        </dx:ASPxDateEdit>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6"></div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Escalation Layer</label>
                                                        <select class="Metropolis" id="cmbEscalationNew" name="cmbEscalationNew" style="width: 100%; height: 30px;">
                                                            <option value="0" selected="selected">--Select--</option>
                                                            <asp:Literal runat="server" ID="litEscalationNew"></asp:Literal>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Category</label>
                                                        <select id="cmbTransaksi" name="cmbTransaksi" onchange="getWS_CategoryType(1);" style="width: 100%; height: 32px; font-size: 12px;" required class="form-control">
                                                            <option value="0" selected="selected">Select</option>
                                                            <asp:Literal runat="server" ID="litTrx"></asp:Literal>
                                                        </select>
                                                        <%--<select name="cmbTransaksi" id="cmbTransaksi" onchange="loadCategory();" class="Metropolis" style="width: 100%; height: 30px;">
                                                            <option value="">--Select--</option>
                                                            <asp:Literal runat="server" ID="litTrx"></asp:Literal>
                                                        </select>--%>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Enquiry Type</label>
                                                        <select id="ASPxComboBox1" name="ASPxComboBox1" onchange="getWS_CategoryTypeDetail(1);" style="width: 100%; height: 32px; font-size: 12px;" required class="form-control">
                                                            <option value="" selected="selected">Select</option>
                                                        </select>
                                                        <%--<dx:ASPxComboBox ID="ASPxComboBox1" ClientInstanceName="ASPxComboBox1" runat="server" Theme="Default"
                                                            ReadOnly="true" Width="100%" Height="30px">
                                                            <ItemStyle>
                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                </HoverStyle>
                                                            </ItemStyle>
                                                        </dx:ASPxComboBox>--%>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Enquiry Detail</label>
                                                        <select name="ASPxComboBox2" id="ASPxComboBox2" onchange="getWS_CategoryTypeReason(1);" style="width: 100%; height: 32px; font-size: 12px;" required class="form-control">
                                                            <option value="" selected="selected">Select</option>
                                                        </select>
                                                        <%--<dx:ASPxComboBox ID="ASPxComboBox2" ClientInstanceName="ASPxComboBox2" runat="server" Theme="Metropolis"
                                                            ReadOnly="true" Width="100%" Height="30px" Cursor="not-allowed">
                                                            <ItemStyle>
                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                </HoverStyle>
                                                            </ItemStyle>
                                                        </dx:ASPxComboBox>--%>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Reason</label>
                                                        <select name="cmbProblem3" id="cmbProblem3" onchange="loadSelectedProblemtiga();" style="width: 100%; height: 32px; font-size: 12px;" required class="form-control">
                                                            <option value="">Select</option>
                                                        </select>
                                                        <%-- <div id="contentLoadProblem" name="contentLoadProblem">
                                                            <label>Reason</label>
                                                            <select class="Metropolis" id="cmbProblem3" onchange="loadSelectedProblemtiga()" style="width: 100%; height: 30px;">
                                                                <option value="0" selected="selected">--Select--</option>
                                                            </select>
                                                        </div>--%>
                                                    </div>
                                                </div>
                                            </div>
                                          
                                            <div id="DivLoadSLA" name="DivLoadSLA" style="margin-left: -15px;"></div>
                                            <dx:ASPxCallbackPanel ID="ASPxCallbackPanel2" runat="server" Width="100%" ClientInstanceName="ASPxCallbackPanel2">
                                                <PanelCollection>
                                                    <dx:PanelContent>
                                                        <asp:HiddenField ID="hd_department" runat="server" />
                                                        <div class="row">
                                                            <div id="Div2" style="height: 0px; visibility: hidden" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label>Status</label>
                                                                    <dx:ASPxComboBox Visible="false" ID="cmb_status" ClientInstanceName="cmb_status" runat="server" TextField="status" Theme="MetropolisBlue" CssClass="form-control chzn-select"
                                                                        ValueField="status" ItemStyle-HoverStyle-BackColor="#F37021" DataSourceID="sql_cmb_status"
                                                                        Width="100%">
                                                                        <Columns>
                                                                            <dx:ListBoxColumn Caption="Status" FieldName="status" />
                                                                        </Columns>
                                                                        <ItemStyle>
                                                                            <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                                                            </HoverStyle>
                                                                        </ItemStyle>
                                                                    </dx:ASPxComboBox>
                                                                    <asp:SqlDataSource ID="sql_cmb_status" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                                        SelectCommand="select * from mStatus"></asp:SqlDataSource>
                                                                </div>
                                                            </div>
                                                            <div id="Div3" style="height: 0px; visibility: hidden" runat="server" visible="false">
                                                                <label>Channel</label>
                                                                <dx:ASPxComboBox Visible="false" ID="cmb_channel" ClientInstanceName="aspx_channel" runat="server" TextField="Name" Theme="MetropolisBlue" CssClass="form-control chzn-select"
                                                                    ValueField="Name" ItemStyle-HoverStyle-BackColor="#F37021" DataSourceID="sql_source_type"
                                                                    Width="100%">
                                                                    <Columns>
                                                                        <dx:ListBoxColumn Caption="Channel" FieldName="Name" />
                                                                    </Columns>
                                                                    <ItemStyle>
                                                                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                                                        </HoverStyle>
                                                                    </ItemStyle>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sql_source_type" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                                    SelectCommand="select * from mSourceType where NA='Y'"></asp:SqlDataSource>
                                                                <asp:TextBox CssClass="form-control" Visible="false" ID="txt_channel" runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                            <div id="Div5" style="height: 0px; visibility: hidden" runat="server" visible="false">
                                                                <label>SLA</label>
                                                                <asp:TextBox CssClass="form-control" ID="txt_sla" runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxCallbackPanel>
                                            <asp:SqlDataSource ID="sql_list_ticket" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>

                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <dx:ASPxMemo ID="ASPxMemo2" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="User Issue Remark" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <dx:ASPxMemo ID="ASPxMemo3" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="Agent Response" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOptionalData">
                                                            Optional Data
                                                            <span class="badge badge-danger pull-right"><i class="fa fa-plus"></i></span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOptionalData" class="panel-collapse collapse" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-3">
                                                                <div class="form-group">
                                                                    <label>Priority Scale</label>
                                                                    <dx:ASPxComboBox ID="cmbskalaprioritas" ClientInstanceName="cmbskalaprioritas" runat="server" TextField="Name" 
                                                                        Theme="Metropolis" CssClass="form-control"
                                                                        ValueField="Name" IncrementalFilteringMode="Contains" DataSourceID="sqlskalaprioritas" AutoPostBack="false"
                                                                        Width="100%">
                                                                        <ItemStyle></ItemStyle>
                                                                    </dx:ASPxComboBox>
                                                                    <asp:SqlDataSource ID="sqlskalaprioritas" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from BTN_SkalaPrioritas where NA='Y'"></asp:SqlDataSource>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3">
                                                                <div class="form-group">
                                                                    <label>Source of Information / Channel</label>
                                                                    <dx:ASPxComboBox ID="cmbsumberinformasi" ClientInstanceName="cmbsumberinformasi" runat="server" TextField="SumberInformasi" 
                                                                        Theme="Metropolis" CssClass="form-control"
                                                                        ValueField="SumberInformasi" IncrementalFilteringMode="Contains" DataSourceID="sql_sumberinformasi" AutoPostBack="false"
                                                                        Width="100%">
                                                                        <ItemStyle></ItemStyle>
                                                                    </dx:ASPxComboBox>
                                                                    <asp:SqlDataSource ID="sql_sumberinformasi" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from BTN_SumberInformasi where NA='Y'"></asp:SqlDataSource>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr />
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <dx:ASPxButton ID="ASPxButtonAddTransaction" runat="server" Theme="Metropolis" Text="Add New Transaction" Width="100%" AutoPostBack="false"
                                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                        <ClientSideEvents Click="function(s, e) { execAddTransaction(); }" />
                                                    </dx:ASPxButton>
                                                </div>
                                                <div class="col-md-6"></div>
                                                <div class="col-md-2">
                                                    <dx:ASPxButton ID="ASPxButton1" runat="server" Theme="Metropolis" Text="Save" Width="100%" AutoPostBack="false"
                                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                        <ClientSideEvents Click="function(s, e) { execSimpanTransaction(); }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="ASPxButton3" runat="server" Theme="Metropolis" Text="Publish" Width="100%" AutoPostBack="false"
                                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                        <ClientSideEvents Click="function(s, e) { execPublishTransaction(); }" />
                                                    </dx:ASPxButton>
                                                </div>
                                                <div class="col-md-2">
                                                    <dx:ASPxButton ID="ASPxButton2" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                        <ClientSideEvents Click="function(s, e) { execCancelTransaction(); }" />
                                                    </dx:ASPxButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-8">
                                            <dx:ASPxCallbackPanel ID="ASPxCallbackPanel3" runat="server" Width="100%" ClientInstanceName="ASPxCallbackPanel3">
                                                <PanelCollection>
                                                    <dx:PanelContent>
                                                        <dx:ASPxGridView ID="ASPxGridView3" ClientInstanceName="ASPxGridView3" runat="server" KeyFieldName="ID" Visible="false"
                                                            Width="1030px" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                            SettingsPager-PageSize="5">
                                                            <SettingsPager>
                                                                <AllButton Text="All">
                                                                </AllButton>
                                                                <NextPageButton Text="Next &gt;">
                                                                </NextPageButton>
                                                                <PrevPageButton Text="&lt; Prev">
                                                                </PrevPageButton>
                                                                <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                            </SettingsPager>
                                                            <SettingsEditing Mode="Inline" />
                                                            <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="false"
                                                                ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                                            <SettingsBehavior ConfirmDelete="true" />
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Action" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                                                                    <DataItemTemplate>
                                                                        <a href="#" onclick="Showdataupdateticket('<%# Eval("ID")%>','<%# Eval("TicketNumber")%>','<%# Eval("CategoryName")%>','<%# Eval("SubCategory1Name")%>','<%# Eval("SubCategory2Name")%>','<%# Eval("SubCategory3Name")%>','<%# Eval("DetailComplaint")%>','<%# Eval("Status")%>','<%# Eval("sla")%>','<%# Eval("Dispatch_user")%>','<%# Eval("SkalaPrioritas")%>','<%# Eval("JenisNasabah")%>','<%# Eval("NomorRekening")%>','<%# Eval("kirimemail")%>','<%# Eval("TicketPosition")%>','<%# Eval("SumberInformasi")%>')">
                                                                            <asp:Image ImageUrl="img/icon/Actions-edit-clear-icon2.png" ID="img_insert" runat="server" ToolTip="Delete Ticket" />
                                                                        </a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                                                                    PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" Width="160"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Channel" FieldName="TicketSourceName" Width="160"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Category" FieldName="CategoryName" Settings-AutoFilterCondition="Contains" Width="160"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Enquiry Type" FieldName="SubCategory1Name" Settings-AutoFilterCondition="Contains" Width="160"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Enquiry Detail" FieldName="SubCategory2Name" Settings-AutoFilterCondition="Contains" Width="160"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Reason" FieldName="SubCategory3Name" Settings-AutoFilterCondition="Contains" Width="300"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" Width="160"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Date" FieldName="DateCreate" Width="160"></dx:GridViewDataTextColumn>
                                                            </Columns>
                                                        </dx:ASPxGridView>
                                                        <asp:SqlDataSource ID="dsTransaction" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                                                    </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxCallbackPanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="TabAttachment">
                                    <iframe id="IframeInsertTicket" style="width: 100%; height: 560px; border: none; overflow: hidden;" runat="server"></iframe>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <%--Pop up data update transaction--%>
    <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2" runat="server" CloseAction="CloseButton" Modal="true" Width="1100px"
        closeonescape="true" ShowPageScrollbarWhenModal="true"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="true" Theme="SoftOrange" Height="630px"
        ShowFooter="True" HeaderText="Form Update Data Transaction" FooterText="" AutoUpdatePosition="true" ScrollBars="None">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div class="panel-tab clearfix">
                    <ul class="tab-bar">
                        <li class="active"><a href="#Tab1" data-toggle="tab"><i class="fa fa-file-text"></i>&nbsp;<strong>Data Transaction Ticket</strong></a></li>
                        <li><a href="#Tab2" data-toggle="tab"><i class="fa fa-group"></i>&nbsp;<strong>Data Transaction Ticket</strong></a></li>
                        <li><a href="#dataEscalation" data-toggle="tab"><i class="fa fa-share-square-o"></i>&nbsp;<strong>Data Escalation Ticket</strong></a></li>
                        <li><a href="#Tab3" data-toggle="tab"><i class="fa fa-hdd-o"></i>&nbsp;<strong>Data Attachment Ticket</strong></a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="Tab1">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Date of Transaction</label>
                                            <dx:ASPxDateEdit AutoPostBack="false" ID="DTUpdateTanggalKejadian" runat="server" CssClass="form-control input-sm" Width="100%"
                                                EditFormatString="yyyy-MM-dd" ReadOnly="true" ClientInstanceName="DTUpdateTanggalKejadian">
                                                <%--<ClientSideEvents Init="function(s,e){ s.SetDate(new Date());}" />--%>
                                            </dx:ASPxDateEdit>
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-3">
                                        <label>Channel</label>
                                        <dx:ASPxComboBox ID="ASPxChannel" ClientInstanceName="ASPxChannel" runat="server" Theme="Metropolis" Height="30px"
                                            ItemStyle-HoverStyle-BackColor="#0076c4" AutoPostBack="false" Enabled="false" Cursor="not-allowed"
                                            Width="100%">
                                            <ItemStyle>
                                                <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                                </HoverStyle>
                                            </ItemStyle>
                                        </dx:ASPxComboBox>
                                    </div>
                                </div>
                                <hr />
                                
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <dx:ASPxComboBox ID="CmbUpdateCategory" ClientInstanceName="CmbUpdateCategory" runat="server" Theme="Default"
                                                CssClass="form-control chzn-select" ReadOnly="true" Width="100%" Cursor="not-allowed">
                                                <ItemStyle>
                                                    <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                    </HoverStyle>
                                                </ItemStyle>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Enquiry Type</label>
                                            <dx:ASPxComboBox ID="CmbUpdateLevel1" ClientInstanceName="CmbUpdateLevel1" runat="server" Theme="Default"
                                                CssClass="form-control chzn-select" ReadOnly="true" Width="100%" Cursor="not-allowed">
                                                <ItemStyle>
                                                    <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                    </HoverStyle>
                                                </ItemStyle>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Enquiry Detail</label>
                                            <dx:ASPxComboBox ID="CmbUpdateLevel2" ClientInstanceName="CmbUpdateLevel2" runat="server" Theme="Default"
                                                CssClass="form-control chzn-select" ReadOnly="true" Width="100%" Cursor="not-allowed">
                                                <ItemStyle>
                                                    <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                    </HoverStyle>
                                                </ItemStyle>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <div class="form-group">
                                            <label>Reason</label>
                                            <dx:ASPxComboBox ID="CmbUpdateLevel3" ClientInstanceName="CmbUpdateLevel3" runat="server" Theme="Default"
                                                CssClass="form-control chzn-select" ReadOnly="true" Width="100%" Cursor="not-allowed">
                                                <ItemStyle>
                                                    <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                    </HoverStyle>
                                                </ItemStyle>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label>Ticket Status</label>
                                        <dx:ASPxComboBox ID="CmbUpdateStatus" ClientInstanceName="CmbUpdateStatus" runat="server" TextField="status" Theme="Default"
                                            ValueField="status" DataSourceID="ds_Status" CssClass="form-control chzn-select"
                                            Width="100%">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="Status" FieldName="status" />
                                            </Columns>
                                            <ItemStyle>
                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                </HoverStyle>
                                            </ItemStyle>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="ds_Status" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mStatus where NA='Y'"></asp:SqlDataSource>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label>Escalation Unit</label>
                                        <dx:ASPxComboBox ID="CmbUpdateEscalationUnit" ClientInstanceName="CmbUpdateEscalationUnit" runat="server" TextField="ORGANIZATION_ID"
                                            Theme="Default" CssClass="form-control chzn-select"
                                            ValueField="ORGANIZATION_NAME" DataSourceID="ds_Unit"
                                            Width="100%">
                                            <Columns>
                                                <%--<dx:ListBoxColumn Caption="ID" FieldName="ORGANIZATION_ID" />--%>
                                                <dx:ListBoxColumn Caption="Name" FieldName="ORGANIZATION_NAME" />
                                            </Columns>
                                            <ItemStyle>
                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                </HoverStyle>
                                            </ItemStyle>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="ds_Unit" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM MORGANIZATION"></asp:SqlDataSource>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label>SLA(Day)</label>
                                        <dx:ASPxTextBox ID="TxtUpdateSLA" runat="server" Width="100%" Height="30px" Theme="Metropolis" ReadOnly="true" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <dx:ASPxMemo ID="MmUpdateComplaint" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="Detail Complaint" ReadOnly="true" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <dx:ASPxMemo ID="MmUpdateResponse" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="Response Complaint" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#OptionalDataUpdate">
                                                Optional Data
                                                <span class="badge badge-danger pull-right"><i class="fa fa-plus"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="OptionalDataUpdate" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Priority Scale</label>
                                                        <dx:ASPxComboBox ID="CmbUpdateSkalaPrioritas" ClientInstanceName="CmbUpdateSkalaPrioritas" runat="server"           CssClass="form-control input-sm"
                                                            ItemStyle-HoverStyle-BackColor="#EE4D2D" AutoPostBack="false" Theme="Default"
                                                            Width="100%" ReadOnly="true">
                                                            <ItemStyle>
                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                </HoverStyle>
                                                            </ItemStyle>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <div class="form-group">
                                                        <label>Source of Information / Channel</label>
                                                        <dx:ASPxComboBox ID="CmbUpdateSumberInformasi" ClientInstanceName="CmbUpdateSumberInformasi" runat="server"
                                                            CssClass="form-control input-sm" ValueField="SumberInformasi" ItemStyle-HoverStyle-BackColor="#EE4D2D" AutoPostBack="false" Width="100%" ReadOnly="true">
                                                            <ItemStyle>
                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                </HoverStyle>
                                                            </ItemStyle>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxButton ID="ASPxUpdateTransaction" ClientInstanceName="ASPxUpdateTransaction" runat="server" Theme="Metropolis" Text="Update" Width="100%" AutoPostBack="false"
                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                            <ClientSideEvents Click="function(s, e) { execUpdateTransaction(); }" />
                                        </dx:ASPxButton>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxButton ID="ASPxCancelTransaction" ClientInstanceName="ASPxCancelTransaction" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                            <ClientSideEvents Click="function(s, e) { execCancelTransactionUpdate(); }" />
                                        </dx:ASPxButton>
                                    </div>
                                </div>
                                <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="Tab2">
                        <br />
                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel4" runat="server" Width="1030px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel4">
                            <PanelCollection>
                                <dx:PanelContent>
                                    <dx:ASPxGridView ID="ASPxGridView4" ClientInstanceName="ASPxGridView4" runat="server" KeyFieldName="ID"
                                        Width="1060px" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                        SettingsPager-PageSize="15">
                                        <SettingsPager>
                                            <AllButton Text="All">
                                            </AllButton>
                                            <NextPageButton Text="Next &gt;">
                                            </NextPageButton>
                                            <PrevPageButton Text="&lt; Prev">
                                            </PrevPageButton>
                                            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                        </SettingsPager>
                                        <SettingsEditing Mode="Inline" />
                                        <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="false"
                                            ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                        <SettingsBehavior ConfirmDelete="true" />
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                                                PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Width="200px"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Width="200px"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataMemoColumn Caption="Response" FieldName="ResponseComplaint" Width="300px"></dx:GridViewDataMemoColumn>
                                            <dx:GridViewDataTextColumn Caption="Dispatch To" FieldName="DispatchTicket" Width="100px"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Dispatch To Layer" FieldName="DispatchToLayer" Width="100px"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Dispatch To Dept" FieldName="ORGANIZATION_NAME" Width="100px"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Created by" FieldName="AgentCreate" Width="100px"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Date Create" FieldName="DateCreate" Width="130px"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" Width="70px"></dx:GridViewDataTextColumn>
                                            <%--<dx:GridViewDataTextColumn Caption="Type" FieldName="InteractionType" Width="70px"></dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataTextColumn Caption="Document" VisibleIndex="11" CellStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" Width="200px">
                                                <DataItemTemplate>
                                                    <a target="_blank" href="uConnector/uConnector_Files/HTML/<%# Eval("GenesysID")%>/<%# Eval("GenesysID")%>.HTML">
                                                        <%# Eval("GenesysID")%>
                                                    </a>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                    <asp:SqlDataSource ID="dsInteraction" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                    </div>
                    <div class="tab-pane fade" id="dataEscalation">
                        <br />
                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel10" runat="server" Width="100%" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel10">
                            <PanelCollection>
                                <dx:PanelContent>
                                    <div id="divNotifikasi" runat="server">
                                        <div class="alert alert-warning" style="width: 100%;">
                                            <strong>Ticket Number :
                                            <asp:Label ID="LblTicketNumber" runat="server"></asp:Label></strong> Has Been Escalation.
                                        </div>
                                    </div>
                                    <div id="divNotifikasiClosed" runat="server">
                                        <div class="alert alert-warning">
                                            <strong>Ticket Number :
                                                                <asp:Label ID="LblTicketNumberClosed" runat="server"></asp:Label></strong> Has Been Solved.
                                        </div>
                                    </div>
                                    <div id="divEscalation" runat="server">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Escalation To Layer</label>
                                                <select class="Metropolis" id="cmbEscalationToLayer" name="cmbEscalationToLayer" onchange="selectUser()" style="width: 100%; height: 30px;">
                                                    <option value="0" selected="selected">--Select--</option>
                                                    <asp:Literal runat="server" ID="ltrEscalationLayer"></asp:Literal>
                                                </select>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Escalation Reason</label>
                                                <dx:ASPxMemo ID="TxtEscalationReason" runat="server" Width="100%" Rows="10" Theme="Metropolis" />
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-8">
                                            </div>
                                            <div class="col-md-2">
                                                <dx:ASPxButton ID="BTN_Update" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                                    HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                    <ClientSideEvents Click="function(s, e) { execEscalation(); }" />
                                                </dx:ASPxButton>
                                            </div>
                                            <div class="col-md-2">
                                                <dx:ASPxButton ID="BTN_Cancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                                    HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                    <ClientSideEvents Click="function(s, e) { execCancelTransactionUpdate(); }" />
                                                </dx:ASPxButton>
                                            </div>
                                        </div>
                                    </div>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                    </div>
                    <div class="tab-pane fade" id="Tab3">
                        <iframe id="frameattachment" style="width: 100%; height: 400px; border: none; overflow: hidden;" runat="server"></iframe>
                    </div>
                </div>

            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <%--Pop up data attchment transaction--%>
    <dx:ASPxPopupControl ID="showAttchment" ClientInstanceName="showAttchment" runat="server" CloseAction="CloseButton" Modal="true" Width="1080px" Height="460px"
        closeonescape="true" ShowPageScrollbarWhenModal="true"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Add Data Attchment" FooterText="" AutoUpdatePosition="true" ScrollBars="None">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl8" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel13" runat="server" Width="830px" ShowLoadingPanel="TRUE" ClientInstanceName="ASPxCallbackPanel13">
                    <PanelCollection>
                        <dx:PanelContent>
                            <asp:HiddenField ID="idticket" runat="server" Value="123" />
                            <!-- /.row -->
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <%--Pop up data email--%>
    <dx:ASPxPopupControl ID="ASPxPopupControl3" ClientInstanceName="ASPxPopupControl3" runat="server" CloseAction="CloseButton" Modal="true" Width="1000px"
        closeonescape="true" ShowPageScrollbarWhenModal="true"
        PopupVerticalAlign="WindowCenter" Height="550px"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Data HTML" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel7" runat="server" Width="1000px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel7">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="row">
                                <div class="col-md-12">
                                    <iframe id="IFrameEmail" runat="server" width="100%" height="550px" frameborder="0px" style="border-style: solid;"></iframe>
                                </div>
                            </div>
                            <div class="row" id="divDocument" runat="server" visible="false" style="margin-top: -100px; text-align: center;">
                                <div class="col-md-12">
                                    <dx:ASPxLabel ID="lblframe" runat="server" Theme="Metropolis" Font-Bold="true" Font-Size="Large"></dx:ASPxLabel>
                                </div>
                            </div>
                            <br />
                            <center>
                                <a href="#" onclick="history.go(-1)">Go Back</a>
                                <%--<button onclick="history.back()">Go Back</button>--%>
                            </center>
                            
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <%--Pop up tambah data customer--%>
    <dx:ASPxPopupControl ID="ASPxPopupControl4" ClientInstanceName="ASPxPopupControl4" runat="server" CloseAction="CloseButton" Modal="true" Width="1000px"
        closeonescape="true" ShowPageScrollbarWhenModal="true"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Data Customer" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <dx:ASPxCallbackPanel ScrollBars="Vertical" Height="600px" ID="ASPxCallbackPanel8" runat="server" Width="1000px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div id="Tabs" role="tabpanel">
                                <%--<div class="panel-tab clearfix">--%>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="tablinks AddCustomer"><a href="#TabAddCustomer" class="linkAddCustomer" onclick="openActiveTab(event, 'TabAddCustomer')" role="tab" data-toggle="tab" aria-controls="TabAddCustomer"><i class="fa fa-file-text"></i>&nbsp;<strong>Add Customer</strong></a></li>
                                        <li class="tablinks DataCustomer"><a href="#TabPreviewCustomer" class="linkDataCustomer" onclick="openActiveTab(event, 'TabPreviewCustomer')" role="tab" data-toggle="tab" aria-controls="TabPreviewCustomer"><i class="fa fa-hdd-o"></i>&nbsp;<strong>Data Customer</strong></a></li>
                                    </ul>
                                <%--</div>--%>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane TabAddCustomer in active" id="TabAddCustomer">
                                        <div id="tabShow1" runat="server">
                                            <br />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Name</label>
                                                    <dx:ASPxTextBox ID="TxtCuxtomerName" runat="server" Theme="Metropolis" Height="30px" Width="100%"></dx:ASPxTextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Email Address</label>
                                                    <dx:ASPxTextBox ID="TxtCustomerEmail" runat="server" Theme="Metropolis" Height="30px" Width="100%">
                                                        <ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                                            <RegularExpression ErrorText="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*.\w+([-.]\w+)*" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>

                                                </div>
                                                <div class="col-md-6">
                                                    <label>Phone Number</label>
                                                    <dx:ASPxTextBox ID="TxtCustomerPhone" runat="server" Theme="Metropolis" Height="30px" Width="100%">
                                                        <ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                                            <RegularExpression ErrorText="Please Enter Numbers Only" ValidationExpression="^\d+$" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Gender</label>
                                                    <dx:ASPxComboBox ID="ASPxComboBox3" runat="server" Width="100%" Height="30px" Theme="Metropolis">
                                                        <Items>
                                                            <dx:ListEditItem Text="--Select--" Value="0" Selected="true" />
                                                            <dx:ListEditItem Text="Male" Value="Male" />
                                                            <dx:ListEditItem Text="Female" Value="Female" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Date of Birth</label>
                                                    <dx:ASPxDateEdit ID="DtCustomerBirth" ClientInstanceName="DtCustomerBirth" runat="server" Width="100%" Height="30px" Theme="Metropolis" DisplayFormatString="yyyy-MM-dd"></dx:ASPxDateEdit>
                                                </div>
                                            </div>
                                            <br />
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Address</label>
                                                    <dx:ASPxMemo ID="ASPxMemo1" runat="server" Width="100%" Rows="5" Theme="Metropolis" />
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-8">
                                                </div>
                                                <div class="col-md-2">
                                                    <dx:ASPxButton ID="btnSubmit" runat="server" Theme="Metropolis" Text="Save" Width="100%" AutoPostBack="false"
                                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                        <ClientSideEvents Click="function(s, e) { execSubmit(); }" />
                                                    </dx:ASPxButton>
                                                </div>
                                                <div class="col-md-2">
                                                    <dx:ASPxButton ID="btnCancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                        <ClientSideEvents Click="function(s, e) { execCancelCustomerEmpty(); }" />
                                                    </dx:ASPxButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane TabPreviewCustomer" id="TabPreviewCustomer">
                                        <div id="tabShow2" runat="server">
                                            <br />
                                            <asp:HiddenField ID="TabName" runat="server"/>
                                            <%--<div class="text-left">
								                <button id="BtnDataCustomer" class="no-border" style="background-color: white">
									                <i class="fa fa-search" title="Search Data Customers"></i>
								                </button>
                                                
							                </div>
                                            <div style="visibility:hidden;"><asp:Label ID="Label1" runat="server"></asp:Label></div>
                                            <br />--%>
                                            <div>
                                                <div id="dxDataCustomer"></div>
                                                <%--<dx:ASPxGridView ID="ASPxGridView7" ClientInstanceName="ASPxGridView7" runat="server" Width="990px" Theme="Metropolis"
                                                    Cursor="pointer" KeyFieldName="CustomerID" Settings-VerticalScrollableHeight="300"
                                                    Styles-Header-Font-Bold="true" Font-Size="X-Small" SettingsPager-PageSize="10">
                                                    <SettingsPager ShowNumericButtons="false">
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10" ShowAllItem="false" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="false" ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true" />
                                                    <SettingsBehavior ConfirmDelete="true" AllowSelectByRowClick="false" AllowFocusedRow="true" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="CustomerID" Width="100px" ReadOnly="true"></dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="Name" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Channel" FieldName="ValueChannel" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataDateColumn Caption="Date of Birth" FieldName="Birth" Width="150px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd"></dx:GridViewDataDateColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Gender" FieldName="JenisKelamin" Width="150px" Settings-AutoFilterCondition="Contains">
                                                            <PropertiesComboBox>
                                                                <Items>
                                                                    <dx:ListEditItem Text="Male" Value="Male" />
                                                                    <dx:ListEditItem Text="Female" Value="Female" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataTextColumn Caption="CIF Number" FieldName="CIF" Width="150px" VisibleIndex="5" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="NIK" FieldName="NIK" Width="150px" VisibleIndex="6" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Address" FieldName="Alamat" Width="300px" Settings-AutoFilterCondition="Contains" PropertiesTextEdit-FocusedStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>
                                                    </Columns>
                                                    <Templates>
                                                        <DetailRow>
                                                            <div class="panel-tab clearfix">
                                                                <ul class="tab-bar">
                                                                    <li class="active"><a href="#TabAccountNumber" data-toggle="tab"><i class="fa fa-credit-card"></i>&nbsp;<strong>Data Account Number</strong></a></li>
                                                                    <li><a href="#TabCustomerChannel" data-toggle="tab"><i class="fa fa-group"></i>&nbsp;<strong>Data Customer Channel</strong></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="tab-content">
                                                                    <div class="tab-pane fade in active" id="TabAccountNumber">
                                                                        <dx:ASPxGridView ID="ASPxGridView10" ClientInstanceName="ASPxGridView10" runat="server" KeyFieldName="ID" SettingsBehavior-AllowFocusedRow="true"
                                                                            DataSourceID="dsTambahAccountNumber" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small" SettingsPager-PageSize="5"
                                                                            OnBeforePerformDataSelect="ASPxGridView10_BeforePerformDataSelect" OnRowUpdating="ASPxGridView10_RowUpdating"
                                                                            OnRowInserting="ASPxGridView10_RowInserting" OnRowDeleting="ASPxGridView10_RowDeleting">
                                                                            <SettingsPager>
                                                                                <AllButton Text="All">
                                                                                </AllButton>
                                                                                <NextPageButton Text="Next &gt;">
                                                                                </NextPageButton>
                                                                                <PrevPageButton Text="&lt; Prev">
                                                                                </PrevPageButton>
                                                                                <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                                            </SettingsPager>
                                                                            <SettingsEditing Mode="Inline" />
                                                                            <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowGroupPanel="false"
                                                                                ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                                                                            <SettingsBehavior ConfirmDelete="true" AllowSelectByRowClick="false" AllowFocusedRow="true" />
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                                                                    ButtonType="Image" FixedStyle="Left" Width="130px">
                                                                                    <EditButton Visible="True">
                                                                                        <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                                                                                    </EditButton>
                                                                                    <NewButton Visible="True">
                                                                                        <Image ToolTip="Add Transaksi" Url="img/icon/Apps-text-editor-icon2.png" />
                                                                                    </NewButton>
                                                                                    <DeleteButton Visible="True">
                                                                                        <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                                                                                    </DeleteButton>
                                                                                    <CancelButton>
                                                                                        <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                                                                        </Image>
                                                                                    </CancelButton>
                                                                                    <UpdateButton>
                                                                                        <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                                                                    </UpdateButton>
                                                                                </dx:GridViewCommandColumn>
                                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                                                                                    PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="Account Number" FieldName="NomorRekening" Settings-AutoFilterCondition="Contains">
                                                                                    <PropertiesTextEdit>
                                                                                        <ValidationSettings CausesValidation="True">
                                                                                            <RegularExpression ErrorText="Format account number is not valid" ValidationExpression="^\d+$" />
                                                                                            <RequiredField ErrorText="Format account number is not valid" IsRequired="True" />
                                                                                        </ValidationSettings>
                                                                                    </PropertiesTextEdit>
                                                                                </dx:GridViewDataTextColumn>
                                                                            </Columns>
                                                                        </dx:ASPxGridView>
                                                                    </div>
                                                                    <div class="tab-pane fade" id="TabCustomerChannel">
                                                                        <dx:ASPxGridView ID="ASPxGridView11" ClientInstanceName="ASPxGridView11" runat="server" Width="100%" Theme="Metropolis"
                                                                            DataSourceID="dsTambahChannel" KeyFieldName="ID" OnRowUpdating="ASPxGridView11_RowUpdating" OnRowDeleting="ASPxGridView11_RowDeleting"
                                                                            Styles-Header-Font-Bold="true" Font-Size="X-Small" OnRowInserting="ASPxGridView11_RowInserting">
                                                                            <SettingsPager>
                                                                                <AllButton Text="All">
                                                                                </AllButton>
                                                                                <NextPageButton Text="Next &gt;">
                                                                                </NextPageButton>
                                                                                <PrevPageButton Text="&lt; Prev">
                                                                                </PrevPageButton>
                                                                            </SettingsPager>
                                                                            <SettingsPager PageSize="5" />
                                                                            <SettingsEditing Mode="Inline" />
                                                                            <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowGroupPanel="false"
                                                                                ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                                                                            <SettingsBehavior ConfirmDelete="true" AllowSelectByRowClick="false" AllowFocusedRow="true" />
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                                                                    ButtonType="Image" FixedStyle="Left" Width="130px">
                                                                                    <EditButton Visible="True">
                                                                                        <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                                                                                    </EditButton>
                                                                                    <NewButton Visible="True">
                                                                                        <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                                                                                    </NewButton>
                                                                                    <DeleteButton Visible="True">
                                                                                        <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                                                                                    </DeleteButton>
                                                                                    <CancelButton>
                                                                                        <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                                                                        </Image>
                                                                                    </CancelButton>
                                                                                    <UpdateButton>
                                                                                        <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                                                                    </UpdateButton>
                                                                                </dx:GridViewCommandColumn>
                                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="20px" ReadOnly="true" Visible="false" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="Channel" FieldName="ValueChannel" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataComboBoxColumn Caption="Type" FieldName="FlagChannel" HeaderStyle-HorizontalAlign="left"
                                                                                    Width="70px">
                                                                                    <PropertiesComboBox>
                                                                                        <Items>
                                                                                            <dx:ListEditItem Text="Phone" Value="Phone" Selected="true" />
                                                                                            <dx:ListEditItem Text="Email" Value="Email" />
                                                                                        </Items>
                                                                                    </PropertiesComboBox>
                                                                                </dx:GridViewDataComboBoxColumn>
                                                                            </Columns>
                                                                        </dx:ASPxGridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </DetailRow>
                                                    </Templates>
                                                    <SettingsDetail ShowDetailRow="true" />
                                                </dx:ASPxGridView>--%>
                                            </div>
                                        </div>
                                        <%--<br />--%>
                                    
                                    </div>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="ASPxPopupControl5" ClientInstanceName="ASPxPopupControl5" runat="server" CloseAction="CloseButton" Modal="true" Width="1100px"
        closeonescape="true" ShowPageScrollbarWhenModal="true" ScrollBars="Vertical"
        PopupVerticalAlign="WindowCenter" Height="600px"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Data Customer" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <%--<div class="text-left">
					<button id="BtnSearchCustomer" class="no-border" style="background-color: white">
						<i class="fa fa-search" title="Search Data Customers"></i>
					</button>
				</div>
                <div style="visibility:hidden;"><asp:Label ID="Label2" runat="server"></asp:Label></div>--%>
                <div id="dxSearchCustomer"></div>
                <%--<dx:ASPxGridView ID="ASPxGridView5" Visible="false" ClientInstanceName="ASPxGridView5" runat="server" Width="100%" Theme="Metropolis"
                    Cursor="pointer" KeyFieldName="CustomerID" Settings-VerticalScrollableHeight="400"
                    Styles-Header-Font-Bold="true" Font-Size="X-Small" SettingsPager-PageSize="20">
                    <SettingsPager ShowNumericButtons="false">
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="50, 100" ShowAllItem="true" />
                    </SettingsPager>
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="false" ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true" />
                    <SettingsBehavior ConfirmDelete="true" AllowSelectByRowClick="false" AllowFocusedRow="true" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Action" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="60px">
                            <DataItemTemplate>
                                <a href="#" onclick="RedirectThreadToMainframe('<%# Eval("CustomerID")%>')">
                                    <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" ToolTip="Select data" />
                                </a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="CustomerID" Width="100px" ReadOnly="true"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="Name" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Phone Number" FieldName="HP" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email Address" FieldName="Email" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Date of Birth" FieldName="Birth" Width="150px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd"></dx:GridViewDataDateColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Gender" FieldName="JenisKelamin" Width="150px" Settings-AutoFilterCondition="Contains">
                            <PropertiesComboBox>
                                <Items>
                                    <dx:ListEditItem Text="Male" Value="Male" />
                                    <dx:ListEditItem Text="Female" Value="Female" />
                                </Items>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataTextColumn Caption="CIF Number" FieldName="CIF" Width="150px" VisibleIndex="5" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="NIK" FieldName="NIK" Width="150px" VisibleIndex="6" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Address" FieldName="Alamat" Width="300px" Settings-AutoFilterCondition="Contains" PropertiesTextEdit-FocusedStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>
                    </Columns>
                    
                </dx:ASPxGridView>--%>
                <%--                <br />
                <div class="row">
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        <dx:ASPxButton ID="BTN_AddNewCustomer" runat="server" Theme="Metropolis" Text="Add New Customer" Width="100%" AutoPostBack="false"
                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                            <ClientSideEvents Click="function(s, e) { execShowAddCustomer(); }" />
                        </dx:ASPxButton>
                    </div>
                </div>--%>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <asp:SqlDataSource ID="dsTambahChannel" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="select * from mCustomerChannel where CustomerID=@customerid">
        <SelectParameters>
            <asp:Parameter Name="customerid" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsTambahChannelSyncronise" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="select * from mCustomerChannel where CustomerID=@customerid">
        <SelectParameters>
            <asp:Parameter Name="customerid" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsTambahAccountNumber" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="select * from BTN_NomorRekening where CustomerID=@customerid">
        <SelectParameters>
            <asp:Parameter Name="customerid" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxPopupControl ID="ASPxPopupControl6" ClientInstanceName="ASPxPopupControl6" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="660px" AllowResize="true" Width="1050px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="Vertical"
        ShowFooter="True" HeaderText="Form Parent Child Ticket" FooterText="" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s, e) { closeReloadPage(); }">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel9" runat="server" Width="1000px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel9">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="panel-tab clearfix">
                                <ul class="tab-bar">
                                    <li class="active"><a href="#TabA" data-toggle="tab"><i class="fa fa-file-text"></i>&nbsp;<strong>Data Transaction Ticket</strong>
                                        <span class="badge badge-warning" style="background-color: #FF8800;">
                                            <dx:ASPxLabel ID="dx_LabelTicketNumber" runat="server" Theme="Metropolis" Font-Size="10px" ForeColor="White" ClientIDMode="Static"></dx:ASPxLabel>
                                        </span>
                                    </a>
                                    </li>
                                    <li><a href="#TabB" data-toggle="tab"><i class="fa fa-cogs"></i>&nbsp;<strong>Data Setting Query Ticket</strong></a></li>
                                    <li><a href="#TabC" onclick="showParent()" data-toggle="tab"><i class="fa fa-sitemap"></i>&nbsp;<strong>Data Parent Child Ticket</strong></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="TabA">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="ASPxGridView6" ClientInstanceName="ASPxGridView6" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                    KeyFieldName="TicketNumber" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="10">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <SettingsBehavior EnableRowHotTrack="true" AllowSelectByRowClick="true" ConfirmDelete="true" AllowSelectSingleRowOnly="true" />
                                                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="30px" CellStyle-HorizontalAlign="Center"></dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn FieldName="TicketNumber" Width="150" VisibleIndex="1" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketSourceName" Caption="Channel" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="GenesysNumber" Caption="Interaction ID" Width="200" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="ThreadID" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="CustomerID" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="Name" Width="200" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="NomorRekening" Width="150" Caption="Account Number" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="HP" Width="150" Caption="Phone Number" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="EMAIL" Width="150" Caption="Email Address" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="CIF" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="NIK" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="AccountInbound" Caption="Account" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="SubCategory3Name" Caption="Reason" Width="300" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataMemoColumn FieldName="DetailComplaint" Caption="User Issue Remark" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="Status" Width="160px" />
                                                        <dx:GridViewDataTextColumn FieldName="UserCreate" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateCreate" Width="150" />
                                                    </Columns>
                                                    <ClientSideEvents SelectionChanged="ASPxGridView6_SelectionChanged" />
                                                </dx:ASPxGridView>
                                                <br />
                                                <dx:ASPxMemo ID="dxmReason" runat="server" Theme="Metropolis" Height="120" Width="100%" NullText="Reason Parent Ticket" ClientIDMode="Static" placeholder="Reason Parent Child Ticket"></dx:ASPxMemo>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="ASPxButton4" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                            <ClientSideEvents Click="function(s, e) { execParentNumber(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="ASPxButton6" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                            <ClientSideEvents Click="function(s, e) { execCancelParentNumber(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="TabB">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="GridView" ClientInstanceName="GridView" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="ds_query" KeyFieldName="ID"
                                                    Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false"
                                                        ShowGroupPanel="false" />
                                                    <SettingsBehavior ConfirmDelete="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                                            ButtonType="Image" FixedStyle="Left" Width="30px">
                                                            <EditButton Visible="true">
                                                                <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                                                            </EditButton>
                                                            <NewButton Visible="true">
                                                                <Image ToolTip="New" Url="img/Icon/Apps-text-editor-icon2.png" />
                                                            </NewButton>
                                                            <DeleteButton Visible="false">
                                                                <Image ToolTip="Delete" Url="img/Icon/Actions-edit-clear-icon2.png" />
                                                            </DeleteButton>
                                                            <CancelButton Visible="true">
                                                                <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                                                </Image>
                                                            </CancelButton>
                                                            <UpdateButton Visible="true">
                                                                <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                                            </UpdateButton>
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Day" FieldName="Day" VisibleIndex="2">
                                                            <PropertiesComboBox>
                                                                <Items>
                                                                    <dx:ListEditItem Text="5" Value="5" />
                                                                    <dx:ListEditItem Text="10" Value="10" />
                                                                    <dx:ListEditItem Text="15" Value="15" />
                                                                    <dx:ListEditItem Text="20" Value="20" />
                                                                    <dx:ListEditItem Text="25" Value="25" />
                                                                    <dx:ListEditItem Text="30" Value="30" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataDateColumn FieldName="FilterDate" Caption="Start Filter Date" VisibleIndex="3" Width="150px"></dx:GridViewDataDateColumn>
                                                    </Columns>
                                                    <Settings ShowGroupPanel="True" />
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="ds_query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                    DeleteCommand="DELETE FROM [Temp_SettingHiStoryTransaction] WHERE [ID] = @ID"
                                                    SelectCommand="select * from Temp_SettingHiStoryTransaction where CreatedBy=@username and Type='ParentChild'"
                                                    UpdateCommand="UPDATE [Temp_SettingHiStoryTransaction] SET [Day] = @Day, [FilterDate] = @FilterDate, Type='ParentChild', [CreatedBy]=@username WHERE [ID] = @ID">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </SelectParameters>
                                                    <DeleteParameters>
                                                        <asp:Parameter Name="ID" Type="Int32" />
                                                    </DeleteParameters>
                                                    <InsertParameters>
                                                        <asp:Parameter Name="Day" Type="String" />
                                                        <asp:Parameter Name="FilterDate" Type="String" />
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </InsertParameters>
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="Day" Type="String" />
                                                        <asp:Parameter Name="FilterDate" Type="String" />
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </UpdateParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="TabC">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="ASPxGridView8" ClientInstanceName="ASPxGridView8" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                    KeyFieldName="ParentNumberID" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="10"
                                                    OnCustomColumnGroup="ASPxGridView8_CustomColumnGroup" OnCustomGroupDisplayText="ASPxGridView8_CustomGroupDisplayText"
                                                    OnCustomColumnSort="ASPxGridView8_CustomColumnSort">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <SettingsBehavior EnableRowHotTrack="true" AllowSelectByRowClick="true" ConfirmDelete="true" />
                                                    <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                                                            <DataItemTemplate>
                                                                <a href="#" onclick="ShowUpdateParentID('<%# Eval("TicketNumber")%>')">
                                                                    <asp:Image ImageUrl="img/icon/Actions-edit-clear-icon2.png" ID="Image1" runat="server" />
                                                                </a>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberID" Caption="Parent Case Number" Width="150" GroupIndex="1" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketNumber" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketSourceName" Caption="Channel" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="GenesysID" Caption="Interaction ID" Width="200" />
                                                        <dx:GridViewDataTextColumn FieldName="ThreadID" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="NIK" Caption="Customer ID" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="Name" Width="200" />
                                                        <dx:GridViewDataTextColumn FieldName="NomorRekening" Width="150" Caption="Account Number" />
                                                        <dx:GridViewDataTextColumn FieldName="HP" Width="150" Caption="Phone Number" />
                                                        <dx:GridViewDataTextColumn FieldName="Email" Width="150" Caption="Email Address" />
                                                        <dx:GridViewDataTextColumn FieldName="CIF" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="NoKTP" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="SubCategory3Name" Caption="Reason" Width="300" />
                                                        <dx:GridViewDataMemoColumn FieldName="DetailComplaint" Caption="User Issue Remark" Width="300" />
                                                        <dx:GridViewDataMemoColumn FieldName="ResponseComplaint" Caption="Agent Response" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentReason" Caption="Parent Child Reason" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="StatusInteraction" Caption="Status" Width="160px" />
                                                        <dx:GridViewDataTextColumn FieldName="UserCreate" Caption="Created Ticket By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateCreate" Caption="Created Date Ticket" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="AgentCreate" Caption="Created Interaction By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateInteraction" Caption="Created Date Interaction" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberCreated" Caption="Created Ticket Parent By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberDate" Caption="Created Date Parent" Width="150" />
                                                    </Columns>
                                                    <SettingsBehavior AutoExpandAllGroups="true" />
                                                    <Settings ShowGroupedColumns="True" />
                                                </dx:ASPxGridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControl7" ClientInstanceName="ASPxPopupControl7" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="150px" AllowResize="true" Width="250px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="None"
        ShowFooter="True" HeaderText="Form Update Amount" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl7" runat="server">
                <dx:ASPxTextBox ID="TxtInputAmount" runat="server" Width="100%" Height="30px">
                    <ClientSideEvents TextChanged="price_InitAndKeyUpdate" ValueChanged="price_InitAndKeyUpdate" />
                </dx:ASPxTextBox>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
