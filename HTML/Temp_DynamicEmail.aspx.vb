﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports DevExpress.Data
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web
Imports DevExpress.Web.ASPxClasses
Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridLookup
Imports DevExpress.Web.Data

Public Class Temp_DynamicEmail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            cmbPerusahaan.Value = "All"
            cmbUnit.Value = "All"

            cmbDivisi.Value = "All"
            cmbDirektorat.Value = "All"
            cmb_UnitCase.Value = "All"
            FillCityCombo("All")
        End If
        sql_transaction_type.InsertParameters("username").DefaultValue = Session("username")
        sql_transaction_type.UpdateParameters("username").DefaultValue = Session("username")
    End Sub
    Protected Sub cmbDirektorat_Callback(sender As Object, e As CallbackEventArgsBase) Handles cmbDirektorat.Callback
        FillCityCombo(e.Parameter)
    End Sub

    Protected Sub FillCityCombo(ByVal idPerusahaan As String)
        If String.IsNullOrEmpty(idPerusahaan) Then

            Return
        End If
        '        SqlDirektorat.SelectCommand = "select * from temp_EmailNotif_Kategori_m Where IDEmailNotif='" & idPerusahaan & "'"
        SqlDirektorat.SelectCommand = "select * from temp_EmailNotif_Kategori_m"
        cmbDirektorat.DataSource = SqlDirektorat
        cmbDirektorat.DataBind()
        '    SqlDirektorat.SelectCommand = "select IDDirektorat as KodeNya,NamaDirektorat as NamaNya from vw_Employee where (NamaDirektorat is not null and NamaDirektorat <>'') group by IDDirektorat,NamaDirektorat order by NamaDirektorat"
    End Sub

    Private Sub cmbUnit_Callback(sender As Object, e As CallbackEventArgsBase) Handles cmbUnit.Callback
        FillUnitCombo(e.Parameter)

    End Sub
    Protected Sub FillUnitCombo(ByVal idDirektorat As String)
        If String.IsNullOrEmpty(idDirektorat) Then

            Return
        End If
        SqlUnit.SelectCommand = "select * from temp_EmailNotif_Related_m"
        cmbUnit.DataSource = SqlUnit
        cmbUnit.DataBind()
        '    SqlDirektorat.SelectCommand = "select IDDirektorat as KodeNya,NamaDirektorat as NamaNya from vw_Employee where (NamaDirektorat is not null and NamaDirektorat <>'') group by IDDirektorat,NamaDirektorat order by NamaDirektorat"
    End Sub

    Private Sub cmbDivisi_Callback(sender As Object, e As CallbackEventArgsBase) Handles cmbDivisi.Callback
        FillDivisiCombo(e.Parameter)

    End Sub
    Protected Sub FillDivisiCombo(ByVal idUnit As String)
        If String.IsNullOrEmpty(idUnit) Then

            Return
        End If

        SqlDivisi.SelectCommand = "select * from temp_EmailNotif_Related_List_m order by JenisList asc"
        cmbDivisi.DataSource = SqlDivisi
        cmbDivisi.DataBind()

        '    SqlDirektorat.SelectCommand = "select IDDirektorat as KodeNya,NamaDirektorat as NamaNya from vw_Employee where (NamaDirektorat is not null and NamaDirektorat <>'') group by IDDirektorat,NamaDirektorat order by NamaDirektorat"
    End Sub


    Private Sub cmb_UnitCase_Callback(sender As Object, e As CallbackEventArgsBase) Handles cmb_UnitCase.Callback
        FillUnitCaseCombo(e.Parameter)
    End Sub
    Protected Sub FillUnitCaseCombo(ByVal idUnitCase As String)
        If String.IsNullOrEmpty(idUnitCase) Then

            Return
        End If
        If cmbUnit.Value = "2" And cmbDivisi.Value = "1" Then
            sql_cmb_UnitCase.SelectCommand = "select * from mOrganization"
            cmb_UnitCase.DataSource = sql_cmb_UnitCase
            cmb_UnitCase.DataBind()
        ElseIf cmbUnit.Value <> "2" And cmbDivisi.Value = "2" Then
            sql_cmb_UnitCase.SelectCommand = "select '1' as ORGANIZATION_ID,'Layer 1' as ORGANIZATION_NAME union select '2' as ORGANIZATION_ID,'Layer 2' as ORGANIZATION_NAME union select '3' as ORGANIZATION_ID,'Layer 3' as ORGANIZATION_NAME"
            cmb_UnitCase.DataSource = sql_cmb_UnitCase
            cmb_UnitCase.DataBind()
        End If

    End Sub

    Private Sub btnSimpan_Click(sender As Object, e As EventArgs) Handles btnSimpan.Click
        Dim aNotif As String = String.Empty
        Dim aNotifKategori As String = String.Empty
        Dim aNotifRelated As String = String.Empty
        Dim aNotifRelated_List As String = String.Empty
        Dim aUnitCase As String = String.Empty
        Dim aEmailNya As String = String.Empty
        aNotif = cmbPerusahaan.Value
        aNotifKategori = cmbDirektorat.Value
        aNotifRelated = cmbUnit.Value
        aNotifRelated_List = cmbDivisi.Value
        aUnitCase = cmb_UnitCase.Value
        aEmailNya = txtEmailNya.Text
        aaa.Text = "exec SP_Email_Notif '" & aNotif & "','" & aNotifKategori & "','" & aNotifRelated & "','" & aNotifRelated_List & "','" & aUnitCase & "','" & aEmailNya & "'"
        'aaa.Text = aNotif & "--" & aNotifKategori & "--" & aNotifRelated & "--" & aNotifRelated_List & "--" & aUnitCase & "--" & aEmailNya
    End Sub
    'Start Cascade Dropdown NEW

    Private Sub gv_EmailNotif_Related_CellEditorInitialize(sender As Object, e As ASPxGridViewEditorEventArgs) Handles gv_EmailNotif_Related.CellEditorInitialize
        Select Case e.Column.FieldName

            Case "JenisKategori"
                InitializeComboRelated(e, "ID", sql_cmb_JenisNotifKategori, AddressOf cmbComboEmailNotif_OnCallback)

            Case Else
        End Select
    End Sub
    Private Sub cmbComboEmailNotif_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)
        FillJenisNotif(TryCast(source, ASPxComboBox), e.Parameter, sql_cmb_JenisNotifKategori)
    End Sub
    Private Sub cmbComboEmailNotifKategori_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)
        FillJenisKategori(TryCast(source, ASPxComboBox), e.Parameter, sql_cmb_JenisNotifRelated)
    End Sub
    Protected Sub InitializeComboRelated(ByVal e As ASPxGridViewEditorEventArgs, ByVal parentComboName As String, ByVal source As SqlDataSource, ByVal callBackHandler As CallbackEventHandlerBase)

        Dim id As String = String.Empty
        If (Not gv_EmailNotif_Related.IsNewRowEditing) Then
            Dim val As Object = gv_EmailNotif_Related.GetRowValuesByKeyValue(e.KeyValue, parentComboName)
            If (val Is Nothing OrElse val Is DBNull.Value) Then
                id = Nothing
            Else
                id = val.ToString()
            End If
        End If
        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        If combo IsNot Nothing Then
            ' unbind combo
            combo.DataSourceID = Nothing
            FillJenisNotif(combo, id, source)
            AddHandler combo.Callback, callBackHandler
        End If
        Return
    End Sub
    Protected Sub InitializeComboRelatedList(ByVal e As ASPxGridViewEditorEventArgs, ByVal parentComboName As String, ByVal source As SqlDataSource, ByVal callBackHandler As CallbackEventHandlerBase)

        Dim id As String = String.Empty
        If (Not gv_EmailNotif_Related_List.IsNewRowEditing) Then
            Dim val As Object = gv_EmailNotif_Related_List.GetRowValuesByKeyValue(e.KeyValue, parentComboName)
            If (val Is Nothing OrElse val Is DBNull.Value) Then
                id = Nothing
            Else
                id = val.ToString()
            End If
        End If
        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        If combo IsNot Nothing Then
            ' unbind combo
            combo.DataSourceID = Nothing
            FillJenisKategori(combo, id, source)
            AddHandler combo.Callback, callBackHandler
        End If
        Return
    End Sub
    Protected Sub FillJenisNotif(ByVal cmb As ASPxComboBox, ByVal id As String, ByVal source As SqlDataSource)
        cmb.Items.Clear()
        ' trap null selection
        If String.IsNullOrEmpty(id) Then
            Return
        End If

        ' get the values
        source.SelectParameters(0).DefaultValue = id
        Dim view As DataView = CType(source.Select(DataSourceSelectArguments.Empty), DataView)
        For Each row As DataRowView In view
            cmb.Items.Add(row(2).ToString(), row(0))
        Next row
    End Sub
    Protected Sub FillJenisKategori(ByVal cmb As ASPxComboBox, ByVal id As String, ByVal source As SqlDataSource)
        cmb.Items.Clear()
        ' trap null selection
        If String.IsNullOrEmpty(id) Then
            Return
        End If

        ' get the values
        source.SelectParameters(0).DefaultValue = id
        Dim view As DataView = CType(source.Select(DataSourceSelectArguments.Empty), DataView)
        For Each row As DataRowView In view
            cmb.Items.Add(row(2).ToString(), row(0))
        Next row
    End Sub
    Private Sub gv_EmailNotif_Related_List_CellEditorInitialize(sender As Object, e As ASPxGridViewEditorEventArgs) Handles gv_EmailNotif_Related_List.CellEditorInitialize
        Select Case e.Column.FieldName

            Case "JenisKategori"
                InitializeComboRelated(e, "ID", sql_cmb_JenisNotifKategori, AddressOf cmbComboEmailNotif_OnCallback)
            Case "JenisRelated"
                InitializeComboRelatedList(e, "ID", sql_cmb_JenisNotifRelated, AddressOf cmbComboEmailNotifKategori_OnCallback)

            Case Else
        End Select
    End Sub

    Private Sub gv_EmailNotif_Related_List_RowInserting(sender As Object, e As ASPxDataInsertingEventArgs) Handles gv_EmailNotif_Related_List.RowInserting
        sql_EmailNotif_Related_List.InsertCommand = "insert into temp_EmailNotif_Related_List (IDEmailRelated,JenisList,LayerID,UnitCaseID,EmailAddress,NA) values (@JenisRelated,@JenisList,@LayerID,@UnitCaseID,@EmailAddress,@NA)"
    End Sub
End Class