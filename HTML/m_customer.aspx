<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="m_customer.aspx.vb" Inherits="ICC.m_customer" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="module" src="scripts/customer.js"></script> 

    <script>
        function deleteGroup(s) {
            $('#MainContent_TrxFromCustomerID').val(s);
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/DeleteGroupingDataCustomer",
                    data: "{TrxFromCustomerID: '" + $('#MainContent_TrxFromCustomerID').val() + "', TrxUserName: '" + $('#MainContent_TrxUserName').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var result = "";
                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem)
                                window.location.href = "m_customer.aspx";
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }
                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
  
        function showGroup(s) {
            $('#MainContent_TrxFromCustomerID').val(s);
            ASPxPopupControl2.Show();
        }
        function updateSync(TrxToCustomerID) {
            var TrxFromCustomerID = $('#MainContent_TrxFromCustomerID').val();
            //var TrxToCustomerID = $('#MainContent_TrxToCustomerID').val(v);
            var TrxUserName = $('#MainContent_TrxUserName').val();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/GroupingDataCustomer",
                    data: "{TrxFromCustomerID: '" + TrxFromCustomerID + "', TrxToCustomerID: '" + TrxToCustomerID + "', TrxUserName: '" + TrxUserName + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var result = "";
                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem)
                                ASPxPopupControl2.Hide();
                                window.location.href = "m_customer.aspx";
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }
                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
  
        function showInsert() {
            $('#MainContent_TrxAction').val("insert");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_btnSubmit_CD').text("Save")
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCuxtomerName_I').val("")
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerEmail_I').val("");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerPhone_I').val("");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbGender_I').val("");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_DtCustomerBirth_I').val("");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerCIF_I').val("");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerNIK_I').val("");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtPelaporAddress_I').val("");
            ASPxPopupControl1.Show();
        }
        function showUpdate(TrxCustomerID) {
            $('#MainContent_TrxAction').val("update");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_btnSubmit_CD').text("Update")
            readCustomer(TrxCustomerID);
        }
        function showDelete(TrxCustomerID) {
            $('#MainContent_TrxAction').val("delete");
            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_btnSubmit_CD').text("Delete")
            readCustomer(TrxCustomerID);
            ASPxPopupControl1.Show();
        }
   
        function encodeData(s) {
            return encodeURIComponent(s).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
        }
  
        function execSubmit() {
            $('#MainContent_TrxTempPhone').val("");
            var TrxAction = $('#MainContent_TrxAction').val();
            var TrxCustomerID = $('#MainContent_TrxCustomerID').val()
            var TrxUserName = $('#MainContent_TrxUserName').val();
            var TrxName = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCuxtomerName_I').val();
            var TrxEmail = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerEmail_I').val();
            var TrxPhone = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerPhone_I').val();
            var TrxGender = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbGender_I').val();
            var TrxBirth = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_DtCustomerBirth_I').val();
            var TrxCIF = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerCIF_I').val();
            var TrxNIK = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerNIK_I').val();
            var TrxAddress = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtPelaporAddress_I').val();

            if (TrxName === '') {
                alert("Name is empty")
                return false;
            }
            if (TrxEmail != '') {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (TrxEmail.match(mailformat)) {

                }
                else {
                    alert("Format email address not valid");
                    return false;
                }

            }
            if (TrxPhone != '') {
                var numberNya = /^[0-9]+$/;
                if (TrxPhone.match(numberNya)) {
                    var PhoneLengt = TrxPhone.toString().length;
                    if (PhoneLengt > '6') {

                    } else {
                        alert("Format phone number is not valid")
                        return false;
                    }
                } else {
                    alert("Phone Number format is numeric")
                    return false;
                }
            }
            if (TrxGender == '--Select--' || TrxGender == '' || TrxGender == '0') {
                alert("Gender is empty")
                return false;
            }
            if (TrxCIF == '' || TrxCIF == '0') {
                //alert(TrxCIF)
            } else {
                var numberNya = /^[0-9]+$/;
                if (TrxCIF.match(numberNya)) {
                    var CIFLengt = TrxCIF.toString().length;
                    if (CIFLengt == '10') {

                    } else {
                        alert("CIF Number format is 10 digit")
                        return false;
                    }
                } else {
                    alert("CIF Number format is numeric")
                    return false;
                }
            }
            if (TrxNIK == '' || TrxNIK == '0') {

            } else {
                var numberNya = /^[0-9]+$/;
                if (TrxNIK.match(numberNya)) {
                    var NIKLengt = TrxNIK.toString().length;
                    if (NIKLengt == '16') {

                    } else {
                        alert("NIK Number format is 16 digit")
                        return false;
                    }
                } else {
                    alert("NIK Number format is numeric")
                    return false;
                }
            }
            if (TrxAddress == '') {
                alert("Address is empty")
                return false;
            }

            if (confirm("Do you want to process?")) {

                if (TrxAction === 'insert') {
                    //alert("Action Insert")
                    $.ajax({
                        type: "POST",
                        url: "WebServiceTransaction.asmx/InsertDataCustomer",
                        data: "{TrxCustomerID: '" + TrxCustomerID + "', TrxName: '" + encodeData(TrxName) + "', TrxEmail: '" + TrxEmail + "', TrxPhone: '" + TrxPhone + "', TrxGender: '" + TrxGender + "', TrxBirth: '" + TrxBirth + "', TrxCIF: '" + TrxCIF + "', TrxNIK: '" + TrxNIK + "', TrxAddress: '" + encodeData(TrxAddress) + "', TrxUserName: '" + TrxUserName + "', TrxMenu: 'TrxMenu', TrxGenesysID:'-'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var json = JSON.parse(data.d);
                            var i, x = "";
                            var result = "";
                            for (i = 0; i < json.length; i++) {
                                if (json[i].Result == "True") {
                                    alert(json[i].msgSystem)
                                    ASPxPopupControl1.Hide();
                                    window.location.href = "m_customer.aspx";
                                } else {
                                    alert(json[i].msgSystem)
                                    return false;
                                }
                            }
                        },
                        error: function (xmlHttpRequest, textStatus, errorThrown) {
                            console.log(xmlHttpRequest.responseText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    })

                } else if (TrxAction === 'update') {
                    //alert("Action Update")
                    $.ajax({
                        type: "POST",
                        url: "WebServiceTransaction.asmx/UpdateDataCustomer",
                        data: "{TrxCustomerID: '" + TrxCustomerID + "', TrxName: '" + encodeData(TrxName) + "', TrxEmail: '" + TrxEmail + "', TrxPhone: '" + TrxPhone + "', TrxGender: '" + TrxGender + "', TrxBirth: '" + TrxBirth + "', TrxCIF: '" + TrxCIF + "', TrxNIK: '" + TrxNIK + "', TrxAddress: '" + encodeData(TrxAddress) + "', TrxUserName: '" + TrxUserName + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var json = JSON.parse(data.d);
                            var i, x = "";
                            var result = "";
                            for (i = 0; i < json.length; i++) {
                                if (json[i].Result == "True") {
                                    alert(json[i].msgSystem)
                                    ASPxPopupControl1.Hide();
                                    window.location.href = "m_customer.aspx";
                                } else {
                                    alert(json[i].msgSystem)
                                    return false;
                                }
                            }
                        },
                        error: function (xmlHttpRequest, textStatus, errorThrown) {
                            console.log(xmlHttpRequest.responseText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    })

                } else if (TrxAction === 'delete') {

                    //alert("Action Delete")
                    $.ajax({
                        type: "POST",
                        url: "WebServiceTransaction.asmx/DeleteDataCustomer",
                        data: "{TrxCustomerID: '" + TrxCustomerID + "', TrxUserName: '" + TrxUserName + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var json = JSON.parse(data.d);
                            var i, x = "";
                            var result = "";
                            for (i = 0; i < json.length; i++) {
                                if (json[i].Result == "True") {
                                    alert(json[i].msgSystem)
                                    ASPxPopupControl1.Hide();
                                    window.location.href = "m_customer.aspx";
                                } else {
                                    alert(json[i].msgSystem);
                                    return false;
                                }
                            }
                        },
                        error: function (xmlHttpRequest, textStatus, errorThrown) {
                            console.log(xmlHttpRequest.responseText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    })

                }
            }
            else
                return false;
        }
        function execCancel() {
            ASPxPopupControl1.Hide();
        }
    
        function readCustomer(TrxCustomerID) {
            $('#MainContent_TrxCustomerID').val(TrxCustomerID);
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/SelectDataCustomer",
                data: "{TrxCustomerID: '" + $('#MainContent_TrxCustomerID').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";
                    for (i = 0; i < json.length; i++) {
                        if (json[i].Result == "True") {

                            var Tlahir = ("" + json[i].CustomerTahunlahir + "-" + json[i].CustomerBulanlahir + "-" + json[i].CustomerHarilahir + "");
                            var dateLahir = new Date(Tlahir)
                            DtCustomerBirth.SetDate(dateLahir);
                            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCuxtomerName_I').val(json[i].CustomerName)
                            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_cmbGender_I').val(json[i].CustomerGender);
                            //$('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_DtCustomerBirth_I').val(Tlahir);
                            var email = json[i].CustomerEmail
                            var emailLengt = email.toString().length;
                            var emailSubstring = email.substr(0, 10);
                            if (emailLengt == '27') {
                                if (emailSubstring = "AutoSystem") {
                                    $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerEmail_I').val("");
                                }
                            } else {
                                $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerEmail_I').val(json[i].CustomerEmail);
                            }
                            var phone = json[i].CustomerHP
                            var phoneLengt = phone.toString().length;
                            var phoneSubstring = phone.substr(0, 10);
                            if (phoneLengt == '27') {
                                if (phoneSubstring = "AutoSystem") {
                                    $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerPhone_I').val("");
                                }
                            } else {
                                $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerPhone_I').val(json[i].CustomerHP);
                            }
                            var cif = json[i].CustomerCIF
                            var cifLengt = cif.toString().length;
                            var cifSubstring = cif.substr(0, 10);
                            if (cifLengt == '27') {
                                if (cifSubstring = "AutoSystem") {
                                    $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerCIF_I').val("");
                                }
                            } else {
                                $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerCIF_I').val(json[i].CustomerCIF);
                            }
                            var nik = json[i].CustomerNIK
                            var nikLengt = nik.toString().length;
                            var nikSubstring = nik.substr(0, 10);
                            if (nikLengt == '27') {
                                if (nikSubstring = "AutoSystem") {
                                    $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerNIK_I').val("");
                                }
                            } else {
                                $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtCustomerNIK_I').val(json[i].CustomerNIK);
                            }
                            $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtPelaporAddress_I').val(json[i].CustomerAddress);
                            ASPxPopupControl1.Show();

                        } else {

                        }
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="TrxFromCustomerID" runat="server" />
    <asp:HiddenField ID="TrxToCustomerID" runat="server" />
    <asp:HiddenField ID="TrxUserName" runat="server" />
    <asp:HiddenField ID="TrxCustomerID" runat="server" />
    <asp:HiddenField ID="TrxAction" runat="server" />
    <asp:HiddenField ID="TrxTempPhone" runat="server" />
    <h4 class="headline">Data Customer
			<span class="line bg-warning"></span>
    </h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-tab clearfix">
                <ul class="tab-bar">
                    <li class="active"><a href="#TabA" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;<strong>Add Data Customer</strong></a></li>
                    <li><a href="#TabB" data-toggle="tab" id="tabDataChannel"><i class="fa fa-headphones"></i>&nbsp;<strong>Searching Other Channel</strong></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="TabA" style="margin-left: -14px; margin-bottom: -15px;">
                        <div id="dxGridCustomer"></div>

                        <%-- <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" KeyFieldName="CustomerID" Width="100%"
                            AutoGenerateColumns="False" DataSourceID="ds_Query"
                            SettingsPager-PageSize="15" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                            <SettingsPager>
                                <AllButton Text="All">
                                </AllButton>
                                <NextPageButton Text="Next &gt;">
                                </NextPageButton>
                                <PrevPageButton Text="&lt; Prev">
                                </PrevPageButton>
                                <PageSizeItemSettings Visible="true" Items="15, 25, 50" ShowAllItem="true" />
                            </SettingsPager>
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                            <SettingsBehavior ConfirmDelete="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="70px">
                                    <DataItemTemplate>
                                        <a href="#" onclick="showInsert()">
                                            <asp:Image ImageUrl="img/icon/Apps-text-editor-icon2.png" ID="img_insert" runat="server" ToolTip="Insert Data Customer" />
                                        </a>
                                        <a href="#" onclick="showUpdate('<%# Eval("CustomerID")%>')">
                                            <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="Image1" runat="server" ToolTip="Update Data Customer" />
                                        </a>
                                        <a href="#" onclick="showDelete('<%# Eval("CustomerID")%>')" style="visibility: hidden;">
                                            <asp:Image ImageUrl="img/icon/Actions-edit-clear-icon2.png" ID="Image2" runat="server" ToolTip="Delete Data Customer" />
                                        </a>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" VisibleIndex="0" Visible="false" Width="100px"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="CustomerID" ReadOnly="true" VisibleIndex="1" Width="150px"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Name" FieldName="Name" Width="200" VisibleIndex="2" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Email" FieldName="Email" Width="200" VisibleIndex="3" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Phone Number" FieldName="HP" Width="150px" VisibleIndex="4" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="CIF Number" FieldName="CIF" Width="150px" VisibleIndex="5" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="NIK" FieldName="NIK" Width="150px" VisibleIndex="6" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Address" FieldName="Alamat" Width="300px" VisibleIndex="7" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Group ID" FieldName="GroupID" Width="100px" VisibleIndex="8" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="8" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                                    <DataItemTemplate>
                                        <a href="#" onclick="showGroup('<%# Eval("CustomerID")%>')">
                                            <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="Image1" runat="server" ToolTip="Update Data Group Customer" />
                                        </a>
                                        <a href="#" onclick="deleteGroup('<%# Eval("CustomerID")%>')">
                                            <asp:Image ImageUrl="img/icon/Actions-edit-clear-icon2.png" ID="Image3" runat="server" ToolTip="Delete Data Group Customer" />
                                        </a>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsDetail ShowDetailRow="true" />
                            <Templates>
                                <DetailRow>
                                    <div class="panel-tab clearfix">
                                        <ul class="tab-bar">
                                            <li class="active"><a href="#Tab1" data-toggle="tab"><i class="fa fa-credit-card"></i>&nbsp;<strong>Data Account Number</strong></a></li>
                                            <li><a href="#Tab2" data-toggle="tab"><i class="fa fa-group"></i>&nbsp;<strong>Data Customer Channel</strong></a></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="Tab1">
                                                <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" runat="server" KeyFieldName="ID" SettingsBehavior-AllowFocusedRow="true"
                                                    DataSourceID="dsAccountNumber" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small" SettingsPager-PageSize="5"
                                                    OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect" OnRowInserting="ASPxGridView2_RowInserting" 
                                                    OnRowUpdating="ASPxGridView2_RowUpdating" OnRowDeleting="ASPxGridView2_RowDeleting">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowGroupPanel="false"
                                                        ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                                                    <SettingsBehavior ConfirmDelete="true" AllowSelectByRowClick="false" AllowFocusedRow="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                                            ButtonType="Image" FixedStyle="Left" Width="130px">
                                                            <EditButton Visible="True">
                                                                <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                                                            </EditButton>
                                                            <NewButton Visible="True">
                                                                <Image ToolTip="Add Transaksi" Url="img/icon/Apps-text-editor-icon2.png" />
                                                            </NewButton>
                                                            <DeleteButton Visible="True">
                                                                <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                                                            </DeleteButton>
                                                            <CancelButton>
                                                                <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                                                </Image>
                                                            </CancelButton>
                                                            <UpdateButton>
                                                                <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                                            </UpdateButton>
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                                                            PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Account Number" FieldName="NomorRekening" Settings-AutoFilterCondition="Contains">
                                                            <PropertiesTextEdit>
                                                                <ValidationSettings CausesValidation="True">
                                                                    <RegularExpression ErrorText="Quantity is not a decimal" ValidationExpression="^\d+$" />
                                                                    <RequiredField ErrorText="Quantity is required" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </PropertiesTextEdit>
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </div>
                                            <div class="tab-pane fade" id="Tab2">
                                                <dx:ASPxGridView ID="ASPxGridView3" ClientInstanceName="ASPxGridView3" runat="server" Width="100%"
                                                    Theme="Metropolis" DataSourceID="dsChannel" KeyFieldName="ID" OnRowInserting="ASPxGridView3_RowInserting" OnRowUpdating="ASPxGridView3_RowUpdating"
                                                    Styles-Header-Font-Bold="true" Font-Size="X-Small" OnRowDeleting="ASPxGridView3_RowDeleting">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                    </SettingsPager>
                                                    <SettingsPager PageSize="5" />
                                                    <SettingsEditing Mode="Inline" />
                                                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowGroupPanel="false"
                                                        ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                                                    <SettingsBehavior ConfirmDelete="true" AllowSelectByRowClick="false" AllowFocusedRow="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                                            ButtonType="Image" FixedStyle="Left" Width="130px">
                                                            <EditButton Visible="True">
                                                                <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                                                            </EditButton>
                                                            <NewButton Visible="True">
                                                                <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                                                            </NewButton>
                                                            <DeleteButton Visible="True">
                                                                <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                                                            </DeleteButton>
                                                            <CancelButton>
                                                                <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                                                </Image>
                                                            </CancelButton>
                                                            <UpdateButton>
                                                                <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                                            </UpdateButton>
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="20px" ReadOnly="true" Visible="false" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Channel" FieldName="ValueChannel" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Type" FieldName="FlagChannel" HeaderStyle-HorizontalAlign="left"
                                                            Width="70px">
                                                            <PropertiesComboBox>
                                                                <Items>
                                                                    <dx:ListEditItem Text="Phone" Value="Phone" Selected="true" />
                                                                    <dx:ListEditItem Text="Email" Value="Email" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </div>
                                        </div>
                                    </div>
                                </DetailRow>
                            </Templates>
                        </dx:ASPxGridView> --%>
                    </div>
                    <div class="tab-pane fade" id="TabB" style="margin-left: -14px; margin-bottom: -15px;">
                        <div id="dxGridChannel"></div>

                        <%--  <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" Theme="Default" ShowHeader="false">
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server">
                                    <dx:ASPxFilterControl ID="ASPxFilterControl1" runat="server" ClientInstanceName="filter" Width="100%" Theme="Metropolis">
                                        <Columns>
                                            <dx:FilterControlColumn PropertyName="Name" DisplayName="Name" />
                                            <dx:FilterControlColumn PropertyName="ValueChannel" DisplayName="Channel" />
                                        </Columns>
                                        <ClientSideEvents Applied="function(s, e) { grid.ApplyFilter(e.filterExpression);}" />
                                    </dx:ASPxFilterControl>
                                    <table style="margin-bottom: -5px;">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton runat="server" ID="btnApply" Text="Submit" AutoPostBack="false" UseSubmitBehavior="false" Width="100px" Style="margin: 12px auto 0; display: block;" Theme="Metropolis" Font-Bold="true">
                                                    <ClientSideEvents Click="function() { filter.Apply(); }" />
                                                </dx:ASPxButton>
                                            </td>
                                            <td></td>
                                            <td>
                                                <dx:ASPxButton runat="server" ID="btnClear" Text="Clear" AutoPostBack="False" Width="100px" UseSubmitBehavior="False" Style="margin: 12px auto 0; display: block;" Theme="Metropolis" Font-Bold="true">
                                                    <ClientSideEvents Click="function() { filter.Clear(); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                        KeyFieldName="CustomerID" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="15"
                                        OnCustomColumnGroup="grid_CustomColumnGroup" OnCustomGroupDisplayText="grid_CustomGroupDisplayText" OnCustomColumnSort="grid_CustomColumnSort">
                                        <SettingsPager>
                                            <AllButton Text="All">
                                            </AllButton>
                                            <NextPageButton Text="Next &gt;">
                                            </NextPageButton>
                                            <PrevPageButton Text="&lt; Prev">
                                            </PrevPageButton>
                                            <PageSizeItemSettings Visible="true" Items="15, 20, 25" ShowAllItem="true" />
                                        </SettingsPager>
                                        <SettingsEditing Mode="Inline" />
                                        <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="20px" ReadOnly="true" Visible="false" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="CustomerID" Settings-AutoFilterCondition="Contains" Width="150"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Customer Name" FieldName="Name" Visible="false" Settings-AutoFilterCondition="Contains" Width="250" GroupIndex="1"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Channel" FieldName="ValueChannel" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Type" FieldName="FlagChannel" Settings-AutoFilterCondition="Contains" Width="150"></dx:GridViewDataTextColumn>
                                        </Columns>
                                        <SettingsBehavior AutoExpandAllGroups="true" />
                                        <Settings ShowGroupedColumns="True" />
                                    </dx:ASPxGridView>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxRoundPanel> --%>
                    </div>
                </div>
            </div>
            <asp:SqlDataSource ID="ds_Query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mcustomer"></asp:SqlDataSource>
            <asp:SqlDataSource ID="dsAccountNumber" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                SelectCommand="select * from BTN_NomorRekening where CustomerID=@customerid">
                <SelectParameters>
                    <asp:Parameter Name="customerid" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="dsChannel" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                SelectCommand="select * from mCustomerChannel where CustomerID=@customerid">
                <SelectParameters>
                    <asp:Parameter Name="customerid" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <%--CustomerID, FlagChannel, ValueChannel, UserCreate--%>
        </div>
    </div>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="true" Width="1000px"
        closeonescape="true"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Data Customer" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="1000px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Name</label>
                                    <dx:ASPxTextBox ID="TxtCuxtomerName" runat="server" Theme="Metropolis" Height="30px" Width="100%"></dx:ASPxTextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email Address</label>
                                    <dx:ASPxTextBox ID="TxtCustomerEmail" runat="server" Theme="Metropolis" Height="30px" Width="100%">
                                        <ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Invalid Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*.\w+([-.]\w+)*" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>
                                <div class="col-md-6">
                                    <label>Phone Number</label>
                                    <dx:ASPxTextBox ID="TxtCustomerPhone" ClientInstanceName="TxtCustomerPhone" runat="server" Theme="Metropolis" Height="30px" Width="100%">
                                        <ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Please Enter Numbers Only" ValidationExpression="^\d+$" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Gender</label>
                                    <dx:ASPxComboBox ID="cmbGender" runat="server" Width="100%" Height="30px" Theme="Metropolis">
                                        <Items>
                                            <dx:ListEditItem Text="--Select--" Value="0" Selected="true" />
                                            <dx:ListEditItem Text="Male" Value="Male" />
                                            <dx:ListEditItem Text="Female" Value="Female" />
                                        </Items>
                                    </dx:ASPxComboBox>
                                </div>
                                <div class="col-md-6">
                                    <label>Date of Birth</label>
                                    <dx:ASPxDateEdit ID="DtCustomerBirth" ClientInstanceName="DtCustomerBirth" runat="server" Width="100%" Height="30px" Theme="Metropolis" DisplayFormatString="yyyy-MM-dd"></dx:ASPxDateEdit>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>CIF Number</label>
                                    <dx:ASPxTextBox ID="TxtCustomerCIF" runat="server" Theme="Metropolis" Height="30px" Width="100%">
                                        <ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Please Enter Numbers Only" ValidationExpression="^\d+$" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>
                                <div class="col-md-6">
                                    <label>NIK</label>
                                    <dx:ASPxTextBox ID="TxtCustomerNIK" ClientInstanceName="TxtCustomerNIK" runat="server" Theme="Metropolis" Height="30px" Width="100%">
                                        <ValidationSettings SetFocusOnError="True" ValidationGroup="EditForm" Display="Dynamic" ErrorTextPosition="Bottom" ErrorFrameStyle-Font-Size="X-Small">
                                            <RequiredField IsRequired="True" ErrorText="Required" />
                                            <RegularExpression ErrorText="Please Enter Numbers Only" ValidationExpression="^\d+$" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Address</label>
                                    <dx:ASPxMemo ID="TxtPelaporAddress" runat="server" Width="100%" Rows="10" Theme="Metropolis" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2">
                                    <dx:ASPxButton ID="btnSubmit" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                        <ClientSideEvents Click="function(s, e) { execSubmit(); }" />
                                    </dx:ASPxButton>
                                </div>
                                <div class="col-md-2">
                                    <dx:ASPxButton ID="btnCancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                        HoverStyle-BackColor="#EE4D2D" Height="30px">
                                        <ClientSideEvents Click="function(s, e) { execCancel(); }" />
                                    </dx:ASPxButton>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2" runat="server" CloseAction="CloseButton" Modal="true" Width="1100px"
        closeonescape="true" PopupVerticalAlign="WindowCenter" Height="550px" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Grouping Data Customer" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <div id="dxGroupingCustomer"></div>
                
                <%-- <dx:ASPxGridView ID="ASPxGridView4" ClientInstanceName="ASPxGridView4" runat="server" KeyFieldName="CustomerID" Width="100%"
                    AutoGenerateColumns="False" SettingsPager-PageSize="15" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
                    </SettingsPager>
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="70px">
                            <DataItemTemplate>
                                <a href="#" onclick="updateSync('<%# Eval("CustomerID")%>')" style="color: black;">
                                    <dx:ASPxButton ID="btnSyncronise" runat="server" Theme="Metropolis" Text="Update" Width="100%" AutoPostBack="false"
                                        HoverStyle-BackColor="#EE4D2D" Height="20px" Font-Size="X-Small" Font-Bold="true">
                                    </dx:ASPxButton>
                                </a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" VisibleIndex="0" Visible="false" Width="100px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="CustomerID" ReadOnly="true" VisibleIndex="1" Width="150px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="Name" Width="150px" VisibleIndex="2" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email" FieldName="Email" Width="150px" VisibleIndex="3" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Phone Number" FieldName="HP" Width="150px" VisibleIndex="4" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="CIF Number" FieldName="CIF" Width="150px" VisibleIndex="5" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="NIK" FieldName="NIK" Width="150px" VisibleIndex="6" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Address" FieldName="Alamat" Width="250px" VisibleIndex="7" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView> --%>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
