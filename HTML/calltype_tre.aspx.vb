﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxGridLookup
Imports DevExpress.Web.ASPxClasses
Partial Class calltype_tre
    Inherits System.Web.UI.Page

    Dim Com As SqlCommand
    Dim Dr As SqlDataReader
    Dim SelTicket, SelTicket1, Categori As String
    Dim Connection, con, ConnectionTest As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Comm, sqlcom As SqlCommand
    Dim Proses As New ClsConn
    Dim Sqldr As SqlDataReader
    Dim sql As String
    Dim _strScript As String = String.Empty
    Dim _strlogTime As String = DateTime.Now.ToString("yyyy")
    Dim _Number As String = String.Empty
    Dim _LastNumber As String = String.Empty
    Dim _strSql As String = String.Empty
    Dim _GenerateNoID As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("NamaForm") = IO.Path.GetFileName(Request.Path)
        DsCategory.SelectCommand = "exec BTN_SPmsublevel3"
        updateAlert()
    End Sub
    Private Sub cmbCombo2_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)
        FillComboUnitKerja(TryCast(source, ASPxComboBox), e.Parameter, dsmSubCategoryLv1)
    End Sub
    Private Sub cmbCombo4_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)
        FillComboSubject(TryCast(source, ASPxComboBox), e.Parameter, dsmSubCategoryLv2)
    End Sub
    Protected Sub ASPxGridLookupTagIDs_OnInit(ByVal sender As Object, ByVal e As EventArgs)
        Dim lookup = CType(sender, ASPxGridLookup)
        'lookup.DataSource = Session("TagsData")
        Dim container = CType(lookup.NamingContainer, GridViewEditItemTemplateContainer)
        If container.Grid.IsNewRowEditing Then Return
        Dim tagIDs = CStr(container.Grid.GetRowValues(container.VisibleIndex, container.Column.FieldName))
        Dim aa As Integer
        If tagIDs IsNot Nothing Then
            'lookup.GridView.Selection.SelectRowByKey(12)
            'lookup.GridView.Selection.SelectRowByKey(13)
            For Each tagID In tagIDs.Split(","c)
                lookup.GridView.Selection.SelectRowByKey(tagIDs)
            Next
        End If
    End Sub
    Protected Sub InitializeComboUnitKerja(ByVal e As ASPxGridViewEditorEventArgs, ByVal parentComboName As String, ByVal source As SqlDataSource, ByVal callBackHandler As CallbackEventHandlerBase)
        Dim id As String = String.Empty
        If (Not ASPxGridView1.IsNewRowEditing) Then
            Dim val As Object = ASPxGridView1.GetRowValuesByKeyValue(e.KeyValue, parentComboName)
            If (val Is Nothing OrElse val Is DBNull.Value) Then
                id = Nothing
            Else
                id = val.ToString()
            End If
        End If
        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        If combo IsNot Nothing Then
            ' unbind combo
            combo.DataSourceID = Nothing
            FillComboSubject(combo, id, source)
            AddHandler combo.Callback, callBackHandler
        End If
        Return
    End Sub
    Protected Sub InitializeComboSubject(ByVal e As ASPxGridViewEditorEventArgs, ByVal parentComboName As String, ByVal source As SqlDataSource, ByVal callBackHandler As CallbackEventHandlerBase)
        Dim id As String = String.Empty
        If (Not ASPxGridView1.IsNewRowEditing) Then
            Dim val As Object = ASPxGridView1.GetRowValuesByKeyValue(e.KeyValue, parentComboName)
            If (val Is Nothing OrElse val Is DBNull.Value) Then
                id = Nothing
            Else
                id = val.ToString()
            End If
        End If
        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        If combo IsNot Nothing Then
            ' unbind combo
            combo.DataSourceID = Nothing
            FillComboSubject(combo, id, source)
            AddHandler combo.Callback, callBackHandler
        End If
        Return
    End Sub
    Protected Sub FillComboUnitKerja(ByVal cmb As ASPxComboBox, ByVal id As String, ByVal source As SqlDataSource)
        cmb.Items.Clear()
        ' trap null selection
        If String.IsNullOrEmpty(id) Then
            Return
        End If

        ' get the values
        source.SelectParameters(0).DefaultValue = id
        Dim view As DataView = CType(source.Select(DataSourceSelectArguments.Empty), DataView)
        For Each row As DataRowView In view
            cmb.Items.Add(row(3).ToString(), row(2))
        Next row
    End Sub
    Protected Sub FillComboSubject(ByVal cmb As ASPxComboBox, ByVal id As String, ByVal source As SqlDataSource)
        cmb.Items.Clear()
        ' trap null selection
        If String.IsNullOrEmpty(id) Then
            Return
        End If

        ' get the values
        source.SelectParameters(0).DefaultValue = id
        Dim view As DataView = CType(source.Select(DataSourceSelectArguments.Empty), DataView)
        For Each row As DataRowView In view
            cmb.Items.Add(row(4).ToString(), row(2))
        Next row
    End Sub
    Protected Sub ASPxGridView1_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles ASPxGridView1.CellEditorInitialize
        Select Case e.Column.FieldName
            'Case "JenisTransaksi"
            '    InitializeCombo(e, "CategoryID", dsmCategory, AddressOf cmbCombo2_OnCallback)
            'Case "UnitKerja"
            '    InitializeComboUnitKerja(e, "CategoryID", BTN_dsTrxLaporan, AddressOf cmbCombo3_OnCallback)
            Case "UnitKerja"
                InitializeComboUnitKerja(e, "CategoryID", dsmSubCategoryLv1, AddressOf cmbCombo2_OnCallback)
            Case "NameSubject"
                InitializeComboSubject(e, "SubCategory1ID", dsmSubCategoryLv2, AddressOf cmbCombo4_OnCallback)

            Case Else
        End Select
        If ASPxGridView1.IsNewRowEditing Then
            If e.Column.FieldName = "NA" Then
                e.Editor.Value = "Y"
            End If
        Else
        End If
    End Sub
    Protected Sub ASPxGridView1_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        Dim _JenisTransaksi As String = e.NewValues("JenisTransaksi").ToString()
        Dim _SubjectTable As String = e.NewValues("SubjectTable").ToString()
        Dim _NameSubject As String = e.NewValues("NameSubject").ToString()
        Dim _UnitKerja As String = e.NewValues("UnitKerja").ToString()
        Dim _ReasonCode As String = String.Empty
        Try
            If String.IsNullOrEmpty(e.NewValues("ReasonCode").ToString) = True Then
            Else
                _ReasonCode = e.NewValues("ReasonCode").ToString
            End If
        Catch ex As Exception
            _ReasonCode = ""
        End Try        
        Dim _SLA As String = e.NewValues("SLA")
        Dim _NA As String = e.NewValues("NA")
        Dim _TujuanEskalasi As String = e.NewValues("TujuanEskalasi")
        Dim _CountingTransaksi As String = String.Empty

        Dim _strCounting As String = "Select COUNT (SubName) as CekTransaksi from mSubCategoryLv3 where CategoryID='" & _JenisTransaksi & "' and SubCategory1ID='" & _UnitKerja & "' and SubCategory2ID='" & _NameSubject & "' and SubName='" & _SubjectTable & "'"
        Sqldr = Proses.ExecuteReader(_strCounting)
        If Sqldr.HasRows Then
            Sqldr.Read()
            _CountingTransaksi = Sqldr("CekTransaksi").ToString
            Proses.LogSuccess(_strlogTime, " _Checking mSubCategoryLv3 - " & _strCounting)
        Else
            e.Cancel = True
            Throw New Exception(_JenisTransaksi & " already exists")
        End If
        Sqldr.Close()

        Dim _strNumber As String = "select SUBSTRING(SubCategory3ID,5,5) as Number from mSubCategoryLv3 order by ID desc"
        Try
            Sqldr = Proses.ExecuteReader(_strNumber)
            If Sqldr.Read() Then
                _Number = Sqldr("Number").ToString
            End If
            Sqldr.Close()
            Proses.LogSuccess(_strlogTime, " _Number mSubCategoryLv3 - " & _strNumber)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, " _Number mSubCategoryLv3 - " & _strNumber)
            Response.Write(ex.Message)
        End Try

        Dim _strLastNumber As String = String.Empty
        Try
            If _Number = "" Then
                _strLastNumber = "select  NLast = Right(100000 + COUNT(ID) + 0, 5)  from mSubCategoryLv3"
            Else
                _strLastNumber = "select  NLast = Right(100" & _Number & " + 1 + 0, 5)  from mSubCategoryLv3"
            End If
            Sqldr = Proses.ExecuteReader(_strLastNumber)
            If Sqldr.HasRows Then
                Sqldr.Read()
                _LastNumber = Sqldr("NLast").ToString
            Else
            End If
            Sqldr.Close()
            Proses.LogSuccess(_strlogTime, " _LastNumber mSubCategoryLv3 - " & _strLastNumber)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, " _LastNumber mSubCategoryLv3 - " & _strLastNumber)
            Response.Write(ex.Message)
        End Try

        _GenerateNoID = "CT3-" & _LastNumber & ""
        Try
            _strScript = "insert into mSubCategoryLv3 (CategoryID, SubCategory1ID, SubCategory2ID, SubCategory3ID, SubName, ReasonCode, SLA, NA, TujuanEskalasi, UserCreate, DateCreate) values ('" & _JenisTransaksi & "', '" & _UnitKerja & "', '" & _NameSubject & "', '" & _GenerateNoID & "', '" & _SubjectTable & "', '" & _ReasonCode & "', '" & _SLA & "', '" & _NA & "', '" & _TujuanEskalasi & "', '" & Session("USERNAME") & "', GETDATE())"
            DsCategory.InsertCommand = _strScript
            Proses.LogSuccess(_strlogTime, "insert mSubCategoryLv3 - " & _strScript)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, "insert mSubCategoryLv3 - " & _strScript)
            Response.Write(ex.Message)
        End Try
        'DsCategory.InsertCommand = "insert into mSubCategoryLv3 (CategoryID,SubCategory1ID,SubCategory2ID,SubCategory3ID,SubName,SLA,NA,TujuanEskalasi) values (@JenisTransaksi,@UnitKerja,@NameSubject,'" & GenerateNoID & "',@SubjectTable,@SLA,@NA,@TujuanEskalasi)"
    End Sub
    Protected Sub ASPxGridView1_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim cekSubjectTable, cekUnitKerja, cekNameSubject, cekJenisTransaksi As String
        Dim OldcekSubjectTable, OldcekUnitKerja, OldcekNamaSubject, OldcekJenisTransaksi As String

        cekJenisTransaksi = e.NewValues("JenisTransaksi").ToString()
        cekSubjectTable = e.NewValues("SubjectTable").ToString()
        cekUnitKerja = e.NewValues("UnitKerja").ToString()
        cekNameSubject = e.NewValues("NameSubject").ToString

        OldcekJenisTransaksi = e.OldValues("JenisTransaksi").ToString()
        OldcekSubjectTable = e.OldValues("SubjectTable").ToString()
        OldcekUnitKerja = e.OldValues("UnitKerja").ToString()
        OldcekNamaSubject = e.OldValues("NameSubject").ToString

        Dim idOrganization As String
        idOrganization = "select * from mOrganization where ORGANIZATION_NAME='" & e.NewValues("TujuanEskalasi").ToString & "'"
        Comm = New SqlCommand(idOrganization, Connection)
        Try
            Connection.Open()
            Dr = Comm.ExecuteReader()
            If Dr.HasRows Then
                Dr.Read()
                idOrganization = Dr("ORGANIZATION_ID").ToString
            Else
                idOrganization = e.NewValues("TujuanEskalasi").ToString
            End If
            Dr.Close()
            Connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
     
        Dim CategoryID, strCategoryID As String
        If cekJenisTransaksi = OldcekJenisTransaksi Then
            strCategoryID = "select * from mCategory where Name='" & e.OldValues("JenisTransaksi").ToString & "'"
        Else
            strCategoryID = "select * from mCategory where Name='" & e.NewValues("JenisTransaksi").ToString & "'"
        End If
        Try
            Comm = New SqlCommand(strCategoryID, Connection)
            Connection.Open()
            Dr = Comm.ExecuteReader()
            If Dr.HasRows Then
                Dr.Read()
                CategoryID = Dr("CategoryID").ToString
            Else
                CategoryID = e.NewValues("JenisTransaksi").ToString
            End If
            Dr.Close()
            Connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        
        Dim Level1ID, strLevel1ID As String
        If cekUnitKerja = OldcekUnitKerja Then
            strLevel1ID = "select * from mSubCategoryLv1 where SubName='" & e.OldValues("UnitKerja").ToString & "'"
        Else
            strLevel1ID = "select * from mSubCategoryLv1 where SubName='" & e.NewValues("UnitKerja").ToString & "'"
        End If
        Try
            Comm = New SqlCommand(strLevel1ID, Connection)
            Connection.Open()
            Dr = Comm.ExecuteReader()
            If Dr.HasRows Then
                Dr.Read()
                Level1ID = Dr("SubCategory1ID").ToString
            Else
                Level1ID = e.NewValues("UnitKerja").ToString
            End If
            Dr.Close()
            Connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
      
        Dim Level2ID, strLevel2ID As String
        If cekNameSubject = OldcekNamaSubject Then
            strLevel2ID = "select * from mSubCategoryLv2 where SubName='" & e.OldValues("NameSubject").ToString & "'"
        Else
            strLevel2ID = "select * from mSubCategoryLv2 where SubName='" & e.NewValues("NameSubject").ToString & "'"
        End If
        Try
            Comm = New SqlCommand(strLevel2ID, Connection)
            Connection.Open()
            Dr = Comm.ExecuteReader()
            If Dr.HasRows Then
                Dr.Read()
                Level2ID = Dr("SubCategory2ID").ToString
            Else
                Level2ID = e.NewValues("NameSubject").ToString
            End If

            Dr.Close()
            Connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        
        If cekSubjectTable <> OldcekSubjectTable And cekUnitKerja <> OldcekUnitKerja Then
            Dim SourceJenisTransaksi As String
            SourceJenisTransaksi = "select COUNT(a.SubName) as CekTransaksi from mSubCategoryLv3 a left outer join mSubCategoryLv2 b on a.SubCategory2ID=b.SubCategory2ID where b.SubName='" & cekNameSubject & "' and a.SubName='" & cekSubjectTable & "'"
            Comm = New SqlCommand(SourceJenisTransaksi, Connection)
            Connection.Open()
            Dr = Comm.ExecuteReader()
            Dr.Read()
            SourceJenisTransaksi = Dr("CekTransaksi").ToString
            Dr.Close()
            Connection.Close()

            If SourceJenisTransaksi > 0 Then
                e.Cancel = True
                Throw New Exception(cekSubjectTable & " already exists")
            Else
                e.Cancel = False
                'DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET CategoryID='" & CategoryID & "', SubCategory1ID='" & Level1ID & "', SubCategory2ID='" & Level2ID & "', SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
                DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
            End If

        ElseIf OldcekNamaSubject <> cekNameSubject Then

            Dim SourceJenisTransaksi As String
            SourceJenisTransaksi = "select COUNT(a.SubName) as CekTransaksi from mSubCategoryLv3 a left outer join mSubCategoryLv2 b on a.SubCategory2ID=b.SubCategory2ID where b.SubName='" & cekNameSubject & "' and a.SubName='" & cekSubjectTable & "'"
            Comm = New SqlCommand(SourceJenisTransaksi, Connection)
            Connection.Open()
            Dr = Comm.ExecuteReader()
            Dr.Read()
            SourceJenisTransaksi = Dr("CekTransaksi").ToString
            Dr.Close()
            Connection.Close()

            If SourceJenisTransaksi > 0 Then
                e.Cancel = True
                Throw New Exception(cekSubjectTable & " already exists")
            Else
                e.Cancel = False
                'DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET CategoryID='" & CategoryID & "', SubCategory1ID='" & Level1ID & "', SubCategory2ID='" & Level2ID & "', SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
                DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
            End If
        ElseIf cekUnitKerja <> OldcekUnitKerja Then
            'DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET CategoryID='" & CategoryID & "', SubCategory1ID='" & Level1ID & "', SubCategory2ID='" & Level2ID & "', SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
            DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
        Else
            e.Cancel = False
            'DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET CategoryID='" & CategoryID & "', SubCategory1ID='" & Level1ID & "', SubCategory2ID='" & Level2ID & "', SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
            DsCategory.UpdateCommand = "UPDATE mSubCategoryLv3 SET SubName=@SubjectTable, NA=@NA, TujuanEskalasi='" & idOrganization & "', ReasonCode=@ReasonCode, SLA=@SLA where ID=@ID"
        End If
    End Sub
    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
