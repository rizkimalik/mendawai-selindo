﻿Imports System.Data
Imports System.Data.SqlClient
Public Class R_Transaction
    Inherits System.Web.UI.Page

    Dim Cls As New WebServiceTransaction
    Dim proses As New ClsConn
    Dim _upage As String
    Dim sqlRead As SqlDataReader
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles Me.Load
        'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd 00:00:01")
        'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd 23:59:59")
        'Try
        '    Dim strSql As New R_Transaction_DBMLDataContext
        '    strSql.CommandTimeout = 480
        '    ASPxGridView1.DataSource = strSql.R_Transaction("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "").ToList()
        '    ASPxGridView1.DataBind()

        '    Dim strQuery As String = "LoadReportingBaseonTransaction_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogSuccess(strLogTime, strQuery)
        'Catch ex As Exception
        '    Dim strQuery As String = "LoadReportingBaseonTransaction_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogError(strLogTime, ex, strQuery)
        'End Try
    End Sub
    'Private Sub btn_Submit_Click(sender As Object, e As EventArgs) Handles btn_Submit.Click
    '    'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = dt_strdate.Text & " 00:01"
    '    'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = dt_endate.Text & " 23:59"
    '    'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd 00:00:01")
    '    'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd 23:59:59")
    '    'Try
    '    '    Dim strSql As New R_Transaction_DBMLDataContext
    '    '    strSql.CommandTimeout = 480
    '    '    ASPxGridView1.DataSource = strSql.R_Transaction("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "").ToList()
    '    '    ASPxGridView1.DataBind()

    '    '    Dim strQuery As String = "SubmitReportingBaseonTransaction_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
    '    '    Cls.LogSuccess(strLogTime, strQuery)
    '    'Catch ex As Exception
    '    '    Dim strQuery As String = "SubmitReportingBaseonTransaction_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
    '    '    Cls.LogError(strLogTime, ex, strQuery)
    '    'End Try
    'End Sub
    Private Sub dt_strdate_Init(sender As Object, e As EventArgs) Handles dt_strdate.Init
        dt_strdate.Value = DateTime.Now
    End Sub
    Private Sub dt_endate_Init(sender As Object, e As EventArgs) Handles dt_endate.Init
        dt_endate.Value = DateTime.Now
    End Sub
    'Private Sub btn_Export_Click(sender As Object, e As EventArgs) Handles btn_Export.Click
    '    'Try
    '    '    'Dim strSql As New R_Transaction_DBMLDataContext
    '    '    'strSql.CommandTimeout = 480
    '    '    'ASPxGridView1.DataSource = strSql.R_Transaction("" & Format(dt_strdate.Value, "yyyy-MM-dd 00:00:01") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd 23:59:59") & "").ToList()
    '    '    'ASPxGridView1.DataBind()

    '    '    Dim strQuery As String = "ExportReportingBaseonTransaction_" & Format(dt_strdate.Value, "yyyy-MM-dd 00:00:01") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
    '    '    Cls.LogSuccess(strLogTime, strQuery)
    '    'Catch ex As Exception
    '    '    Dim strQuery As String = "ExportReportingBaseonTransaction_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
    '    '    Cls.LogError(strLogTime, ex, strQuery)
    '    'End Try
    '    'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = dt_strdate.Text & " 00:01"
    '    'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = dt_endate.Text & " 23:59"
    '    'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd 00:00:01")
    '    'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd 23:59:59")

    '    Dim TimeS As DateTime
    '    Dim Val As String = Cls.FunctionDataSetting("Report")

    '    TimeS = dt_endate.Value
    '    'Dim strTgl As String
    '    'Dim strString As String = "SELECT DATEADD(DAY," & Cls.FunctionDataSetting("Report") & ",'" & dt_endate.Value & "') as TglSelected"
    '    'Try
    '    '    sqlRead = proses.ExecuteReader(strString)
    '    '    If sqlRead.HasRows Then
    '    '        sqlRead.Read()
    '    '        strTgl = sqlRead("TglSelected").ToString
    '    '    End If
    '    'Catch ex As Exception
    '    '    Cls.LogError(strLogTime, ex, strString)
    '    'End Try

    '    Dim fromDate As DateTime = Format(dt_strdate.Value, "yyyy-MM-dd")
    '    Dim toDate As DateTime = Format(dt_endate.Value, "yyyy-MM-dd")
    '    Dim ddate As String
    '    ddate = DateDiff(DateInterval.Day, toDate, fromDate)
    '    Dim Idat As Integer = Convert.ToInt32(ddate)
    '    Dim Ival As Integer = Convert.ToInt32(Val)
    '    'If dt_strdate.Value < strTgl Then

    '    If Ival > Idat Then
    '        alertJS("Export maximum " & ReplaceSpecialLetter(Cls.FunctionDataSetting("Report")) & " days")
    '    Else

    '        Dim casses As String = ddList.SelectedValue
    '        Select Case casses
    '            Case "xlsx"
    '                ASPxGridViewExporter1.WriteXlsxToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '            Case "xls"
    '                ASPxGridViewExporter1.WriteXlsToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '            Case "rtf"
    '                ASPxGridViewExporter1.Landscape = True
    '                ASPxGridViewExporter1.LeftMargin = 35
    '                ASPxGridViewExporter1.RightMargin = 30
    '                ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
    '                ASPxGridViewExporter1.MaxColumnWidth = 108
    '                ASPxGridViewExporter1.WriteRtfToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '            Case "pdf"
    '                ASPxGridViewExporter1.Landscape = True
    '                ASPxGridViewExporter1.LeftMargin = 35
    '                ASPxGridViewExporter1.RightMargin = 30
    '                ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
    '                ASPxGridViewExporter1.MaxColumnWidth = 108
    '                ASPxGridViewExporter1.WritePdfToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '            Case "csv"
    '                ASPxGridViewExporter1.WriteCsvToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '        End Select
    '        'Cls.LogSuccess(strLogTime, strString)
    '    End If
    '    'Dim casses As String = ddList.SelectedValue
    '    'Select Case casses
    '    '    Case "xlsx"
    '    '        ASPxGridViewExporter1.WriteXlsxToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '    '    Case "xls"
    '    '        ASPxGridViewExporter1.WriteXlsToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '    '    Case "rtf"
    '    '        ASPxGridViewExporter1.Landscape = True
    '    '        ASPxGridViewExporter1.LeftMargin = 35
    '    '        ASPxGridViewExporter1.RightMargin = 30
    '    '        ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
    '    '        ASPxGridViewExporter1.MaxColumnWidth = 108
    '    '        ASPxGridViewExporter1.WriteRtfToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '    '    Case "pdf"
    '    '        ASPxGridViewExporter1.Landscape = True
    '    '        ASPxGridViewExporter1.LeftMargin = 35
    '    '        ASPxGridViewExporter1.RightMargin = 30
    '    '        ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
    '    '        ASPxGridViewExporter1.MaxColumnWidth = 108
    '    '        ASPxGridViewExporter1.WritePdfToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '    '    Case "csv"
    '    '        ASPxGridViewExporter1.WriteCsvToResponse("ReportingBaseonTransaction_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
    '    'End Select
    'End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Function alertJS(ByVal alert As String)
        Dim message As String = alert
        Dim sb As New System.Text.StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload=function(){")
        sb.Append("alert('")
        sb.Append(message)
        sb.Append("')};")
        sb.Append("</script>")
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
    End Function
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "-", "")
        ReplaceSpecialLetter = TmpStr
    End Function
End Class