﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxGridView
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.DirectoryInfo
Imports System.Diagnostics
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web
Imports DevExpress.Data.Linq
Imports DevExpress.XtraEditors
Imports System
'Imports System.Data.Linq

Public Class m_customer
    Inherits System.Web.UI.Page

    Dim strExecute As New ClsConn
    Dim sqldr, readPhone, readAccount As SqlDataReader
    Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim strSql As String = String.Empty
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim _upage As String = String.Empty
    'Dim instance As New DbContext(nameOrConnectionString)

    Private entityServerModeSource As EntityServerModeSource

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TrxUserName.Value = Session("username")
        If Request.QueryString("account") <> "" And Request.QueryString("accountid") <> "" Then
            ASPxPopupControl1.ShowOnPageLoad = True
        End If
        _updatePage()

        'entityServerModeSource = New EntityServerModeSource With {.ElementType = GetType(mCustomer), .KeyExpression = "CustomerID"}
        'Dim context As New mCustomer()
    End Sub

    'Protected Sub EntityServerModeDataSource_Selecting(ByVal sender As Object, ByVal e As DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs)
    '    EntityServerModeDataSource.DataBind()
    'End Sub
    'Private Sub ASPxGridView5_Load(sender As Object, e As EventArgs) Handles ASPxGridView5.Load
    '    entityServerModeSource = New EntityServerModeSource With {.ElementType = GetType(mCustomer), .KeyExpression = "CustomerID"}
    '    Dim context As New mCustomer()
    '    'EntityServerModeDataSource.ViewStateMode = True
    '    'grid1.DataSourceID = "CustomerID"
    '    ''grid1.DataBind()

    '    'grid1.DataSourceID = EntityServerModeDataSource
    'End Sub
    'Protected Sub EntityServerModeDataSource_Selecting(ByVal sender As Object, ByVal e As DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs)
    '    'Dim db = New m_Cust1DataContext()
    '    'e.KeyExpression = "CustomerID"
    '    'e.QueryableSource = db.mCustomers
    '    'e.QueryableSource = db.mCustomers_)
    'End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        'Session("customerid") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        Session("customerid") = (TryCast(sender, ASPxGridView)).GetMasterRowFieldValues("CustomerID").ToString()
        dsAccountNumber.SelectParameters("customerid").DefaultValue = Session("customerid")
        dsChannel.SelectParameters("customerid").DefaultValue = Session("customerid")
    End Sub
    Protected Sub ASPxGridView2_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        If Session("customerid").ToString().ToString() <> "" Then
            Dim _strScript As String = String.Empty
            Dim _AccountNumber As String = e.NewValues("NomorRekening")
            Dim substAccountNumber As Integer = _AccountNumber.Length
            If (ValidasiAccountNumberLength(substAccountNumber)) = False Then
                e.Cancel = True
                Throw New Exception(_AccountNumber & " Format account number is not valid")
            Else
                strSql = "select NomorRekening  from BTN_NomorRekening WHERE NomorRekening='" & _AccountNumber & "'"
                sqldr = strExecute.ExecuteReader(strSql)
                If sqldr.HasRows() Then
                    sqldr.Read()
                    e.Cancel = True
                    Throw New Exception(_AccountNumber & " already exists")
                Else
                    e.Cancel = False
                    _strScript = "insert into BTN_NomorRekening(CustomerID, NomorRekening, Usercreate) VALUES ('" & Session("customerid") & "', '" & _AccountNumber & "', '" & Session("username") & "')"
                    'strExecute.LogSuccess(strLogTime, _strScript)
                    dsAccountNumber.InsertCommand = _strScript
                End If
                sqldr.Close()
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView2_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        If Session("customerid").ToString() <> "" Then
            Dim _strScript As String = String.Empty
            Dim _idTransaction As String = e.Keys("ID")
            Dim _AccountNumber As String = e.NewValues("NomorRekening")
            Dim substAccountNumber As Integer = _AccountNumber.Length
            If (ValidasiAccountNumberLength(substAccountNumber)) = False Then
                e.Cancel = True
                Throw New Exception(_AccountNumber & " Format account number is not valid")
            Else
                strSql = "select CustomerID, NomorRekening from BTN_NomorRekening WHERE NomorRekening='" & _AccountNumber & "'"
                sqldr = strExecute.ExecuteReader(strSql)
                If sqldr.HasRows() Then
                    sqldr.Read()
                    If Session("customerid").ToString() <> sqldr("CustomerID").ToString() Then
                        e.Cancel = True
                        Throw New Exception(_AccountNumber & " already exists")
                    Else
                        e.Cancel = False
                        _strScript = "update BTN_NomorRekening set NomorRekening='" & _AccountNumber & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsAccountNumber.UpdateCommand = _strScript
                    End If
                Else
                    _strScript = "update BTN_NomorRekening set NomorRekening='" & _AccountNumber & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                    'strExecute.LogSuccess(strLogTime, _strScript)
                    dsAccountNumber.UpdateCommand = _strScript
                End If
                sqldr.Close()
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView2_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        If Session("customerid").ToString() <> "" Then
            Dim _idTransaction As String = e.Keys("ID")
            Dim _NomorRekening As String = e.Values("NomorRekening")
            Dim _strScript As String = String.Empty
            Dim _strScriptloq As String = String.Empty
            _strScript = "delete from BTN_NomorRekening where ID='" & _idTransaction & "'"
            Try
                _strScriptloq = _strScript & " - Account Number : " & _NomorRekening
                'strExecute.LogSuccess(strLogTime, _strScriptloq)
                dsAccountNumber.DeleteCommand = _strScript
            Catch ex As Exception
                'strExecute.LogError(strLogTime, ex, _strScriptloq)
            End Try
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView3_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        If Session("customerid").ToString() <> "" Then
            Dim _strScript As String = String.Empty
            Dim ValueChannel As String = e.NewValues("ValueChannel")
            Dim FlagChannel As String = e.NewValues("FlagChannel")
            Dim substValueChannel As Integer = ValueChannel.Length
            If FlagChannel = "Email" Then
                If IsValidEmailFormat(ValueChannel) = True Then
                    strSql = "select CustomerID, ValueChannel from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        e.Cancel = True
                        Throw New Exception(ValueChannel & " already exists")
                    Else
                        e.Cancel = False
                        _strScript = "insert into mCustomerChannel (CustomerID, ValueChannel, FlagChannel, UserCreate) values('" & Session("customerid") & "', '" & ValueChannel & "', '" & FlagChannel & "', '" & Session("username") & "')"
                        strExecute.LogSuccess(strLogTime, _strScript)
                        dsChannel.InsertCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format email address is not valid")
                End If
            ElseIf FlagChannel = "Phone" Then
                If ValueChannel <> "" Then
                    If (ValidasiPhoneLength(substValueChannel)) = True Then
                        strSql = "select CustomerID, ValueChannel from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                        sqldr = strExecute.ExecuteReader(strSql)
                        If sqldr.HasRows() Then
                            sqldr.Read()
                            e.Cancel = True
                            Throw New Exception(ValueChannel & " already exists")
                        Else
                            e.Cancel = False
                            _strScript = "insert into mCustomerChannel (CustomerID, ValueChannel, FlagChannel, UserCreate) values('" & Session("customerid") & "', '" & ValueChannel & "', '" & FlagChannel & "', '" & Session("username") & "')"
                            strExecute.LogSuccess(strLogTime, _strScript)
                            dsChannel.InsertCommand = _strScript
                        End If
                        sqldr.Close()
                    Else
                        e.Cancel = True
                        Throw New Exception(ValueChannel & " Format phone number is not valid")
                    End If
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " phone number is not empty")
                End If
            Else
                e.Cancel = True
                Throw New Exception("Type is not empty")
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView3_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        If Session("customerid").ToString().ToString() <> "" Then
            Dim _strScript As String = String.Empty
            Dim _idTransaction As String = e.Keys("ID")
            Dim ValueChannel As String = e.NewValues("ValueChannel")
            Dim FlagChannel As String = e.NewValues("FlagChannel")
            Dim substValueChannel As Integer = ValueChannel.Length
            If FlagChannel = "Email" Then
                If IsValidEmailFormat(ValueChannel) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        If Session("customerid").ToString() <> sqldr("CustomerID").ToString() Then
                            e.Cancel = True
                            Throw New Exception(ValueChannel & " already exists")
                        Else
                            e.Cancel = False
                            _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                            strExecute.LogSuccess(strLogTime, _strScript)
                            dsChannel.UpdateCommand = _strScript
                        End If
                    Else
                        _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        strExecute.LogSuccess(strLogTime, _strScript)
                        dsChannel.UpdateCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format email address is not valid")
                End If
            ElseIf FlagChannel = "Phone" Then
                If (ValidasiPhoneLength(substValueChannel)) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        If Session("customerid").ToString() <> sqldr("CustomerID").ToString() Then
                            e.Cancel = True
                            Throw New Exception(ValueChannel & " already exists")
                        Else
                            e.Cancel = False
                            _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                            strExecute.LogSuccess(strLogTime, _strScript)
                            dsChannel.UpdateCommand = _strScript
                        End If
                    Else
                        _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        strExecute.LogSuccess(strLogTime, _strScript)
                        dsChannel.UpdateCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format phone number is not valid")
                End If
            Else
                e.Cancel = True
                Throw New Exception("Type is not empty")
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView3_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        If Session("customerid").ToString() <> "" Then
            Dim _idTransaction As String = e.Keys("ID")
            Dim _ValueChannel As String = e.Values("ValueChannel")
            Dim _strScript As String = String.Empty
            Dim _strScriptloq As String = String.Empty
            _strScript = "delete from mCustomerChannel where ID='" & _idTransaction & "'"
            Try
                _strScriptloq = _strScript & " - Channel : " & _ValueChannel
                strExecute.LogSuccess(strLogTime, _strScriptloq)
                dsChannel.DeleteCommand = _strScript
            Catch ex As Exception
                strExecute.LogError(strLogTime, ex, _strScriptloq)
            End Try
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Function IsValidEmailFormat(ByVal s As String) As Boolean
        Try
            Return Regex.IsMatch(s, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$*([;])*")
        Catch
            Return False
        End Try
        Return True
    End Function
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Function ValidasiPhoneLength(ByVal _Value As Integer)
        Dim _strPhone As String = "select PhoneLength from Temp_SettingParameter"
        readPhone = strExecute.ExecuteReader(_strPhone)
        If readPhone.HasRows() Then
            readPhone.Read()
            If _Value > readPhone("PhoneLength") Then
                Return True
            Else
                Return False
            End If
        Else
        End If
        readPhone.Close()
    End Function
    Function ValidasiAccountNumberLength(ByVal _Value As Integer)
        Dim _strAccount As String = "select AcountNumberLength from Temp_SettingParameter"
        readAccount = strExecute.ExecuteReader(_strAccount)
        If readAccount.HasRows() Then
            readAccount.Read()
            If _Value <> readAccount("AcountNumberLength") Then
                Return False
            Else
                Return True
            End If
        Else
        End If
        readAccount.Close()
    End Function
    'Private Sub ASPxGridView4_Load(sender As Object, e As EventArgs) Handles ASPxGridView4.Load
    '    Try
    '        Dim strSql As New T_DataCustomerDataContext
    '        strSql.CommandTimeout = 480
    '        ASPxGridView4.DataSource = strSql.L_DataCustomer().ToList()
    '        ASPxGridView4.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Private Sub grid_Load(sender As Object, e As EventArgs) Handles grid.Load
    '    Try
    '        Dim strSql As New m_customer_DBMLDataContext
    '        strSql.CommandTimeout = 480
    '        grid.DataSource = strSql.SP_Temp_SearchingChannel()
    '        grid.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
    '    Response.Redirect("m_customer.aspx?idpage=" & Request.QueryString("idpage") & "")
    'End Sub
    Protected Sub grid_CustomColumnGroup(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
    Private Sub CompareColumnValues(ByVal e As DevExpress.Web.ASPxGridView.CustomColumnSortEventArgs)
        If e.Column.FieldName = "Name" Then
            e.Handled = True
        End If
    End Sub
    Protected Sub grid_CustomGroupDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Name" Then
            'e.DisplayText = "Data : "
        End If
    End Sub
    Protected Sub grid_CustomColumnSort(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
End Class