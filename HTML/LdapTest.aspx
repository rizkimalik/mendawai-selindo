﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="LdapTest.aspx.vb" Inherits="ICC.LdapTest" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function showPelapor(checked) {
            if (checked) {;
                alert(checked);
                document.getElementById("MainContent_hd_ldap_setting").value = "1";
            } else {
                alert(checked);
                document.getElementById("MainContent_hd_ldap_setting").value = "0";
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Setting LDAP Applikasi
			<span class="line bg-warning"></span>
    </h4>
    <asp:HiddenField ID="hd_ldap_setting" runat="server" />
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
                <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" KeyFieldName="ID" SettingsPager-PageSize="15"
                    DataSourceID="sql_ldap" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                    </SettingsPager>
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
                        ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <Columns>
                        <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                            ButtonType="Image" FixedStyle="Left" Width="70">
                            <EditButton Visible="True">
                                <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                            </EditButton>
                            <DeleteButton Visible="false">
                                <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                            </DeleteButton>
                            <CancelButton>
                                <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                </Image>
                            </CancelButton>
                            <UpdateButton>
                                <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                            </UpdateButton>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Caption="LDAP Server" FieldName="LDAPServer"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Username" FieldName="Username"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Password" FieldName="Password"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" HeaderStyle-HorizontalAlign="Center">
                            <PropertiesComboBox>
                                <Items>
                                    <dx:ListEditItem Text="Active" Value="Y" />
                                    <dx:ListEditItem Text="In Active" Value="N" />
                                </Items>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                    </Columns>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="sql_ldap" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from icc_ldap_setting"></asp:SqlDataSource>
            </div>
            <hr />
            <div class="form-group">
                <label><i class="fa fa-user"></i>&nbsp;LDAP Server</label>
                <asp:TextBox ID="txt_ldap" runat="server" CssClass="form-control input-sm bounceIn animation-delay2"></asp:TextBox>
            </div>
            <div class="form-group">
                <label><i class="fa fa-user"></i>&nbsp;Username</label>
                <asp:TextBox ID="txt_username" runat="server" CssClass="form-control input-sm bounceIn animation-delay2"></asp:TextBox>
            </div>
            <div class="form-group">
                <label><i class="fa fa-question-circle"></i>&nbsp;Password</label>
                <asp:TextBox ID="txt_password" runat="server" CssClass="form-control input-sm bounceIn animation-delay4"></asp:TextBox>
            </div>
               <div class="form-group">
                <label><i class="fa fa-question-circle"></i>&nbsp;Password</label>
                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control input-sm bounceIn animation-delay4"></asp:TextBox>
            </div>
            <div class="seperator"></div>
            <div class="row" id="lblError" runat="server" visible="false">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="B_notError" runat="server">&times;</button>
                        <strong>
                            <asp:Label ID="lbl_Error" runat="server"></asp:Label>
                        </strong>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="text-right">
                    <button id="btn_ldap1" runat="server" class="btn btn-success" type="submit"><i class="fa fa-user"></i>&nbsp;Submit LDAP 1</button>
                    <button id="btn_ldap2" runat="server" class="btn btn-success" type="submit"><i class="fa fa-user"></i>&nbsp;Connect To LDAP</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
