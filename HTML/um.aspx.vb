﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports System.Windows.Input
Public Class um
    Inherits System.Web.UI.Page

    Dim ceknull As String
    Dim sqlcon, con, sqlcon1, sqlcon2 As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom, sqlcom1, sqlcom2, com As SqlCommand
    Dim sqldr, sqldr1, sqldr2 As SqlDataReader

    Private Sub um_Init(sender As Object, e As EventArgs) Handles Me.Init
        Session("NamaForm") = IO.Path.GetFileName(Request.Path)
        sql_organization.SelectCommand = "SELECT ORGANIZATION_ID, ORGANIZATION_NAME, description FROM mOrganization"
        sql_level_user.SelectCommand = "SELECT * FROM MLEVELUSER where LevelUserID <> '319'"
        sql_menu.DeleteCommand = "DELETE FROM USER4 WHERE MENUID=@MENUID AND USERID='" & Session("userSelect") & "'"
        sql_sub_menu.DeleteCommand = "DELETE FROM USER4 WHERE USERID='" & Session("userSelect") & "' AND MenuID='" & Session("MenuID") & "' AND SUBMENUID=@SUBMENUID"
        sql_karyawan.SelectCommand = "SELECT * FROM MKARYAWAN"
        sql_menu_tree.DeleteCommand = "DELETE FROM USER4 WHERE MENUIDTREE=@SubMenuIDTree"
        updateAlert()
        If Session("rbuser") = "" Then
            Dim col As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
            col.Visible = False
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID"
        Else
            Dim col As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
            col.Visible = True
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.SOSMED, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN join BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID where msuser.leveluser='" & Session("rbuser") & "'"
        End If
        'If Request.QueryString("action") = "insert" Then
        '    Lblnotif.Text = " Username " & Request.QueryString("username") & " has been save "
        'Else
        'End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("rbuser") = "" Then
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID"
        Else
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID where msuser.leveluser='" & Session("rbuser") & "'"
        End If

    End Sub

    Protected Sub TicketNumber_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("userSelect") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_menu.SelectCommand = "select DISTINCT USER4.MenuID, USER1.MenuName from user4 LEFT OUTER JOIN User1 ON.USER4.MenuID = USER1.MenuID where USER4.UserID='" & Session("userSelect") & "'"

        Dim lvlID As String
        Dim strSql As String = "select mLevelUser.LevelUserID from msUser inner join mLevelUser on msUser.LEVELUSER = mLevelUser.Description where msUser.USERNAME='" & Session("userSelect") & "'"
        sqlcom = New SqlCommand(strSql, sqlcon)
        Try
            sqlcon.Open()
            sqldr = sqlcom.ExecuteReader
            sqldr.Read()
            lvlID = sqldr("LevelUserID").ToString
            sqldr.Close()
            sqlcon.Close()
        Catch ex As Exception

        End Try
        sql_menu.InsertCommand = "INSERT INTO USER4 (USERID, MENUID, LEVELUSERID) VALUES ('" & Session("userSelect") & "', @MenuName, '" & lvlID & "')"
    End Sub

    Protected Sub GridList_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("MenuID") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_subMenu_dr.SelectCommand = "SELECT * FROM USER2 WHERE MENUID='" & Session("MenuID") & "'"
        sql_sub_menu.SelectCommand = "select DISTINCT user2.MenuID, USER4.SubMenuID, USER2.SubMenuName from user4 LEFT OUTER JOIN User2 ON.USER4.SubMenuID = USER2.SubMenuID where USER4.UserID='" & Session("userSelect") & "' And USER2.MenuID='" & Session("MenuID") & "'"
        sql_sub_menu.InsertCommand = "INSERT INTO USER4 (USERID, MENUID, SubMenuID) VALUES ('" & Session("userSelect") & "', '" & Session("MenuID") & "', @SubMenuName)"
    End Sub

    Protected Sub gv_menu_tree_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("SubMenuID") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_menu_tree.SelectCommand = "select DISTINCT USER3.SubMenuIDTree, USER3.MenuTreeName from user4 LEFT OUTER JOIN USER3 ON.USER4.MenuIDTree = USER3.SubMenuIDTree where USER3.SubMenuID='" & Session("SubMenuID") & "'"
        sql_User3.SelectCommand = "Select * from user3 where SubMenuID='" & Session("SubMenuID") & "'"

        Dim lvlID As String
        Dim strSql As String = "select LevelUserID from user4 where UserID='" & Session("userSelect") & "'"
        sqlcom = New SqlCommand(strSql, sqlcon)
        Try
            sqlcon.Open()
            sqldr = sqlcom.ExecuteReader
            sqldr.Read()
            lvlID = sqldr("LevelUserID").ToString
            sqldr.Close()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        sql_menu_tree.InsertCommand = "INSERT INTO USER4 (USERID, MENUID, SUBMENUID, MENUIDTREE, LEVELUSERID) VALUES ('" & Session("userSelect") & "','" & Session("MenuID") & "', '" & Session("SubMenuID") & "' , @MenuTreeName,'" & lvlID & "')"
    End Sub

    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        sql_organization.SelectCommand = "SELECT ORGANIZATION_ID, ORGANIZATION_NAME, description FROM mOrganization"
        If Session("rbuser") = "" Then
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID"
        Else
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID where msuser.leveluser='" & Session("rbuser") & "'"
        End If
    End Sub

    Private Sub ASPxGridView1_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles ASPxGridView1.RowDeleting
        Dim userdelete As String = e.Values("USERNAME")

        sql_user.DeleteCommand = "DELETE MSUSER WHERE USERNAME=@USERNAME"
        Dim strdelete As String = "DELETE USER4 WHERE USERID='" & userdelete & "'"
        Try
            sqlcom = New SqlCommand(strdelete, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub ASPxGridView1_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        Dim username As String = e.NewValues("USERNAME")

        Dim organizationcaseunit As String = String.Empty
        organizationcaseunit = e.NewValues("ORGANIZATION_NAME")
        Dim ORGANIZATION_ID As String = String.Empty
        Dim strSql As String = "SELECT ORGANIZATION_ID FROM mOrganization WHERE ORGANIZATION_NAME='" & organizationcaseunit & "'"
        sqlcom = New SqlCommand(strSql, sqlcon)
        Try
            sqlcon.Open()
            sqldr = sqlcom.ExecuteReader
            If sqldr.HasRows() Then
                sqldr.Read()
                ORGANIZATION_ID = sqldr("ORGANIZATION_ID").ToString
            Else
            End If
            sqldr.Close()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim usercheck As String = String.Empty
        Dim strcheck = "Select COUNT (USERID) as data from MSUSER where username='" & username & "'"
        com = New SqlCommand(strcheck, sqlcon)
        Try
            sqlcon.Open()
            sqldr = com.ExecuteReader()
            If sqldr.HasRows Then
                sqldr.Read()
                usercheck = sqldr("data")
            End If
            sqldr.Close()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        If usercheck > 0 Then
            e.Cancel = True
            Throw New Exception("Username " & username & " sudah terdaftar")
        Else
            Dim password As String = e.NewValues("PASSWORD")
            If Session("rbuser") = "Agent" Then
                sql_user.InsertCommand = "INSERT INTO MSUSER (USERNAME, NAME, LEVELUSER,  EMAIL, INBOUND, EMAIL_ADDRESS) VALUES (@USERNAME,  @NAME, '" & Session("rbuser") & "', '', @EMAIL, @INBOUND, @EMAIL_ADDRESS)"
            ElseIf Session("rbuser") = "CaseUnit" Then
                sql_user.InsertCommand = "INSERT INTO MSUSER (USERNAME, NAME, LEVELUSER, ORGANIZATION, EMAIL_ADDRESS) VALUES (@USERNAME,  @NAME, '" & Session("rbuser") & "', '" & ORGANIZATION_ID & "', @EMAIL_ADDRESS)"
            ElseIf Session("rbuser") = "Supervisor" Then
                sql_user.InsertCommand = "INSERT INTO MSUSER (USERNAME, NAME, LEVELUSER,  EMAIL_ADDRESS) VALUES (@USERNAME,  @NAME, '" & Session("rbuser") & "', '', @EMAIL_ADDRESS)"
            Else
                sql_user.InsertCommand = "INSERT INTO MSUSER (USERNAME, NAME, LEVELUSER, EMAIL_ADDRESS) VALUES (@USERNAME,  @NAME, '" & Session("rbuser") & "', @EMAIL_ADDRESS)"
            End If
            'Response.Redirect("um.aspx?idpage=" & Request.QueryString("idpage") & "&action=insert&username=" & username & "")
            'Lblnotif.Text = "Your update has been saved successfully"
            'Throw New EX("Nama user " & username & " sudah terdaftar")
            Dim TARGET_URL As String = "um.aspx?idpage=" & Request.QueryString("idpage") & "&action=insert&username=" & username & """"
            If Page.IsCallback Then
                DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback(TARGET_URL)
            Else
                Response.Redirect(TARGET_URL)
            End If
        End If

    End Sub

    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim organizationcaseunit As String = String.Empty
        organizationcaseunit = e.NewValues("ORGANIZATION_NAME")
        Dim ORGANIZATION_ID As String = String.Empty
        Dim strSql As String = "SELECT ORGANIZATION_ID FROM mOrganization WHERE ORGANIZATION_NAME='" & organizationcaseunit & "'"
        sqlcom = New SqlCommand(strSql, sqlcon)
        Try
            sqlcon.Open()
            sqldr = sqlcom.ExecuteReader
            If sqldr.HasRows() Then
                sqldr.Read()
                ORGANIZATION_ID = sqldr("ORGANIZATION_ID").ToString
            Else
            End If
            sqldr.Close()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        If Session("rbuser") = "Agent" Then
            sql_user.UpdateCommand = "UPDATE MSUSER SET NAME=@NAME, EMAIL=@EMAIL, LEVELUSER=@LEVELUSER, INBOUND=@INBOUND, EMAIL_ADDRESS=@EMAIL_ADDRESS, NA=@NA WHERE USERNAME=@USERNAME"
        ElseIf Session("rbuser") = "CaseUnit" Then
            sql_user.UpdateCommand = "UPDATE MSUSER SET ORGANIZATION='" & ORGANIZATION_ID & "', NAME=@NAME, EMAIL_ADDRESS=@EMAIL_ADDRESS,  LEVELUSER=@LEVELUSER, NA=@NA WHERE USERNAME=@USERNAME"
        ElseIf Session("rbuser") = "Supervisor" Then
            sql_user.UpdateCommand = "UPDATE MSUSER SET NAME=@NAME, EMAIL=@EMAIL, LEVELUSER=@LEVELUSER, INBOUND=@INBOUND, EMAIL_ADDRESS=@EMAIL_ADDRESS, NA=@NA WHERE USERNAME=@USERNAME"
        Else
            sql_user.UpdateCommand = "UPDATE MSUSER SET NAME=@NAME, EMAIL_ADDRESS=@EMAIL_ADDRESS,  LEVELUSER=@LEVELUSER, NA=@NA WHERE USERNAME=@USERNAME"
        End If
        If Session("rbuser") = "" Then
            sql_user.SelectCommand = "SELECT USERID, USERNAME,  MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, mOrganization.ORGANIZATION_NAME, mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN"
        Else
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, mOrganization.ORGANIZATION_NAME, mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN where msuser.leveluser='" & Session("rbuser") & "'"
        End If
    End Sub

    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Private Sub ASPxCallbackPanel3_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel3.Callback
    '    If hdleveluser.Value = "1" Then
    '        divleveluser.Visible = True
    '        Dim col As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("NamaGrup"), GridViewDataComboBoxColumn)
    '        col.Visible = True

    '        Dim col1 As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
    '        col1.Visible = True
    '    Else
    '        divleveluser.Visible = False
    '        Dim col As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("NamaGrup"), GridViewDataComboBoxColumn)
    '        col.Visible = False

    '        Dim col1 As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
    '        col1.Visible = False
    '    End If
    'End Sub

    Protected Sub ASPxRadioButtonList1_SelectedIndexChanged(sender As Object, e As EventArgs)
        If Me.ASPxRadioButtonList1.Value = "Agent" Then
            Me.Label2.Text = Me.ASPxRadioButtonList1.Value
            Dim col As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
            col.Visible = True

            Dim col1 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("ORGANIZATION_NAME"), GridViewDataComboBoxColumn)
            col1.Visible = False

            Dim col2 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("NamaGrup"), GridViewDataComboBoxColumn)
            col2.Visible = True

            Dim col3 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("INBOUND"), GridViewDataColumn)
            col3.Visible = True

            Dim col4 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("EMAIL"), GridViewDataColumn)
            col4.Visible = True

            Session("rbuser") = "Agent"
        ElseIf Me.ASPxRadioButtonList1.Value = "CaseUnit" Then
            Me.Label2.Text = Me.ASPxRadioButtonList1.Value
            Dim col As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
            col.Visible = True

            Dim col1 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("ORGANIZATION_NAME"), GridViewDataComboBoxColumn)
            col1.Visible = True

            Dim col2 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("NamaGrup"), GridViewDataComboBoxColumn)
            col2.Visible = False

            Dim col3 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("INBOUND"), GridViewDataColumn)
            col3.Visible = False

            Dim col4 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("EMAIL"), GridViewDataColumn)
            col4.Visible = False

            Session("rbuser") = "CaseUnit"
        ElseIf Me.ASPxRadioButtonList1.Value = "Supervisor" Then
            Me.Label2.Text = Me.ASPxRadioButtonList1.Value
            Dim col As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
            col.Visible = True

            Dim col1 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("ORGANIZATION_NAME"), GridViewDataComboBoxColumn)
            col1.Visible = False

            Dim col2 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("NamaGrup"), GridViewDataComboBoxColumn)
            col2.Visible = True

            Dim col3 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("INBOUND"), GridViewDataColumn)
            col3.Visible = False

            Dim col4 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("EMAIL"), GridViewDataColumn)
            col4.Visible = False

            Session("rbuser") = "Supervisor"
        Else
            Me.Label2.Text = Me.ASPxRadioButtonList1.Value
            Dim col As GridViewCommandColumn = TryCast(ASPxGridView1.Columns("Action"), GridViewCommandColumn)
            col.Visible = True

            Dim col1 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("ORGANIZATION_NAME"), GridViewDataComboBoxColumn)
            col1.Visible = False

            Dim col2 As GridViewDataComboBoxColumn = TryCast(ASPxGridView1.Columns("NamaGrup"), GridViewDataComboBoxColumn)
            col2.Visible = False

            Dim col3 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("INBOUND"), GridViewDataColumn)
            col3.Visible = False

            Dim col4 As GridViewDataColumn = TryCast(ASPxGridView1.Columns("EMAIL"), GridViewDataColumn)
            col4.Visible = False

            Session("rbuser") = "Administrator"
        End If
        If Session("rbuser") = "" Then
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID"
        Else
            sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, MAX_OUTBOUND, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.SOSMED, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, MSUSER.NA, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID where msuser.leveluser='" & Session("rbuser") & "'"
        End If
    End Sub

    Private Sub kembali_ServerClick(sender As Object, e As EventArgs) Handles kembali.ServerClick
        Session("rbuser") = ""
        Response.Redirect("um.aspx?idpage=" & Request.QueryString("idpage") & "")
    End Sub

    Function javascript_function(ByVal alert As String)
        Dim message As String = alert
        Dim sb As New System.Text.StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload=function(){")
        sb.Append("alert('")
        sb.Append(message)
        sb.Append("')};")
        sb.Append("</script>")
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
    End Function

    'Protected Sub ASPxGridView1_RowInserted(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertedEventArgs)
    '    If e.Exception Is Nothing Then
    '        CType(sender, ASPxGridView).JSProperties("cpInsertNote") = "The row is inserted successfully"
    '    End If
    'End Sub
End Class