﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Public Class callProgressbtn
    Inherits System.Web.UI.Page

    Dim clsglobe As New ClsConn
    Dim strsql, tmpStr, strsqlCus, updateSql As String
    Dim tbldata, tbldataCus As DataTable
    Dim campaign As String
    Dim StartCall, EndCall As DateTime
    Dim sqlcom As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqldr, read, sqldra As SqlDataReader
    Dim execute As New ClsConn
    Dim updated_at As String = String.Empty
    Dim status_call_web As Integer
    Dim cust_nik As String = String.Empty
    Dim vstr As Integer
    Dim followup As String = ConfigurationManager.AppSettings("14")
    Dim telpdimatikan As String = ConfigurationManager.AppSettings("1")
    Dim sql As String = String.Empty
    Dim jml As Integer
    Dim Proses As New ClsConn

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strsql = "insert into prospect_data_cc_detail(prospect_data_id, cust_nik, cust_email, program, product_code, status_call_web, reason_code, reason_call, note_call , phone_call, create_by, created_at, amount_call, tanggal_followup, jam_followup) values " & _
                     "('" & Request.QueryString("idTable") & "','" & Request.QueryString("nik") & "','" & Request.QueryString("email") & "'," & _
                     "'" & Request.QueryString("program") & "','" & Request.QueryString("productcode") & "','" & Request.QueryString("vreasoncode") & "','" & Request.QueryString("reasoncode") & "','" & ReplaceSpecialLetter(Request.QueryString("reasoncall")) & "','" & Request.QueryString("callnote") & "','" & Request.QueryString("callno") & "','" & Session("username") & "',getdate(),'" & ReplaceSpecialLetter(Request.QueryString("amount")) & "','" & Request.QueryString("tglfollowup") & "','" & Request.QueryString("jamfollowup") & "')"
            clsglobe.ExecuteNonQuery(strsql)

            updateSql = "update prospect_data_cc set status_call_web='" & Request.QueryString("vreasoncode") & "', updated_at=getdate() where id='" & Request.QueryString("idTable") & "'"
            clsglobe.ExecuteNonQuery(updateSql)

            Response.Write("Data has been save")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        s_calldata_outbound()
    End Sub
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, " ", "")
        TmpStr = Replace(TmpStr, "Rp", "")
        TmpStr = Replace(TmpStr, ".00", "")
        TmpStr = Replace(TmpStr, ",", "")
        ReplaceSpecialLetter = TmpStr
    End Function
    Private Sub s_calldata_outbound()
        strsql = "select id, status_call_web from prospect_data_cc where id= '" & Request.QueryString("idTable") & "'"
        sqldr = execute.ExecuteReader(strsql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()

                ID = sqldr("id").ToString
                status_call_web = sqldr("status_call_web")

                If status_call_web = 2 Then

                    Dim strConnString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
                    Dim con As New SqlConnection(strConnString)
                    Dim cmd As New SqlCommand()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "sp_checking_oubound"
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = Request.QueryString("idTable")
                    cmd.Parameters.Add("@status_call_web", SqlDbType.VarChar).Value = status_call_web
                    cmd.Connection = con
                    Try
                        con.Open()
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        Throw ex
                    Finally
                        con.Close()
                        con.Dispose()
                    End Try
                    'updateSql = "update prospect_data_cc set status_call_web='" & Request.QueryString("vreasoncode") & "', updated_at=getdate() where id='" & Request.QueryString("idTable") & "'"
                    'clsglobe.ExecuteNonQuery(updateSql)
                Else
                    checktelpdimatikan(Request.QueryString("idTable"), status_call_web)
                End If

            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Function checktelpdimatikan(ByVal id As String, ByVal status_call_web As Integer)
        Dim Time As String = DateTime.Now.ToString("yyyy-MM-dd")
        sql = "Select COUNT (id) as dataid from prospect_data_cc_detail where prospect_data_id='" & id & "' and status_call_web='3' and created_at between '" & Time & " 00:01:00' and '" & Time & " 23:59:00'"
        sqldra = Proses.ExecuteReader(sql)
        Try
            If sqldra.HasRows Then
                sqldra.Read()
                jml = sqldra("dataid")
                If jml > 2 Then
                    updatetprospectdata(id, status_call_web)
                Else

                End If
            End If
            sqldra.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Function
    Function updatetprospectdata(ByVal id As String, ByVal status_call_web As String)
        If status_call_web = 3 Then
            Dim channelHistory As String = "update prospect_data_cc set status_call_web='1' where id='" & id & "'"
            sqlcom = New SqlCommand(channelHistory, sqlcon)
            Try
                sqlcon.Open()
                sqlcom.ExecuteNonQuery()
                sqlcon.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
        Else
        End If
        Dim insert As String = "insert into prospect_data_loq (id_process, status_call_web, updated_at, created_at) values('" & id & "','" & status_call_web & "',getdate(),'" & Session("username") & "')"
        sqlcom = New SqlCommand(insert, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
    End Function
End Class