﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView
Imports functKawasan

Partial Class AjaxPages_onLoadChart
    Inherits System.Web.UI.Page
    Dim MyConnsosmed As New SqlConnection(ConfigurationManager.ConnectionStrings("SosmedConnection").ConnectionString)
    Dim MyConn As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim MyConn1 As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim MyConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim myInsert, mySelect, mySelect2, myUpdate, com As SqlCommand
    Dim sqlDr, sqlDr2, sqlDrx As SqlDataReader
    Dim querySelectData, querySelectData2, dataNya, queryUpdateData As String
    Dim lastIDdesc, dataChart, dataClean As String
    Dim inqNya, reqNya, comNya, ootNya, dayNya, labelNya As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim aa As String = ""
        'Session("Org") = "Dept1"
        If Request.QueryString("source") = "line" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select * from v_ChartTicketLine Where (usercreate='" & Session("UserName") & "' or usercreate is null) order by JamNya Asc"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= "["
                While sqlDr.Read()
                    'If sqlDr.HasRows() Then
                        inqNya = sqlDr("Inquiries")

                        comNya = sqlDr("Complaint")

                        reqNya = sqlDr("Request")

                        ootNya = sqlDr("OOT")
                    'Else
                    '    inqNya = 0
                    '    comNya = 0
                    '    reqNya = 0
                    '    ootNya = 0
                    'End If

                    dataChart &= "{""y"":""" & sqlDr("JamNya") & """,""a"":""" & inqNya & """,""b"":""" & comNya & """,""c"":""" & reqNya & """,""d"":""" & ootNya & """},"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length - 1)
                dataChart = dataChart & "]"
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)
            ' select * from v_CreateTicketHours
            ''    Response.Write("[{""y"":""2006"",""a"":""10"",""b"":""50"",""c"":""50"",""d"":""50""}," & _
            ''                "{""y"":""2007"",""a"":""45"",""b"":""35"",""c"":""50"",""d"":""50""}," & _
            ''                "{""y"":""2008"",""a"":""20"",""b"":""90"",""c"":""50"",""d"":""50""}," & _
            ''                "{""y"":""2009"",""a"":""30"",""b"":""55"",""c"":""50"",""d"":""50""}," & _
            ''                "{""y"":""2010"",""a"":""20"",""b"":""30"",""c"":""50"",""d"":""50""}]")
        ElseIf Request.QueryString("source") = "getotala" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(ID) as total_ticket from tticket where Convert(Date, DateCreate) = Convert(Date, getdate()) and UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<span><i class='fa fa-list'></i> Total Ticket " & sqlDr("total_ticket").ToString & "</span>" & _
                                 "<div class='seperator'></div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "getotalb" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(ID) as total_ticket from tticket where Convert(Date, DateCreate) = Convert(Date, getdate()) and UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='col-xs-6'>" & _
                                        "<h6 class='no-margin'><i class='fa fa-list'></i> Total Ticket </h6>" & _
                                    "</div>" & _
                                    "<div class='col-xs-6 text-right'>" & _
                                        "<h6 class='no-margin'>" & sqlDr("total_ticket").ToString & "</h6>" & _
                                    "</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "getotalc" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(ID) as total_ticket from tticket where Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='col-xs-6'>" & _
                                        "<h4 class='no-margin'><i class='fa fa-list'></i> Total Ticket </h4>" & _
                                    "</div>" & _
                                    "<div class='col-xs-6 text-right'>" & _
                                        "<h4 class='no-margin'>" & sqlDr("total_ticket").ToString & "</h4>" & _
                                    "</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "bar" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select * from v_SumYesterdayCategory where UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= "["
                If sqlDr.Read() Then
                    dayNya = sqlDr("dayNya")
                    inqNya = sqlDr("Inquiries")

                    comNya = sqlDr("Complaint")

                    reqNya = sqlDr("Request")

                    ootNya = sqlDr("OOT")
                Else
                    dayNya = "Yesterday"

                    inqNya = 0

                    comNya = 0

                    reqNya = 0

                    ootNya = 0
                End If


                dataChart &= "{""y"":""" & dayNya & """,""a"":""" & inqNya & """,""b"":""" & comNya & """,""c"":""" & reqNya & """,""d"":""" & ootNya & """},"

                sqlDr.Close()
                MyConn.Close()

                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select * from v_SumTodayCategory where UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()

                If sqlDr.Read() Then
                    dayNya = sqlDr("dayNya")
                    inqNya = sqlDr("Inquiries")

                    comNya = sqlDr("Complaint")

                    reqNya = sqlDr("Request")

                    ootNya = sqlDr("OOT")
                Else
                    dayNya = "Today"

                    inqNya = 0

                    comNya = 0

                    reqNya = 0

                    ootNya = 0
                End If

                dataChart &= "{""y"":""" & dayNya & """,""a"":""" & inqNya & """,""b"":""" & comNya & """,""c"":""" & reqNya & """,""d"":""" & ootNya & """},"

                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length - 1)
                dataChart = dataChart & "]"
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

            'select categoryName,count(id)Jml from tticket where UserCreate='AGENT1'   
            'and CONVERT(date, DateCreate)=CONVERT(date, getdate()-1)
            'group by categoryName



            'Response.Write("[{""y"":""Yesterday"",""a"":""100"",""b"":""90""}," & _
            '"{""y"":""Today"",""a"":""75"",""b"":""65""}]")

        ElseIf Request.QueryString("source") = "donut" Then
           
    try
                 MyConn.Open()
                    'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = " select ComplaintLevel,UserCreate,count(id)Jml from tticket " & _
                "where Convert(Date, DateCreate) = Convert(Date, getdate()) and UserCreate='" & Session("username") & "'" & _
                "group by ComplaintLevel,UserCreate"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= "["
                While sqlDr.Read()

                    dataChart &= "{""label"":""" & sqlDr("ComplaintLevel").ToString & """,""value"":""" & sqlDr("Jml").ToString & """},"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length - 1)
                dataChart = dataChart & "]"
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

            'Response.Write("[{""label"":""Download Sales"",""value"":""1236""}," & _
            '           "{""label"":""In-Store Sales"",""value"":""3091""}," & _
            '           "{""label"":""Mail-Order Sales"",""value"":""2781""}]")
        ElseIf Request.QueryString("source") = "datatable" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select * from tticket where Convert(Date, DateCreate) = Convert(Date, getdate()) and Status='Open'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<tr><td>" & sqlDr("TicketNumber").ToString & "</td>" & _
                                    "<td><span class='label label-info'>" & sqlDr("Status").ToString & "</span></td> " & _
                                    "<td>" & sqlDr("UserCreate").ToString & "</td> " & _
                                    "<td>" & sqlDr("CategoryName").ToString & "</td> " & _
                                    "<td>" & sqlDr("dispatch_user").ToString & "</td> " & _
                                    "<td>" & sqlDr("dispatch_tgl").ToString & "</td></tr>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)
        ElseIf Request.QueryString("source") = "div4" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='close' and Convert(Date, DateCreate) = Convert(Date, getdate()) and UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Close</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div2" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='open' and Convert(Date, DateCreate) = Convert(Date, getdate()) and UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Open</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div1" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='progress' and Convert(Date, DateCreate) = Convert(Date, getdate()) and UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Progress</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div3" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='pending' and Convert(Date, DateCreate) = Convert(Date, getdate()) and UserCreate='" & Session("username") & "'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Pending</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "statusagent" Then
            Dim strFunction As String
            Dim strArr() As String
            Dim count As Integer
            Dim COrganization As String = ""
            Dim whereTambahanDept As String = ""
            Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
            mySelect = New SqlCommand(strSql, MyConn1)
            Try
                MyConn1.Open()
                sqlDr2 = mySelect.ExecuteReader()
                While sqlDr2.Read()
                    COrganization &= sqlDr2("USERNAME") & ",".ToString
                End While
                sqlDr2.Close()
                MyConn1.Close()

                If COrganization <> "" Then
                    strFunction = COrganization
                    strArr = strFunction.Split(",")
                    For count = 0 To strArr.Length - 1
                        If count = 0 Then
                            whereTambahanDept &= "USERNAME = '" & strArr(count) & "' "
                        ElseIf count < strArr.Length - 1 Then
                            whereTambahanDept &= "OR USERNAME = '" & strArr(count) & "' "
                        Else
                            'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                        End If
                    Next
                    Session("Organization") = whereTambahanDept
                Else
                    Session("Organization") = whereTambahanDept = "UserCreate ='" & COrganization & "'"
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Try
                Dim tampunginbound As String = ""
                Dim tampungoutbound As String = ""
                Dim tampungfax As String = ""
                Dim tampungsms As String = ""
                Dim tampungemail As String = ""
                Dim tampungfacebook As String = ""
                Dim tampungtwitter As String = ""
                Dim tampungdescAUX As String = ""

                MyConn.Open()
                querySelectData = "select USERNAME, INBOUND, OUTBOUND, FAX, SMS, EMAIL, FACEBOOK, TWITTER, CHAT, DescAUX from msuser where " & Session("Org") & ""
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()
                    dataChart &= "<tr>"
                    If sqlDr("DescAUX").ToString = "Ready" Then
                        dataChart &= "<td><img src='images/A.png' style='width:20px; height:20px;' /></td>"
                    Else
                        dataChart &= "<td><img src='images/NA.png' style='width:20px; height:20px;' /></td>"
                    End If
                    dataChart &= "<td><span><strong>" & sqlDr("USERNAME").ToString & "</strong>&nbsp;||&nbsp;" & sqlDr("DescAUX").ToString & "</td>" & _
                    "<td>"
                    If sqlDr("INBOUND").ToString = "True" Then
                        dataChart &= "<i class='fa fa-mobile-phone'></i>&nbsp;"
                    End If
                    If sqlDr("FAX").ToString = "True" Then
                        dataChart &= "<i class='fa fa-fax'></i>&nbsp;"
                    End If
                    If sqlDr("SMS").ToString = "True" Then
                        dataChart &= "<i class='fa fa-envelope'></i>&nbsp;"
                    End If
                    If sqlDr("EMAIL").ToString = "True" Then
                        dataChart &= "<i class='fa fa-envelope-o'></i>&nbsp;"
                    End If
                    If sqlDr("FACEBOOK").ToString = "True" Then
                        dataChart &= "<i class='fa fa-facebook'></i>&nbsp;"
                    End If
                    If sqlDr("TWITTER").ToString = "True" Then
                        dataChart &= "<i class='fa fa-twitter'></i>&nbsp;"
                    End If
                    If sqlDr("CHAT").ToString = "True" Then
                        dataChart &= "<i class='fa fa-comment-o'></i>"
                    End If
                    dataChart &= "</td>"

                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "datatableSpv" Then
            Dim strFunction As String
            Dim strArr() As String
            Dim count As Integer
            Dim COrganization As String = ""
            Dim whereTambahanDept As String = ""
            Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
            mySelect = New SqlCommand(strSql, MyConn1)
            Try
                MyConn1.Open()
                sqlDr2 = mySelect.ExecuteReader()
                While sqlDr2.Read()
                    COrganization &= sqlDr2("USERNAME") & ",".ToString
                End While
                sqlDr2.Close()
                MyConn1.Close()

                If COrganization <> "" Then
                    strFunction = COrganization
                    strArr = strFunction.Split(",")
                    For count = 0 To strArr.Length - 1
                        If count = 0 Then
                            whereTambahanDept &= "UserCreate = '" & strArr(count) & "' "
                        ElseIf count < strArr.Length - 1 Then
                            whereTambahanDept &= "OR UserCreate = '" & strArr(count) & "' "
                        Else
                            'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                        End If
                    Next
                    Session("Organization") = whereTambahanDept
                Else
                    Session("Organization") = whereTambahanDept = "UserCreate ='" & COrganization & "'"
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select * from tticket where Convert(Date, DateCreate) = Convert(Date, getdate()) and (" & Session("Organization") & ")"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<tr><td>" & sqlDr("TicketNumber").ToString & "</td>" & _
                                    "<td><span class='label label-info'>" & sqlDr("Status").ToString & "</span></td> " & _
                                    "<td>" & sqlDr("UserCreate").ToString & "</td> " & _
                                    "<td>" & sqlDr("CategoryName").ToString & "</td> " & _
                                    "<td><a href='#' onclick=''>" & sqlDr("dispatch_user").ToString & "</a></td> " & _
                                    "<td>" & sqlDr("dispatch_tgl").ToString & "</td></tr>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "datatableSpvDetil" Then
            Dim strFunction As String
            Dim strArr() As String
            Dim count As Integer
            Dim COrganization As String = ""
            Dim whereTambahanDept As String = ""
            Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
            mySelect = New SqlCommand(strSql, MyConn1)
            Try
                MyConn1.Open()
                sqlDr2 = mySelect.ExecuteReader()
                While sqlDr2.Read()
                    COrganization &= sqlDr2("USERNAME") & ",".ToString
                End While
                sqlDr2.Close()
                MyConn1.Close()

                If COrganization <> "" Then
                    strFunction = COrganization
                    strArr = strFunction.Split(",")
                    For count = 0 To strArr.Length - 1
                        If count = 0 Then
                            whereTambahanDept &= "UserCreate = '" & strArr(count) & "' "
                        ElseIf count < strArr.Length - 1 Then
                            whereTambahanDept &= "OR UserCreate = '" & strArr(count) & "' "
                        Else
                            'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                        End If
                    Next
                    Session("Organization") = whereTambahanDept
                Else
                    Session("Organization") = whereTambahanDept = "UserCreate ='" & COrganization & "'"
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select * from tticket where Convert(Date, DateCreate) = Convert(Date, getdate()) and (" & Session("Organization") & ")"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<tr><td>" & sqlDr("TicketNumber").ToString & "</td>" & _
                                    "<td><span class='label label-info'>" & sqlDr("Status").ToString & "</span></td> " & _
                                    "<td>" & sqlDr("UserCreate").ToString & "</td> " & _
                                    "<td>" & sqlDr("CategoryName").ToString & "</td> " & _
                                    "<td><a href='#' onclick=''>" & sqlDr("dispatch_user").ToString & "</a></td> " & _
                                    "<td>" & sqlDr("dispatch_tgl").ToString & "</td></tr>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div4spv" Then
            Dim strFunction As String
            Dim strArr() As String
            Dim count As Integer
            Dim COrganization As String = ""
            Dim whereTambahanDept As String = ""
            Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
            mySelect = New SqlCommand(strSql, MyConn1)
            Try
                MyConn1.Open()
                sqlDr2 = mySelect.ExecuteReader()
                While sqlDr2.Read()
                    COrganization &= sqlDr2("USERNAME") & ",".ToString
                End While
                sqlDr2.Close()
                MyConn1.Close()

                If COrganization <> "" Then
                    strFunction = COrganization
                    strArr = strFunction.Split(",")
                    For count = 0 To strArr.Length - 1
                        If count = 0 Then
                            whereTambahanDept &= "UserCreate = '" & strArr(count) & "' "
                        ElseIf count < strArr.Length - 1 Then
                            whereTambahanDept &= "OR UserCreate = '" & strArr(count) & "' "
                        Else
                            'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                        End If
                    Next
                    Session("Organization") = whereTambahanDept
                Else
                    Session("Organization") = whereTambahanDept = "UserCreate ='" & COrganization & "'"
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where (" & Session("Organization") & ") and Status='close' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Close</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div2spv" Then
            Dim strFunction As String
            Dim strArr() As String
            Dim count As Integer
            Dim COrganization As String = ""
            Dim whereTambahanDept As String = ""
            Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
            mySelect = New SqlCommand(strSql, MyConn1)
            Try
                MyConn1.Open()
                sqlDr2 = mySelect.ExecuteReader()
                While sqlDr2.Read()
                    COrganization &= sqlDr2("USERNAME") & ",".ToString
                End While
                sqlDr2.Close()
                MyConn1.Close()

                If COrganization <> "" Then
                    strFunction = COrganization
                    strArr = strFunction.Split(",")
                    For count = 0 To strArr.Length - 1
                        If count = 0 Then
                            whereTambahanDept &= "UserCreate = '" & strArr(count) & "' "
                        ElseIf count < strArr.Length - 1 Then
                            whereTambahanDept &= "OR UserCreate = '" & strArr(count) & "' "
                        Else
                            'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                        End If
                    Next
                    Session("Organization") = whereTambahanDept
                Else
                    Session("Organization") = whereTambahanDept = "UserCreate ='" & COrganization & "'"
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where (" & Session("Organization") & ") and Status='open' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Open</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div1spv" Then
            Dim strFunction As String
            Dim strArr() As String
            Dim count As Integer
            Dim COrganization As String = ""
            Dim whereTambahanDept As String = ""
            Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
            mySelect = New SqlCommand(strSql, MyConn1)
            Try
                MyConn1.Open()
                sqlDr2 = mySelect.ExecuteReader()
                While sqlDr2.Read()
                    COrganization &= sqlDr2("USERNAME") & ",".ToString
                End While
                sqlDr2.Close()
                MyConn1.Close()

                If COrganization <> "" Then
                    strFunction = COrganization
                    strArr = strFunction.Split(",")
                    For count = 0 To strArr.Length - 1
                        If count = 0 Then
                            whereTambahanDept &= "UserCreate = '" & strArr(count) & "' "
                        ElseIf count < strArr.Length - 1 Then
                            whereTambahanDept &= "OR UserCreate = '" & strArr(count) & "' "
                        Else
                            'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                        End If
                    Next
                    Session("Organization") = whereTambahanDept
                Else
                    Session("Organization") = whereTambahanDept = "UserCreate = '" & COrganization & "'"
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where (" & Session("Organization") & ") and Status='progress' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Progress</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div3spv" Then
            Dim strFunction As String
            Dim strArr() As String
            Dim count As Integer
            Dim COrganization As String = ""
            Dim whereTambahanDept As String = ""
            Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
            mySelect = New SqlCommand(strSql, MyConn1)
            Try
                MyConn1.Open()
                sqlDr2 = mySelect.ExecuteReader()
                While sqlDr2.Read()
                    COrganization &= sqlDr2("USERNAME") & ",".ToString
                End While
                sqlDr2.Close()
                MyConn1.Close()

                If COrganization <> "" Then
                    strFunction = COrganization
                    strArr = strFunction.Split(",")
                    For count = 0 To strArr.Length - 1
                        If count = 0 Then
                            whereTambahanDept &= "UserCreate = '" & strArr(count) & "' "
                        ElseIf count < strArr.Length - 1 Then
                            whereTambahanDept &= "OR UserCreate = '" & strArr(count) & "' "
                        Else
                            'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                        End If
                    Next
                    Session("Organization") = whereTambahanDept
                Else
                    Session("Organization") = whereTambahanDept = "UserCreate ='" & COrganization & "'"
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where (" & Session("Organization") & ") and Status='pending' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Pending</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "barMgr" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select CHANNELID,CHANNELTYPE,count(CHANNELID) as TotChannel " & _
                                    "from icc_channel_history " & _
                                    "group by CHANNELID,CHANNELTYPE "
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= "["
                While sqlDr.Read()
                    dayNya = sqlDr("CHANNELID")
                    labelNya = sqlDr("CHANNELTYPE")

                    comNya = 0

                    reqNya = 0

                    MyConn2.Open()
                    querySelectData2 = "select CHANNELID,count(CHANNELID) as TotChannel  from icc_channel_history " & _
                        " where CHANNELID='" & dayNya & "' group by CHANNELID "
                    mySelect2 = New SqlCommand(querySelectData2, MyConn2)
                    sqlDr2 = mySelect2.ExecuteReader()
                    If sqlDr2.Read() Then
                        inqNya = sqlDr("TotChannel")



                    End If

                    sqlDr2.Close()
                    MyConn2.Close()

                    MyConn2.Open()
                    querySelectData2 = "select IdTabel,count(TicketNumber) as TicketCreated  from tTicket " & _
                        " where IdTabel='" & dayNya & "' group by IdTabel "
                    mySelect2 = New SqlCommand(querySelectData2, MyConn2)
                    sqlDr2 = mySelect2.ExecuteReader()
                    If sqlDr2.Read() Then
                        comNya = sqlDr2("TicketCreated")


                    End If

                    sqlDr2.Close()
                    MyConn2.Close()

                    MyConn2.Open()
                    querySelectData2 = "select TicketNumber,count(TicketNumber) as TicketClosed  from tCloseTicket " & _
                        " where TicketNumber='" & dayNya & "' group by TicketNumber "
                    mySelect2 = New SqlCommand(querySelectData2, MyConn2)
                    sqlDr2 = mySelect2.ExecuteReader()
                    If sqlDr2.Read() Then


                        reqNya = sqlDr2("TicketClosed")

                    End If

                    sqlDr2.Close()
                    MyConn2.Close()


                    dataChart &= "{""y"":""" & labelNya & """,""a"":""" & inqNya & """,""b"":""" & comNya & """,""c"":""" & reqNya & """},"
                End While


                sqlDr.Close()
                MyConn.Close()


                dataChart = dataChart.Substring(0, dataChart.Length - 1)
                dataChart = dataChart & "]"
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div4mgr" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='close' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Close</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div2mgr" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='open' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Open</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div1mgr" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='progress' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Progress</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "div3mgr" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(NIK) as Total from tTicket where Status='pending' and Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<div class='value'>" & sqlDr("Total").ToString & "</div>" & _
                                "<div class='title'>Ticket Pending</div>"
                End While
                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)


        ElseIf Request.QueryString("source") = "datatablemgr" Then

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select * from tticket where Convert(Date, DateCreate) = Convert(Date, getdate())"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<tr><td>" & sqlDr("TicketNumber").ToString & "</td>" & _
                                    "<td><span class='label label-info'>" & sqlDr("Status").ToString & "</span></td> " & _
                                    "<td>" & sqlDr("UserCreate").ToString & "</td> " & _
                                    "<td>" & sqlDr("CategoryName").ToString & "</td> " & _
                                    "<td><a href='#' onclick=''>" & sqlDr("dispatch_user").ToString & "</a></td> " & _
                                    "<td>" & sqlDr("dispatch_tgl").ToString & "</td></tr>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "getmail" Then

            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT (IVC_ID) As Count_Email from ICC_EMAIL_IN where Reading='0' and agent='AGENT1'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<span class='shortcut-icon'>" & _
                                "<i class='fa fa-envelope-o'></i>" & _
                                "<span class='shortcut-alert'>" & _
                                    "<asp:Label ID='lbl_email' runat='server'>" & sqlDr("Count_Email").ToString & "</asp:Label>" & _
                                "</span>" & _
                                "</span>" & _
                                "<span class='text'>Email</span>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "gettwitter" Then
            Try
                MyConnsosmed.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select count(id) as count_twitter from getsosmed_one where (dSource='twmention' or dSource='tw_Dm') and P_flagread='0'"
                mySelect = New SqlCommand(querySelectData, MyConnsosmed)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<span class='shortcut-icon'>" & _
                                "<i class='fa fa-twitter'></i>" & _
                                "<span class='shortcut-alert'>" & _
                                    "<asp:Label ID='lbl_email' runat='server'>" & sqlDr("count_twitter").ToString & "</asp:Label>" & _
                                "</span>" & _
                                "</span>" & _
                                "<span class='text'>Twitter</span>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "getfacebook" Then
            Try
                MyConnsosmed.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select count(idtbl) as count_facebook from v_allsosmed where (dSource='inbox' or dSource='fb_Post' or dSource='fb_comment' or dSource='fb_reply') and flagread='0'"
                mySelect = New SqlCommand(querySelectData, MyConnsosmed)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<span class='shortcut-icon'>" & _
                                "<i class='fa fa-facebook'></i>" & _
                                "<span class='shortcut-alert'>" & _
                                    "<asp:Label ID='lbl_email' runat='server'>" & sqlDr("count_facebook").ToString & "</asp:Label>" & _
                                "</span>" & _
                                "</span>" & _
                                "<span class='text'>Facebook</span>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "getsms" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT (MessageID) As Count_Sms from ICC_SMSInput where Reading='0' and agent='AGENT1'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<span class='shortcut-icon'>" & _
                                "<i class='fa fa-envelope-o'></i>" & _
                                "<span class='shortcut-alert'>" & _
                                    "<asp:Label ID='lbl_email' runat='server'>" & sqlDr("Count_Sms").ToString & "</asp:Label>" & _
                                "</span>" & _
                                "</span>" & _
                                "<span class='text'>SMS</span>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "getfax" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select count(ID) as Count_fax from TXFaxIN where ReadWeb is null"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<span class='shortcut-icon'>" & _
                                "<i class='fa fa-fax'></i>" & _
                                "<span class='shortcut-alert'>" & _
                                    "<asp:Label ID='lbl_email' runat='server'>" & sqlDr("Count_fax").ToString & "</asp:Label>" & _
                                "</span>" & _
                                "</span>" & _
                                "<span class='text'>FAX</span>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)

        ElseIf Request.QueryString("source") = "getchat" Then
            Try
                MyConn.Open()
                'querySelectData = "select left(Tme, len(Tme)-3) as Tme,CategoryName,Jml from v_CreateTicketHours"
                querySelectData = "select COUNT(trxChatID) as count_chat from tChat where BlinkChat='New'"
                mySelect = New SqlCommand(querySelectData, MyConn)
                sqlDr = mySelect.ExecuteReader()
                dataChart &= ""
                While sqlDr.Read()

                    dataChart &= "<span class='shortcut-icon'>" & _
                                "<i class='fa fa-comment-o'></i>" & _
                                "<span class='shortcut-alert'>" & _
                                    "<asp:Label ID='lbl_email' runat='server'>" & sqlDr("count_chat").ToString & "</asp:Label>" & _
                                "</span>" & _
                                "</span>" & _
                                "<span class='text'>Chat</span>"
                End While


                sqlDr.Close()
                MyConn.Close()
                dataChart = dataChart.Substring(0, dataChart.Length)
                dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Response.Write(dataChart)
            'Response.Write("[{""y"":""Yesterday"",""a"":""100"",""b"":""90""}," & _
            '"{""y"":""Today"",""a"":""75"",""b"":""65""}]")

        End If
    End Sub
End Class
