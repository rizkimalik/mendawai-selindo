﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class FormDispatch
    Inherits System.Web.UI.Page

    Dim com As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sql As String = String.Empty
    Dim sqldr As SqlDataReader
    Dim dataForm As String = String.Empty
    Dim dataBlockForm As String = String.Empty
    Dim dataCloseForm As String = String.Empty
    Dim dataPosition As String = String.Empty
    Dim labelDispatch As String = String.Empty
    Dim statusTicket As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            sqlcon.Open()
            Dim strdata As String = "select * from tTicket where TicketNumber='" & Request.QueryString("id") & "'"
            com = New SqlCommand(strdata, sqlcon)
            sqldr = com.ExecuteReader()
            dataForm &= ""
            If sqldr.HasRows Then
                sqldr.Read()
                statusTicket = sqldr("Status").ToString
                dataPosition = sqldr("TicketPosition").ToString
                If dataPosition = "3" Then
                    labelDispatch = "Re-Assign"
                Else
                    labelDispatch = "Dispatch"
                End If
            End If
            sqldr.Close()
            sqlcon.Close()

            dataForm &= "<div class='row'> " & _
                               "<div class='col-md-12 col-sm-12'> " & _
                                   "<div class='row'> " & _
                                       "<div class='col-md-12 col-sm-12'> " & _
                                           "<div class='form-group'> " & _
                                               "<label>Description</label> " & _
                                               "<textarea rows='8' id='lblDispatchDesc' cols='50' class='form-control input-sm bounceIn animation-delay4'></textarea> " & _
                                           "</div> " & _
                                       "</div> " & _
                                   "</div> " & _
                                    "<div class='row'> " & _
                                       "<div class='col-md-12 col-sm-12' style='margin-top: -5px;'> " & _
                                           "<div class='form-group'> " & _
                                               "<div class='text-right'> " & _
                                                   "<a role='button' id='update' onclick='dispatchTicket(" & Session("UserDispatchTo") & ")' class='btn btn-info'> " & _
                                                   "<i class='fa fa-save'></i>&nbsp;" & labelDispatch & "</a> " & _
                                               "</div> " & _
                                           "</div> " & _
                                       "</div> " & _
                                   "</div> " & _
                               "</div> " & _
                           "</div> "
            dataForm = dataForm.Substring(0, dataForm.Length)
            dataForm = dataForm & ""

            dataBlockForm &= "<div class='row'> " & _
                               "<div class='col-md-12 col-sm-12'> " & _
                                   "<div class='row'> " & _
                                       "<div class='col-md-12 col-sm-12'> " & _
                                           "<div class='form-group'> " & _
                                               "<label>Mohon maaf data ini sudah di eskalasi</label> " & _
                                           "</div> " & _
                                       "</div> " & _
                                   "</div> " & _
                               "</div> " & _
                           "</div> "
            dataBlockForm = dataBlockForm.Substring(0, dataBlockForm.Length)
            dataBlockForm = dataBlockForm & ""


            dataCloseForm &= "<div class='row'> " & _
                               "<div class='col-md-12 col-sm-12'> " & _
                                   "<div class='row'> " & _
                                       "<div class='col-md-12 col-sm-12'> " & _
                                           "<div class='form-group'> " & _
                                               "<label>Mohon maaf ticket ini sudah di closed</label> " & _
                                           "</div> " & _
                                       "</div> " & _
                                   "</div> " & _
                               "</div> " & _
                           "</div> "
            dataCloseForm = dataCloseForm.Substring(0, dataCloseForm.Length)
            dataCloseForm = dataCloseForm & ""

            If Session("LoginTypeAngka") = dataPosition Then
                If statusTicket = "Closed" Then
                    Response.Write(dataCloseForm)
                Else
                    Response.Write(dataForm)
                End If
            Else
                If statusTicket = "Closed" Then
                    Response.Write(dataCloseForm)
                Else
                    Response.Write(dataBlockForm)
                End If
            End If
           
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
End Class