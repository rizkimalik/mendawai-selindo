﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class updateoutboundcall
    Inherits System.Web.UI.Page

    Dim strsql As String
    Dim sqldr, read, sqldra As SqlDataReader
    Dim execute As New ClsConn
    Dim id As String = String.Empty
    Dim updated_at As String = String.Empty
    Dim status_call_web As Integer
    Dim vstr As Integer
    Dim followup As String = ConfigurationManager.AppSettings("14")
    Dim telpdimatikan As String = ConfigurationManager.AppSettings("1")
    Dim sqlcom As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Proses As New ClsConn
    Dim sql As String = String.Empty
    Dim cust_nik As String = String.Empty
    Dim jml As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strsql = "select * from prospect_data_cc where (status_call_web <> '0' and status_call_web <> '1')"
        sqldr = execute.ExecuteReader(strsql)
        Try
            While sqldr.Read()
                id = sqldr("id").ToString
                updated_at = CDate(sqldr("updated_at").ToString).ToString("yyyy-MM-dd")
                status_call_web = sqldr("status_call_web").ToString
                cust_nik = sqldr("cust_nik").ToString

                Dim strConnString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
                Dim con As New SqlConnection(strConnString)
                Dim cmd As New SqlCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "sp_checking_oubound"
                cmd.Parameters.Add("@updated_at", SqlDbType.Date).Value = updated_at
                cmd.Connection = con
                Try
                    con.Open()
                    cmd.ExecuteNonQuery()
                    read = cmd.ExecuteReader()
                    read.Read()
                    vstr = read("jumlahDay").ToString
                    read.Close()

                Catch ex As Exception
                    Throw ex
                Finally
                    con.Close()
                    con.Dispose()
                End Try

                If status_call_web = "2" Then
                    If vstr > followup Then
                        updatetprospectdata(id, status_call_web)
                    Else
                    End If
                Else
                    checktelpdimatikan(id, status_call_web, cust_nik)
                    'If vstr > telpdimatikan Then
                    '    checktelpdimatikan(id, cust_nik, status_call_web)                       
                    'Else
                    '    checktelpdimatikan(id, cust_nik, status_call_web)
                    'End If
                End If

            End While
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Function updatetprospectdata(ByVal id As String, ByVal status_call_web As String)
        Dim channelHistory As String = "update prospect_data_cc set status_call_web='1' where id='" & id & "'"
        sqlcom = New SqlCommand(channelHistory, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
        Dim insert As String = "insert into prospect_data_loq (id_process, status_call_web, updated_at, created_at) values('" & id & "','" & status_call_web & "',getdate(),'" & Session("username") & "')"
        sqlcom = New SqlCommand(insert, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
    End Function

    Function updatetprospectdata_telpondimatikan(ByVal id As String, ByVal status_call_web As String)
        Dim channelHistory As String = "update prospect_data_cc set status_call_web='1' where id='" & id & "'"
        sqlcom = New SqlCommand(channelHistory, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
        Dim insert As String = "insert into prospect_data_loq (id_process, status_call_web, updated_at, created_at) values('" & id & "','" & status_call_web & "',getdate(),'" & Session("username") & "')"
        sqlcom = New SqlCommand(insert, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
    End Function

    Function checktelpdimatikan(ByVal id As String, ByVal status_call_web As String, ByVal cust_nik As String)
        Dim Time As String = DateTime.Now.ToString("yyyy-MM-dd")
        sql = "Select COUNT (id) as dataid from prospect_data_cc_detail where prospect_data_id='" & id & "'"
        sqldra = Proses.ExecuteReader(sql)
        Try
            If sqldra.HasRows Then
                sqldra.Read()
                jml = sqldra("dataid")
                If jml > 2 Then
                    updatetprospectdata_telpondimatikan(id, status_call_web)
                Else

                End If
            End If
            sqldra.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Function
End Class