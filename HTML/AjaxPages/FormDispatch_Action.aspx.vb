﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Public Class FormDispatch_Action
    Inherits System.Web.UI.Page

    Dim sqlcom, com As SqlCommand
    Dim mySelect, mySelect1, mySelectTujuan As SqlCommand
    Dim sqldr, sqldr1, sqldr2 As SqlDataReader
    Dim sqlcon, con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon1 As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim execute As New ClsConn
    Dim EmailForm As String = ConfigurationManager.AppSettings("EmailForm")
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim strSql As String = String.Empty
    Dim _ClassFunction As New WebServiceTransaction

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strdataStatus As String
        Dim updateTicket As String = "update tTicket set TicketPosition='" & Request.QueryString("jenis") & "' where TicketNumber='" & Request.QueryString("ticketnumber") & "' "
        Try
            sqlcom = New SqlCommand(updateTicket, sqlcon)
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim strdataChannel As String = "select * from tTicket where TicketNumber='" & Request.QueryString("ticketnumber") & "' "
        mySelect = New SqlCommand(strdataChannel, sqlcon)
        Try
            sqlcon.Open()
            sqldr1 = mySelect.ExecuteReader()
            If sqldr1.HasRows Then
                sqldr1.Read()
                strdataStatus = sqldr1("Status").ToString
            Else
                strdataStatus = "None"
            End If
            sqldr1.Close()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)


        End Try
        Dim strFirstCreate As String
        If cekFirstCreate(Request.QueryString("ticketnumber"), Session("username")) = True Then
            strFirstCreate = "No"
        Else
            strFirstCreate = "Yes"
        End If
        Dim dept As String = String.Empty
        If Session("lvluser") = "Unit Kerja" Then
            dept = "None"
        Else
            dept = Request.QueryString("deptid")
        End If

        Dim interaction As String = String.Empty
        If Request.QueryString("jenis") = "1" Then
            interaction = "insert into tInteraction (FirstCreate,TicketNumber, ResponseComplaint, Status, AgentCreate, DateCreate,DispatchTicket,DispatchToLayer,DispatchToDept,Channel,ThreadID, GenesysID) " & _
                          "values('" & strFirstCreate & "','" & Request.QueryString("ticketnumber") & "', '" & Request.QueryString("response") & "', '" & strdataStatus & "', '" & Session("username") & "', getdate(),'Yes','" & Request.QueryString("jenis") & "','None','" & Request.QueryString("channel") & "','" & Request.QueryString("TrxThreadID") & "','" & Request.QueryString("TrxGenesysID") & "')"
        ElseIf Request.QueryString("jenis") = "2" Then
            interaction = "insert into tInteraction (FirstCreate,TicketNumber, ResponseComplaint, Status, AgentCreate, DateCreate,DispatchTicket,DispatchToLayer,DispatchToDept,Channel,ThreadID, GenesysID) " & _
             "values('" & strFirstCreate & "','" & Request.QueryString("ticketnumber") & "', '" & Request.QueryString("response") & "', '" & strdataStatus & "', '" & Session("username") & "', getdate(),'Yes','" & Request.QueryString("jenis") & "','None','" & Request.QueryString("channel") & "','" & Request.QueryString("TrxThreadID") & "','" & Request.QueryString("TrxGenesysID") & "')"
        Else
            interaction = "insert into tInteraction (FirstCreate,TicketNumber, ResponseComplaint, Status, AgentCreate, DateCreate,DispatchTicket,DispatchToLayer,DispatchToDept,Channel,ThreadID, GenesysID) " & _
            "values('" & strFirstCreate & "','" & Request.QueryString("ticketnumber") & "', '" & Request.QueryString("response") & "', '" & strdataStatus & "', '" & Session("username") & "', getdate(),'Yes','" & Request.QueryString("jenis") & "','" & dept & "','" & Request.QueryString("channel") & "','" & Request.QueryString("TrxThreadID") & "','" & Request.QueryString("TrxGenesysID") & "')"

        End If
        Try
            sqlcom = New SqlCommand(interaction, sqlcon)
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        FunctionUpdateThread(Request.QueryString("TrxGenesysID"), Request.QueryString("ticketnumber"))
    End Sub
    Private Function cekFirstCreate(ByVal TicketNumber As String, ByVal AgentCreate As String) As Boolean
        Dim strdataChannel As String = "select count(*) from tInteraction where TicketNumber='" & TicketNumber & "' and AgentCreate='" & AgentCreate & "' and FirstCreate='Yes'"
        mySelect = New SqlCommand(strdataChannel, sqlcon1)
        Try
            sqlcon1.Open()
            sqldr2 = mySelect.ExecuteReader()
            If sqldr2.HasRows Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
            sqldr2.Close()
            sqlcon1.Close()
        End Try
    End Function
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "'", """")
        ReplaceSpecialLetter = TmpStr
    End Function
    Function FunctionCountingUpdateThread(ByVal GenesysID As String, ByVal TicketNumber As String)
        Dim strdataChannel As String = "SELECT COUNT(*) AS DATA FROM TR_THREAD WHERE GenesysNumber='" & GenesysID & "' AND (TicketNumber='' or TicketNumber is null)"
        mySelect = New SqlCommand(strdataChannel, sqlcon1)
        Try
            sqlcon1.Open()
            sqldr2 = mySelect.ExecuteReader()
            If sqldr2.HasRows() Then
                sqldr2.Read()
                If (sqldr("DATA").ToString = "0" Or sqldr2("DATA").ToString = "") Then
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return True
                Else
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return False
                End If
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
            sqldr2.Close()
            sqlcon1.Close()
        End Try

    End Function
    Function FunctionUpdateThread(ByVal GenesysID As String, ByVal TicketNumber As String)
        If FunctionCountingUpdateThread(GenesysID, TicketNumber) = True Then
            Dim updateTicket As String = "UPDATE TR_THREAD SET TicketNumber='" & TicketNumber & "' WHERE GenesysNumber='" & GenesysID & "' "
            Try
                sqlcom = New SqlCommand(updateTicket, sqlcon)
                sqlcon.Open()
                sqlcom.ExecuteNonQuery()
                sqlcon.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        Else
        End If
    End Function
End Class