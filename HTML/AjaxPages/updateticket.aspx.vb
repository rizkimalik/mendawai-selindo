﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Public Class updateticket
    Inherits System.Web.UI.Page

    Dim sqlcom As SqlCommand
    Dim sqlcom1 As SqlCommand
    Dim sqlcom2 As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon1 As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon2 As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim mySelect, mySelect1, mySelectTujuan As SqlCommand
    Dim sqldr, sqldr1, sqldr2, sqldr3 As SqlDataReader
    Dim loq As New cls_globe

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If cekStatusSebelumnya(Request.QueryString("ticketid"), Request.QueryString("status"), Session("username")) = False Then
            Dim strFirstCreate As String
            If cekFirstCreate(Request.QueryString("ticketid"), Session("username")) = True Then
                strFirstCreate = "No"
            Else
                strFirstCreate = "Yes"
            End If
            Dim interaction As String = "insert into tInteraction (FirstCreate,TicketNumber, ResponseComplaint, Status, AgentCreate, DateCreate, leveluser) " & _
                                       "values('" & strFirstCreate & "','" & Request.QueryString("ticketid") & "', '" & Request.QueryString("response") & "', '" & Request.QueryString("status") & "', '" & Session("username") & "', getdate(), '" & Session("lvluser") & "')"
            Try
                sqlcom = New SqlCommand(interaction, sqlcon)
                sqlcon.Open()
                sqlcom.ExecuteNonQuery()
                sqlcon.Close()
                loq.writedata(Session("UserName"), "Insert", "insert tInteraction success", interaction, Session("username"))
            Catch ex As Exception
                loq.writedata(Session("UserName"), "Insert", "insert tInteraction gagal", interaction, Session("username"))
                Response.Write(ex.Message)
            End Try

            Dim updateTicket As String = "update tTicket set TicketSourceName='" & Request.QueryString("source") & "',status='" & Request.QueryString("status") & "', responComplaint='" & Request.QueryString("response") & "',Divisi = '" & Request.QueryString("divisi") & "'  where ID='" & Request.QueryString("id") & "' "
            Try
                sqlcom = New SqlCommand(updateTicket, sqlcon)
                sqlcon.Open()
                sqlcom.ExecuteNonQuery()
                sqlcon.Close()
                loq.writedata(Session("UserName"), "Update", "update tTicket success", updateTicket, Session("username"))
            Catch ex As Exception
                loq.writedata(Session("UserName"), "Update", "update tTicket gagal", updateTicket, Session("username"))
                Response.Write(ex.Message)
            End Try


            Response.Write("True")
        Else
            If Request.QueryString("status") = "Closed" Then
                Dim updateTicket As String = "update tTicket set status='" & Request.QueryString("status") & "', dateclose=GETDATE(), userclose='" & Session("username") & "', responComplaint='" & Request.QueryString("response") & "' where ID='" & Request.QueryString("id") & "' "
                Try
                    sqlcom = New SqlCommand(updateTicket, sqlcon)
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                    loq.writedata(Session("UserName"), "Update", "update tTicket success", updateTicket, Session("username"))
                Catch ex As Exception
                    loq.writedata(Session("UserName"), "Update", "update tTicket gagal", updateTicket, Session("username"))
                    Response.Write(ex.Message)
                End Try
                Dim updateClose As String = "update tCloseTicket set StatusTicket='" & Request.QueryString("status") & "', DateCloseActual=GETDATE() where TicketNumber='" & Request.QueryString("ticketid") & "'"
                Try
                    sqlcom = New SqlCommand(updateClose, sqlcon)
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                    loq.writedata(Session("UserName"), "Update", "update tCloseTicket success", updateClose, Session("username"))
                Catch ex As Exception
                    loq.writedata(Session("UserName"), "Update", "update tCloseTicket gagal", updateClose, Session("username"))
                    Response.Write(ex.Message)
                End Try

            Else

                Dim updateTicket As String = "update tTicket set TicketSourceName='" & Request.QueryString("source") & "',status='" & Request.QueryString("status") & "', responComplaint='" & Request.QueryString("response") & "',Divisi = '" & Request.QueryString("divisi") & "'  where ID='" & Request.QueryString("id") & "' "
                Try
                    sqlcom = New SqlCommand(updateTicket, sqlcon)
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                    loq.writedata(Session("UserName"), "Update", "update tTicket success", updateTicket, Session("username"))
                Catch ex As Exception
                    loq.writedata(Session("UserName"), "Update", "update tTicket gagal", updateTicket, Session("username"))
                    Response.Write(ex.Message)
                End Try

                Dim updateEnhanceBTN As String = "update BTN_trxTticket set TglKejadian='" & Request.QueryString("tglkejadian") & "',IdPenyebab='" & Request.QueryString("idpenyebab") & "', " & _
                    "StrPenyebab='" & Request.QueryString("strpenyebab") & "',StrPenerima = '" & Request.QueryString("strpenerima") & "',IdStatusPelapor = '" & Request.QueryString("idstatuspelapor") & "',StrStatusPelapor = '" & Request.QueryString("strstatuspelapor") & "'  where TicketNumber='" & Request.QueryString("ticketid") & "' "
                Try
                    sqlcom = New SqlCommand(updateEnhanceBTN, sqlcon)
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                    loq.writedata(Session("UserName"), "Update", "update BTN_trxTticket success", updateEnhanceBTN, Session("username"))
                Catch ex As Exception
                    loq.writedata(Session("UserName"), "Update", "update BTN_trxTticket gagal", updateEnhanceBTN, Session("username"))
                    Response.Write(ex.Message)
                End Try
            End If

            Dim strFirstCreate As String
            If cekFirstCreate(Request.QueryString("ticketid"), Session("username")) = True Then
                strFirstCreate = "No"
            Else
                strFirstCreate = "Yes"
            End If

            Dim interaction As String = "insert into tInteraction (FirstCreate,TicketNumber, ResponseComplaint, Status, AgentCreate, DateCreate, leveluser) " & _
                                        "values('" & strFirstCreate & "','" & Request.QueryString("ticketid") & "', '" & Request.QueryString("response") & "', '" & Request.QueryString("status") & "', '" & Session("username") & "', getdate(), '" & Session("lvluser") & "')"
            Try
                sqlcom = New SqlCommand(interaction, sqlcon)
                sqlcon.Open()
                sqlcom.ExecuteNonQuery()
                sqlcon.Close()
                loq.writedata(Session("UserName"), "Insert", "update tInteraction success", interaction, Session("username"))
            Catch ex As Exception
                loq.writedata(Session("UserName"), "Insert", "update tInteraction gagal", interaction, Session("username"))
                Response.Write(ex.Message)
            End Try

            If Request.QueryString("ivcid") <> "" Then
                Dim updateEmail As String = "update ICC_EMAIL_IN set TicketNumber='" & Request.QueryString("ticketid") & "', Reading='1' where IVC_ID='" & Request.QueryString("ivcid") & "'"
                sqlcom = New SqlCommand(updateEmail, sqlcon)
                Try
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                Catch ex As Exception
                    Response.Write(DirectCast("", String))
                End Try
                Dim channelHistory As String = "update ICC_CHANNEL_HISTORY set STATUS='1' where CHANNELID='" & Request.QueryString("ivcid") & "'"
                sqlcom = New SqlCommand(channelHistory, sqlcon)
                Try
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                Catch ex As Exception
                    Response.Write(DirectCast("", String))
                End Try
            Else

            End If
        End If
        
    End Sub
    Private Function cekFirstCreate(ByVal TicketNumber As String, ByVal AgentCreate As String) As Boolean
        Dim strdataChannel As String = "select * from tInteraction where TicketNumber='" & TicketNumber & "' and AgentCreate='" & AgentCreate & "' and FirstCreate='Yes'"
        mySelect = New SqlCommand(strdataChannel, sqlcon1)
        Try
            sqlcon1.Open()
            sqldr2 = mySelect.ExecuteReader()
            If sqldr2.HasRows Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Response.Write(ex.Message)

            sqldr2.Close()
            sqlcon1.Close()
        End Try
    End Function
    Private Function cekStatusSebelumnya(ByVal TicketNumber As String, ByVal StatusPost As String, ByVal LevelUser As String) As Boolean
        Dim strdataChannel As String = "select * from tticket where TicketNumber='" & TicketNumber & "'"
        mySelect = New SqlCommand(strdataChannel, sqlcon2)
        Try
            sqlcon2.Open()
            sqldr3 = mySelect.ExecuteReader()
            If sqldr3.HasRows Then
                sqldr3.Read()
                If sqldr3("Status") = StatusPost Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
            sqldr3.Close()
            sqlcon2.Close()
        End Try
    End Function
End Class