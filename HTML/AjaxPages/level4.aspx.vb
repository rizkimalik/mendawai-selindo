﻿Imports System.Data
Imports System.Data.SqlClient
Public Class level4
    Inherits System.Web.UI.Page

    Dim MyConn As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim mySelect, mySelectTujuan As SqlCommand
    Dim sqlDr, sqlDrTujuan As SqlDataReader
    Dim dataNya, dataNyaTujuan, valTujuanEskalasi, valStatus, valChannel, dataNyaChannel, dataNyaStatus As String
    Dim _ExtendID As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("jenis") = "edit" Then
            Dim strdata As String = "select * from tticket where TicketNumber='" & Request.QueryString("id") & "' order by DateCreate Asc"
            mySelect = New SqlCommand(strdata, MyConn)
            Try
                MyConn.Open()
                sqlDr = mySelect.ExecuteReader()
                'dataNya &= "<label>Kategori Permasalahan</label><br/>"
                'dataNya &= "<select class='SoftOrange' id='cmbProblem3' onchange='loadSelectedProblemtiga()' style='width:100%;height:30px;'>"
                'dataNya &= "<option value='0' selected='selected'>--Select--</option>"
                dataNya &= "<div class='col-md-3 col-sm-3'><div class='form-group'>"
                dataNya &= "<label>SLA (Day)</label>"

                While sqlDr.Read()
                    'dataNya &= "<option value='" & sqlDr("SubCategory3ID").ToString & "' >" & sqlDr("SubName").ToString & "</option>"
                    dataNya &= " <input type='text' value='" & sqlDr("SLA").ToString & "' id='hd_sla' readonly='readonly' class='form-control' style='width:240px;height:30px;'>"
                    valTujuanEskalasi = sqlDr("Divisi").ToString
                    valStatus = sqlDr("Status").ToString
                    valChannel = sqlDr("TicketSourceName").ToString
                    _ExtendID = sqlDr("ExtendID").ToString
                End While
                sqlDr.Close()
                MyConn.Close()
                dataNya &= "</div></div>"

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


            Dim strdataStatus As String = String.Empty
            strdataStatus = "select * from mStatus WHERE NA='Y' order by Urutan ASC"
            mySelect = New SqlCommand(strdataStatus, MyConn)
            Try
                MyConn.Open()
                sqlDr = mySelect.ExecuteReader()
                dataNyaStatus &= "<div class='col-md-3 col-sm-3'><div class='form-group'><label>Ticket Status</label><br/>"
                dataNyaStatus &= "<select class='Metropolis' id='cmbStatusTicket' style='width:100%;height:30px;'>"
                While sqlDr.Read()
                    If sqlDr("status").ToString = valStatus Then
                        dataNyaStatus &= "<option value='" & sqlDr("status").ToString & "' selected='selected'>" & sqlDr("status").ToString & "</option>"
                    Else
                        dataNyaStatus &= "<option value='" & sqlDr("status").ToString & "' >" & sqlDr("status").ToString & "</option>"
                    End If

                End While
                sqlDr.Close()
                MyConn.Close()
                dataNyaStatus &= "</select></div></div>"

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Dim strdataTujuan As String = "SELECT ORGANIZATION_ID, ORGANIZATION_NAME, CHANNEL_CODE FROM MORGANIZATION where flag=0 order by ORGANIZATION_NAME ASC"
            mySelect = New SqlCommand(strdataTujuan, MyConn)
            Try
                MyConn.Open()
                sqlDr = mySelect.ExecuteReader()
                If Request.QueryString("LoginAngka") = "3" Then
                    ''Selain Agent tidak bisa ubah Channel
                    dataNyaTujuan &= "<div class='col-md-3 col-sm-3'><div class='form-group'><label>Escalation Unit</label><br/>"
                    dataNyaTujuan &= "<select class='Metropolis' id='cmbTujuanEskalasi' style='width:100%;height:30px;' style='margin-left:10px;'>"
                Else
                    dataNyaTujuan &= "<div class='col-md-3 col-sm-3'><div class='form-group'><label>Escalation Unit</label><br/>"
                    dataNyaTujuan &= "<select class='Metropolis' id='cmbTujuanEskalasi' style='width:100%;height:30px;' style='margin-left:10px;'>"
                End If
                While sqlDr.Read()
                    If sqlDr("ORGANIZATION_ID").ToString = valTujuanEskalasi Then
                        dataNyaTujuan &= "<option value='" & sqlDr("ORGANIZATION_ID").ToString & "' selected='selected'>" & sqlDr("ORGANIZATION_NAME").ToString & "</option>"
                    Else
                        dataNyaTujuan &= "<option value='" & sqlDr("ORGANIZATION_ID").ToString & "' >" & sqlDr("ORGANIZATION_NAME").ToString & "</option>"
                    End If

                End While
                sqlDr.Close()
                MyConn.Close()
                dataNyaTujuan &= "</select></div></div>"
                Response.Write(dataNyaChannel & dataNyaStatus & dataNyaTujuan & dataNya)
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

        Else

            Dim strdata As String = "select * from msubcategorylv3 where SubCategory3ID='" & Request.QueryString("id") & "' and NA='Y' order by SubName Asc"
            mySelect = New SqlCommand(strdata, MyConn)
            Try
                MyConn.Open()
                sqlDr = mySelect.ExecuteReader()
                'dataNya &= "<label>Kategori Permasalahan</label><br/>"
                'dataNya &= "<select class='SoftOrange' id='cmbProblem3' onchange='loadSelectedProblemtiga()' style='width:100%;height:30px;'>"
                'dataNya &= "<option value='0' selected='selected'>--Select--</option>"
                dataNya &= "<div class='col-md-3 col-sm-3'><div class='form-group' style='margin-left:5px;'>"
                dataNya &= "<label>SLA (Day)</label>"

                While sqlDr.Read()
                    'dataNya &= "<option value='" & sqlDr("SubCategory3ID").ToString & "' >" & sqlDr("SubName").ToString & "</option>"
                    dataNya &= " <input type='text' value='" & sqlDr("SLA").ToString & "' id='hd_sla' readonly='readonly' class='form-control' style='width:240px;height:30px;'>"
                    valTujuanEskalasi = sqlDr("TujuanEskalasi").ToString

                End While
                sqlDr.Close()
                MyConn.Close()
                dataNya &= "</div></div>"


            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Dim strdataStatus As String = ""
            strdataStatus = "select * from mStatus WHERE NA='Y' order by Urutan ASC"
            mySelect = New SqlCommand(strdataStatus, MyConn)
            Try
                MyConn.Open()
                sqlDr = mySelect.ExecuteReader()
                dataNyaStatus &= "<div class='col-md-3 col-sm-3'><div class='form-group'><label>Ticket Status</label><br/>"
                dataNyaStatus &= "<select id='cmbStatusTicket' style='width:100%;height:32px;font-size: 12px;' required class='form-control'>"
                While sqlDr.Read()
                    If sqlDr("status").ToString = valStatus Then
                        dataNyaStatus &= "<option value='" & sqlDr("status").ToString & "' selected='selected'>" & sqlDr("status").ToString & "</option>"
                    Else
                        dataNyaStatus &= "<option value='" & sqlDr("status").ToString & "' >" & sqlDr("status").ToString & "</option>"
                    End If

                End While
                sqlDr.Close()
                MyConn.Close()
                dataNyaStatus &= "</select></div></div>"

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Dim strdataTujuan As String = "SELECT ORGANIZATION_ID, ORGANIZATION_NAME, CHANNEL_CODE FROM MORGANIZATION where flag=0 order by ORGANIZATION_NAME ASC"
            mySelect = New SqlCommand(strdataTujuan, MyConn)
            Try
                MyConn.Open()
                sqlDr = mySelect.ExecuteReader()
                dataNyaTujuan &= "<div class='col-md-3 col-sm-3'><div class='form-group' style='margin-left:5px;'><label>Escalation Unit</label><br/>"
                If Session("LoginType") = "layer1" Then
                    dataNyaTujuan &= "<select disabled='true' readonly id='cmbTujuanEskalasi' style='width:100%;height:30px;margin-left:2px;' required class='form-control'>"
                Else
                    dataNyaTujuan &= "<select id='cmbTujuanEskalasi' style='width:100%;height:32px;font-size: 12px;margin-left:2px;' required class='form-control'>"

                End If
                While sqlDr.Read()
                    If sqlDr("ORGANIZATION_ID").ToString = valTujuanEskalasi Then
                        dataNyaTujuan &= "<option value='" & sqlDr("ORGANIZATION_ID").ToString & "' selected='selected'>" & sqlDr("ORGANIZATION_NAME").ToString & "</option>"
                    Else
                        dataNyaTujuan &= "<option value='" & sqlDr("ORGANIZATION_ID").ToString & "' >" & sqlDr("ORGANIZATION_NAME").ToString & "</option>"
                    End If

                End While
                sqlDr.Close()
                MyConn.Close()
                dataNyaTujuan &= "</select></div></div>"
                Response.Write(dataNyaChannel & dataNyaStatus & dataNyaTujuan & dataNya)
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
    End Sub
End Class