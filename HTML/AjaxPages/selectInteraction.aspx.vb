﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class selectInteraction
    Inherits System.Web.UI.Page

    Dim com As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sql As String = String.Empty
    Dim sqldr, sqldr1, sqldr2 As SqlDataReader
    Dim dataChart As String = String.Empty
    Dim MyConn As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim MyConn1 As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim mySelect, mySelect1, mySelectTujuan As SqlCommand
    Dim sqlDrTujuan As SqlDataReader
    Dim strdataChannelPrint, strdataChannelLevelUser, dataNya, dataNyaTujuan, valTujuanEskalasi, valStatus, valChannel, dataNyaChannel, dataNyaStatus As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            sqlcon.Open()
            Dim strdata As String = "select tticket.idTabel, tticket.AccountInbound, tInteraction.*,mOrganization.ORGANIZATION_NAME, convert(varchar, tInteraction.DateCreate, 13) as tanggal, msuser.leveluser from tInteraction left outer join mOrganization on REPLACE(tInteraction.DispatchToDept,'None',0)=mOrganization.ORGANIZATION_ID " & _
                                    "left outer join msuser on tInteraction.agentcreate = msuser.username left outer join tticket on tticket.TicketNumber = tInteraction.TicketNumber where tInteraction.TicketNumber ='" & Request.QueryString("id") & "'  order by tInteraction.DateCreate Desc"
            com = New SqlCommand(strdata, sqlcon)
            sqldr = com.ExecuteReader()
            dataChart &= ""
            While sqldr.Read()
                If sqldr("DispatchToDept").ToString = "None" Then

                Else
                    Dim strdataChannel As String = "select b.LEVELUSER,* from mOrganization a left outer join msUser b on a.ORGANIZATION_ID=b.ORGANIZATION where a.ORGANIZATION_ID='" & sqldr("DispatchToDept").ToString & "'"
                    mySelect = New SqlCommand(strdataChannel, MyConn)
                    Try
                        MyConn.Open()
                        sqldr1 = mySelect.ExecuteReader()
                        If sqldr1.HasRows Then
                            sqldr1.Read()
                            strdataChannelPrint = sqldr1("ORGANIZATION_NAME").ToString
                            strdataChannelLevelUser = sqldr1("LEVELUSER").ToString
                            If strdataChannelLevelUser = "CaseUnit" Then
                                strdataChannelLevelUser = "L3"
                            ElseIf strdataChannelLevelUser = "Supervisor" Then
                                strdataChannelLevelUser = "L2"
                            Else
                                strdataChannelLevelUser = "L1"
                            End If
                        Else
                            strdataChannelPrint = "None"
                            strdataChannelLevelUser = "None"
                        End If

                    Catch ex As Exception
                        Response.Write(ex.Message)

                        sqldr1.Close()
                        MyConn.Close()
                    End Try

                End If
                
                dataChart &= "<tr><td style='font-size: x-small;text-align:left;width:150px;'>" & sqldr("idTabel").ToString & "</td>" & _
                             "<td style='font-size: x-small;text-align:left;width:150px;'>" & sqldr("AccountInbound").ToString & "</td>" & _
                             "<td style='font-size: x-small;text-align:left;width:400px;'>" & sqldr("ResponseComplaint").ToString & "</td>" & _
                              "<td style='font-size: x-small;text-align:left;width:150px;'>" & sqldr("AgentCreate").ToString & " (" & sqldr("leveluser") & ")</td> " & _
                              "<td style='font-size: x-small;text-align:left;width:80px;'>" & sqldr("Status").ToString & "</td> " & _
                              "<td style='font-size: x-small;text-align:left;width:80px;'>" & sqldr("DispatchTicket").ToString & "</td> " & _
                              "<td style='font-size: x-small;text-align:left;width:120px;'>" & sqldr("DispatchToLayer").ToString & "</td> " & _
                              "<td style='font-size: x-small;text-align:left;width:120px;'>" & sqldr("ORGANIZATION_NAME").ToString & "</td> " & _
                              "<td style='font-size: x-small;text-align:left;width:150px;'>" & sqldr("tanggal").ToString & "</td></tr>"


            End While
            sqldr.Close()
            sqlcon.Close()
            dataChart = dataChart.Substring(0, dataChart.Length)
            dataChart = dataChart & ""
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Response.Write(dataChart)
    End Sub
End Class