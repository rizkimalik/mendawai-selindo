﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class assignemail
    Inherits System.Web.UI.Page

    Dim execute As New ClsConn
    Dim sqlcom, comm As SqlCommand
    Dim sqldr As SqlDataReader
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim connection As SqlConnection
    Dim connectionString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("ket") = "email" Then
            checkmaxemail()
        Else
            actionassignemail()
        End If
    End Sub
    Private Sub checkmaxemail()
        Dim jml As Integer
        Dim jmlmaxemail As Integer
        Dim sql As String = String.Empty
        Dim sqlstr As String = String.Empty
        sql = "select count(*) as data from [dbo].[ICC_CHANNEL_HISTORY] where CHANNELTYPE ='email' and AGENT ='" & Request.QueryString("agent") & "' and STATUS <> '1' and KATEGORI_EMAIL <> 'SPAM'"
        Try
            sqldr = execute.ExecuteReader(sql)
            If sqldr.HasRows Then
                sqldr.Read()
                jml = sqldr("data").ToString
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        sqlstr = "select max_email from msuser where USERNAME='" & Request.QueryString("agent") & "'"
        Try
            sqldr = execute.ExecuteReader(sqlstr)
            If sqldr.HasRows Then
                sqldr.Read()
                jmlmaxemail = sqldr("max_email").ToString
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        If jml >= jmlmaxemail Then
            Response.Write("False")
        Else
            actionassignemail()
        End If
    End Sub
    Private Sub actionassignemail()
        connection = New SqlConnection(connectionString)
        comm = New SqlCommand()
        comm.Connection = connection
        comm.CommandType = CommandType.StoredProcedure
        comm.CommandText = "sp_assign_email"
        comm.Parameters.Add("@ivcid", Data.SqlDbType.VarChar).Value = Request.QueryString("ivcid")
        comm.Parameters.Add("@agent", Data.SqlDbType.VarChar).Value = Request.QueryString("agent")
        comm.Parameters.Add("@reason", Data.SqlDbType.VarChar).Value = Request.QueryString("reason")
        comm.Parameters.Add("@ket", Data.SqlDbType.VarChar).Value = Request.QueryString("ket")
        Try
            connection.Open()
            sqldr = comm.ExecuteReader()
            connection.Close()
            Response.Write("True")
        Catch ex As Exception
            Response.Write("False")
        End Try
    End Sub
End Class