﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class sendemailcustomer
    Inherits System.Web.UI.Page

    Dim execute As New ClsConn
    Dim sqlcom As SqlCommand
    Dim sqldr As SqlDataReader
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim connection As SqlConnection
    Dim connectionString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim EmailForm As String = ConfigurationManager.AppSettings("EmailForm")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim systemapplikasi As String = String.Empty
        Dim jenispengaduan As String = String.Empty
        Dim kategori As String = String.Empty
        Dim subkategori As String = String.Empty
        Dim description As String = String.Empty
        Dim name As String = String.Empty
        Dim sql As String = "select a.*, b.Name from tticket a left outer join mcustomer b on a.nik = b.customerid where a.ticketnumber='" & Request.QueryString("idticket") & "'"
        sqldr = execute.ExecuteReader(sql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                systemapplikasi = sqldr("CategoryName").ToString
                jenispengaduan = sqldr("SubCategory1Name").ToString
                kategori = sqldr("SubCategory2Name").ToString
                subkategori = sqldr("SubCategory3Name").ToString
                description = sqldr("DetailComplaint").ToString
                name = sqldr("Name").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        
        If Request.QueryString("status") = "yes" Then
            Dim RequestTittle As String = "Data Transaksi Dengan Ticket Number [" & Request.QueryString("idticket") & "]"
            'Dim Body As String = "<hr noshade='noshade' style='background-color:#3c8dbc; height: 5px;'/>" &
            '                        "<p>Yth. Bapak/Ibu,</p>" &
            '                        "<br/>" &
            '                        "<p>Terima kasih telah menghubungi layanan sahabat BTN.</p>" &
            '                        "<br/>" &
            '                        "<p>Laporan/Permintaan/Pertanyaan Bapak/Ibu telah kami terima dan terdaftar dengan nomor pelaporan " & Request.QueryString("idticket") & "</p>" &
            '                        "<br/>" &
            '                        "<p>Salam Hangat, </p>" &
            '                        "<br/>" &
            '                        "<p>SAHABAT BTN</p>" &
            '                        "<br />" &
            '                        "<p>PT Bank Tabungan Negara (Persero) Tbk Menara BTN</p>" &
            '                        "<p>Jl Gajah Mada No.1 Jakarta 10130</p>" &
            '                        "<p>Layanan Contact Center : 1500-286</p>" &
            '                        "<p>Layanan Live Chat 24 Jam : btnproperti.co.id</p>" &
            '                        "<p>Email : btncontactcenter@btn.co.id</p>" &
            '                        "<p>Website : www.btn.co.id</p>" &
            '                        "<p>Kantor Cabang BTN : Senin-Jumat, Jam operasional 08:00 - 15:30 WIB</p>" &
            '                        "<hr noshade='noshade' style='background-color:#3c8dbc; height: 5px;'/>"

            Dim body As String = "<Table>" &
               "<tr>" &
                   "<td colspan='3'>Yth. Bpk/Ibu</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>Terima kasih telah menghubungi layanan Sahabat BTN. Semoga dalam keadaan sehat selalu.</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>Laporan/Permintaan/Pertanyaan Bapak/Ibu telah kami terima dan terdaftar dengan nomor pelaporan #" & Request.QueryString("idticket") & ".</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>Apabila ada hal lain yang ingin ditanyakan, Bapak/Ibu dapat mengirimkan email kembali ke</td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'><u>btncontactcenter@btn.co.id</u> atau dapat menghubungi kami melalui:</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td style='width: 200px;'>Layanan Contact Center 24 Jam</td>" &
                   "<td style='width: 10px;'>:</td>" &
                   "<td>1500-286</td>" &
               "</tr>" &
               "<tr>" &
                   "<td style='width: 200px;'>Layanan Live Chat 24 Jam</td>" &
                   "<td style='width: 10px;'>:</td>" &
                   "<td>btnproperti.co.id</td>" &
               "</tr>" &
               "<tr>" &
                   "<td style='width: 200px;'>Website</td>" &
                   "<td style='width: 10px;'>:</td>" &
                   "<td>www.btn.co.id</td>" &
               "</tr>" &
               "<tr>" &
                   "<td style='width: 200px;'>Kantor Cabang BTN</td>" &
                   "<td style='width: 10px;'>:</td>" &
                   "<td style='width: 400px;'>Senin-Jumat, Jam operasional 08:00 - 15:30 W.I.B</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>Terima kasih sudah memilih Bank BTN menjadi layanan perbankan Bapak/Ibu, kepuasan nasabah merupakan prioritas kami.</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>Salam hangat,</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>SAHABAT BTN</td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td></td>" &
                   "<td></td>" &
                   "<td></td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>PT Bank Tabungan Negara (Persero) Tbk</td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>Menara BTN</td>" &
               "</tr>" &
               "<tr>" &
                   "<td colspan='3'>Jl. Gajah Mada No.1 Jakarta 10130</td>" &
               "</tr>" &
           "</table>"

            If Request.QueryString("channel") = "Email" Then
                Dim EMAIL_ID As String = String.Empty
                Dim strsql As String = "SELECT EMAIL_ID FROM ICC_EMAIL_IN WHERE IVC_ID='" & Request.QueryString("ivcid") & "'"
                sqldr = execute.ExecuteReader(strsql)
                Try
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        EMAIL_ID = sqldr("EMAIL_ID").ToString
                    Else
                    End If
                    sqldr.Close()
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                Dim insertdata As String = "insert into ICC_EMAIL_OUT (EMAIL_ID, DIRECTION, EFROM, ETO, ESUBJECT, EBODY_TEXT, EBODY_HTML, Email_Date, TicketNumber, JENIS_EMAIL_INTERNAL, AGENT) VALUES ('" & EMAIL_ID & "','out', '" & EmailForm & "', '" & Request.QueryString("emailto") & "', '" & RequestTittle & "', '" & ReplaceSpecialLetter(Body) & "', '" & ReplaceSpecialLetter(Body) & "', GETDATE(), '" & Request.QueryString("idticket") & "', 'email customer', '" & Session("username") & "')"
                sqlcom = New SqlCommand(insertdata, sqlcon)
                Try
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                Catch ex As Exception
                    Response.Write(DirectCast("", String))
                End Try
            Else
                Dim FolderName As String = DateTime.Now.ToString("yyyyMMddhhmmssfff")
                Dim insertdata As String = "insert into ICC_EMAIL_OUT (EMAIL_ID, DIRECTION, EFROM, ETO, ESUBJECT, EBODY_TEXT, EBODY_HTML, Email_Date, TicketNumber, JENIS_EMAIL_INTERNAL, AGENT) VALUES ('ICC-" & FolderName & "','out', '" & EmailForm & "', '" & Request.QueryString("emailto") & "', '" & RequestTittle & "', '" & ReplaceSpecialLetter(body) & "', '" & ReplaceSpecialLetter(body) & "', GETDATE(), '" & Request.QueryString("idticket") & "', 'email customer', '" & Session("username") & "')"
                sqlcom = New SqlCommand(insertdata, sqlcon)
                Try
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                Catch ex As Exception
                    Response.Write(DirectCast("", String))
                End Try
            End If
        Else

        End If
        
        If Request.QueryString("channel") = "Email" Then
            'Setelah email dibaca dan dibuat ticket - Update ICC_CHANNEL_HISTORY SET STATUS=1
            Dim upicchistory As String = "UPDATE ICC_CHANNEL_HISTORY SET STATUS='1', TICKETNUMBER='" & Request.QueryString("idticket") & "', FLAGPHONE='Y' WHERE CHANNELID=" & Request.QueryString("ivcid") & ""
            sqlcom = New SqlCommand(upicchistory, sqlcon)
            Try
                sqlcon.Open()
                sqlcom.ExecuteNonQuery()
                sqlcon.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try

            'Setelah email dibaca dan dibuat ticket maka ticket number akan dimasukan kedalam data email masuk - Update ICC_EMAIL_IN SET TICKETNUMBER=VALUE
            Dim upiccemailin As String = "UPDATE ICC_EMAIL_IN SET TICKETNUMBER='" & Request.QueryString("idticket") & "', FLAG='1' WHERE IVC_ID=" & Request.QueryString("ivcid") & ""
            sqlcom = New SqlCommand(upiccemailin, sqlcon)
            Try
                sqlcon.Open()
                sqlcom.ExecuteNonQuery()
                sqlcon.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
        Else

        End If
    End Sub
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "'", """")
        ReplaceSpecialLetter = TmpStr
    End Function
    Private Sub BodyTabel()
        Dim body As String = "<Table>" &
                "<tr>" &
                    "<td colspan='3'>Yth. Bpk/Ibu</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>Terima kasih telah menghubungi layanan Sahabat BTN. Semoga dalam keadaan sehat selalu.</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>Laporan/Permintaan/Pertanyaan Bapak/Ibu telah kami terima dan terdaftar dengan nomor pelaporan #xxxx. (No Tiket).</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>Apabila ada hal lain yang ingin ditanyakan, Bapak/Ibu xxx dapat mengirimkan email kembali ke</td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'><u>btncontactcenter@btn.co.id</u> atau dapat menghubungi kami melalui:</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td style='width: 300px;'>Layanan Contact Center 24 Jam</td>" &
                    "<td style='width: 10px;'>:</td>" &
                    "<td>1500-286</td>" &
                "</tr>" &
                "<tr>" &
                    "<td style='width: 300px;'>Layanan Live Chat 24 Jam</td>" &
                    "<td style='width: 10px;'>:</td>" &
                    "<td>btnproperti.co.id</td>" &
                "</tr>" &
                "<tr>" &
                    "<td style='width: 300px;'>Website</td>" &
                    "<td style='width: 10px;'>:</td>" &
                    "<td>www.btn.co.id</td>" &
                "</tr>" &
                "<tr>" &
                    "<td style='width: 300px;'>Kantor Cabang BTN</td>" &
                    "<td style='width: 10px;'>:</td>" &
                    "<td>Senin-Jumat, Jam operasional 08:00 - 15:30 W.I.B</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>Terima kasih sudah memilih Bank BTN menjadi layanan perbankan Bapak/Ibu, kepuasan nasabah merupakan prioritas kami.</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>Salam hangat,</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>SAHABAT BTN</td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td></td>" &
                    "<td></td>" &
                    "<td></td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>PT Bank Tabungan Negara (Persero) Tbk</td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>Menara BTN</td>" &
                "</tr>" &
                "<tr>" &
                    "<td colspan='3'>Jl. Gajah Mada No.1 Jakarta 10130</td>" &
                "</tr>" &
            "</table>"
    End Sub
End Class