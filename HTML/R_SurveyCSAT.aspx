﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="R_SurveyCSAT.aspx.vb" Inherits="ICC.R_SurveyCSAT" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="module" src="scripts/report_survey_csat.js"></script> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h4 class="headline">Report Survey CSAT
			<span class="line bg-warning"></span>
        </h4>
        <br />
        <dx:ASPxLabel Visible="false" runat="server" ID="sqlOutput"></dx:ASPxLabel>
    </div>
    <div class="row" style="margin-bottom: -15px;">
        <div class="col-sm-2">
            <label>Start Date</label>
            <dx:ASPxDateEdit ID="dt_strdate" runat="server" CssClass="form-control input-sm" Width="100%" DisplayFormatString="yyyy-MM-dd" 
                NullText="yyyy-MM-dd" EditFormat="Custom" EditFormatString="yyyy-MM-dd" AllowUserInput="false" AutoPostBack="false">
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2">
            <label>End Date</label>
            <dx:ASPxDateEdit ID="dt_endate" runat="server" CssClass="form-control input-sm" Width="100%" DisplayFormatString="yyyy-MM-dd" 
                NullText="yyyy-MM-dd" EditFormat="Custom" EditFormatString="yyyy-MM-dd" AllowUserInput="false" AutoPostBack="false">
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2">
		    <label>By</label>
            <select id="cb_type" class="form-control input-sm">
                <option value="0" Selected>Create Date</option>
                <option value="1">CSAT Date</option>
            </select>
            <%-- <dx:ASPxComboBox ID="cb_type" runat="server" CssClass="form-control input-sm" ValueType="System.String">
                <Items>
                    <dx:ListEditItem Text="Create Date" Value="0" Selected="true" />
                    <dx:ListEditItem Text="CSAT Date" Value="1" />                    
                </Items>
            </dx:ASPxComboBox> --%>
	    </div>
        <div class="col-sm-2" style="margin-top: 5px;" >
            <br />
            <button type="button" id="btn-submit" class="btn btn-sm btn-default">Submit</button>

            <%-- <dx:ASPxButton ID="btn_Submit" runat="server" Theme="Metropolis" AutoPostBack="False" Text="Submit"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton> --%>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <div style="overflow: hidden;">
                <div id="dxReportSurveyCSAT"></div>

                <%-- <dx:ASPxGridView ID="ASPxGridView1" runat="server" KeyFieldName="ID"
                    Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                    SettingsPager-PageSize="10">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="15, 20, 25" ShowAllItem="true" />
                    </SettingsPager>
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFilterRow="false" ShowFilterRowMenu="false"
                        ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <Columns>
                       <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                            PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="UniqueID" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
						<dx:GridViewDataTextColumn Caption="Channel" FieldName="Channel" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Thread Ticket" FieldName="ThreadTicket" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Result Survey CSAT" FieldName="ResultCSAT" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Value Detail" FieldName="ValueDetail" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Amount" FieldName="Amount" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
						<dx:GridViewDataDateColumn Caption="CSAT Date" FieldName="DateCSAT" Width="200px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd hh:mm:ss"></dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn Caption="Reason Code" FieldName="ReasonCode" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Created Date Ticket" FieldName="CreatedDateTicket" Width="200px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd hh:mm:ss"></dx:GridViewDataDateColumn>
                    </Columns>
                    <SettingsBehavior AutoExpandAllGroups="true" />
                    <Settings ShowGroupedColumns="True" />
                    <Settings ShowGroupPanel="True" />
                </dx:ASPxGridView> --%>
            </div>

        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="csv" Text="CSV" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <%--<asp:ListItem Value="rtf" Text="RTF" /> --%>
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <button type="button" id="btn-export" class="btn btn-sm btn-default">Export</button>

            <%-- <dx:ASPxButton ID="btn_Export" runat="server" Text="Export" Theme="Metropolis" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton> --%>
        </div>
    </div>
    <hr />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1"></dx:ASPxGridViewExporter>

</asp:Content>
