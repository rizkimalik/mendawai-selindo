﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxGridView
Public Class R_ManagementActivity
    Inherits System.Web.UI.Page

    Dim Cls As New WebServiceTransaction
    Dim proses As New ClsConn
    Dim _upage As String
    Dim sqlRead As SqlDataReader
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _updatePage()
    End Sub
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd")
        LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd")
    End Sub
    Private Sub btn_Submit_Click(sender As Object, e As EventArgs) Handles btn_Submit.Click
        LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd")
        LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd")
    End Sub
    Private Sub btn_Export_Click(sender As Object, e As EventArgs) Handles btn_Export.Click
        'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = dt_strdate.Text & " 00:01"
        'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = dt_endate.Text & " 23:59"
        LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd")
        LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd")

        Dim TimeS As DateTime
        TimeS = dt_endate.Value
        Dim strTgl As String
        Dim strString As String = "SELECT DATEADD(DAY," & Cls.FunctionDataSetting("Report") & ",'" & TimeS & "') as TglSelected"
        Try
            sqlRead = proses.ExecuteReader(strString)
            If sqlRead.HasRows Then
                sqlRead.Read()
                strTgl = sqlRead("TglSelected").ToString
            End If
        Catch ex As Exception
            Cls.LogError(strLogTime, ex, strString)
        End Try

        Dim fromDate As DateTime = Format(dt_strdate.Value, "yyyy-MM-dd")
        Dim toDate As DateTime = Format(dt_endate.Value, "yyyy-MM-dd")
        Dim ddate As String
        ddate = DateDiff("m", fromDate, toDate)

        'If dt_strdate.Value < strTgl Then
        If ddate > 0 Then
            alertJS("Export maximum " & ReplaceSpecialLetter(Cls.FunctionDataSetting("Report")) & " days")
        Else
            Cls.LogSuccess(strLogTime, strString)
            Dim casses As String = ddList.SelectedValue
            Select Case casses
                Case "xlsx"
                    ASPxGridViewExporter1.WriteXlsxToResponse("ReportManagementActivity_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
                Case "xls"
                    ASPxGridViewExporter1.WriteXlsToResponse("ReportManagementActivity_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
                Case "rtf"
                    ASPxGridViewExporter1.Landscape = True
                    ASPxGridViewExporter1.LeftMargin = 35
                    ASPxGridViewExporter1.RightMargin = 30
                    ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                    ASPxGridViewExporter1.MaxColumnWidth = 108
                    ASPxGridViewExporter1.WriteRtfToResponse("ReportManagementActivity_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
                Case "pdf"
                    ASPxGridViewExporter1.Landscape = True
                    ASPxGridViewExporter1.LeftMargin = 35
                    ASPxGridViewExporter1.RightMargin = 30
                    ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                    ASPxGridViewExporter1.MaxColumnWidth = 108
                    ASPxGridViewExporter1.WritePdfToResponse("ReportManagementActivity_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
                Case "csv"
                    ASPxGridViewExporter1.WriteCsvToResponse("ReportManagementActivity_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            End Select
        End If
    End Sub
    Function alertJS(ByVal alert As String)
        Dim message As String = alert
        Dim sb As New System.Text.StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload=function(){")
        sb.Append("alert('")
        sb.Append(message)
        sb.Append("')};")
        sb.Append("</script>")
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
    End Function
    Private Sub dt_strdate_Init(sender As Object, e As EventArgs) Handles dt_strdate.Init
        dt_strdate.Value = DateTime.Now
    End Sub
    Private Sub dt_endate_Init(sender As Object, e As EventArgs) Handles dt_endate.Init
        dt_endate.Value = DateTime.Now
    End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "-", "")
        ReplaceSpecialLetter = TmpStr
    End Function
End Class