﻿Imports System.Data
Imports System.Data.SqlClient
Public Class know2
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As New SqlCommand
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginType") = "Admin" Then
            fileManager.SettingsEditing.AllowCreate = True
            fileManager.SettingsEditing.AllowDelete = True
            fileManager.SettingsEditing.AllowRename = True
            fileManager.SettingsEditing.AllowMove = True

            fileManager.SettingsToolbar.ShowCreateButton = True
            fileManager.SettingsToolbar.ShowDownloadButton = True
        Else
            fileManager.SettingsEditing.AllowCreate = False
            fileManager.SettingsEditing.AllowDelete = False
            fileManager.SettingsEditing.AllowRename = False
            fileManager.SettingsEditing.AllowMove = False

            fileManager.SettingsToolbar.ShowCreateButton = False
            fileManager.SettingsToolbar.ShowDownloadButton = True
            fileManager.SettingsUpload.Enabled = False
        End If
        updateAlert()
    End Sub
    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class