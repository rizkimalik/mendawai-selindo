﻿Public Class RedirectAccess
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("idx") = "1" Then
            frameRedirect.Src = "LicenseInfo.html"
        Else
            frameRedirect.Src = "LicenseFull.html"
        End If
    End Sub

End Class