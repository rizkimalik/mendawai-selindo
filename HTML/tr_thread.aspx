﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="tr_thread.aspx.vb" Inherits="ICC.tr_thread" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function ShowThread(val) {
            $('#MainContent_TrxGenesysID').val(val);
            ASPxPopupControl1.Show();
        }
        function execThread(val) {
            var TrxUserName = $('#MainContent_TrxUsername').val();
            var TrxGenesysID = $('#MainContent_TrxGenesysID').val();
            var TrxReason = $('#MainContent_ASPxPopupControl1_TxtThreadReason_I').val();
            if (TrxGenesysID === '') {
                alert("Interaction ID is empty")
                return false;
            }
            if (TrxReason === '') {
                alert("Thread Reason is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/UpdateThread",
                    data: "{TrxUserName: '" + TrxUserName + "', TrxGenesysID: '" + TrxGenesysID + "', TrxReason: '" + TrxReason + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var result = "";
                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result == "True") {
                                alert(json[i].msgSystem)
                                ASPxPopupControl1.Hide();
                                window.location.href = "tr_thread.aspx";
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }
                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
    </script>
    <script>
        function execCancel() {
            ASPxPopupControl1.Hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="TrxUsername" runat="server" />
    <asp:HiddenField ID="TrxGenesysID" runat="server" />
    <h4 class="headline">Data Thread Transaction
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" Width="100%" ClientInstanceName="ASPxGridView1" runat="server" AutoGenerateColumns="False" KeyFieldName="ID"
        Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="15, 25, 50" ShowAllItem="true" />
        </SettingsPager>
        <SettingsPager PageSize="15" />
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true"
            ShowGroupPanel="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                <DataItemTemplate>
                    <a href="mainframe.aspx?channel=<%# Eval("ValueThread")%>&threadid=<%# Eval("ThreadID")%>&genesysid=<%# Eval("GenesysNumber")%>&account=<%# Eval("Account")%>&accountid=<%# Eval("AccountContactID")%>&subject=<%# Eval("Subject")%>&source=thread&customerid=<%# Eval("CustomerID")%>">
                        <asp:Image ImageUrl="img/Icon/Text-Edit-icon2.png" ID="img_insert" runat="server" ToolTip="Add Ticket" />
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ValueThread" Caption="Channel" Settings-AutoFilterCondition="Contains" Width="80"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CustomerID" Caption="Customer ID" Settings-AutoFilterCondition="Contains" Width="110"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TicketNumber" Caption="Ticket Number" Settings-AutoFilterCondition="Contains" Width="110"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GenesysNumber" Caption="Interaction ID" Settings-AutoFilterCondition="Contains" Width="200px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ThreadID" Caption="Thread ID" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ThreadTicket" Caption="Thread Ticket" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Account" Caption="Account" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="AccountContactID" Caption="Contact ID" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="AgentID" Caption="AgentID" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Subject" Caption="Subject" Settings-AutoFilterCondition="Contains" Width="300px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PhoneChat" Caption="Phone Chat" Settings-AutoFilterCondition="Contains" Width="150px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DateCreate" Caption="Date" Settings-AutoFilterCondition="Contains" Width="130px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Action" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                <DataItemTemplate>
                    <a href="#" onclick="ShowThread('<%# Eval("GenesysNumber")%>')">
                        <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" ToolTip="Update Thread" />
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Settings ShowGroupPanel="True" />
    </dx:ASPxGridView>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="true" Width="800px"
        closeonescape="true"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Thread Ignore" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div class="row">
                    <div class="col-md-12">
                        <label>Thread Reason</label>
                        <dx:ASPxMemo ID="TxtThreadReason" runat="server" Width="100%" Rows="10" Theme="Metropolis" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <dx:ASPxButton ID="BTN_Thread" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                            <ClientSideEvents Click="function(s, e) { execThread(); }" />
                        </dx:ASPxButton>
                    </div>
                    <div class="col-md-2">
                        <dx:ASPxButton ID="BTN_Cancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                            <ClientSideEvents Click="function(s, e) { execCancel(); }" />
                        </dx:ASPxButton>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
