﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.Web.ASPxClasses.Internal
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Partial Public Class uploadcontrol
    Inherits Page
    Private Const ThumbnailSize As Integer = 100

    Dim Time As String = DateTime.Now.ToString("yyyyMMddhhmmss")
    Dim comm, sqlcomm As SqlCommand
    Dim sqlcon, sqlconn As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqldr, readPhatHTML, readStatus As SqlDataReader
    Dim execute As New ClsConn
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

    End Sub
    Protected Sub UploadControl_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)
        Try
            e.CallbackData = SavePostedFiles(e.UploadedFile)
        Catch ex As Exception
            e.IsValid = False
            e.ErrorText = ex.Message
        End Try
    End Sub
    Private Function SavePostedFiles(ByVal uploadedFile As UploadedFile) As String
        Dim _UploadDirectoryAttachment As String = String.Empty
        Dim _ViewDirectoryAttachment As String = String.Empty
        Dim _strPhatHTML As String = "select * from Temp_SettingParameter"
        readPhatHTML = execute.ExecuteReader(_strPhatHTML)
        If readPhatHTML.HasRows() Then
            readPhatHTML.Read()
            _UploadDirectoryAttachment = readPhatHTML("UploadDirectoryAttachment").ToString()
            _ViewDirectoryAttachment = readPhatHTML("ViewDirectoryAttachment").ToString()
        Else
        End If
        readPhatHTML.Close()

        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If

        Dim FolderName As String = DateTime.Now.ToString("yyyyMMddhhmmss")
        Dim directoryPath As String = Server.MapPath(String.Format(_UploadDirectoryAttachment & "/{0}/", Request.QueryString("idticketutama").Trim()))
        If Not Directory.Exists(directoryPath) Then
            Directory.CreateDirectory(directoryPath)
        Else
            ' Folder Sudah Ada
            ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Directory already exists.');", True)
        End If

        Dim fileInfo As New FileInfo(uploadedFile.FileName)
        Dim namafile As String = Time + "-" + fileInfo.Name
        Dim resFileName As String = MapPath(_UploadDirectoryAttachment + "/" + Request.QueryString("idticketutama") + "/" + namafile)
        uploadedFile.SaveAs(resFileName)

        'UploadingUtils.RemoveFileWithDelay(uploadedFile.FileName, resFileName, 5)
        Dim URL As String = _ViewDirectoryAttachment + "/" + Request.QueryString("idticketutama") + "/" + namafile
        Dim fileLabel As String = fileInfo.Name
        Dim fileLength As String = uploadedFile.ContentLength / 1024 & "K"

        Dim insAttch As String = "Insert Into BTN_Trx_Attchment (TicketNumber, Path, Usercreate) values('" & Request.QueryString("idticketutama") & "','" & URL & "','" & Session("username") & "')"
        Try
            comm = New SqlCommand(insAttch, sqlcon)
            sqlcon.Open()
            comm.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            'execute.LogError(strLogTime, ex, insAttch)
            Response.Write(ex.Message)
        Finally
            execute.LogSuccess(strLogTime, insAttch)
        End Try

        Return String.Format("{0} ({1})|{2}", fileLabel, fileLength, fileInfo.Name)
    End Function
    Private Sub uploadcontrol_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Request.QueryString("idticketutama") <> "" Then
            If (ValidasiTicketStatus(Request.QueryString("idticketutama")) = False) Then
                divtable.Visible = True
                showAttachment()
            Else
                showAttachment()
                divFileUpload.Visible = False
                divtable.Visible = True
            End If
        Else
        End If
    End Sub
    Function ValidasiTicketStatus(ByVal _Value As String)
        Dim _strStatus As String = "select Status from tTicket where TicketNumber='" & _Value & "'"
        readStatus = execute.ExecuteReader(_strStatus)
        If readStatus.HasRows() Then
            readStatus.Read()
            If (readStatus("Status") = "Solved" Or readStatus("Status") = "Closed") Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
        readStatus.Close()
    End Function
    Private Sub showAttachment()
        Try
            Dim count As String = String.Empty
            Dim Sql As String = "SELECT count(ID) AS DATA FROM BTN_Trx_Attchment WHERE TicketNumber='" & Request.QueryString("idticketutama") & "'"
            sqldr = execute.ExecuteReader(Sql)
            Try
                If sqldr.HasRows() Then
                    sqldr.Read()
                    count = sqldr("DATA").ToString
                Else
                End If
                sqldr.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            If count > 0 Then
                divtable.Visible = True
            Else
                divtable.Visible = False
            End If
            ds_attchment.SelectCommand = "SELECT replace(Path,' ','%20') as Path, Filename, TicketNumber, ID, Usercreate, Datecreate FROM BTN_Trx_Attchment where TicketNumber='" & Request.QueryString("idticketutama") & "'"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class