﻿Imports System.Data
Imports System.Data.SqlClient
Public Class mDept
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As New SqlCommand
    Dim sqldr As SqlDataReader
    Dim strUserUser, strDateUse, strModuleUse, strActivityUse, strUserRole, strDescriptionUse, valInsert As String
    Dim strExecute As New ClsConn
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sql_dept.SelectCommand = "SELECT ORGANIZATION_ID, ORGANIZATION_NAME, EMAIL FROM MORGANIZATION"
        updateAlert()
    End Sub
    Private Sub ASPxGridView1_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles ASPxGridView1.RowDeleting
        Dim _ID As String = e.Keys("ORGANIZATION_ID").ToString
        Dim _NamaDepartment As String = e.Values("ORGANIZATION_NAME").ToString
        Dim _strScript As String = String.Empty
        Dim _strScriptloq As String = String.Empty
        _strScript = "delete from MORGANIZATION where ORGANIZATION_ID='" & _ID & "'"
        Try
            _strScriptloq = _strScript & " - ORGANIZATION NAME : " & _NamaDepartment
            strExecute.LogSuccess(strLogTime, _strScriptloq)
            sql_dept.DeleteCommand = _strScript
        Catch ex As Exception
            strExecute.LogError(strLogTime, ex, _strScriptloq)
        End Try
    End Sub
    Private Sub ASPxGridView1_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        Dim _StringCounting As Integer
        Dim _strScript As String = String.Empty
        Dim _NamaDepartment As String = e.NewValues("ORGANIZATION_NAME").ToString
        Dim _EmailDepartment As String = e.NewValues("EMAIL").ToString
        If _NamaDepartment <> "" Then
            Dim _strDepartment As String
            _strDepartment = "Select COUNT (ORGANIZATION_NAME) as AdaApaGak from MORGANIZATION where (ORGANIZATION_NAME='" & _NamaDepartment & "' or EMAIL='" & _EmailDepartment & "')"
            sqlcom = New SqlCommand(_strDepartment, con)
            con.Open()
            sqldr = sqlcom.ExecuteReader()
            sqldr.Read()
            _StringCounting = sqldr("AdaApaGak")
            con.Close()
            If _StringCounting > 0 Then
                strExecute.LogSuccess(strLogTime, _strDepartment)
                e.Cancel = True
                Throw New Exception("Data already exits")
            Else
                e.Cancel = False
                _strScript = "INSERT INTO MORGANIZATION (ORGANIZATION_NAME, EMAIL, USERCREATE) VALUES ('" & _NamaDepartment & "', '" & _EmailDepartment & "', '" & Session("username") & "')"
                sql_dept.InsertCommand = "INSERT INTO MORGANIZATION (ORGANIZATION_NAME, EMAIL, USERCREATE) VALUES ('" & _NamaDepartment & "', '" & _EmailDepartment & "', '" & Session("username") & "')"
                strExecute.LogSuccess(strLogTime, _strScript)
            End If
        Else
            e.Cancel = True
            Throw New Exception("Unit kerja is empty")
        End If
    End Sub
    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim _strScript As String = String.Empty
        Dim _StringCounting As Integer
        Dim _NamaDepartment As String = e.NewValues("ORGANIZATION_NAME").ToString
        Dim _EmailDepartment As String = e.NewValues("EMAIL").ToString
        Dim _ID As String = e.Keys("ORGANIZATION_ID").ToString()
        If _NamaDepartment <> "" Then
            Dim _strDepartment As String
            _strDepartment = "Select COUNT (ORGANIZATION_NAME) as AdaApaGak from MORGANIZATION where (ORGANIZATION_NAME='" & _NamaDepartment & "' or EMAIL='" & _EmailDepartment & "')"
            sqlcom = New SqlCommand(_strDepartment, con)
            con.Open()
            sqldr = sqlcom.ExecuteReader()
            sqldr.Read()
            _StringCounting = sqldr("AdaApaGak")
            con.Close()
            If _StringCounting > 0 Then
                'strExecute.LogSuccess(strLogTime, _strDepartment)
                'e.Cancel = True
                'Throw New Exception("Data already exits")
                e.Cancel = False
                _strScript = "UPDATE MORGANIZATION SET ORGANIZATION_NAME='" & _NamaDepartment & "', EMAIL='" & _EmailDepartment & "', USERUPDATE='" & Session("username") & "', DATEUPDATE=GETDATE() WHERE ORGANIZATION_ID='" & _ID & "'"
                sql_dept.UpdateCommand = _strScript
                strExecute.LogSuccess(strLogTime, _strScript)
            Else
                e.Cancel = False
                _strScript = "UPDATE MORGANIZATION SET ORGANIZATION_NAME='" & _NamaDepartment & "', EMAIL='" & _EmailDepartment & "', USERUPDATE='" & Session("username") & "', DATEUPDATE=GETDATE() WHERE ORGANIZATION_ID='" & _ID & "'"
                sql_dept.UpdateCommand = _strScript
                strExecute.LogSuccess(strLogTime, _strScript)
            End If
        Else
            e.Cancel = True
            Throw New Exception("Unit kerja is empty")
        End If
    End Sub
    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class