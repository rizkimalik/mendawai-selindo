﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="dash_ticketing.aspx.vb" Inherits="ICC.dash_ticketing" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
        <script src="js/highchart.js"></script>
        <script src="js/highchart-more.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', (event) => {
                const UserName = '<%= Session("username")%>';
                const UserLevel = '<%= Session("lvluser")%>';
                const UserOrg = '<%= Session("organization")%>';
                const LayerID = '<%= Session("LoginType")%>';
                const url = 'WebServiceTransaction.asmx/New_DashboardSection';

                //CHART AGENT CREATE	
                const DashLine = Highcharts.chart('chart_agent_create', {
                    chart: {
                        type: 'spline',
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        title: {
                            text: ''
                        },
                        type: 'datetime',
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                    },
                    legend: {
                        enabled: true,
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -10,
                        y: 10,
                        floating: true,
                        borderWidth: 0,
                        backgroundColor: 'transparent',
                        shadow: false
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br/>',
                        pointFormat: '{point.name} : {point.y}',
                        useHTML: true,
                        crosshairs: false,
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                enable: false
                            }
                        },
                    },
                    credits:{
                        enabled : false
                    },
                    series: []
                    // series: [{
                        // name: '',
                        // data: []
                    // }]
                    /* series: [{
                        name: 'John',
                        data: [5, 3, 4, 7, 2]
                    }, {
                        name: 'Jane',
                        data: [2, 2, 3, 2, 1]
                    }, {
                        name: 'Joe',
                        data: [3, 4, 4, 2, 5]
                    }] */
                    
                });

                //CHART SUMMARY CREATE
                const DashBar = Highcharts.chart('chart_summary_create', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            style: {
                                fontSize: '9px',
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        headerFormat: '<center><b>{series.name}</b></center>',
                        pointFormat: 'Total : {point.y}',
                        useHTML: true,
                        crosshairs: false,
                    
                    },
                    plotOptions: {
                        column: {
                            dataLabels: {
                                enabled: false
                            },
                        },
                        series: {
                            stacking: 'normal'
                        }
                    },
                    legend: {
                        enabled: true,
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -10,
                        y: 10,
                        floating: true,
                        borderWidth: 0,
                        backgroundColor: 'transparent',
                        shadow: false
                    },
                    credits:{
                        enabled : false
                    },
                    series: []
                    // series: [{
                        // name: '',
                        // data: []
                    // }] 
                });


                async function DashTotalTicket(){
                    const config = {
                        method: 'POST',
                        headers: {
                            "Content-Type" : "application/json"
                        },
                        body: JSON.stringify({
                            Data1:"DashTotalTicket",
                            Data2:"Administrator",
                            Data3:UserName,
                            Data4:UserOrg,
                            Data5:"ADMIN",
                            Data6:LayerID
                        })
                    }
                    
                    try {
                        const res = await fetch(url, config);
                        const obj = await res.json();
                        const data = JSON.parse(obj.d);
                        // console.log(obj);

                        if(res.ok){
                            let html = "";
                            let icon = "";
                            let background = "";
                            for (i = 0; i < data.length; i++) {
                                if(data[i].StatusData == "Open"){
                                    icon = 'fa-folder-open-o';
                                }
                                else if(data[i].StatusData == "Progress"){
                                    icon = 'fa-spinner';
                                }
                                else if(data[i].StatusData == "Pending"){
                                    icon = 'fa-warning';
                                }
                                else if(data[i].StatusData == "Solved"){
                                    icon = 'fa-check';
                                } 
                                else if(data[i].StatusData == "Closed"){
                                    icon = 'fa-thumbs-up';
                                } 

                                if (data[i].JumlahData > 0) {
                                    background = 'bg-success';
                                }
                                else {
                                    background = 'bg-info';
                                }

                                html += `<div class="col-md-3 col-sm-4">
                                    <div class="panel-stat3 ${background}">
                                        <h1 class="m-top-none">${data[i].JumlahData}</h1>
                                        <h5>Ticket ${data[i].StatusData}</h5> 
                                        <div class="stat-icon"><i class="fa ${icon} fa-3x"></i></div>
                                    </div>
                                </div>`;
                                
                            }
                            $("#TotalTicket").html(html);
                        }
                    } 
                    catch (error) {
                        console.log(error);	
                    }

                }

                async function DashLineTicket(){		
                    if (DashLine) {
                        const x_axis1 = DashLine.xAxis[0];
                        const config = {
                            method: 'POST',
                            headers: {
                                "Content-Type" : "application/json"
                            },
                            body: JSON.stringify({
                                Data1:"DashLineTicket",
                                Data2:"Administrator",
                                Data3:UserName,
                                Data4:UserOrg,
                                Data5:"ADMIN",
                                Data6:LayerID
                            })
                        }

                        try {
                            const res = await fetch(url,config);
                            const obj = await res.json();
                            const data = JSON.parse(obj.d);
                            const category = (Object.keys(data[0])).slice(2);
                            // console.log(obj);

                            if (res.ok) {
                                let JamNya = [],
                                Complaint = [],
                                Feedback = [],
                                Information = [],
                                Request = [],
                                SecurityAndOthers = [];
                                
                                let total_agent = 0;
                                let colors = ["#00a0dc","#8d6cab","#dd5143","#e68523","#57bfc1","#edb220","#dc4b89","#69a62a","#046293","#66418c"];
                                
                                for(var i = DashLine.series.length -1; i > -1; i--) {
                                    DashLine.series[i].remove();
                                }
                                
                                for(let i=0; i < data.length; i++){
                                    JamNya.push(data[i].JamNya);
                                    Complaint.push(data[i].Complaint);
                                    Feedback.push(data[i].Feedback);
                                    Information.push(data[i].Information);
                                    Request.push(data[i].Request);
                                    // SecurityAndOthers.push(data[i].SecurityAndOthers);

                                    total_agent += (parseInt(data[i].Complaint) + parseInt(data[i].Feedback) + parseInt(data[i].Information) + parseInt(data[i].Request));
                                }

                                x_axis1.setCategories(JamNya);
                                for (let i = 0; i < category.length; i++) {
                                    let total = "";
                                    if (category[i] == "Complaint") {
                                        total = Complaint;
                                    }
                                    else if (category[i] == "Feedback") {
                                        total = Feedback;
                                    }
                                    else if (category[i] == "Information") {
                                        total = Information;
                                    }
                                    else if (category[i] == "Request") {
                                        total = Request;
                                    }

                                    DashLine.addSeries({
                                        name: category[i],
                                        color : colors[i],
                                        data: total
                                    });
                                }
                                
                                DashLine.redraw();
                                $("#total_agent").html(total_agent);
                            }
                        } 
                        catch (error) {
                            console.log(error);	
                        }
                    }
                }

                async function DashBarTicket(){
                    const config = {
                        method: 'POST',
                        headers: {
                            "Content-Type" : "application/json"
                        },
                        body: JSON.stringify({
                            Data1:"DashBarTicket",
                            Data2:"Administrator",
                            Data3:UserName,
                            Data4:UserOrg,
                            Data5:"ADMIN",
                            Data6:LayerID
                        })
                    }
                    
                    try {
                        const res = await fetch(url, config);
                        const obj = await res.json();
                        const data = JSON.parse(obj.d);
                        // console.log(obj);

                        if(res.ok){
                            var total_summary = 0;
                            for(var i = DashBar.series.length -1; i > -1; i--) {
                                DashBar.series[i].remove();
                            }
                            
                            for(var i=0; i < data.length; i++){
                                var Ticket = data[i].Ticket;
                                var Jumlah = parseInt( data[i].Jumlah );
                                var colors = ["#00a0dc","#8d6cab","#dd5143","#e68523","#57bfc1","#edb220","#dc4b89","#69a62a","#046293","#66418c"];
                                total_summary+=Jumlah;
                                
                                DashBar.addSeries({
                                    name: Ticket,
                                    data: [{
                                        name: Ticket, 
                                        y: Jumlah
                                    }],
                                    color: colors[i]
                                });	
                            }
                            DashBar.redraw();	
                            $("#total_summary").html(total_summary);
                            
                        } 
                    } 
                    catch (error) {
                        console.log(error);	
                    }

                }

                async function DashTableTicket(){
                    const config = {
                        method: 'POST',
                        headers: {
                            "Content-Type" : "application/json"
                        },
                        body: JSON.stringify({
                            Data1:"DashTableTicket",
                            Data2:"Administrator",
                            Data3:UserName,
                            Data4:UserOrg,
                            Data5:"ADMIN",
                            Data6:LayerID
                        })
                    }
                    
                    try {
                        const res = await fetch(url, config);
                        const obj = await res.json();
                        const data = JSON.parse(obj.d);
                       
                        if(res.ok){
                            $('#table_agent_create').DataTable( {
                                "data": data,
                                "destroy": true,
                                "width": "100%",
                                "columns" : [
                                    { "data" : "TicketNumber" },
                                    { "data" : "CategoryName" },
                                    { "data" : "SubCategory1Name" },
                                    { "data" : "SubCategory2Name" },
                                    { "data" : "SubCategory3Name" },
                                    { "data" : "SLA" },
                                    { "data" : "Status" },
                                    { "data" : "UserCreate" },
                                    { "data" : "DateCreates" },
                                ]
                            });
                        }
                    } 
                    catch (error) {
                        console.log(error);	
                    }

                }

                function RefreshLoadData() {
                    DashTotalTicket();	
                    DashLineTicket();
                    DashBarTicket();
                    DashTableTicket();
                }
                RefreshLoadData();
            });
            

        </script>
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Dashboard Ticket</h3>
            </div>
            <ul class="page-stats">
                <li>
                    <a href="#" onclick="RefreshLoadData()" class="refresh-widget" data-toggle="tooltip"
                        data-placement="bottom" title="Refresh Data" data-original-title="Refresh"><i
                            class="fa fa-refresh"></i> refresh</a>
                </li>
            </ul>
        </div>
        <!-- /main-header -->

        <div class="row">
            <div id="TotalTicket"></div>
            <div class="loading-overlay">
                <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="pull-left"><i class="fa fa-bar-chart-o fa-lg"></i> Agent Create Ticket</span>
                        <ul class="tool-bar">
                            <li><a href="#" class="refresh-widget" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Refresh" id="btn-refresh"><i class="fa fa-refresh"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body no-padding">
                        <div id="chart_agent_create" class="graph" style="height:220px"></div>
                    </div>
                    <div class="panel-footer">
                        <div class="col-xs-6">
                            <span class="no-margin"><i class="fa fa-envelope"></i> Total Ticket</span>
                        </div><!-- /.col -->
                        <div class="row">
                            <div class="col-xs-6 text-right">
                                <span class="no-margin" id="total_agent"></span>
                            </div><!-- /.col -->
                        </div>
                    </div>
                </div><!-- /panel -->
            </div><!-- /col -->
            
            
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="pull-left"><i class="fa fa-bar-chart-o fa-lg"></i> Summary Create Ticket</span>
                    </div>
                    <div class="panel-body no-padding">
                        <div id="chart_summary_create" class="graph" style="height:220px"></div>
                    </div>
                    <div class="panel-footer">
                        <div class="col-xs-6">
                            <span class="no-margin"><i class="fa fa-envelope"></i> Total Ticket</span>
                        </div><!-- /.col -->
                        <div class="row">
                            <div class="col-xs-6 text-right">
                                <span class="no-margin" id="total_summary"></span>
                            </div><!-- /.col -->
                        </div>
                    </div>
                </div><!-- /panel -->
            </div><!-- /col -->		
            
        </div><!-- /row -->
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="pull-left"><i class="fa fa-bar-chart-o fa-lg"></i> Data Detail Ticket</span>
                    </div>
                    <div class="panel-body no-padding">
                        <table class="table table-bordered table-condensed table-hover table-striped" id="table_agent_create">
                            <thead>
                                <tr>
                                    <th>Ticket Number</th> 
                                    <th>Category</th> 
                                    <th>Enquiry Type</th> 
                                    <th>Enquiry Detail</th> 
                                    <th>Reaseon</th> 
                                    <th>SLA</th> 
                                    <th>Status</th> 
                                    <th>User Create</th> 
                                    <th>Date Create</th> 
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div><!-- /panel -->
            </div><!-- /col -->
        </div><!-- /row -->

    </asp:Content>
