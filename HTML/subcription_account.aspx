﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="subcription_account.aspx.vb" Inherits="ICC.subcription_account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" Runat="Server">
    <script>
        function popupwindow(url, title, w, h) {
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            return window.open(url, '_blank', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        }
    </script>
    <script type="module" src="scripts/subcription_account.js"></script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Main Content -->
    <div class="hk-pg-wrapper">
        <!-- Container -->
        <div class="ml-20 mr-20 mt-20">
            <!-- Title -->
            <div class="hk-pg-header align-items-top">
                <div>
                    <h2 class="hk-pg-title font-weight-600 mb-10">Subcription Account</h2>
                    <p>Activation account sosial media. <a href="#">Learn more.</a></p>
                </div>
                <div class="headline"><span class="line bg-warning"></span></div>
            </div>
            <!-- /Title -->
            
            <!-- Row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body pa-0">
                            <div class="row padding-md">
                                <div class="col-lg-12 text-center">
                                    <h6>Click button below to <span class="badge badge-info">Subcribe</span></h6>

                                    <a href="#" class="btn btn-default quick-btn m-right-md" onclick="alert('Contact Us for Registration')">
                                        <div class="avatar avatar-xs">
                                            <img src="img/channel/chat.png" alt="chat" class="avatar-img rounded-circle">
                                        </div>
                                        <span class="text-mute">Web Chat</span>
                                    </a>
                                    <a class="btn btn-default quick-btn m-right-md" href="#" onclick="popupwindow('https://www.facebook.com/login.php?skip_api_login=1&api_key=113869198637480&kid_directed_site=0&app_id=113869198637480&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fv15.0%2Fdialog%2Foauth%3Fapp_id%3D113869198637480%26auth_type%26cbt%3D1673331503332%26channel_url%3Dhttps%253A%252F%252Fstaticxx.facebook.com%252Fx%252Fconnect%252Fxd_arbiter%252F%253Fversion%253D46%2523cb%253Df3d7866720d6b74%2526domain%253Ddevelopers.facebook.com%2526is_canvas%253Dfalse%2526origin%253Dhttps%25253A%25252F%25252Fdevelopers.facebook.com%25252Ff17d9937af9cffc%2526relation%253Dopener%26client_id%3D113869198637480%26config_id%26display%3Dpopup%26domain%3Ddevelopers.facebook.com%26e2e%3D%257B%257D%26fallback_redirect_uri%3Dhttps%253A%252F%252Fdevelopers.facebook.com%252Fdocs%252Ffacebook-login%252Fweb%252F%26force_confirmation%3Dfalse%26id%3Df3c5b5242f4c9f8%26locale%3Den_US%26logger_id%3D5c853e46-1067-45b4-b3c4-3109b71858c1%26messenger_page_id%26origin%3D1%26plugin_prepare%3Dtrue%26redirect_uri%3Dhttps%253A%252F%252Fstaticxx.facebook.com%252Fx%252Fconnect%252Fxd_arbiter%252F%253Fversion%253D46%2523cb%253Df3c67d3704b0328%2526domain%253Ddevelopers.facebook.com%2526is_canvas%253Dfalse%2526origin%253Dhttps%25253A%25252F%25252Fdevelopers.facebook.com%25252Ff17d9937af9cffc%2526relation%253Dopener.parent%2526frame%253Df3c5b5242f4c9f8%26ref%3DLoginButton%26reset_messenger_state%3Dfalse%26response_type%3Dsigned_request%252Ctoken%252Cgraph_domain%26scope%26sdk%3Djoey%26size%3D%257B%2522width%2522%253A600%252C%2522height%2522%253A679%257D%26url%3Ddialog%252Foauth%26version%3Dv15.0%26ret%3Dlogin%26fbapp_pres%3D0%26tp%3Dunspecified&cancel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df3c67d3704b0328%26domain%3Ddevelopers.facebook.com%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fdevelopers.facebook.com%252Ff17d9937af9cffc%26relation%3Dopener.parent%26frame%3Df3c5b5242f4c9f8%26error%3Daccess_denied%26error_code%3D200%26error_description%3DPermissions%2Berror%26error_reason%3Duser_denied&display=popup&locale=en_GB&pl_dbl=0','fb','1000','600')">
                                    <!-- <a class="btn btn-default quick-btn m-right-md" href="#" onclick="popupwindow('https://mendawai.com/sosialapi/sosial/facebook/facebook/loginlink','fb','1000','600')"> -->
                                    <!-- <a class="btn btn-default quick-btn m-right-md" href="#" onclick="alert('Contact Us for Registration')"> -->
                                        <div class="avatar avatar-xs">
                                            <img src="img/channel/facebook.png" alt="facebook" class="avatar-img rounded-circle">
                                        </div>
                                        <span class="text-mute">Facebook</span>
                                    </a>
                                    <a class="btn btn-default quick-btn m-right-md" href="#" onclick="popupwindow('https://mendawai.com/sosialapi/sosial/twitter/twitter/GetOauthToken','tw','1000','600')">
                                        <div class="avatar avatar-xs">
                                            <img src="img/channel/twitter.png" alt="twitter" class="avatar-img rounded-circle">
                                        </div>
                                        <span class="text-mute">Twitter</span>
                                    </a>
                                    <!-- <a class="btn btn-default quick-btn m-right-md" href="#" onclick="popupwindow('https://mendawai.com/sosialapi/sosial/instagram/instagram/loginlink','ig','1000','600')"> -->
                                    <a class="btn btn-default quick-btn m-right-md" href="#" onclick="alert('Contact Us for Registration')">
                                        <div class="avatar avatar-xs">
                                            <img src="img/channel/instagram.png" alt="twitter" class="avatar-img rounded-circle">
                                        </div>
                                        <span class="text-mute">Instagram</span>
                                    </a>
                                    <a class="btn btn-default quick-btn m-right-md" href="#" onclick="alert('Contact Us for Registration')">
                                        <div class="avatar avatar-xs">
                                            <img src="img/channel/whatsapp.png" alt="whatsapp" class="avatar-img rounded-circle">
                                        </div>
                                        <span class="text-mute">Whatsapp</span>
                                    </a>
                                    <a class="btn btn-default quick-btn m-right-md" href="#" onclick="alert('Contact Us for Registration')">
                                        <div class="avatar avatar-xs">
                                            <img src="img/channel/email.png" alt="email" class="avatar-img rounded-circle">
                                        </div>
                                        <span class="text-mute">Email</span>
                                    </a>
                                    <hr />
                                </div>
                            </div>
                            
                            <div class="row padding-md">
                                <div id="dxGridDataSubcription"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->
        
    </div>
</asp:Content>

