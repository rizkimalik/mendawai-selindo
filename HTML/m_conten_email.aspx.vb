﻿Imports DevExpress.Web.Data
Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports DevExpress.Web
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxHtmlEditor
Imports DevExpress.Web.ASPxEditors

Public Class m_conten_email
    Inherits System.Web.UI.Page

    Dim proses As New ClsConn
    Dim _upage As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _updatePage()
    End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub ASPxGridView_Email_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles ASPxGridView_Email.RowUpdating
        Dim grid As ASPxGridView = TryCast(sender, ASPxGridView)
        Dim htmlEditor As ASPxHtmlEditor = TryCast(ASPxGridView_Email.FindEditFormTemplateControl("html_editor_Body"), ASPxHtmlEditor)
        e.NewValues("Header_HTML") = htmlEditor.Html
        Dim htmlEditorFooter As ASPxHtmlEditor = TryCast(ASPxGridView_Email.FindEditFormTemplateControl("html_footer"), ASPxHtmlEditor)
        e.NewValues("Footer_HTML") = htmlEditorFooter.Html
        Dim txtSubjectNya As ASPxTextBox = TryCast(ASPxGridView_Email.FindEditFormTemplateControl("txtSubject"), ASPxTextBox)
        e.NewValues("Subject") = txtSubjectNya.Text
    End Sub

    'Private Sub ASPxGridView_Email_RowInserting(sender As Object, e As ASPxDataInsertingEventArgs) Handles ASPxGridView_Email.RowInserting

    'End Sub

    'Private Sub ASPxGridView_Email_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles ASPxGridView_Email.RowUpdating
    '    'Dim grid As ASPxGridView = TryCast(sender, ASPxGridView)
    '    'e.NewValues("ID") = Guid.NewGuid().ToString().Replace("-", "")
    '    'Dim htmlEditor As ASPxHtmlEditor = TryCast(ASPxGridView_Email.FindEditFormTemplateControl("ASPxHtmlEditor1"), ASPxHtmlEditor)
    '    'e.NewValues("Footer_HTML") = htmlEditor.Html
    'End Sub
End Class