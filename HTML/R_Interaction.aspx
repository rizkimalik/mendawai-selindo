﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="R_Interaction.aspx.vb" Inherits="ICC.R_Interaction" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h4 class="headline">Report Interaction Ticket
			<span class="line bg-warning"></span>
        </h4>
        <br />
        <dx:ASPxLabel Visible="false" runat="server" ID="sqlOutput"></dx:ASPxLabel>
    </div>
    <div class="row" style="margin-bottom: -15px;">
        <div class="col-sm-2">
            <label>Start Date</label>
            <dx:ASPxDateEdit ID="dt_strdate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2">
            <label>End Date</label>
            <dx:ASPxDateEdit ID="dt_endate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2" style="margin-top: 5px;">
            <br />
            <dx:ASPxButton ID="btn_Submit" runat="server" Theme="Metropolis" AutoPostBack="False" Text="Submit" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <div style="overflow: hidden;">
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" KeyFieldName="ID" DataSourceID="LinqDataSource1"
                    Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                    SettingsPager-PageSize="10">
                    <%-- OnCustomColumnGroup="ASPxGridView1_CustomColumnGroup" OnCustomGroupDisplayText="ASPxGridView1_CustomGroupDisplayText"
                    OnCustomColumnSort="ASPxGridView1_CustomColumnSort">--%>
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="15, 20, 25" ShowAllItem="true" />
                    </SettingsPager>
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFilterRow="false" ShowFilterRowMenu="false"
                        ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                            PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" Visible="true" Width="150px" VisibleIndex="0" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Channel" FieldName="TicketSourceName" Width="100px" VisibleIndex="1" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="NIK" Width="160px" VisibleIndex="2" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="CIF Number" FieldName="CIF" Width="160px" VisibleIndex="3" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Width="200px" VisibleIndex="4" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Width="160px" VisibleIndex="5" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Thread Ticket" FieldName="ThreadTicket" Width="160px" VisibleIndex="6" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataMemoColumn Caption="Agent Response" FieldName="ResponseComplaint" Width="300px" VisibleIndex="7" Settings-AutoFilterCondition="Contains"></dx:GridViewDataMemoColumn>
                        <dx:GridViewDataTextColumn Caption="Escalation To" FieldName="DispatchTicket" Width="160px" VisibleIndex="8" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Escalation To Layer" FieldName="DispatchToLayer" Width="160px" VisibleIndex="9" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Escalation To Dept" FieldName="ORGANIZATION_NAME" Width="160px" VisibleIndex="10" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Amount" FieldName="Amount" Width="150px" Settings-AutoFilterCondition="Contains" VisibleIndex="11" PropertiesTextEdit-DisplayFormatString="{0:n2}"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Created by" FieldName="AgentCreate" Width="160px" VisibleIndex="12" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" Width="160px" VisibleIndex="13" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Type" FieldName="InteractionType" Width="160px" VisibleIndex="14" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date Create" FieldName="DateCreate" Width="130px" VisibleIndex="15" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </div>

        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <asp:ListItem Value="rtf" Text="RTF" />
                <asp:ListItem Value="csv" Text="CSV" />
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <dx:ASPxButton ID="btn_Export" runat="server" Text="Export" Theme="Metropolis" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1"></dx:ASPxGridViewExporter>
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ICC.V_InteractionDBMLDataContext"
        EntityTypeName="" TableName="V_InteractionS" Where="DateCreate >= @StartTanggalFilter And DateCreate <= @EndTanggalFilter" OrderBy="DateCreate ASC">
        <WhereParameters>
            <asp:SessionParameter Name="StartTanggalFilter" SessionField="StartTanggalFilter" Type="DateTime"/>
            <asp:SessionParameter Name="EndTanggalFilter" SessionField="EndTanggalFilter" Type="DateTime"/>
        </WhereParameters>
    </asp:LinqDataSource>
</asp:Content>

