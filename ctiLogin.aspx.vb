﻿Imports System
Imports System.Web.UI
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class ctiLogin
    Inherits System.Web.UI.Page

    Dim Proses As New ClsConn
    Dim sqldr As SqlDataReader
    Dim sql As String
    Dim valuesatu As Integer = 1
    Dim valuedua As Integer = 2
    Dim valuetiga As Integer = 3
    Dim ValueEmpat As Integer = 4
    Dim valueLima As Integer = 5
    Dim leveluser As String
    Dim value As String
    Dim Type As String
    Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlconaux As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Com, sqlcom, sqlcomaux As SqlCommand
    Dim f_User As String
    Dim loq As New cls_globe
    Dim connString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim recLDAP As Integer
    Dim recCount As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Using conn As New SqlConnection(connString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("Select COUNT (ID) as LDAPCount from ICC_LDAP_Setting WHERE NA='Y'", conn)
                recCount = cmd.ExecuteScalar()
            End Using
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        'Response.Write(Request.QueryString("password"))

        'Dim dc As String = HttpUtility.UrlDecode(Request.QueryString("password"))
        recCount = "0"
        If recCount <> 0 Then
            Dim LDAPServer As String = ConfigurationManager.AppSettings("LDAP")
            If ValidateActiveDirectoryLogin(LDAPServer, Request.QueryString("user"), Request.QueryString("password")) = True Then
                Try
                    Using conn As New SqlConnection(connString)
                        conn.Open()
                        Dim cmd As SqlCommand = New SqlCommand("Select COUNT (UserID) as userID from msUser where UserName=@uservalue", conn)
                        Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                        uservalue.Value = Request.QueryString("user")
                        cmd.Parameters.Add(uservalue)
                        recLDAP = cmd.ExecuteScalar()

                        If recLDAP = 1 Then
                            loq.writedata(Request.QueryString("user"), "Login", "login success, user applikasi exits", Request.QueryString("user"), "login")
                            AccessLogin(Request.QueryString("user"))
                        Else
                            loq.writedata(Request.QueryString("user"), "Login", "login failed, user applikasi not found", Request.QueryString("user"), "login")
                            lblError.Visible = True
                            lbl_Error.Text = "User Applikasi not found"
                        End If
                    End Using
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            Else

            End If
        Else
            If Request.QueryString("user") <> "" Then
                Try
                    Using conn As New SqlConnection(connString)
                        conn.Open()
                        Dim cmd As SqlCommand = New SqlCommand("Select COUNT (UserID) as userID from msUser where UserName=@uservalue", conn)
                        Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                        uservalue.Value = Request.QueryString("user")
                        cmd.Parameters.Add(uservalue)
                        recLDAP = cmd.ExecuteScalar()

                        If recLDAP = 1 Then
                            loq.writedata(Request.QueryString("user"), "Login", "login success, user applikasi exits", Request.QueryString("user"), "login")
                            AccessLogin(Request.QueryString("user"))
                        Else
                            loq.writedata(Request.QueryString("user"), "Login", "login failed, user applikasi not found", Request.QueryString("user"), "login")
                            lblError.Visible = True
                            lbl_Error.Text = "User Applikasi not found"
                        End If

                    End Using
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            Else
                loq.writedata(Request.QueryString("user"), "Login", "login failed, user empty", "user kosong", "login")
                lblError.Visible = True
                lbl_Error.Text = "Please, input username"
            End If
        End If
    End Sub
    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.SearchScope = DirectoryServices.SearchScope.OneLevel
        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)
            loq.writedata(Session("UserName"), "Login", "login success, user LDAP exits", Request.QueryString("user"), "login")
        Catch
            Success = False
            loq.writedata(Session("UserName"), "Login", "login failed, user LDAP not found", Request.QueryString("user"), "login")
            lblError.Visible = True
            lbl_Error.Text = "User LDAP not found"
        End Try
        Return Success
    End Function
    Function AccessLogin(ByVal username As String) As String
        Session("NoExt") = Request.QueryString("phone")

        sql = "EXEC SP_LOGIN_APPLIKASI  " & username & ""
        Try
            sqldr = Proses.ExecuteReader(sql)
            If sqldr.HasRows Then
                sqldr.Read()
                leveluser = sqldr("LAYER").ToString
                Session("UserName") = sqldr("UserName").ToString
                Session("lblUserName") = sqldr("UserName").ToString
                Session("UnitKerja") = sqldr("ORGANIZATION").ToString
                Session("Org") = sqldr("ORGANIZATION_NAME").ToString
                Session("NameKaryawan") = sqldr("NAME").ToString
                Session("LoginType") = sqldr("LAYER").ToString
                'Session("divisi") = sqldr("Divisi").ToString
                Session("lvluser") = sqldr("LevelUser").ToString
                'Session("path") = sqldr("Path").ToString
                Session("channel_code") = sqldr("CHANNEL_CODE").ToString
                Session("organization") = sqldr("ORGANIZATION").ToString
                Session("orgSupervisor") = sqldr("ORGANIZATION").ToString
                Session("sessionchat") = sqldr("CHAT").ToString
                Session("unitkerjaagent") = sqldr("ORGANIZATION").ToString
                Session("lokasiPengaduan") = "Agent Call Center"
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Session("LoginTypeSbg") = leveluser

        If Session("LoginType") = "" Then

        ElseIf Session("LoginType") = "layer1" Then
            Session("LoginTypeAngka") = valuesatu
        ElseIf Session("LoginType") = "layer2" Then
            Session("LoginTypeAngka") = valuetiga
        ElseIf Session("LoginType") = "Supervisor" Then
            Session("LoginTypeAngka") = valuedua
        ElseIf Session("LoginType") = "Admin" Then
        End If

        Dim updatelogin As String = "UPDATE MSUSER SET LOGIN='1', IdAUX='9',DescAUX='READY' WHERE USERNAME ='" & username & "'"
        Try
            Com = New SqlCommand(updatelogin, Con)
            Con.Open()
            Com.ExecuteNonQuery()
            Con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim strsqlSelect As String = String.Empty
        Dim iddesc As String = String.Empty
        Dim auxdesc As String = String.Empty
        Dim strAux As String = "select * from icc_aux_history where userid='" & Session("username") & "' order by id desc"
        Try
            sqldr = Proses.ExecuteReader(strAux)
            If sqldr.HasRows Then
                sqldr.Read()
                iddesc = sqldr("id").ToString
                auxdesc = sqldr("AUXDESCRIPTION").ToString
                If auxdesc <> "READY" Then
                    Try
                        strsqlSelect = "update icc_aux_history set Datecreate=getdate(), state='1' where id='" & iddesc & "'"
                        Proses.ExecuteNonQuery(strsqlSelect)
                    Catch ex As Exception
                        Response.Write(ex.Message)
                    End Try
                Else

                End If
            Else
                Try
                    strsqlSelect = "insert into ICC_AUX_HISTORY (userid, auxid, auxdescription, state) values('" & Session("username") & "','9','READY','1')"
                    Proses.ExecuteNonQuery(strsqlSelect)
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim ilogin As String = "INSERT INTO ICC_LOG_IN (USERID,ACTIVITY_DATE,AUX_DESCRIPTION) VALUES('" & username & "',GETDATE(),'LOGIN')"
        sqlcom = New SqlCommand(ilogin, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        If Request.QueryString("") <> "" Then
            Dim updatelogin1 As String = "UPDATE MSUSER SET LOGIN='1',IdAUX='9',DescAUX='READY' WHERE USERNAME ='" & Request.QueryString("user") & "'"
            sqlcomaux = New SqlCommand(updatelogin1, sqlconaux)
            Try
                sqlconaux.Open()
                sqlcomaux.ExecuteNonQuery()
                sqlconaux.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        Else
        End If

        If Request.QueryString("user") = "" Then
            Response.Redirect("HTML/dashboard.aspx")
        Else
            If Request.QueryString("phone") <> "" Then
                inbound_channel_history(Request.QueryString("phone"))
            Else
            End If
            'Response.Redirect("HTML/new_inbox.aspx")
            Response.Redirect("HTML/tr_utama.aspx?new=1&phone=" & Request.QueryString("phone") & "")
        End If
    End Function
    Function inbound_channel_history(ByVal phone As String)
        Dim vPhone As Integer
        Using conn As New SqlConnection(connString)
            conn.Open()

            Dim cmd As SqlCommand = New SqlCommand("Select COUNT (IVC_ID) as data from ICC_CHANNEL_HISTORY where CustomerID=@CustomerID and FLAGPHONE='N'", conn)
            Dim uservalue As SqlParameter = New SqlParameter("@CustomerID", SqlDbType.VarChar, 150)
            uservalue.Value = phone
            cmd.Parameters.Add(uservalue)
            vPhone = cmd.ExecuteScalar()

            If vPhone = 1 Then
                Dim updateiccchat As String = "UPDATE ICC_CHANNEL_HISTORY SET FLAGPHONE='Y' WHERE CustomerID='" & phone & "' And FLAGPHONE='N'"
                Com = New SqlCommand(updateiccchat, Con)
                Try
                    Con.Open()
                    Com.ExecuteNonQuery()
                    Con.Close()
                    loq.writedata(Session("UserName"), "Update", "Update ICC_CHANNEL_HISTORY", phone, Session("username"))
                Catch ex As Exception
                    Response.Write(DirectCast("", String))
                End Try
                Try
                    Dim insertdata As String = "insert into ICC_CHANNEL_HISTORY (CHANNELTYPE, DATED, AGENT, CHANNELDESC, CustomerID, STATUS, DIRECTION, FLAGNOTIF) " & _
                                               "VALUES ('INBOUND', GETDATE(), '" & Session("username") & "', 'ICC_CALL_HISTORY_IN', '" & phone & "', '1', '1', '1')"
                    Com = New SqlCommand(insertdata, Con)
                    Try
                        Con.Open()
                        Com.ExecuteNonQuery()
                        Con.Close()
                    Catch ex As Exception
                        Response.Write(DirectCast("", String))
                    End Try
                    loq.writedata(Session("UserName"), "insert", "insert channel inbound", phone, Session("username"))
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            Else
                Try
                    Dim insertdata As String = "insert into ICC_CHANNEL_HISTORY (CHANNELTYPE, DATED, AGENT, CHANNELDESC, CustomerID, STATUS, DIRECTION, FLAGNOTIF) " & _
                                               "VALUES ('INBOUND', GETDATE(), '" & Session("username") & "', 'ICC_CALL_HISTORY_IN', '" & phone & "', '1', '1', '1')"
                    Com = New SqlCommand(insertdata, Con)
                    Try
                        Con.Open()
                        Com.ExecuteNonQuery()
                        Con.Close()
                    Catch ex As Exception
                        Response.Write(DirectCast("", String))
                    End Try
                    loq.writedata(Session("UserName"), "insert", "insert channel inbound", phone, Session("username"))
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            End If
        End Using
    End Function
End Class