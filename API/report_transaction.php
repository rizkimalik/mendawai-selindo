<?php
require_once "config/connection.php";
require_once "helper/response.php";
require_once "helper/replace_badstring.php";

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

$action = $_GET['action'] ?? '';
$response = [];

if (!empty($action)) {
    switch ($action) {
        case "data":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $skip = isset($_GET['skip']) ? $_GET['skip'] : 0;
                $take = isset($_GET['take']) ? $_GET['take'] : 15;
                $sort = isset($_GET['sort']) ? json_decode($_GET['sort'], true) : '';
                $filter = isset($_GET['filter']) ? json_decode($_GET['filter'], true) : '';
                $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : '';
                $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : '';
                $customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : '';

                $orderby = 'ORDER BY TicketNumber ASC';
                if ($sort) {
                    $desc = $sort[0]['desc'] == true ? 'desc' : 'asc';
                    $orderby = "ORDER BY ".$sort[0]['selector']." $desc";
                }

                $filtering = '';
                if ($filter) {
                    $filtering = "AND ".$filter[0]." LIKE '%".$filter[2]."%'";
                }
                
                $customer = '';
                if ($customer_id) {
                    $customer = "AND NIK=" . $customer_id;
                }

                $query = "SELECT * FROM vw_R_Transaction WHERE CreatedDate Between '$start_date 00:00:00' and '$end_date 23:59:00' and TicketNumber <> ''
                    $customer
                    $filtering
                    $orderby
                    OFFSET $skip ROWS FETCH NEXT $take ROWS ONLY";

                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        // replace bad string
                        $row["ResponComplaint"] = replace_badstring($row['ResponComplaint']);
                        $row["DetailComplaint"] = replace_badstring($row['DetailComplaint']);
                        $data[] = $row;
                    }

                    $query_total = "SELECT COUNT(*) as total FROM vw_R_Transaction WHERE CreatedDate Between '$start_date 00:00:00' and '$end_date 23:59:00' and TicketNumber <> '' $customer $filtering";
                    $sql_total = sqlsrv_query($db, $query_total);
                    $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'totalCount' => $row_total['total'],
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('report_transaction', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;

        case "export":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : '';
                $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : '';
                $customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : '';

                $customer = '';
                if ($customer_id) {
                    $customer = "AND NIK=" . $customer_id;
                }

                $query = "SELECT * FROM vw_R_Transaction WHERE CreatedDate Between '$start_date 00:00:00' and '$end_date 23:59:00' and TicketNumber <> '' $customer ORDER BY TicketNumber ASC";

                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        // replace bad string
                        $row["ResponComplaint"] = replace_badstring($row['ResponComplaint']);
                        $row["DetailComplaint"] = replace_badstring($row['DetailComplaint']);
                        $data[] = $row;
                    }

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('report_transaction', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;
    }
} else {
    $response = response_error('report_transaction', 'no parameter action.');
    echo json_encode($response);
}
