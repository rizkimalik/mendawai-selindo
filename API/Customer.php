<?php
require_once "config/connection.php";
require_once "helper/response.php";

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

$action = $_GET['action'] ?? '';
$response = [];

if (!empty($action)) {
    switch ($action) {
        case "data":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $skip = isset($_GET['skip']) ? $_GET['skip'] : 0;
                $take = isset($_GET['take']) ? $_GET['take'] : 15;
                $sort = isset($_GET['sort']) ? json_decode($_GET['sort'], true) : '';
                $filter = isset($_GET['filter']) ? json_decode($_GET['filter'], true) : '';

                $orderby = 'ORDER BY CustomerID DESC';
                if ($sort) {
                    $desc = $sort[0]['desc'] == true ? 'desc' : 'asc';
                    $orderby = "ORDER BY ".$sort[0]['selector']." $desc";
                }

                $filtering = '';
                if ($filter) {
                    $filtering = "WHERE ".$filter[0]." LIKE '%".$filter[2]."%'";
                }

                $query = "SELECT CustomerID,Name,Birth,JenisKelamin,Email,Alamat,PhoneNumber,NIK,CIF,GroupID FROM mCustomer 
                    $filtering
                    $orderby
                    OFFSET $skip ROWS FETCH NEXT $take ROWS ONLY";

                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        $data[] = $row;
                    }

                    $query_total = "SELECT COUNT(*) AS total from mCustomer $filtering";
                    $sql_total = sqlsrv_query($db, $query_total);
                    $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'totalCount' => $row_total['total'],
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('Customer', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;
        
        case "channel":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $skip = isset($_GET['skip']) ? $_GET['skip'] : 0;
                $take = isset($_GET['take']) ? $_GET['take'] : 15;
                $sort = isset($_GET['sort']) ? json_decode($_GET['sort'], true) : '';
                $filter = isset($_GET['filter']) ? json_decode($_GET['filter'], true) : '';

                $orderby = 'ORDER BY CustomerID DESC';
                if ($sort) {
                    $desc = $sort[0]['desc'] == true ? 'desc' : 'asc';
                    $orderby = "ORDER BY ".$sort[0]['selector']." $desc";
                }

                $filtering = '';
                if ($filter) {
                    $filtering = "WHERE ".$filter[0]." LIKE '%".$filter[2]."%'";
                }

                $query = "select * from (
                        SELECT a.CustomerID,a.FlagChannel,a.ValueChannel,b.Name
                        FROM mCustomerChannel a 
                        left outer join mCustomer b on a.CustomerID=b.CustomerID  
                    ) as all_channel 
                    $filtering
                    $orderby
                    OFFSET $skip ROWS FETCH NEXT $take ROWS ONLY";

                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        $data[] = $row;
                    }

                    $query_total = "select COUNT(*) AS total from (
                        SELECT a.CustomerID,a.FlagChannel,a.ValueChannel,b.Name
                        FROM mCustomerChannel a 
                        left outer join mCustomer b on a.CustomerID=b.CustomerID  
                    ) as all_channel 
                    $filtering";
                    $sql_total = sqlsrv_query($db, $query_total);
                    $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'totalCount' => $row_total['total'],
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('Customer', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;
    }
} else {
    $response = response_error('Customer', 'no parameter action.');
    echo json_encode($response);
}
