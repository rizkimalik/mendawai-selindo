<?php
require_once "config/connection.php";
require_once "helper/response.php";

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

$action = $_GET['action'] ?? '';
$response = [];

if (!empty($action)) {
    switch ($action) {
        case "agent_online":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $query = "select username, ISNULL(max_email, 0) as max_email, ISNULL(max_whatsapp, 0) as max_whatsapp, ISNULL(max_chat, 0) as max_chat, ISNULL(max_twitter, 0) as max_twitter FROM msUser where login='1' and leveluser='Layer 1' and (chat='1' or whatsapp='1' or email='1' or twitter='1')";
                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        if ($row['max_email'] == 0) {
                            $query_total = "SELECT COUNT(EMAIL_ID) as total_handle FROM ICC_EMAIL_IN WHERE agent='$row[username]' and CONVERT(date, email_date) = convert(date, getdate())";
                            $sql_total = sqlsrv_query($db, $query_total);
                            $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);
                        }
                        else {
                            $query_total = "SELECT COUNT(chat_id) as total_handle FROM (SELECT chat_id,agent_handle FROM tChat WHERE flag_to='customer'  and flag_end='N' AND agent_handle='$row[username]' and CONVERT(date, date_create) = convert(date, getdate()) GROUP BY chat_id,agent_handle) as view_chat";
                            $sql_total = sqlsrv_query($db, $query_total);
                            $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);
                        }
                        
                        $row['total_handle'] = $row_total['total_handle'];
                        $data[] = $row;
                    }

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('dashboard', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;

        case "total_channel":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $query = "select ChannelName FROM mChannel where Active='Y'";
                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        if ($row['ChannelName'] == 'Email') {
                            $query_total = "SELECT COUNT(email_id) as total FROM ICC_EMAIL_IN  WHERE CONVERT(date, email_date) = convert(date, getdate()) and (agent is not null or agent !='')";
                            $sql_total = sqlsrv_query($db, $query_total);
                            $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);

                            // $query_queing = "SELECT COUNT(email_id) as queing FROM ICC_EMAIL_IN  WHERE CONVERT(date, email_date) = convert(date, getdate()) and agent is null or agent='' AND CNT='0'";
                            $query_queing = "SELECT COUNT(email_id) as queing FROM ICC_EMAIL_IN  WHERE agent is null or agent='' AND CNT='0'";
                            $sql_queing = sqlsrv_query($db, $query_queing);
                            $row_queing = sqlsrv_fetch_array($sql_queing, SQLSRV_FETCH_ASSOC);
                        }
                        else {
                            $query_total = "SELECT COUNT(chat_id) as total FROM (SELECT chat_id,agent_handle,channel FROM v_tChat 
                            WHERE flag_to='customer' AND (select top 1 * From STRING_SPLIT (channel, '_'))='$row[ChannelName]' and CONVERT(date, date_create) = convert(date, getdate())
                            and (agent_handle is not null or agent_handle !='')
                            GROUP BY chat_id,agent_handle,channel) as view_chat";
                            $sql_total = sqlsrv_query($db, $query_total);
                            $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);
                            
                            // WHERE flag_to='customer' AND channel='$row[ChannelName]' and CONVERT(date, date_create) = convert(date, getdate())
                            $query_queing = "SELECT COUNT(chat_id) as queing FROM (SELECT chat_id,agent_handle,channel FROM v_tChat 
                            WHERE flag_to='customer' AND (select top 1 * From STRING_SPLIT (channel, '_'))='$row[ChannelName]' and agent_handle is null or agent_handle=''
                            GROUP BY chat_id,agent_handle,channel) as view_chat";
                            $sql_queing = sqlsrv_query($db, $query_queing);
                            $row_queing = sqlsrv_fetch_array($sql_queing, SQLSRV_FETCH_ASSOC);
                        }

                        $row['Total'] = $row_total['total'];
                        $row['Queing'] = $row_queing['queing'];
                        $data[] = $row;
                    }

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('dashboard', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;

        case "grafik_channel":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $query = "select * FROM v_24Hours";
                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        $query_channel = "select ChannelName FROM mChannel where Active='Y'";
                        $sql_channel = sqlsrv_query($db, $query_channel);

                        while ($row_channel = sqlsrv_fetch_array($sql_channel, SQLSRV_FETCH_ASSOC)) {
                            if ($row_channel['ChannelName'] == 'Email') {
                                $query_total = "SELECT DATEPART(HOUR,email_date) AS email_date, COUNT(email_id) as total FROM ICC_EMAIL_IN 
                                WHERE DATEPART(HOUR,email_date)='$row[Jam]' and CONVERT(date, email_date) = convert(date, getdate()) GROUP BY DATEPART(HOUR,email_date)";
                                $sql_total = sqlsrv_query($db, $query_total);
                            }
                            else {
                                $query_total = "SELECT channel,DATEPART(HOUR,date_create) AS date_create, chat_id, COUNT(chat_id) as total FROM tChat 
                                    WHERE flag_to='customer' AND DATEPART(HOUR,date_create)='$row[Jam]' and (select top 1 * From STRING_SPLIT (channel, '_'))='$row_channel[ChannelName]' 
                                    and CONVERT(date, date_create) = convert(date, getdate())
                                    GROUP BY channel,DATEPART(HOUR,date_create),chat_id";
                                $sql_total = sqlsrv_query($db, $query_total);
                            }
                            $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);
    
                            $row[$row_channel['ChannelName']] = $row_total['total'] ?? 0;
                        }
                        $data[] = $row;
                    }

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('dashboard', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;
            
        case "top10_chat":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $query = "select top 10 chat_id,customer_id,user_id,name,message,email,channel,agent_handle,convert(varchar, date_create, 20) as date_create 
                from tChat where flag_end='N' and flag_to='customer' and CONVERT(date, date_create) = convert(date, getdate()) order by id desc";
                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        $data[] = $row;
                    }

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('dashboard', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;
    }
} else {
    $response = response_error('dashboard', 'no parameter action.');
    echo json_encode($response);
}
