<?php 
require_once "helper/logger.php";

function response_success($logname, $data){
    log_it($logname, $data);
    $response = [
        'status' => 200,
        'message' => 'success',
        'total' => count($data),
        'data' => $data
    ];

    return $response;
    die();
}

function response_error($logname, $data){
    log_error($logname, $data);
    http_response_code(500);
    $response = [
        'status' => 500,
        'message' => 'error',
        'data' => $data
    ];
    return $response;
    die();
}

function response_method(){
    http_response_code(405);
    $response = [
        'status' => 405,
        'message' => 'Method Not Allowed.',
        'data' => $_SERVER['REQUEST_METHOD']
    ];
    return $response;
    die();
}
