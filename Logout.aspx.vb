﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient
Imports System
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Threading
Imports System.Web.Configuration

Public Class Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.RemoveAll()
        Response.Redirect("~/Local.aspx")
    End Sub

End Class