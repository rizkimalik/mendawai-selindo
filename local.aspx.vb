﻿Imports System
Imports System.Web.UI
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class local
    Inherits System.Web.UI.Page

    Public AddCookiess As String = ConfigurationManager.AppSettings.Item("AddCookiess")

    Dim Proses As New ClsConn
    Dim sqldr As SqlDataReader
    Dim sql As String
    Dim valuesatu As Integer = 1
    Dim valuedua As Integer = 2
    Dim valuetiga As Integer = 3
    Dim ValueEmpat As Integer = 4
    Dim valueLima As Integer = 5

    Dim valueDispatchLayer2 As Integer = 2
    Dim valueDispatchLayer3 As Integer = 3
    Dim valueReassignLayer1 As Integer = 1

    Dim leveluser As String
    Dim value As String
    Dim CountLDAP As String = String.Empty
    Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlconaux As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim com, sqlcom, sqlcomaux As SqlCommand
    Dim loq As New cls_globe
    Dim connString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim recLDAP As Integer
    Dim recCount As Integer
    Dim _ClassFunction As New WebServiceTransaction
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim VariabelCookiesUsername As New HttpCookie("CookiesUserName")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("error") = "y" Then
            lblError.Visible = True
            lbl_Error.Text = "User is not active"
        Else
        End If
    End Sub
    Function AccessLogin(ByVal username As String, ByVal passsword As String)
        sql = "EXEC SP_LOGIN_APPLIKASI_LOCAL  '" & username & "', '" & passsword & "'"
        Try
            sqldr = Proses.ExecuteReader(sql)
            If sqldr.HasRows Then
                sqldr.Read()
                leveluser = sqldr("LAYER").ToString
                Session("UserName") = sqldr("UserName").ToString
                Session("lblUserName") = sqldr("UserName").ToString
                Session("UnitKerja") = sqldr("ORGANIZATION").ToString
                Session("Org") = sqldr("ORGANIZATION_NAME").ToString
                Session("NameKaryawan") = sqldr("NAME").ToString
                Session("LoginType") = sqldr("LAYER").ToString
                Session("lvluser") = sqldr("LevelUser").ToString
                Session("channel_code") = sqldr("CHANNEL_CODE").ToString
                Session("organization") = sqldr("ORGANIZATION").ToString
                Session("orgSupervisor") = sqldr("ORGANIZATION").ToString
                Session("lokasiPengaduan") = ""
                Session("sessionchat") = sqldr("CHAT").ToString
                Session("unitkerjaagent") = sqldr("ORGANIZATION").ToString
                Session("ROLE") = sqldr("LEVELUSER").ToString
                Session("LEVELUSERID") = sqldr("ROLE_ID").ToString
                Session("LoginTypeAngka") = sqldr("NumberNya").ToString
                Session("_LoginState") = sqldr("LoginState").ToString
                Session("NamaGrup") = sqldr("ORGANIZATION_NAME").ToString

                VariabelCookiesUsername.Values("CookiesUserName") = sqldr("UserName").ToString
                VariabelCookiesUsername.Expires = DateTime.Now.AddDays(AddCookiess)
                Response.Cookies.Add(VariabelCookiesUsername)
                sqldr.Close()

                Try
                    Dim query As String = "update msUser set SessionID='" & Session("SessionID") & "' where username='" & Session("username") & "'"
                    Proses.ExecuteNonQuery(query)
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

            Else
                Response.Redirect("login.aspx?error=y")
            End If
            'sqldr.Close()
            '_ClassFunction.LogSuccess(strLogTime, sql)


        Catch ex As Exception
            '_ClassFunction.LogError(strLogTime, ex, sql)
            Response.Write(ex.Message)
        End Try

        Dim _uplogin As String = String.Empty
        Try
            _uplogin = "UPDATE MSUSER SET LOGIN='1', IdAUX='9',DescAUX='READY' WHERE USERNAME ='" & username & "'"
            Proses.ExecuteNonQuery(_uplogin)
            _ClassFunction.LogSuccess(strLogTime, _uplogin)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, _uplogin)
            Response.Write(ex.Message)
        End Try

        Dim _iLogin As String = "INSERT INTO ICC_LOG_IN (USERID,ACTIVITY_DATE,AUX_DESCRIPTION) VALUES('" & username & "',GETDATE(),'LOGIN')"
        sqlcom = New SqlCommand(_iLogin, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
            _ClassFunction.LogSuccess(strLogTime, _iLogin)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, _iLogin)
            Response.Write(ex.Message)
        End Try

        Response.Redirect("HTML/new_inbox.aspx?idpage=1012&status=inbox")
    End Function
    Private Sub Btn_Simpan_ServerClick(sender As Object, e As EventArgs) Handles Btn_Simpan.ServerClick
        TemplateStatus()
        AccessLogin(txt_username.Text, txt_password.Text)
    End Sub
    Private Sub TemplateStatus()
        Dim _strColumn As String = String.Empty
        _strColumn = "select count(*) as data from mStatus"
        Try
            sqldr = Proses.ExecuteReader(_strColumn)
            If sqldr.HasRows Then
                sqldr.Read()
                If sqldr("data").ToString = "1" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "2" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "3" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "4" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "5" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "6" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "7" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "8" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                Else
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                End If
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class