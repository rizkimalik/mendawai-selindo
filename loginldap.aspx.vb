﻿Imports System
Imports System.Web.UI
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class loginldap
    Inherits System.Web.UI.Page
    Public ConLDAP As String = ConfigurationManager.AppSettings.Item("LDAP")

    Dim Proses As New ClsConn
    Dim sqldr As SqlDataReader
    Dim sql As String
    Dim valuesatu As Integer = 1
    Dim valuedua As Integer = 2
    Dim valuetiga As Integer = 3
    Dim ValueEmpat As Integer = 4
    Dim valueLima As Integer = 5

    Dim valueDispatchLayer2 As Integer = 2
    Dim valueDispatchLayer3 As Integer = 3
    Dim valueReassignLayer1 As Integer = 1

    Dim leveluser As String
    Dim value As String
    Dim Type As String
    Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlconaux As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Com, sqlcom, sqlcomaux As SqlCommand
    Dim f_User As String
    Dim loq As New cls_globe
    Dim connString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.RemoveAll()
        FormsAuthentication.SignOut()
        sql = "select Url from SML_ImageLogin where Status='Active'"
        sqldr = Proses.ExecuteReader(sql)
        Try
            If sqldr.HasRows Then
                sqldr.Read()
                Session("image") = sqldr("Url")
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        'Session("image") = "Images/SMLBackground.jpeg"
    End Sub
    Private Sub Btn_Simpan_ServerClick(sender As Object, e As EventArgs) Handles Btn_Simpan.ServerClick
        f_query(txt_username.Text, txt_password.Text)
    End Sub
    Private Function LoginLDAP(LDAP As String, Username As String, Password As String) As Boolean
        If LDAP = "" Then Exit Function
        Dim Entry As New DirectoryServices.DirectoryEntry("LDAP://" & LDAP, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.Filter = "(sAMAccountName=" & Username & ")"
        Try
            Dim Results As System.DirectoryServices.SearchResultCollection = Searcher.FindAll
            Session("NamaLengkap") = Results(0).GetDirectoryEntry.Properties("displayname").Value
            Session("email") = Results(0).GetDirectoryEntry.Properties("mail").Value
            Session("LoginID") = Results(0).GetDirectoryEntry.Properties("sAMAccountName").Value
            Session("NIK") = Results(0).GetDirectoryEntry.Properties("sAMAccountName").Value
            If Session("NIK") = "" Then
                Session("NIK") = Results(0).GetDirectoryEntry.Properties("displayname").Value
            End If
            Session("Jabatan") = Results(0).GetDirectoryEntry.Properties("description").Value
            Session("Department") = Results(0).GetDirectoryEntry.Properties("department").Value
            Session("LoginID") = Results(0).GetDirectoryEntry.Properties("sAMAccountName").Value
            Session("passAsli") = Password
            Return True
        Catch ex As Exception
            'lbl_Alert.Visible = True
            'lbl_Alert.Text = "User ID And Password Are Incorrect!"
        End Try
    End Function
    Function f_query(ByVal user As String, ByVal pass As String)

        Dim LDAPServer As String = ConfigurationManager.AppSettings("LDAP")
        If ValidateActiveDirectoryLogin(LDAPServer, user, pass) = True Then

            Dim recCount As Integer
            If user <> "" Then
                Using conn As New SqlConnection(connString)
                    conn.Open()
                    Dim cmd As SqlCommand = New SqlCommand("Select COUNT (UserID) as userID from msUser where UserName=@uservalue", conn)
                    Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                    'Dim passvalue As SqlParameter = New SqlParameter("@passvalue", SqlDbType.VarChar, 150)
                    uservalue.Value = user
                    'passvalue.Value = pass
                    cmd.Parameters.Add(uservalue)
                    'cmd.Parameters.Add(passvalue)
                    recCount = cmd.ExecuteScalar()
                End Using
            Else

            End If

            If recCount = 1 Then
                sql = "EXEC SP_LOGIN_APPLIKASI  '" & user & "'"
                Try
                    sqldr = Proses.ExecuteReader(sql)
                    If sqldr.HasRows Then
                        sqldr.Read()
                        leveluser = sqldr("LAYER").ToString
                        Session("UserName") = sqldr("UserName").ToString
                        Session("lblUserName") = sqldr("UserName").ToString
                        'Session("UnitKerja") = sqldr("UNITKERJA").ToString
                        Session("Org") = sqldr("ORGANIZATION_NAME").ToString
                        Session("NameKaryawan") = sqldr("NAME").ToString
                        Session("LoginType") = sqldr("LAYER").ToString
                        'Session("divisi") = sqldr("Divisi").ToString
                        Session("lvluser") = sqldr("LevelUser").ToString
                        'Session("path") = sqldr("Path").ToString
                        Session("channel_code") = sqldr("CHANNEL_CODE").ToString
                        Session("organization") = sqldr("ORGANIZATION").ToString
                        Session("orgSupervisor") = sqldr("ORGANIZATION").ToString
                        Session("password") = sqldr("PASSWORD").ToString
                    End If
                    sqldr.Close()
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                Dim updatelogin As String = "UPDATE MSUSER SET LOGIN='1', IdAUX='9', DescAUX='READY' WHERE USERNAME ='" & user & "'"
                Try
                    Com = New SqlCommand(updatelogin, Con)
                    Con.Open()
                    Com.ExecuteNonQuery()
                    Con.Close()
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                Dim strsqlSelect As String = String.Empty
                Dim iddesc As String = String.Empty
                Dim auxdesc As String = String.Empty
                Dim strAux As String = "select * from icc_aux_history where userid='" & Session("username") & "' order by id desc"
                Try
                    sqldr = Proses.ExecuteReader(strAux)
                    If sqldr.HasRows Then
                        sqldr.Read()
                        iddesc = sqldr("id").ToString
                        auxdesc = sqldr("AUXDESCRIPTION").ToString
                        If auxdesc <> "READY" Then
                            Try
                                strsqlSelect = "update icc_aux_history set Datecreate=getdate(), state='1' where id='" & iddesc & "'"
                                Proses.ExecuteNonQuery(strsqlSelect)
                            Catch ex As Exception
                                Response.Write(ex.Message)
                            End Try
                        Else
                            Try
                                Dim strsqldua As String = String.Empty
                                strsqldua = "update msUser set IdAUX='" & sqldr("AUXID") & "', DescAUX='" & sqldr("AuxDescription") & "' where username='" & Session("username") & "'"
                                Proses.ExecuteNonQuery(strsqldua)
                            Catch ex As Exception
                                Response.Write(ex.Message)
                            End Try
                        End If
                    Else
                        Try
                            strsqlSelect = "insert into ICC_AUX_HISTORY (userid, auxid, auxdescription, state) values('" & Session("username") & "','9','READY','1')"
                            Proses.ExecuteNonQuery(strsqlSelect)
                        Catch ex As Exception
                            Response.Write(ex.Message)
                        End Try
                    End If
                    sqldr.Close()
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                Dim ilogin As String = "INSERT INTO ICC_LOG_IN (USERID,ACTIVITY_DATE,AUX_DESCRIPTION) VALUES('" & user & "',GETDATE(),'LOGIN')"
                sqlcom = New SqlCommand(ilogin, sqlcon)
                Try
                    sqlcon.Open()
                    sqlcom.ExecuteNonQuery()
                    sqlcon.Close()
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                Session("LoginTypeAngka") = value
                Session("LoginTypeSbg") = leveluser

                If Session("LoginTypeSbg") = "Supervisor" Then

                    Dim strFunction As String
                    Dim strArr() As String
                    Dim count As Integer
                    Dim COrganization As String = ""
                    Dim whereTambahanDept As String = ""
                    Dim strSql As String = "Select USERNAME from msUser where ORGANIZATION='" & Session("Org") & "' and leveluser = 'Agent'"
                    sqlcom = New SqlCommand(strSql, sqlcon)
                    Try
                        sqlcon.Open()
                        sqldr = sqlcom.ExecuteReader
                        While sqldr.Read
                            COrganization &= sqldr("USERNAME") & ",".ToString
                        End While
                        sqldr.Read()
                        sqlcon.Close()

                        If COrganization <> "" Then
                            strFunction = COrganization
                            strArr = strFunction.Split(",")
                            For count = 0 To strArr.Length - 1
                                If count = 0 Then
                                    whereTambahanDept &= "USERNAME = '" & strArr(count) & "' "
                                ElseIf count < strArr.Length - 1 Then
                                    whereTambahanDept &= "OR USERNAME = '" & strArr(count) & "' "
                                Else
                                    'whereTambahanDept &= "OR UserID = '" & strArr(count) & "' "
                                End If
                            Next
                            Session("Organization") = whereTambahanDept
                        Else
                            Session("Organization") = whereTambahanDept = "USERNAME ='" & COrganization & "'"
                        End If
                    Catch ex As Exception
                        Response.Write(ex.Message)
                    End Try

                Else

                End If

                If Request.QueryString("") <> "" Then
                    Dim updatelogin1 As String = "UPDATE MSUSER SET LOGIN='1',IdAUX='9',DescAUX='READY' WHERE USERNAME ='" & Request.QueryString("user") & "'"
                    sqlcomaux = New SqlCommand(updatelogin1, sqlconaux)
                    Try
                        sqlconaux.Open()
                        sqlcomaux.ExecuteNonQuery()
                        sqlconaux.Close()
                    Catch ex As Exception
                        Response.Write(ex.Message)
                    End Try
                Else

                End If

                If Session("LoginType") = "" Then
                ElseIf Session("LoginType") = "layer1" Then
                    Session("LoginTypeAngka") = valuesatu
                ElseIf Session("LoginType") = "layer2" Then
                    Session("LoginTypeAngka") = valuetiga
                    'ElseIf Session("LoginType") = "layer3" Then
                    '    Session("LoginTypeAngka") = valuetiga
                ElseIf Session("LoginType") = "Supervisor" Then
                    Session("LoginTypeAngka") = valuedua
                ElseIf Session("LoginType") = "Admin" Then
                    Session("LoginTypeAngka") = ValueEmpat
                End If
                If Session("LoginType") = "layer1" Then
                    Response.Redirect("HTML/inbox.aspx")
                Else
                    Response.Redirect("HTML/inbox.aspx")
                End If
            Else
                Response.Redirect("login.aspx?desc=ldap")
            End If
        Else
            Response.Redirect("login.aspx?desc=ldap")
        End If
    End Function
    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.SearchScope = DirectoryServices.SearchScope.OneLevel
        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)
            'lblError.Visible = True
            'lbl_Succes.Visible = True
            Response.Redirect("masup.aspx")
        Catch
            Success = False
            'lbl_Error.Visible = True
            'lblError.Visible = True
            Response.Redirect("gagal.aspx")
        End Try
        Return Success
    End Function
End Class