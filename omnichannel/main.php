<?php include 'layouts/header.php'; ?>

<div class="layout-wrapper d-lg-flex">
    <?php include 'components/sidebar.php'; ?>

    <div class="w-100 overflow-hidden">
        <div class="user-chat-overlay"></div>
        <div class="chat-content d-lg-flex ">
            <div class="w-100 overflow-hidden position-relative">
                <div id="users-chat" class="position-relative">
                    <?php include 'components/navbar.php'; ?>

                    <!-- chat conversation -->
                    <div class="chat-conversation p-3 p-lg-4 " id="chat-conversation" data-simplebar>
                        <ul class="list-unstyled chat-conversation-list" id="chat-conversation-list"></ul>
                    </div>
                    <!-- end chat conversation end -->

                    <?php include 'components/form-send.php'; ?>
                </div>
            </div>

            <?php
                include 'components/panel-call.php';
                include 'components/panel-profile.php';
            ?>
        </div>
    </div>

    <!-- <x-modal /> -->
</div>

<?php include 'layouts/footer.php'; ?>