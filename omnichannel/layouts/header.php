<!doctype html>
<html lang="eng" style="--bs-primary-rgb:80, 165, 241;">

<head>

    <meta charset="utf-8" />
    <title>App Omnichannel Agent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive Omnichannel" name="description" />
    <meta name="keywords" content="chat, web chat template, chat status, chat template, communication, discussion, group chat, message, messenger template, status" />
    <meta content="Mendawai" name="author" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="favicon.ico" id="tabIcon">
    <!-- glightbox css -->
    <link rel="stylesheet" href="assets/css/glightbox.min.css">
    <!-- swiper css -->
    <link rel="stylesheet" href="assets/css/swiper-bundle.min.css">
    <link href="assets/boxicons/css/boxicons.min.css" rel="stylesheet">
    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href="assets/sweetalert/sweetalert2.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
   
</head>

<body>