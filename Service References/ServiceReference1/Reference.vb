﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace ServiceReference1
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute(ConfigurationName:="ServiceReference1.MainTrxSoap")>  _
    Public Interface MainTrxSoap
        
        'CODEGEN: Generating message contract since element name HelloWorldResult from namespace http://tempuri.org/ is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/HelloWorld", ReplyAction:="*")>  _
        Function HelloWorld(ByVal request As ServiceReference1.HelloWorldRequest) As ServiceReference1.HelloWorldResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/HelloWorld", ReplyAction:="*")>  _
        Function HelloWorldAsync(ByVal request As ServiceReference1.HelloWorldRequest) As System.Threading.Tasks.Task(Of ServiceReference1.HelloWorldResponse)
        
        'CODEGEN: Generating message contract since element name NIK from namespace http://tempuri.org/ is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/WScoba", ReplyAction:="*")>  _
        Function WScoba(ByVal request As ServiceReference1.WScobaRequest) As ServiceReference1.WScobaResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/WScoba", ReplyAction:="*")>  _
        Function WScobaAsync(ByVal request As ServiceReference1.WScobaRequest) As System.Threading.Tasks.Task(Of ServiceReference1.WScobaResponse)
        
        'CODEGEN: Generating message contract since element name messagaeNya from namespace http://tempuri.org/ is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/WSPushNotif", ReplyAction:="*")>  _
        Function WSPushNotif(ByVal request As ServiceReference1.WSPushNotifRequest) As ServiceReference1.WSPushNotifResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/WSPushNotif", ReplyAction:="*")>  _
        Function WSPushNotifAsync(ByVal request As ServiceReference1.WSPushNotifRequest) As System.Threading.Tasks.Task(Of ServiceReference1.WSPushNotifResponse)
        
        'CODEGEN: Generating message contract since element name tokenNya from namespace http://tempuri.org/ is not marked nillable
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/WSPushToken", ReplyAction:="*")>  _
        Function WSPushToken(ByVal request As ServiceReference1.WSPushTokenRequest) As ServiceReference1.WSPushTokenResponse
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/WSPushToken", ReplyAction:="*")>  _
        Function WSPushTokenAsync(ByVal request As ServiceReference1.WSPushTokenRequest) As System.Threading.Tasks.Task(Of ServiceReference1.WSPushTokenResponse)
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class HelloWorldRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="HelloWorld", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.HelloWorldRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.HelloWorldRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute()>  _
    Partial Public Class HelloWorldRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class HelloWorldResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="HelloWorldResponse", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.HelloWorldResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.HelloWorldResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class HelloWorldResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public HelloWorldResult As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal HelloWorldResult As String)
            MyBase.New
            Me.HelloWorldResult = HelloWorldResult
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class WScobaRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="WScoba", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.WScobaRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.WScobaRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class WScobaRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public NIK As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal NIK As String)
            MyBase.New
            Me.NIK = NIK
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class WScobaResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="WScobaResponse", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.WScobaResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.WScobaResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class WScobaResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public WScobaResult As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal WScobaResult As String)
            MyBase.New
            Me.WScobaResult = WScobaResult
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class WSPushNotifRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="WSPushNotif", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.WSPushNotifRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.WSPushNotifRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class WSPushNotifRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public messagaeNya As String
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=1)>  _
        Public titleNya As String
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=2)>  _
        Public tokenNya As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal messagaeNya As String, ByVal titleNya As String, ByVal tokenNya As String)
            MyBase.New
            Me.messagaeNya = messagaeNya
            Me.titleNya = titleNya
            Me.tokenNya = tokenNya
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class WSPushNotifResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="WSPushNotifResponse", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.WSPushNotifResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.WSPushNotifResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class WSPushNotifResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public WSPushNotifResult As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal WSPushNotifResult As String)
            MyBase.New
            Me.WSPushNotifResult = WSPushNotifResult
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class WSPushTokenRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="WSPushToken", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.WSPushTokenRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.WSPushTokenRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class WSPushTokenRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public tokenNya As String
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=1)>  _
        Public userNya As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal tokenNya As String, ByVal userNya As String)
            MyBase.New
            Me.tokenNya = tokenNya
            Me.userNya = userNya
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class WSPushTokenResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="WSPushTokenResponse", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As ServiceReference1.WSPushTokenResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As ServiceReference1.WSPushTokenResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class WSPushTokenResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public WSPushTokenResult As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal WSPushTokenResult As String)
            MyBase.New
            Me.WSPushTokenResult = WSPushTokenResult
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface MainTrxSoapChannel
        Inherits ServiceReference1.MainTrxSoap, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class MainTrxSoapClient
        Inherits System.ServiceModel.ClientBase(Of ServiceReference1.MainTrxSoap)
        Implements ServiceReference1.MainTrxSoap
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_HelloWorld(ByVal request As ServiceReference1.HelloWorldRequest) As ServiceReference1.HelloWorldResponse Implements ServiceReference1.MainTrxSoap.HelloWorld
            Return MyBase.Channel.HelloWorld(request)
        End Function
        
        Public Function HelloWorld() As String
            Dim inValue As ServiceReference1.HelloWorldRequest = New ServiceReference1.HelloWorldRequest()
            inValue.Body = New ServiceReference1.HelloWorldRequestBody()
            Dim retVal As ServiceReference1.HelloWorldResponse = CType(Me,ServiceReference1.MainTrxSoap).HelloWorld(inValue)
            Return retVal.Body.HelloWorldResult
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_HelloWorldAsync(ByVal request As ServiceReference1.HelloWorldRequest) As System.Threading.Tasks.Task(Of ServiceReference1.HelloWorldResponse) Implements ServiceReference1.MainTrxSoap.HelloWorldAsync
            Return MyBase.Channel.HelloWorldAsync(request)
        End Function
        
        Public Function HelloWorldAsync() As System.Threading.Tasks.Task(Of ServiceReference1.HelloWorldResponse)
            Dim inValue As ServiceReference1.HelloWorldRequest = New ServiceReference1.HelloWorldRequest()
            inValue.Body = New ServiceReference1.HelloWorldRequestBody()
            Return CType(Me,ServiceReference1.MainTrxSoap).HelloWorldAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_WScoba(ByVal request As ServiceReference1.WScobaRequest) As ServiceReference1.WScobaResponse Implements ServiceReference1.MainTrxSoap.WScoba
            Return MyBase.Channel.WScoba(request)
        End Function
        
        Public Function WScoba(ByVal NIK As String) As String
            Dim inValue As ServiceReference1.WScobaRequest = New ServiceReference1.WScobaRequest()
            inValue.Body = New ServiceReference1.WScobaRequestBody()
            inValue.Body.NIK = NIK
            Dim retVal As ServiceReference1.WScobaResponse = CType(Me,ServiceReference1.MainTrxSoap).WScoba(inValue)
            Return retVal.Body.WScobaResult
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_WScobaAsync(ByVal request As ServiceReference1.WScobaRequest) As System.Threading.Tasks.Task(Of ServiceReference1.WScobaResponse) Implements ServiceReference1.MainTrxSoap.WScobaAsync
            Return MyBase.Channel.WScobaAsync(request)
        End Function
        
        Public Function WScobaAsync(ByVal NIK As String) As System.Threading.Tasks.Task(Of ServiceReference1.WScobaResponse)
            Dim inValue As ServiceReference1.WScobaRequest = New ServiceReference1.WScobaRequest()
            inValue.Body = New ServiceReference1.WScobaRequestBody()
            inValue.Body.NIK = NIK
            Return CType(Me,ServiceReference1.MainTrxSoap).WScobaAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_WSPushNotif(ByVal request As ServiceReference1.WSPushNotifRequest) As ServiceReference1.WSPushNotifResponse Implements ServiceReference1.MainTrxSoap.WSPushNotif
            Return MyBase.Channel.WSPushNotif(request)
        End Function
        
        Public Function WSPushNotif(ByVal messagaeNya As String, ByVal titleNya As String, ByVal tokenNya As String) As String
            Dim inValue As ServiceReference1.WSPushNotifRequest = New ServiceReference1.WSPushNotifRequest()
            inValue.Body = New ServiceReference1.WSPushNotifRequestBody()
            inValue.Body.messagaeNya = messagaeNya
            inValue.Body.titleNya = titleNya
            inValue.Body.tokenNya = tokenNya
            Dim retVal As ServiceReference1.WSPushNotifResponse = CType(Me,ServiceReference1.MainTrxSoap).WSPushNotif(inValue)
            Return retVal.Body.WSPushNotifResult
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_WSPushNotifAsync(ByVal request As ServiceReference1.WSPushNotifRequest) As System.Threading.Tasks.Task(Of ServiceReference1.WSPushNotifResponse) Implements ServiceReference1.MainTrxSoap.WSPushNotifAsync
            Return MyBase.Channel.WSPushNotifAsync(request)
        End Function
        
        Public Function WSPushNotifAsync(ByVal messagaeNya As String, ByVal titleNya As String, ByVal tokenNya As String) As System.Threading.Tasks.Task(Of ServiceReference1.WSPushNotifResponse)
            Dim inValue As ServiceReference1.WSPushNotifRequest = New ServiceReference1.WSPushNotifRequest()
            inValue.Body = New ServiceReference1.WSPushNotifRequestBody()
            inValue.Body.messagaeNya = messagaeNya
            inValue.Body.titleNya = titleNya
            inValue.Body.tokenNya = tokenNya
            Return CType(Me,ServiceReference1.MainTrxSoap).WSPushNotifAsync(inValue)
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_WSPushToken(ByVal request As ServiceReference1.WSPushTokenRequest) As ServiceReference1.WSPushTokenResponse Implements ServiceReference1.MainTrxSoap.WSPushToken
            Return MyBase.Channel.WSPushToken(request)
        End Function
        
        Public Function WSPushToken(ByVal tokenNya As String, ByVal userNya As String) As String
            Dim inValue As ServiceReference1.WSPushTokenRequest = New ServiceReference1.WSPushTokenRequest()
            inValue.Body = New ServiceReference1.WSPushTokenRequestBody()
            inValue.Body.tokenNya = tokenNya
            inValue.Body.userNya = userNya
            Dim retVal As ServiceReference1.WSPushTokenResponse = CType(Me,ServiceReference1.MainTrxSoap).WSPushToken(inValue)
            Return retVal.Body.WSPushTokenResult
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function ServiceReference1_MainTrxSoap_WSPushTokenAsync(ByVal request As ServiceReference1.WSPushTokenRequest) As System.Threading.Tasks.Task(Of ServiceReference1.WSPushTokenResponse) Implements ServiceReference1.MainTrxSoap.WSPushTokenAsync
            Return MyBase.Channel.WSPushTokenAsync(request)
        End Function
        
        Public Function WSPushTokenAsync(ByVal tokenNya As String, ByVal userNya As String) As System.Threading.Tasks.Task(Of ServiceReference1.WSPushTokenResponse)
            Dim inValue As ServiceReference1.WSPushTokenRequest = New ServiceReference1.WSPushTokenRequest()
            inValue.Body = New ServiceReference1.WSPushTokenRequestBody()
            inValue.Body.tokenNya = tokenNya
            inValue.Body.userNya = userNya
            Return CType(Me,ServiceReference1.MainTrxSoap).WSPushTokenAsync(inValue)
        End Function
    End Class
End Namespace
