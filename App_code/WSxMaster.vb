﻿Imports System.ComponentModel
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Data.SqlClient
'Imports System.Web
'Imports System.Web.Services.Protocols
'Imports System.Data
'Imports System.Net
'Imports System.IO
'Imports System.Xml


<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
<ScriptService()>
<ToolboxItem(False)>
Public Class WSxMaster
    Inherits System.Web.Services.WebService

    Private ReadOnly js As New JavaScriptSerializer()
    Private ReadOnly sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As SqlCommand
    Dim sqldr As SqlDataReader
    Dim strQuery As String = String.Empty

    Public Class ListUser
        Public Property Username As String
        Public Property Name As String
    End Class

    Public Class DetailUser
        Public Property Username As String
        Public Property Name As String
        Public Property LevelUser As String
    End Class

    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function ListAllUser() As String
        Try
            strQuery = "select top 10 USERNAME,NAME from msuser"
            sqlcom = New SqlCommand(strQuery, sqlcon)
            sqlcon.Open()
            sqldr = sqlcom.ExecuteReader()

            Dim DataListUser As New List(Of ListUser)()
            While sqldr.Read()
                Dim obj As New ListUser With {
                    .Username = sqldr("USERNAME").ToString(),
                    .Name = sqldr("NAME").ToString()
                }
                DataListUser.Add(obj)
            End While
            Return js.Serialize(DataListUser)

        Catch ex As Exception
            Return js.Serialize(ex.Message)
        End Try
    End Function


    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function ViewUser(Username As String) As String
        Try
            strQuery = "select USERNAME,NAME,LEVELUSER from msuser where USERNAME='" & Username & "'"
            sqlcom = New SqlCommand(strQuery, sqlcon)
            sqlcon.Open()
            sqldr = sqlcom.ExecuteReader()

            Dim DataListUser As New List(Of DetailUser)()
            sqldr.Read()
            Dim obj As New DetailUser With {
                    .Username = sqldr("USERNAME").ToString(),
                    .Name = sqldr("NAME").ToString(),
                    .LevelUser = sqldr("LEVELUSER").ToString()
                }
            DataListUser.Add(obj)
            Return js.Serialize(DataListUser)

        Catch ex As Exception
            Return js.Serialize(ex.Message)
        End Try
    End Function

End Class
